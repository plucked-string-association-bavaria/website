# Mandolin orchestra

[![pipeline status](https://gitlab.com/plucked-string-association-bavaria/website/badges/main/pipeline.svg)](https://gitlab.com/plucked-string-association-bavaria/website/-/commits/main)

**Mandolin orchestra** is a static homepage for the Bavarian Mandolin and Guitar Association made with GatsbyJS.
The live site can be viewed at https://www.bdz-bayern.de/.

## Screenshot

![Screenshot software](screenshot.png "screenshot software")

## Usage

- Clone repository: `git clone https://gitlab.com/plucked-string-association-bavaria/website.git`
- Install [Node.js](https://nodejs.org)
- Install Gatsby CLI globally on your system: `npm install -g gatsby-cli`
- Install Yarn: `npm install -g yarn`
- Install the project dependencies: `yarn` (this command has to be run in your project directory)
- Start a local develop server by running `gatsby develop` in your project directory
- Generate a production site by running `gatsby build` in your project directory

## Contributors

- see [Contributors](https://gitlab.com/plucked-string-association-bavaria/website/-/graphs/main)

## License

- see [LICENSE](https://gitlab.com/plucked-string-association-bavaria/website/-/blob/main/LICENSE) file
