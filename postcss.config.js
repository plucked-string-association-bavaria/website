module.exports = () => ({
  plugins: {
    "postcss-import": {},
    tailwindcss: {},
    "postcss-preset-env": {
      stage: 1,
      "nesting-rules": true,
      autoprefixer: { autoplace: "autoplace" }, // autoprefix-options -> https://github.com/postcss/autoprefixer#options
    },
  },
});
