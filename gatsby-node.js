const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

const {
  isCurrentDate,
  hasValidTag,
} = require("./src/components/dates/date-filterer");

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `` });
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    });
  }
};

exports.createPages = async ({ actions, graphql, reporter }) => {
  // create blog pages
  const resultBlog = await graphql(`
    {
      allMarkdownRemark(
        sort: { frontmatter: { date: DESC } }
        filter: { frontmatter: { layout: { eq: "blog" } } }
      ) {
        edges {
          node {
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const { createRedirect } = actions;

  createRedirect({
    fromPath: "/",
    toPath: "/bdz",
    isPermanent: true,
    redirectInBrowser: true,
  });

  // Handle errors
  if (resultBlog.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }

  const { createPage } = actions;
  const blogPostTemplate = path.resolve(`src/templates/blog-post.js`);
  resultBlog.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      id: node.id,
      component: blogPostTemplate,
      context: {
        slug: node.fields.slug,
      }, // additional data can be passed via context
    });
  });

  const resultDates = await graphql(`
    {
      allMarkdownRemark(filter: { frontmatter: { layout: { eq: "dates" } } }) {
        edges {
          node {
            frontmatter {
              beginning
              tags
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  // Handle errors
  if (resultDates.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }

  const currentDates = resultDates.data.allMarkdownRemark.edges.filter(
    ({ node }) => isCurrentDate(node.frontmatter.beginning)
  );

  createDatePages(createPage, currentDates);
};

function createDatePages(createPage, currentDates) {
  currentDates.forEach(({ node }) => {
    createPage({
      path: node.fields.slug,
      id: node.id,
      component: path.resolve(`./src/templates/date-details-bdz.js`),
      context: {
        slug: node.fields.slug,
      }, // additional data can be passed via context
    });
  });

  createDateSubpage(currentDates, "BLZO", "date-details-blzo.js", createPage);
  createDateSubpage(currentDates, "BLJZO", "date-details-bljzo.js", createPage);
  for (tag of [
    "Bayuna",
    "Kinderkurs",
    "Osterkurs",
    "Pfingstkurs",
    "Schweinfurter Seminar",
    "uHu",
  ]) {
    createDateSubpage(currentDates, tag, "date-details-bdz.js", createPage);
  }
}

function createDateSubpage(allDates, tagToFilter, template, createPage) {
  allDates
    .filter(({ node }) => hasValidTag(node.frontmatter.tags, tagToFilter))
    .forEach(({ node }) => {
      createPage({
        path: transformTagToRelativePath(tagToFilter) + node.fields.slug,
        id: node.id,
        component: path.resolve("./src/templates/" + template),
        context: {
          slug: node.fields.slug,
        }, // additional data can be passed via context
      });
    });
}

function transformTagToRelativePath(tag) {
  return tag.toLowerCase().replace(" ", "-");
}
