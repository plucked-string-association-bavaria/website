const color_1st_100 = "hsl(193 100% 90%)";
const color_1st_def = "hsl(193 100% 29%)";
const color_1st_900 = "hsl(193 100% 18%)";
const color_blzo_1st_100 = "hsl(336 94% 92%)";
const color_blzo_1st_def = "hsl(336 100% 45%)";
const color_bljzo_1st_100 = "hsl(201 100% 91%)";
const color_bljzo_1st_def = "hsl(201 100% 45%)";
const width_page = "1440px";

module.exports = {
  content: ["./src/**/*.js"],
  theme: {
    // As a color picker, use https://coolors.co/ or http://www.workwithcolor.com/hsl-color-picker-01.htm
    // Example: Click on "generate", click on any hex-value, change to HSL, start collecting your colors.
    // We overwrite all tailwind colors (therefore our colors are not in extend).
    colors: {
      transparent: "transparent",
      current: "currentColor",

      black: "hsl(0 0% 0%)",
      white: "hsl(0 0% 100%)",

      "1st": {
        50: "hsl(193 100% 98%)",
        100: color_1st_100,
        300: "hsl(193 100% 39%)",
        400: "hsl(193 100% 34%)",
        def: color_1st_def,
        700: "hsl(193 100% 24%)",
      },
      "2nd": {
        def: "hsl(193 100% 15%)",
      },
      contentBG: "hsl(193 50% 97%)",
      success: {
        100: "hsl(96 62% 94%)",
        300: "hsl(96 62% 79%)",
        def: "hsl(96 62% 64%)",
        700: "hsl(96 62% 44%)",
        900: "hsl(96 62% 34%)",
      },
      info: {
        100: "hsl(198 48% 93%)",
        300: "hsl(198 48% 87%)",
        def: "hsl(198 48% 81%)",
        700: "hsl(198 48% 56%)",
        900: "hsl(198 48% 30%)",
      },
      warning: {
        100: "hsl(29 100% 95%)",
        300: "hsl(29 100% 82%)",
        def: "hsl(29 100% 67%)",
        700: "hsl(29 100% 47%)",
        900: "hsl(29 100% 27%)",
      },
      error: {
        100: "hsl(355 97% 94%)",
        300: "hsl(355 97% 84%)",
        def: "hsl(355 97% 64%)",
        700: "hsl(355 97% 44%)",
        900: "hsl(355 97% 24%)",
      },
      gray: {
        100: "hsl(0 0% 90%)",
        300: "hsl(0 0% 70%)",
        def: "hsl(0 0% 50%)",
        700: "hsl(0 0% 30%)",
        900: "hsl(0 0% 10%)",
      },
      border: "hsl(193 23% 84%)",
      heading: {
        def: color_1st_900,
      },
      nav: {
        def: "hsl(193 20% 50%)",
      },
      text: {
        def: "hsl(193 100% 10%)",
        light: "hsl(193 40% 30%)",
        quote: "hsl(193 40% 80%)",
      },
      link: {
        def: color_1st_def,
      },
      strong: {
        def: color_1st_def,
      },
      mark: {
        color: color_1st_100,
        bg: color_1st_def,
      },

      blzo: {
        "1st": {
          50: "hsl(336 94% 98.5%)",
          100: color_blzo_1st_100,
          300: "hsl(336 94% 80%)",
          400: "hsl(336 94% 65%)",
          def: color_blzo_1st_def,
          700: "hsl(336 100% 36%)",
        },
        "2nd": {
          def: "hsl(336 93% 12%)",
        },
        contentBG: "hsl(300 8% 97%)",
        border: "hsl(336 28% 84%)",
        heading: {
          def: "hsl(336 100% 22%)",
        },
        nav: {
          def: "hsl(336 12% 50%)",
        },
        text: {
          def: "hsl(336 93% 12%)",
          light: "hsl(336 40% 30%)",
          quote: "hsl(336 40% 80%)",
        },
        link: {
          def: color_blzo_1st_def,
        },
        strong: {
          def: color_blzo_1st_def,
        },
        mark: {
          color: color_blzo_1st_100,
          bg: color_blzo_1st_def,
        },
      },
      bljzo: {
        "1st": {
          50: "hsl(201 100% 96%)",
          100: color_bljzo_1st_100,
          300: "hsl(201 100% 80%)",
          400: "hsl(201 100% 70%)",
          def: color_bljzo_1st_def,
          700: "hsl(201 100% 36%)",
        },
        "2nd": {
          def: "hsl(201 100% 14%)",
        },
        contentBG: "hsl(201 50% 97%)",
        border: "hsl(201 23% 84%)",
        heading: {
          def: "hsl(201 100% 22%)",
        },
        nav: {
          def: "hsl(201 12% 50%)",
        },
        text: {
          def: "hsl(201 100% 14%)",
          light: "hsl(201 40% 30%)",
          quote: "hsl(201 40% 80%)",
        },
        link: {
          def: color_bljzo_1st_def,
        },
        strong: {
          def: color_bljzo_1st_def,
        },
        mark: {
          color: color_bljzo_1st_100,
          bg: color_bljzo_1st_def,
        },
      },
    },

    extend: {
      screens: {
        content: "888px", // individuell einstellen wegen <ol> und <ul>
        mainnav: "960px",
      },
      spacing: {
        project: "1.5rem",
      },
      boxShadow: {
        DEFAULT:
          "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
        md: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
        lg: "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 0 12px -2px rgba(0, 0, 0, 0.15)",
      },
      multiplier: {
        heading: "1.1",
        //  gedacht fuer paddings
        "rythm-x": {
          md: "1.6",
          lg: "1.8",
          xl: "2",
        },
        // gedacht fuer margins
        "rythm-y": {
          md: "1.25",
          lg: "1.375",
          xl: "1.5",
          negative: "-.75rem", // <p>, <ul>, <ol> closer to headings
        },
      },

      width: {
        // -unique
        // unboxed -> '100%'
        // boxed z.B. -> 'calc(1280px + (var(--rythm-x) * 2))'
        max: "100%",

        // wenn header 100% -> 'calc(100% - (var(--rythm-x) * 2))'
        header: width_page,

        // wenn page + footer 100% -> '100%'
        page: width_page,
        footer: width_page,

        // content -> eigener Wert
        content: {
          default: "768px",
          wide: "1152px",
        },
      },

      fontFamily: {
        /* tailwind def vars */
        sans: ["Jost", "sans-serif"],
        serif: ["Georgia", "serif"],
        mono: ['"Courier New"', "monospace"],
        /* xtra */
        heading: ["Jost", "sans-serif"],
      },
      fontSize: {
        base: "1rem",
        h1: "1.875rem",
        h1Big: "2.125rem",
        h2: "1.5rem",
        h3: "1.375rem",
        h4: "1.25rem",
        h5: "1.125rem",
        h6: "1.075rem",
        small: ".85rem",
        quote: "1.875rem",
      },
      lineHeight: {
        normal: "1.2",
        relaxed: "1.625",
        heading: "1.2",
      },
      borderRadius: {
        lg: ".875rem",
      },
      keyframes: {
        mainnavvisibility: {
          "0%": {
            opacity: "0",
          },
          "33%": {
            opacity: "0.5",
          },
          "100%": {
            opacity: "1",
          },
        },
      },
      animation: {
        fadein: "mainnavvisibility 1s",
      },
    },
  },
  plugins: [],
};
