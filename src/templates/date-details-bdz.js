import React from "react";
import { graphql } from "gatsby";
import BdzDate from "../components/dates/bdzdate";
import LayoutBdz from "../components/layout-bdz";

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data

  return (
    <LayoutBdz pageName="is--dates-single ">
      <div className="text-components dates__wrapper-single">
        <BdzDate node={markdownRemark} showExcerpt={false} />
      </div>
    </LayoutBdz>
  );
}

export const pageQuery = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      frontmatter {
        title
        location
        beginning
        end
        hideTime
        showEnd
        link
        tags
      }
      excerpt
      fields {
        slug
      }
      html
    }
  }
`;
