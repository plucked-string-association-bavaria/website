import React from "react";
import { graphql } from "gatsby";
import LayoutBdz from "../components/layout-bdz";
import { localizeDateLongVersion } from "../components/dates/date-localizer";
import Button from "../components/button";

export default function Template({
  data, // this prop will be injected by the GraphQL query below.
}) {
  const { markdownRemark } = data; // data.markdownRemark holds your post data
  const { frontmatter, html } = markdownRemark;

  let htmlWithFigureCaptions = html.replaceAll(
    /<img .*?title="(.*?)">/g,
    function (match, p1) {
      return (
        "<figure>" + match + "<figcaption>" + p1 + "</figcaption></figure>"
      );
    },
  );

  let attachmentSection;
  if (frontmatter.attachment) {
    attachmentSection = (
      <div>
        <h3>Anhang:</h3>
        <p>
          <Button>
            <a href={frontmatter.attachment} download>
              {frontmatter.attachment.split("/").pop()}
            </a>
          </Button>
        </p>
      </div>
    );
  }

  return (
    <LayoutBdz pageName="is--dates">
      <div className="text-components">
        <header>
          <time className="block mb-0">
            {localizeDateLongVersion(frontmatter.date)}
          </time>
          <h1>{frontmatter.title}</h1>
        </header>
        <article dangerouslySetInnerHTML={{ __html: htmlWithFigureCaptions }} />
        {attachmentSection}
      </div>
    </LayoutBdz>
  );
}

export const pageQuery = graphql`
  query ($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        title
        attachment
      }
    }
  }
`;
