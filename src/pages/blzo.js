import React from "react";
import LayoutBlzo from "../components/layout-blzo";
import blzoEycatcher from "../images/blzo/eyecatcher-blzo.jpg";
import blzo2022 from "../images/blzo/blzo2022.jpg";
import Music from "../images/global/svg/music.inline.svg";
import Check from "../images/global/svg/check.inline.svg";
import { Link } from "gatsby";

const blzoMain = () => {
  return (
    <LayoutBlzo pageName="is--orchester">
      <div className="text-components">
        <header className="heading">
          <p className="prae">Das Bayerische</p>
          <h1 className="big">Landeszupforchester</h1>
        </header>
        <article>
          <div className="intro">
            <p>
              <span className="svg-initiale float-left mr-3 -mt-2">
                <Music />
              </span>
              Das Bayerische Landeszupforchester, aktuell unter der Leitung von
              Oliver Kälberer, ist ein überregionales Ensemble mit Mitgliedern
              aus ganz Bayern und ist eine Fördermaßnahme des "Bundes Deutscher
              Zupfmusiker, Landesverband Bayern e.V.". Den Klangkörper des BLZO
              bilden Mandoline, Mandola, Gitarre und Kontrabass, gespielt von
              engagierten und qualifizierten Laien, Musikpädagog:innen und
              ehemaligen Preisträger:innen des Wettbewerbs "Jugend musiziert".
              Das Orchester genießt aufgrund herausragender Leistungen
              bundesweit große Anerkennung und verkörpert durch Spielfreude,
              Musikalität und präzises Zusammenspiel höchste Qualität.
            </p>
          </div>

          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={blzoEycatcher}
              alt="Bayerisches Landeszupforchester Konzertfoto 2016"
            />
            <figcaption>
              Bayerisches Landeszupforchester 2019 in Großalmerode
            </figcaption>
          </figure>

          <h2>Das Orchester</h2>

          <p>
            Das Orchester trifft sich drei- bis viermal im Jahr zu mehrtägigen
            Probenphasen und erarbeitet Originalwerke für Zupforchester und
            sorgfältige Adaptionen aus allen Epochen. Der Dirigent Oliver
            Kälberer wird dabei von einem vierköpfigen{" "}
            <Link to="/blzo/team">Dozententeam</Link> unterstützt: Ariane Lorch,
            Elena Kisseljow, Malte Weyland und Karoline Laier.
          </p>

          <p>
            Dabei begeistert das Orchester sein Publikum nicht nur bei
            zahlreichen Konzerten in Deutschland. Gastspiele führten es auch
            nach Russland, Kanada, Japan, Thailand, England, Italien, Luxemburg,
            in die USA und in die Niederlande. 2015 unternahm man eine
            einwöchige Reise nach Schottland. Inzwischen dokumentiert eine Reihe
            von <Link to="/blzo/media">Tonträgern</Link> das Schaffen des
            Orchesters.
          </p>

          <div className="is--section width-full">
            <h2 className="text-center">
              Die Zielsetzungen und Aufgaben des BLZO sind:
            </h2>
            <ul className="flex flex-col lg:flex-row gap-8 list-none is--grid width-page">
              <li className="panel lg:w-1/3">
                <h4 className="flex">
                  <span className="svg-initiale flex--child mr-2">
                    <Check />
                  </span>
                  <span>
                    Die Weiterentwicklung der musikalischen Inhalte der
                    Zupfmusik
                  </span>
                </h4>
                Das Orchester entwickelt seine Spielweise und Anschlagstechnik
                stetig weiter und will wertvolle Originalliteratur
                interpretieren. Durch eine enge Zusammenarbeit mit
                Komponist:innen werden neue Werke für Zupforchester initiiert;
                insbesondere gilt es, die Möglichkeiten des Zusammenspiels auf
                Zupfinstrumenten auszuloten, was sowohl die Einbeziehung von
                Solo- und Zusatzinstrumenten beinhaltet als auch Gesang, Tanz
                und Chor.
              </li>
              <li className="panel lg:w-1/3">
                <h4 className="flex">
                  <span className="svg-initiale flex--child mr-2">
                    <Check />
                  </span>
                  <span>Förderung von Multiplikator:innen</span>
                </h4>
                Mit der Förderung von Multiplikator:innen wird angestrebt, dass
                die Arbeitsergebnisse des BLZO Eingang in die musikalische
                Arbeit der Heimatorchester finden. Darüber hinaus wird versucht,
                für alte und neue Werke, für folkloristische und konzertante
                Werke Interesse und Verständnis zu wecken. Schließlich wird das
                Empfinden für eine werk- und instrumentengerechte Interpretation
                geschult.
              </li>
              <li className="panel lg:w-1/3">
                <h4 className="flex">
                  <span className="svg-initiale flex--child mr-2">
                    <Check />
                  </span>
                  <span>
                    Konzertorchester für die öffentliche Wahrnehmung der
                    Zupfmusik
                  </span>
                </h4>
                Das BLZO demonstriert die Möglichkeiten des Zusammenspiels auf
                Zupfinstrumenten. In abwechslungsreichen, kurzweiligen und alle
                Stilepochen umfassenden Konzerten werden immer mehr
                Musikinteressent:innen zu Freund:innen des kultivierten
                Zupforchesterklanges.
              </li>
            </ul>
          </div>

          <figure className="thumbnail width-page">
            <img src={blzo2022} alt="Bayerisches Landeszupforchester 2022" />
            <figcaption>Bayerisches Landeszupforchester 2022</figcaption>
          </figure>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoMain;

export { Head } from "../components/layout-blzo";
