import React from "react";
import { Link } from "gatsby";
import LayoutBdz from "../components/layout-bdz";
import Icon404 from "../images/global/svg/is404.inline.svg";
import Button from "../components/button";

const NotFound = () => {
  return (
    <LayoutBdz>
      <div className="width-content text-components text-center">
        <h3>Diese Seite existiert nicht.</h3>

        <Icon404
          className="flex mx-auto my-20 p-5 w-32 h-32 text-[var(--color-1st-100)] rounded-full opacity-70 bg-[var(--color-heading-def)]"
          style={{
            boxShadow:
              "0 0 0 20px var(--color-1st-def),0 0 0 40px var(--color-1st-300), 0 0 0 60px var(--color-1st-100)",
          }}
        />

        <Button>
          <Link to="/">Zurück zur Startseite.</Link>
        </Button>
      </div>
    </LayoutBdz>
  );
};

export default NotFound;

export { Head } from "../components/layout-bdz";
