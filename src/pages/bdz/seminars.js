import React from "react";
import LayoutBdz from "../../components/layout-bdz";
import { Link } from "gatsby";
import Read from "../../images/global/svg/bookopen.inline.svg";
import EggEaster from "../../images/global/svg/eggEaster.inline.svg";
import Pentecost from "../../images/global/svg/pentecost.inline.svg";
import Uhu from "../../images/global/svg/uHu.inline.svg";
import Bayuan from "../../images/global/svg/bayuan.inline.svg";
import SWseminar from "../../images/global/svg/swSeminar.inline.svg";
import ChildSeminar from "../../images/global/svg/childSeminar.inline.svg";

const bdzSeminars = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">Kurse</h1>
          <p>
            Der BDZ Landesverband Bayern veranstaltet jedes Jahr vielfältige
            Kurse für verschiedene Zielgruppen.
          </p>
        </header>

        <article className="is--teaser">
          <ul className="list-none width-content-wide">
            <li className="is--33Width">
              <span>
                <Link to="osterkurs">
                  <figure>
                    <EggEaster />
                  </figure>
                  <h4>Osterkurs</h4>
                  <p>
                    Der Osterkurs ist ein einwöchiger Musiklehrgang im Süden
                    Bayerns. Er richtet sich vor allem an Jugendliche von 12 bis
                    18 Jahren, aber auch Kinder ab 10 Jahren und Erwachsene sind
                    hier gut aufgehoben.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--33Width">
              <span>
                <Link to="pfingstkurs">
                  <figure>
                    <Pentecost />
                  </figure>
                  <h4>Pfingstkurs</h4>
                  <p>
                    Der Pfingstkurs ist ein einwöchiger Musiklehrgang im Norden
                    Bayerns. Er richtet sich vor allem an Jugendliche von 12 bis
                    18 Jahren, aber auch Kinder ab 10 Jahren und Erwachsene sind
                    hier gut aufgehoben.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--33Width">
              <span>
                <Link to="uhu">
                  <figure>
                    <Uhu />
                  </figure>
                  <h4>Spaß mit Zupfmusik für uHu</h4>
                  <p>
                    Ein Kurs mit viel Spaß an Musik und Geselligkeit für alle
                    unter Hundert (uHu) mit Fokus auf die Altersgruppe 50+.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--33Width">
              <span>
                <Link to="bayuna">
                  <figure>
                    <Bayuan />
                  </figure>
                  <h4>Ensemble Bayuna</h4>
                  <p>
                    Bayuna ist ein Orchesterprojekt, das 2016 gegründet wurde
                    und sich seitdem zu zwei Wochenendprobenphasen pro Jahr an
                    wechselnden Orten in Bayern trifft. Zum Konzept von Bayuna
                    gehören momentan jährlich wechselnde Programme zu
                    ausgewählten Themen.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--33Width">
              <span>
                <Link to="schweinfurter-seminar">
                  <figure>
                    <SWseminar />
                  </figure>
                  <h4>Schweinfurter Seminar</h4>
                  <p>
                    Das Schweinfurter Seminar ist ein einwöchiger Musiklehrgang
                    in den Sommerferien. Er richtet sich an Erwachsene aller
                    Leistungsstufen inklusive professioneller Musiker:innen.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--33Width">
              <span>
                <Link to="kinderkurs">
                  <figure>
                    <ChildSeminar />
                  </figure>
                  <h4>Kinderkurse</h4>
                  <p>
                    Kinderkurse führen den musikalischen Nachwuchs mit Spiel und
                    Spaß an gemeinsames Musizieren heran.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
          </ul>
        </article>
      </div>
    </LayoutBdz>
  );
};
export default bdzSeminars;

export { Head } from "../../components/layout-bdz";
