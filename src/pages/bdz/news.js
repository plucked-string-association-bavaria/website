import React from "react";
import { Link, graphql } from "gatsby";
import LayoutBdz from "../../components/layout-bdz";
import Calendar from "../../images/global/svg/calendar.inline.svg";
import Read from "../../images/global/svg/bookopen.inline.svg";
import Document from "../../images/global/svg/document.inline.svg";

const bdzNews = () => {
  return (
    <LayoutBdz pageName="is--dates">
      <div className="text-components width-content">
        <header className="page-title">
          <h1 className="big">Aktuelles</h1>
          <p>
            Wir möchten Ihnen an dieser Stelle einen Einblick in unsere aktuelle
            Verbandstätigkeit geben.
          </p>
        </header>
        <article className="is--teaser">
          <ul className="list-none width-content-wide">
            <li className="is--autoWidth">
              <span>
                <Link to="blog">
                  <figure>
                    <Document />
                  </figure>
                  <h4>Zupfbote</h4>
                  <p>
                    Im Zupfboten finden Sie Berichte zu den aktuellen Projekten
                    und Konzerten innerhalb unseres Landesverbands.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li className="is--autoWidth">
              <span>
                <Link to="dates">
                  <figure>
                    <Calendar />
                  </figure>
                  <h4>Veranstaltungen</h4>
                  <p>
                    Unter Veranstaltungen möchten wir Sie über geplante Kurse
                    und Konzerte informieren.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
          </ul>
        </article>
      </div>
    </LayoutBdz>
  );
};
export default bdzNews;

export const query = graphql`
  {
    allMarkdownRemark(
      sort: { frontmatter: { date: DESC } }
      filter: { frontmatter: { layout: { eq: "blog" } } }
      limit: 2
    ) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            date(formatString: "DD MMMM, YYYY")
          }
          excerpt
          fields {
            slug
          }
        }
      }
    }
  }
`;

export { Head } from "../../components/layout-bdz";
