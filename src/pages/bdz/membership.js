import React from "react";
import LayoutBdz from "../../components/layout-bdz";
import { Link } from "gatsby";
import bdzMandolineEnsemble from "../../images/bdz/bdz-mandolin-ensemble.webp";
import Check from "../../images/global/svg/check.inline.svg";

const bdzMembership = () => {
  return (
    <LayoutBdz pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">Mitgliedschaft</h1>
        </header>
        <article>
          <ul className="flex flex-col lg:flex-row gap-8 list-none is--grid width-page">
            <li className="panel lg:w-1/2">
              <h4 className="flex justify-center">
                <span className="svg-initiale is--sm flex--child mr-2">
                  <Check />
                </span>
                <span className="self-center">
                  Wie kann ich Mitglied werden?
                </span>
              </h4>
              <p>
                Wir würden uns sehr freuen, wenn Sie uns als Mitglied
                unterstützen und unsere Angebote nutzen. Die Neuaufnahme von
                Mitgliedern erfolgt über den{" "}
                <a href="https://zupfmusiker.de/bdz-der-verband/mitglied-werden/">
                  BDZ Bundesverband
                </a>
                . Ihnen wird nach der Aufnahme abhängig von Ihrer Anschrift dann
                der passende Landesverband zugeteilt. Es gibt sowohl
                Einzelmitgliedschaften (für Einzelpersonen) als auch ordentliche
                Mitgliedschaften (für Vereine/Orchester/Ensembles).
              </p>
            </li>
            <li className="panel lg:w-1/2">
              <h4 className="flex justify-center">
                <span className="svg-initiale is--sm flex--child mr-2">
                  <Check />
                </span>
                <span className="self-center">Wie finanzieren wir uns?</span>
              </h4>
              <p>
                Der BDZ Bayern finanziert sich aus Mitgliedsbeiträgen und
                Kursgebühren. Zusätzlich wird der BDZ aus Mitteln des
                Bayerischen Staatsministerium für Wissenschaft und Kunst sowie
                der Kulturstiftung des Bezirks Unterfranken gefördert. Die
                Mittel werden komplett für Lehrgangsmaßnahmen und Förderung der
                Mitgliedsorchester verwendet. Der{" "}
                <Link to="/bdz/people">Vorstand</Link> arbeitet ehrenamtlich.
                Gerne können Sie unsere Arbeit durch Spenden unterstützen. Auf
                Wunsch stellen wir Ihnen eine Spendenquittung aus.
              </p>
            </li>
          </ul>

          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bdzMandolineEnsemble}
              alt="Symbolbild Bund Deutscher Zupfmusiker Landesverband Bayern e.V."
            />
          </figure>
        </article>
      </div>
    </LayoutBdz>
  );
};
export default bdzMembership;

export { Head } from "../../components/layout-bdz";
