import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import { Link } from "gatsby";
import uHu1 from "../../../images/bdz/uHu1.jpg";
import uHu2 from "../../../images/bdz/uHu2.jpg";
import uHu3 from "../../../images/bdz/uHu3.jpg";
import uHu4 from "../../../images/bdz/uHu4.jpg";
import uHu5 from "../../../images/bdz/uHu5.jpg";
import uHu6 from "../../../images/bdz/uHu6.jpg";
import CourseDate from "../../../components/dates/coursedate";

const uhu = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">
            <small>Spaß an Zupfmusik mit 50+:</small>
            Die uHus
          </h1>
        </header>

        <article>
          <p>
            Seit 2012 wird ein Lehrgang speziell für die Altersgruppe um 50+
            angeboten: frei von Leistungsdruck und anderen Zwängen (aber nicht
            ohne Ehrgeiz) steht der Spaß an Zupfmusik im Vordergrund. In Form
            von Ensembles, Liedbegleitungs- und anderen Kleingruppen können sich
            die Teilnehmer:innen ihre Lieblings-Betätigungsfelder mit ihrem
            Instrument auswählen und praktizieren.
          </p>
          <p>
            Zielgruppe: Hobbymusiker:innen, die nach längerer Pause wieder
            angefangen haben, zu musizieren, bzw. erwachsene ZO-Spieler:innen,
            die gerne mal nur mit gleichaltrigen oder älteren zusammen
            musizieren wollen (ohne untere/obere Altersgrenze!).
          </p>
          <p>
            Begleitet werden die Teilnehmer von den drei Dozent:innen: meistens
            Petra Breitenbach (Lohr), Michael Diedrich (Mosbach) und Jürgen
            Thiergärtner (Schonungen), die sich ganz auf die Altersgruppe
            einstellen. Auflockernde Spiele bauen innere Blockaden ab und üben
            ganz nebenbei die Koordination der Hände. In kleinen und
            anschaulichen Schritten werden verschiedene Techniken vermittelt.
            Wertvolle Tipps zu abwechselnden Themen, z.B. wie man zu Hause
            effektiv üben kann oder sich auch im fortgeschrittenen Alter noch
            spieltechnisch verbessern kann (mentales Training!), runden das
            Kursprogramm ab.
          </p>
          <p>
            Zum Ausklang der täglichen Arbeit trifft man sich auf ein Glas Wein
            und zum Plaudern und Lachen im Felsenkeller. Hier werden schnell
            neue Bekanntschaften geknüpft und alte vertieft. Alle Beteiligten
            erleben regelmäßig das Motto „Spaß an…“ und „schön, wieder hier zu
            sein“ sehr intensiv und empfinden das Wochenende als zu kurz.
          </p>
          <p>
            Deshalb gründete sich gleich nach dem ersten Kurs 2012 das
            überregionale Zupforchester (die „uHus“). Hier trifft man sich bis
            zum nächsten Wochenendkurs im November dreimal pro Jahr zu
            Probentagen, an denen die Stücken weiter geübt und vertieft werden.
          </p>
          <p>
            Neue Spieler:innen sind jederzeit herzlich willkommen und können
            sich gern bei <Link to="/bdz/people">Petra Breitenbach</Link>{" "}
            melden.
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="uHu" />

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail -m-px -m-py">
            <img
              src={uHu1}
              alt="Teilnehmende des uHu-Orchesters mit vielen Instrumenten"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img
              src={uHu2}
              alt="Teilnehmende des uHu-Orchesters bei der Probe"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img
              src={uHu3}
              alt="Gute Stimmung bei einer Probe des uHu-Orchesters"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail -m-px -m-py">
            <img
              src={uHu4}
              alt="Teilnehmende des uHu-Orchesters vor dem alten Rathaus Lohr"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img src={uHu5} alt="Geselliges Zusammensein beim uHu-Orchester" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img src={uHu6} alt="Geselliges Zusammensein beim uHu-Orchester" />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default uhu;

export { Head } from "../../../components/layout-bdz";
