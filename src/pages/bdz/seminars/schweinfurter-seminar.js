import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import sw_seminar_1 from "../../../images/sw_seminar/sw_seminar_1.jpg";
import sw_seminar_2 from "../../../images/sw_seminar/sw_seminar_2.jpg";
import sw_seminar_3 from "../../../images/sw_seminar/sw_seminar_3.jpg";
import sw_seminar_4 from "../../../images/sw_seminar/sw_seminar_4.jpg";
import sw_seminar_5 from "../../../images/sw_seminar/sw_seminar_5.jpg";
import sw_seminar_6 from "../../../images/sw_seminar/sw_seminar_6.jpg";
import sw_seminar_7 from "../../../images/sw_seminar/sw_seminar_7.jpg";
import sw_seminar_8 from "../../../images/sw_seminar/sw_seminar_8.jpg";
import CourseDate from "../../../components/dates/coursedate";

const schweinfurterSeminar = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">Schweinfurter Seminar</h1>
        </header>
        <article>
          <h2>Kursporträt</h2>
          <p>
            Das Schweinfurter Seminar ist eine Fortbildungsmöglichkeit des BDZ
            LV Bayern. Das Seminar wurde 1971 erstmals von Gerhard Vogt in
            Schweinfurt veranstaltet und findet seitdem jedes Jahr im August für
            eine Woche statt. Seit vielen Jahren wird es mittlerweile unter der
            Leitung von Bianca Brand in der Bayerischen Musikakademie Hammelburg
            mit ca. 30-40 Teilnehmer:innen und 5-6 Dozent:innen sehr erfolgreich
            durchgeführt. Die Bayerische Musikakademie Hammelburg bietet dem
            Kurs mit insgesamt 81 Übernachtungszimmer mit 136 Betten sowie 25
            Unterrichtsräumen, Gemeinschaftsräumen und zahlreiche
            Konzertmöglichkeiten einen perfekten Ort für musikalische Inputs,
            geselliges Zusammensein und eine tolle Kursatmosphäre.
          </p>
          <p>
            Wir begrüßen bei unserem einwöchigen Kurs
            Zupfinstrumentenspieler:innen ab 18 Jahren aller Leistungsstufen.
            Mandolinen-, Mandola- und Gitarrenspieler:innen, die Lust am
            gemeinsamen Musizieren und Freude an der Zupfmusik haben, sind bei
            uns herzlich willkommen. Sowohl Spieler:innen aus dem Laienbereich
            und den verschiedenen Heimatorchestern, als auch deren Leiter:innen,
            Ausbilder:innen und Lehrer:innen werden durch die namhaften
            Dozent:innen gefördert und unterrichtet. Jede:r Teilnehmer:in erhält
            dabei täglich 30 Minuten Einzelunterricht und kann sich zudem aus
            einem reichhaltigen Unterrichtsangebot (Technikstunden, Dirigieren,
            didaktische Vorträge, Forumsunterricht, Workshops) einen eigenen
            Stundenplan zusammenstellen. Viele Ensemble-Stunden und Konzerte
            runden das Seminar ab und geben Anregungen für das eigene
            Musizieren.
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="Schweinfurter Seminar" />

      <div className="text-components">
        <p>
          Die Teilnehmer:innenzahl ist begrenzt! Es besteht kein Anspruch auf
          eine:n bestimmte:n Dozent:in. Die Anmeldungen werden nach
          Eingangsdatum der Anzahlung berücksichtigt. Nach dem Anmeldeschluss
          wird ein Informationsschreiben mit Anreiseplan an alle gemeldeten
          Teilnehmer:innen versendet.
        </p>
      </div>

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail -m-px -m-py">
            <img
              src={sw_seminar_1}
              alt="Technikunterricht Mandoline und Mandola"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={sw_seminar_2}
              alt="Gitarrenquartett beim Abschlusskonzert der Teilnehmer:innen"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={sw_seminar_3}
              alt="Gitarrenensemble beim Schweinfurter Seminar"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={sw_seminar_4}
              alt="Kursorchester des Schweinfurter Seminars"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              className="object-cover h-full"
              src={sw_seminar_5}
              alt="Dozentenkonzert beim Schweinfurter Seminar"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={sw_seminar_6} alt="Geselliger Abend im Felsenkeller" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={sw_seminar_7} alt="Technikunterricht Gitarre" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={sw_seminar_8}
              alt="Tischkickern beim Schweinfurter Seminar"
            />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default schweinfurterSeminar;

export { Head } from "../../../components/layout-bdz";
