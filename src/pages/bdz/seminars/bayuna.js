import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import { Link } from "gatsby";
import { Buffer } from "buffer";
import bayuna1 from "../../../images/bayuna/bayuna1.jpg";
import bayuna2 from "../../../images/bayuna/bayuna2.jpg";
import bayuna3 from "../../../images/bayuna/bayuna3.jpg";
import bayuna4 from "../../../images/bayuna/bayuna4.jpg";
import bayuna5 from "../../../images/bayuna/bayuna5.jpg";
import bayuna6 from "../../../images/bayuna/bayuna6.jpg";
import bayuna7 from "../../../images/bayuna/bayuna7.jpg";
import bayuna8 from "../../../images/bayuna/bayuna8.jpg";
import bayuna9 from "../../../images/bayuna/bayuna9.jpg";
import bayuna10 from "../../../images/bayuna/bayuna10.jpg";
import CourseDate from "../../../components/dates/coursedate";

const bayuna = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">Bayuna</h1>
        </header>
        <article>
          <p>
            Das Projektorchester Bayuna wurde 2016 gegründet, um eine Brücke
            zwischen dem{" "}
            <Link to="/bljzo">Bayerischen Landesjugendzupforchester</Link> und
            dem <Link to="/blzo">Bayerischen Landeszupforchester</Link> zu
            schaffen. Mit dem Dirigenten Silvan Wagner ist Bayuna mittlerweile
            zu einem Kurs des BDZ Landesverband Bayern mit ca. 20 begeisterten
            Spieler:innen ab 20 Jahren herangewachsen. Das Ziel ist es, jährlich
            zwei Probenphasen auf forderndem Niveau durchzuführen, die ein in
            sich abgeschlossenes Projekt bilden. Dadurch ist eine projektweise
            Teilnahme möglich, was insbesondere geringere zeitliche
            Verbindlichkeiten erfordert. Die Programme werden jeweils zu einem
            gemeinsam gewählten Thema erstellt und von Spieler:innen und
            Dirigent gleichermaßen musikalisch und im Kontext erkundet.
          </p>
          <p>
            Organisatorisch funktioniert das Orchester basisdemokratisch. Es
            gibt also keine Stimmführer:innen, die Spieler:innen tragen die
            Verantwortung gemeinsam. Außerdem werden Termine und Projekte per
            Abstimmung beschlossen. Was bei all dem natürlich im Vordergrund
            steht: Der gemeinsame Spaß an der Musik!
          </p>
          <h2>Aktuelles Projekt:</h2>
          <p>
            2024 werden wir mit Ravenscroft-Bearbeitungen wundervolle
            Renaissance-Miniaturen erarbeitet, die den Grundstock für unser
            Konzertprogramm abgeben. Mit „Alone at the Wedding“ von Alexander
            Mathewson und „Dance games“ von Svetlin Hristov haben wir zwei sehr
            rhythmisch-tänzerische Stücke junger zeitgenössischer
            osteuropäischer Komponisten ins Programm genommen, die metrisch und
            harmonisch sehr spannend sind. „Alone at the Wedding“ ist extra für
            uns neu gesetzt worden mit einer Sologeige. Unser Programm ist
            sicherlich anspruchsvoll und fordernd, aber wir hoffen, dass die
            Arbeit daran sehr spannend und musikalisch erfüllend sein wird.
          </p>
          <h2>Beispiele für bisherige Projekte</h2>

          <ul>
            <li>Zeigenössische Zupfmusik</li>
            <li>
              Beethoven Gesamtwerk für Mandoline und Kagel: Ludwig van/Zupfmusi
            </li>
            <li>Música de Sudamérica</li>
            <li>Musik des Mittelalters</li>
            <li>Musik der Jahrhundertwende</li>
          </ul>

          <h2>Lust auf Mitspielen?</h2>
          <p>
            Ein Einstieg bei Bayuna ist (fast) immer möglich – wir freuen uns
            über jede:n neue:n Teilnehmer:in! Vor der Probenphase werden die
            Orchesternoten per Post zugeschickt um sich genügend vorbereiten zu
            können. Es gibt kein besonderes Auswahlvorspiel, die erste Teilnahme
            zählt als Testprobenphase. Die aktuelle Ausschreibung, welche
            Informationen und den Anmeldebogen zur nächsten Probenphase enthält,
            kann im Abschnitt „Termine“ gefunden werden.
          </p>
          <p>
            Weitere Infos gibt es unter{" "}
            <strong>
              {Buffer.from("aW5mbyBhdCBiYXl1bmEuZGU=", "base64").toString()}
            </strong>
            .
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="Bayuna" />

      <div className="width-content text-components">
        <article>
          <p>
            Neuigkeiten gibt es auch auf der{" "}
            <a href="https://www.facebook.com/bayunaensemble/">Facebookseite</a>{" "}
            von Bayuna.
          </p>
        </article>
      </div>

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail -m-px -m-py">
            <img src={bayuna1} alt="Bayuna in Bayreuth" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna2} alt="Bayuna auf dem Weihnachtsmarkt" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna3} alt="Bayuna auf einer Baustelle" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={bayuna4}
              alt="Bayuna beim Landesmusikfest des BDZ Bayern 2019"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              className="object-cover h-full"
              src={bayuna5}
              alt="Bayuna vor einem ausgebrannten Auto"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna6} alt="Bayuna in Lohr" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna7} alt="Bayuna in Aschaffenburg" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna8} alt="Bayuna auf dem Weihnachtsmarkt" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna9} alt="Bayuna und eine Statue" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img src={bayuna10} alt="Bayuna blockiert Zugangstreppen" />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default bayuna;

export { Head } from "../../../components/layout-bdz";
