import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import { Link } from "gatsby";
import osterkurs1 from "../../../images/bdz/seminars/osterkurs1.jpg";
import osterkurs2 from "../../../images/bdz/seminars/osterkurs2.jpg";
import osterkurs3 from "../../../images/bdz/seminars/osterkurs3.jpg";
import CourseDate from "../../../components/dates/coursedate";

const osterkurs = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">Osterkurs</h1>
        </header>
        <article>
          <p>
            Der Osterkurs ist ein Lehrgang für Mandoline und Gitarre des Bundes
            Deutscher Zupfmusiker Landesverband Bayern e.V., der seit 1976
            jährlich im südbayerischen Raum veranstaltet wird. Er verbindet in
            besonderer Weise Freizeit mit Gleichgesinnten und intensive
            musikalische Weiterbildung &ndash; die optimale Kombination von Spaß
            und Lernen in den Osterferien.{" "}
          </p>
          <p>
            Mit seinem Rahmenprogramm an gemeinschaftlichen Aktivitäten (Spiele
            am Abend, Rallye zum Abschluss etc.) richtet er sich vor allem an
            die Altersgruppe zwischen 12 und 18 Jahren. Aber auch Jüngere sind
            willkommen (Mindestalter 10 Jahre) und nach oben gibt es keine
            Altersgrenze. Angehende Student:innen, erwachsene
            Hobbyinstrumentalist:innen und musizierende Senior:innen fühlen sich
            genauso gut aufgehoben und profitieren von dem fachlichen Angebot:
            Einzelunterricht am Instrument (30 Minuten täglich), Technik,
            Theorieunterricht und Ensemblespiel werden ergänzt durch wechselnde
            Zusatzkurse wie zum Beispiel Liedbegleitung, Dirigieren,
            Bodypercussion und Instrumentenpflege.
          </p>
          <p>
            Außerdem besteht die Möglichkeit, eine D-Musikprüfung nach den
            Kriterien des Bundes Deutscher Zupfmusiker abzulegen. Die drei
            Stufen der D-Prüfung sind gleichwertig zu den jeweiligen
            freiwilligen Leistungsprüfungen der bayerischen Musikschulen. Sie
            dienen der individuellen Leistungseinordnung, sind aber auch eine
            perfekte Vorbereitung für ein eventuelles Additum Musik. Der
            Theorieunterricht richtet sich nach den Anforderungen der jeweiligen
            Prüfungsstufe und im Einzelunterricht kann man dem Programm für den
            praktischen Prüfungsteil den letzten Schliff geben.
          </p>
          <p>
            Der Kurs beginnt immer am Ostermontag mittags und endet am
            darauffolgenden Samstag mit einem Abschlusskonzert aller
            Teilnehmer:innen vor dem Mittagessen. Organisator und Kursleiter ist
            seit vielen Jahren der{" "}
            <Link to="/bdz/people">Vizepräsident des Landesverbandes</Link>{" "}
            Heiko Holzknecht.
          </p>
          <p>
            Als Dozent:innen werden qualifizierte Pädagog:innen mit viel
            Erfahrung in der Arbeit mit Jugendlichen eingestellt.
          </p>
          <p>
            Durch die großzügige Förderung des Bayerischen Staatsministerium für
            Wissenschaft und Kunst ist es uns möglich, die Kursgebühren
            inklusive Unterkunft und Verpflegung gering zu halten.
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="Osterkurs" />

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail -m-px -m-py">
            <img src={osterkurs1} alt="Teilnehmende dirigieren am Osterkurs" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img src={osterkurs2} alt="Teilnehmende singen beim Osterkurs" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img
              src={osterkurs3}
              alt="Teilnehmende musizieren beim Abschlusskonzert des Osterkurs"
            />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default osterkurs;

export { Head } from "../../../components/layout-bdz";
