import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import { Link } from "gatsby";
import pfingstkurs1 from "../../../images/bdz/seminars/pfingstkurs1.jpg";
import pfingstkurs2 from "../../../images/bdz/seminars/pfingstkurs2.jpg";
import pfingstkurs3 from "../../../images/bdz/seminars/pfingstkurs3.jpg";
import CourseDate from "../../../components/dates/coursedate";

const pfingstkurs = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">Pfingstkurs</h1>
        </header>
        <article>
          <p>
            Der Pfingstkurs ist ein Lehrgang für Mandoline, Gitarre und (seit
            ein paar Jahren) auch für Kontrabass des Bundes Deutscher
            Zupfmusiker Landesverband Bayern e.V., der seit 1976 jährlich im
            nordbayerischen Raum veranstaltet wird. Er verbindet in besonderer
            Weise Freizeit mit Gleichgesinnten und intensive musikalische
            Weiterbildung und ist die optimale Kombination von Spaß und Lernen
            in den Pfingstferien.
          </p>
          <p>
            Mit seinem Rahmenprogramm an gemeinschaftlichen Aktivitäten (Spiele
            am Abend, Rallye zum Abschluss etc.) richtet er sich vor allem an
            die Altersgruppe zwischen 12 und 18 Jahren. Aber auch Jüngere sind
            willkommen (Mindestalter 10 Jahre) und nach oben gibt es keine
            Altersgrenze. Auch angehende Student:innen, erwachsene
            Hobbyinstrumentalist:innen und musizierende Senior:innen (z.B. die
            „uHus“) fühlen sich gut aufgehoben und profitieren von dem
            fachlichen Angebot: Einzelunterricht am Instrument (30 Minuten
            täglich), Techniktraining, Theorieunterricht und Ensemblespiel
            werden ergänzt durch wechselnde Zusatzkurse wie zum Beispiel
            Liedbegleitung, Dirigieren, Bodypercussion und Jazzharmonik.
          </p>
          <p>
            Außerdem besteht die Möglichkeit, eine D-Musikprüfung nach den
            Kriterien des Bundes Deutscher Zupfmusiker abzulegen. Die drei
            Stufen der D-Prüfung sind gleichwertig zu den jeweiligen
            Freiwilligen Leistungsprüfungen der bayerischen Musikschulen. Sie
            dienen der individuellen Leistungseinordnung, sind aber auch eine
            perfekte Vorbereitung für ein eventuelles Additum Musik. Der
            Theorieunterricht richtet sich nach den Anforderungen der jeweiligen
            Prüfungsstufe und im Einzelunterricht kann man dem Programm für den
            praktischen Prüfungsteil den letzten Schliff geben.
          </p>
          <p>
            Der Kurs beginnt immer am Pfingstmontag mittags und endet am
            darauffolgenden Samstag mit einem Abschlusskonzert aller
            Teilnehmer:innnen vor dem Mittagessen. Organisatorin und
            Kursleiterin ist seit vielen Jahren die{" "}
            <Link to="/bdz/people">Musikleiterin des Landesverbandes</Link>{" "}
            Petra Breitenbach. Als Dozenten werden qualifizierte Pädagog:innen
            mit viel Erfahrung in der Arbeit mit Jugendlichen verpflichtet.
          </p>
          <p>
            Die günstigen Gesamtgebühren für Kurs, Unterkunft und Verpflegung
            sind nur möglich durch die großzügige Förderung durch das Bayerische
            Staatsministerium für Wissenschaft und Kunst und die Kulturstiftung
            des Bezirkes Unterfranken.
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="Pfingstkurs" />

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail -m-px -m-py">
            <img
              src={pfingstkurs1}
              alt="Ein gemischtes Ensemble mit Boomwhackers musiziert beim Pfingstkurs"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img
              src={pfingstkurs2}
              alt="Ein Kontrobassquintett tritt auf beim Pfingstkurs"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 thumbnail md:-m-px -m-py">
            <img
              src={pfingstkurs3}
              alt="Teilnehmende erhalten ihre Teilnahmeurkunden beim Pfingstkurs"
            />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default pfingstkurs;

export { Head } from "../../../components/layout-bdz";
