import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import ch_seminar_1 from "../../../images/ch_seminar/ch_seminar_1.jpg";
import ch_seminar_2 from "../../../images/ch_seminar/ch_seminar_2.jpg";
import ch_seminar_3 from "../../../images/ch_seminar/ch_seminar_3.jpg";
import ch_seminar_4 from "../../../images/ch_seminar/ch_seminar_4.jpg";
import CourseDate from "../../../components/dates/coursedate";

const kinderkurs = () => {
  return (
    <LayoutBdz pageName="is--seminar">
      <div className="width-content text-components">
        <header>
          <h1 className="big">Kinderkurse</h1>
        </header>
        <article>
          <p>
            Die Kinderkurse „Musik, Spiel und Spaß“ sind
            Fortbildungsmöglichkeiten des BDZ Landesverband Bayern. Die Kurse
            finden seit 1994 statt und werden meist im März eines jeden Jahres
            für ein Wochenende auf der Burg Rothenfels (bei Lohr) und im
            Schullandheim Reichmannshausen (bei Schweinfurt) mit ca. 20-30
            Kindern sehr erfolgreich durchgeführt. Die Leitung haben Petra
            Breitenbach bzw. Bianca Brand.
          </p>
          <p>
            6- bis 12-jährige Kinder, die seit mind. einem Jahr Mandoline oder
            Gitarre spielen, setzen sich spielerisch mit der Zupfmusik
            auseinander; auf Burg Rothenfels können auch Kinder mit den
            Instrumenten Violine, Viola, Violoncello und Kontrabass teilnehmen.
            Jährlich wechselnde altersgerechte Mottos sorgen für große
            Abwechslung auch bei den treuen Kursteilnehmer:innen. Alle Kinder
            erhalten Einzelunterricht auf ihrem Instrument und lernen das
            Zusammenspiel und Miteinander in der Gruppe und im großen
            Kursorchester.
          </p>
          <p>
            Die Dozent:innen sind Musikpädadogen:innen aus den angrenzenden
            Musikschulen und freuen sich auf Kinder, die Spaß am Musizieren
            haben und von Samstagvormittag bis Sonntagnachmittag nicht nur
            Musikalisches erleben möchten, sondern auch gemeinsam basteln
            (passend zum Motto), Fußball spielen, rhythmische Spiele und
            Wanderungen durchführen. Der Spaß kommt nicht zu kurz!
          </p>
          <p>
            Abgerundet wird der Sonntag mit einem nachmittäglichen
            Abschlusskonzert, bei dem die Kinder ihren Eltern und Angehörigen
            stolz ihr erlerntes Können auf den Zupf- und Streichinstrumenten
            demonstrieren und vor allem zeigen, wie schön das Zusammenspiel in
            Gruppen ist.
          </p>
        </article>
      </div>

      <CourseDate subpageFilter="Kinderkurs" />

      <div className="is--section width-full text-components">
        <header className="width-content text-components">
          <h1>Galerie</h1>
        </header>
        <div className="gallery flex flex-col md:flex-row md:flex-wrap is--grid md:ml-px">
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail -m-px -m-py">
            <img src={ch_seminar_1} alt="Gruppenfoto der Teilnehmer:innen" />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={ch_seminar_2}
              alt="Als Menschen aus aller Welt verkleidete Kursteilnehmer:innen"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={ch_seminar_3}
              alt="Als Tiere verkleidete Kursteilnehmer:innen"
            />
          </figure>
          <figure className="galleryItem w-full md:w-1/2 lg:w-1/3 xl:w-1/4 thumbnail md:-m-px -m-py">
            <img
              src={ch_seminar_4}
              alt="Experimente zu zweit auf einer Gitarre"
            />
          </figure>
        </div>
      </div>
    </LayoutBdz>
  );
};
export default kinderkurs;

export { Head } from "../../../components/layout-bdz";
