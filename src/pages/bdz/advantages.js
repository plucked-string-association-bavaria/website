import React from "react";
import LayoutBdz from "../../components/layout-bdz";
import { Link } from "gatsby";
import satzung from "../../attachments/satzung.pdf";
import lehrerliste from "../../images/bdz/musikschulen_zupforchester.jpg";
import bdzOrchestraImage from "../../images/bdz/bdz-orchestra.webp";
import Check from "../../images/global/svg/check.inline.svg";

const bdzAdvantages = () => {
  return (
    <LayoutBdz pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">Was bieten wir?</h1>
        </header>
        <article>
          <ul className="flex flex-col lg:flex-row gap-8 list-none is--grid width-page">
            <li className="panel lg:w-1/2">
              <h4 className="flex justify-center">
                <span className="svg-initiale is--sm flex--child mr-2">
                  <Check />
                </span>
                <span className="self-center">Für das einzelne Mitglied:</span>
              </h4>
              <ul>
                <li>
                  Vergünstigte Teilnahme am vielfältigen{" "}
                  <Link to="/bdz/seminars">Kursangebot</Link> des BDZ LV Bayern
                </li>
                <li>
                  Kostenloser Bezug der Verbandszeitschrift „Auftakt!“
                  (Bundesverband)
                </li>
                <li>Mitwirkung in den überregionalen Auswahlorchestern</li>
                <li>
                  <a href={lehrerliste}>
                    Übersicht der Mandolinenlehrer:innen und Zupforchester in
                    Bayern
                  </a>
                </li>
              </ul>
            </li>
            <li className="panel lg:w-1/2">
              <h4 className="flex justify-center">
                <span className="svg-initiale is--sm flex--child mr-2">
                  <Check />
                </span>
                <span className="self-center">Für Mitgliedsorchester:</span>
              </h4>
              <ul>
                <li>
                  Übernahme der GEMA-Gebühren für selbstveranstaltete Konzerte
                  der Mitgliedsorchester (Bundesverband)
                </li>
                <li>Dirigenten- und Notenzuschuss</li>
                <li>Zugriff auf das umfangreiche Notenarchiv</li>
                <li>Mitwirkung bei überregionalen Veranstaltungen</li>
              </ul>
            </li>
          </ul>

          <ul className="list-none width-content">
            <li className="panel">
              <h4 className="flex justify-center">
                <span className="svg-initiale is--sm flex--child mr-2">
                  <Check />
                </span>
                <span className="self-center">
                  Wie funktioniert die Verbandsarbeit?
                </span>
              </h4>
              <p>Der Verband befasst sich ehrenamtlich mit folgenden Themen:</p>
              <ul>
                <li>
                  Regelmäßige Aktualisierung und Durchführung des Kursprogramms
                </li>
                <li>Entwicklung neuer zeitgemäßer Projekte</li>
                <li>
                  Vorbereitung auf und Abnahme von Laienmusikprüfungen, z.B.
                  D1–D3
                </li>
                <li>
                  Empfehlung von Juroren bei Wettbewerben, z.B. Jugend musiziert
                </li>
                <li>
                  Mitarbeit bei der Erstellung von Literaturlisten, z.B.
                  „Additum Musik“ an bayerischen Gymnasien
                </li>
                <li>
                  Ausrichtung überregionaler Veranstaltungen und
                  Landesmusikfeste
                </li>
                <li>Einwerben von Zuschüssen</li>
              </ul>
              <p>
                Es ist immer möglich, hier mitzuarbeiten – wir würden uns sehr
                über Unterstützung freuen! Kontaktieren Sie bei Interesse gerne
                den <Link to="/bdz/people">Vorstand</Link>.
              </p>
              <p>
                Die Satzung des Landesverbands kann{" "}
                <a href={satzung} download>
                  hier
                </a>{" "}
                eingesehen werden.
              </p>
            </li>
          </ul>

          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bdzOrchestraImage}
              alt="Ein Orchester des Bund Deutscher Zupfmusiker Landesverband Bayern"
            />
          </figure>
        </article>
      </div>
    </LayoutBdz>
  );
};
export default bdzAdvantages;

export { Head } from "../../components/layout-bdz";
