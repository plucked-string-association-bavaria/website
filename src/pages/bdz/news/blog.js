import React from "react";

import LayoutBdz from "../../../components/layout-bdz";
import BlogIntro from "../../../components/blog-intro";
import Filter from "../../../images/global/svg/filter.inline.svg";
import Trash from "../../../images/global/svg/trash.inline.svg";
import FilteredBlog from "../../../components/filtered-blog";

export default class BdzBlog extends React.Component {
  constructor(props) {
    super(props);
    this.selectRef = React.createRef();
  }

  state = {
    selected: "",
  };

  handleInputChange = (event) => {
    this.setState({ selected: event.target.value });
    this.render();
  };

  resetFilter = () => {
    this.setState({ selected: "" });
    this.selectRef.current.value = "";
    this.render();
  };

  createOptionsForYears = () => {
    let currentYear = new Date().getFullYear();
    let options = [];
    for (let i = currentYear; i >= 2012; i--) {
      options.push(
        <option value={i} key={i}>
          {i}
        </option>
      );
    }
    return options;
  };

  render() {
    return (
      <LayoutBdz pageName="is--dates">
        <div className="text-components mb-0">
          <header className="page-title">
            <h1 className="big">Der bayerische Zupfbote</h1>
          </header>

          <BlogIntro />

          <div className="is--section width-full is--blog">
            <aside className="filter">
              <span className="filter-icon">
                <Filter />
              </span>
              <span className="select">
                <select
                  className="select"
                  ref={this.selectRef}
                  onChange={this.handleInputChange}
                  defaultValue=""
                >
                  <option value="" disabled>
                    Filter
                  </option>
                  {this.createOptionsForYears()}
                </select>
                <span className="focus">&nbsp;</span>
              </span>

              <button onClick={this.resetFilter}>
                <span className="trash-icon">
                  <Trash />
                </span>
                <strong>zurücksetzen</strong>
              </button>
            </aside>
            <FilteredBlog currentUserFilter={this.state.selected} />
          </div>
        </div>
      </LayoutBdz>
    );
  }
}

export { Head } from "../../../components/layout-bdz";
