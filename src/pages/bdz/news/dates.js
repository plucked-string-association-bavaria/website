import React from "react";
import LayoutBdz from "../../../components/layout-bdz";
import BdzDatesWithFilter from "../../../components/dates/bdzdates-with-filter";

const bdzDates = () => {
  return (
    <LayoutBdz pageName="is--dates">
      <div className="width-content">
        <header className="text-components">
          <h1>Veranstaltungen, Kurse und Termine</h1>
        </header>
        <BdzDatesWithFilter />
      </div>
    </LayoutBdz>
  );
};
export default bdzDates;

export { Head } from "../../../components/layout-bdz";
