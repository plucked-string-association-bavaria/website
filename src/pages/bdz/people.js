import { Buffer } from "buffer";
import { Link } from "gatsby";
import React from "react";
import LayoutBdz from "../../components/layout-bdz";
import praesident from "../../images/bdz/praesident.jpg";
import finanzen from "../../images/bdz/finanzen.jpg";
import musikleitung from "../../images/bdz/musikleitung.jpg";
import jugendleitung from "../../images/bdz/jugendleitung.jpg";
import jugendleitungVize from "../../images/bdz/jugendleitung_vize.jpg";
import CashRegister from "../../images/global/svg/cash-register.inline.svg";
import ChalkboardTeacher from "../../images/global/svg/chalkboard-teacher.inline.svg";
import Notes from "../../images/global/svg/music.inline.svg";
import Users from "../../images/global/svg/users.inline.svg";

const bdzPeople = () => {
  return (
    <LayoutBdz pageName="is--team">
      <div className="text-components width-page">
        <header className="page-title">
          <h1 className="big lg:text-center width-page">Vorstand</h1>
        </header>
        <ul className="employees flex flex-col list-none">
          <li>
            <header>
              <h2>
                <strong>Dr. Thomas Hammer</strong>
                <span className="is--task">
                  <Users />
                  <small>Präsident</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={praesident} alt="Porträt Dr. Thomas Hammer"></img>
                </span>
              </figure>
              <p>
                Hauptamtlich arbeite ich als promovierter Physiker an der
                Entwicklung innovativer Technologien im Bereich Energie und
                Umwelt. In meiner Freizeit bin ich begeisterter Mandolinen- und
                Mandolaspieler, z.B. im{" "}
                <Link to="/blzo">Bayerischen Landeszupforchester</Link> und als
                Gründungsmitglied des Zupfmusikensembles Röttenbach. Das
                gemeinsame Musizieren schätze ich als einen wunderbaren
                Ausgleich zu meinem interessanten und herausfordernden Beruf.
              </p>
              <p>
                Seit November 2020 bin ich nun ehrenamtlicher Präsident des BDZ
                LV Bayerns. Ich bin davon überzeugt, dass die Zupfmusikszene
                einen Verband braucht, der Interessen bündelt und Musiker:innen
                dabei unterstützt, musikalische Ideen umzusetzen. Dadurch
                erhalten wir eine zeitgemäße, lebendige Zupfmusikszene. Deshalb
                stehe ich vor allem als Ansprechpartner für die Fragen unserer
                Mitglieder gerne zur Verfügung. Kommen Sie bitte mit konkreten
                Fragen und Problemen auch direkt auf mich zu.
              </p>
              <p>
                (
                {Buffer.from(
                  "cHJhZXNpZGVudCBhdCBiZHotYmF5ZXJuLmRl",
                  "base64"
                ).toString()}
                )
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Thomas Zapf</strong>
                <span className="is--task">
                  <CashRegister />
                  <small>Schatzmeister</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={finanzen} alt="Porträt Thomas Zapf"></img>
                </span>
              </figure>
              <p>
                Neben meiner beruflichen Tätigkeit als Bauingenieur im Bereich
                Hochwasserschutz spiele ich seit meiner Jugend in diversen
                Gruppen und Orchestern Gitarre, seit 1986 u.a. im{" "}
                <Link to="/bljzo">Bayerischen Landesjugendzupforchester</Link>{" "}
                und später im{" "}
                <Link to="/blzo">Bayerischen Landeszupforchester</Link>.
              </p>
              <p>
                Seit 1999 bin ich Schatzmeister beim BDZ - Landesverband Bayern
                und damit verantwortlich für die Finanzen des Verbands und die
                Bearbeitung der Zuschüsse für unsere Mitgliedsorchester. Bei
                Fragen und Anregungen zum Thema Bezuschussung können Sie sich
                gerne an mich wenden.
              </p>
              <p>
                (
                {Buffer.from(
                  "ZmluYW56ZW4gYXQgYmR6LWJheWVybi5kZQ==",
                  "base64"
                ).toString()}
                )
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Petra Breitenbach</strong>
                <span className="is--task">
                  <Notes />
                  <small>Musikleitung</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={musikleitung} alt="Porträt Petra Breitenbach"></img>
                </span>
              </figure>
              <p>
                Mit dem BDZ Landesverband Bayern bin ich seit Jahrzehnten eng
                verbunden. Schon als Schülerin nutzte ich das vielfältige
                Kursangebot – ich erlebte so viele besondere Momente, dass ich
                meinen ursprünglichen Berufswunsch änderte und Musik
                (Gitarre/Mandoline/EMP) studierte.
              </p>
              <p>
                Seit 1980 bin ich Mitglied im{" "}
                <Link to="/blzo">Bayerischen Landeszupforchester</Link> und
                Dozentin bei diversen Kursen. Neben meiner Tätigkeit als
                Leiterin der Sing- und Musikschule Lohr am Main und des
                Zupf-Ensemble Lohr (seit 1982) engagiere ich mich seit Jahren
                als Musikleiterin im Landesverband. Mir ist eine gute Vernetzung
                der Zupfmusiker sehr wichtig sowie der regelmäßige Austausch mit
                anderen Musikfreunden. Ich stelle mich der Herausforderung,
                moderne Strömungen zu erkennen und erhaltenswerte Traditionen zu
                bewahren. Gerne können Sie mich bei musikalischen Fragen oder
                Anregungen kontaktieren.
              </p>
              <p>
                (
                {Buffer.from(
                  "bXVzaWtsZWl0dW5nIGF0IGJkei1iYXllcm4uZGU=",
                  "base64"
                ).toString()}
                )
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Antonia Platzdasch</strong>
                <span className="is--task">
                  <ChalkboardTeacher />
                  <small>Jugendleitung</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img
                    src={jugendleitung}
                    alt="Porträt Antonia Platzdasch"
                  ></img>
                </span>
              </figure>
              <p>
                Seit Oktober 2020 studiere ich an der HfMT Köln Standort
                Wuppertal Mandoline. Ich habe von klein auf in verschiedenen
                Orchestern gespielt, seit meinem 11. Lebensjahr auch im{" "}
                <Link to="/bljzo">Bayerischen Landesjugendzupforchester</Link>.
                Aktuell spiele ich zusätzlich zum BLJZO auch im Bayerischen
                Landeszupforchester. Seit 2019 bin ich Mitglied im
                Bundesjugendzupforchester. Ich genieße das musikalische
                Miteinander und den regen Austausch, der sich vor und nach den
                Proben ergibt. Dies trug dazu bei, dass ich jetzt Mandoline
                studiere.
              </p>
              <p>
                Auch wenn ich jetzt aufgrund des Studiums in Wuppertal wohne,
                bin ich doch in Bayern verankert und weiterhin dort musikalisch
                aktiv. So habe ich im November 2020 gerne die Wahl zur
                Jugendleiterin angenommen. Mir ist es wichtig, dass du als
                Jugendliche:r dich mit all deinen Wünschen, Anregungen, Sorgen
                oder Problemen in musikalischer Hinsicht an eine:n Vertreter:in
                des BDZ LV Bayern wenden kannst. Als Jugendleiterin möchte ich
                dir die Gelegenheit geben, mit mir über all das zu sprechen. Ich
                helfe gerne, wo ich kann. Für neue kreative Ideen habe ich immer
                ein offenes Ohr und freue mich auf ein kommunikatives
                Miteinander.
              </p>
              <p>
                (
                {Buffer.from(
                  "anVnZW5kbGVpdHVuZyBhdCBiZHotYmF5ZXJuLmRl",
                  "base64"
                ).toString()}
                )
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Joëlle Krinner</strong>
                <span className="is--task">
                  <ChalkboardTeacher />
                  <small>Stellvertretende Jugendleitung</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img
                    src={jugendleitungVize}
                    alt="Porträt Joëlle Krinner"
                  ></img>
                </span>
              </figure>
              <p>
                Seit ich klein bin, liebe ich die Musik. Mit 6 habe ich das
                Mandoline spielen angefangen und seitdem in mehreren Ensembles
                gespielt. Aktuell spiele ich neben meinem Medizinstudium im{" "}
                <Link to="/bljzo">Bayerischen Landesjugendzupforchester</Link>,
                in dem ich seit 2018 Mitglied bin. Sowohl der Zusammenhalt und
                Spaß vor und nach den Proben als auch das gemeinsame Musizieren
                bereitet mir viel Freude. Als stellvertretende Jugendleitung
                möchte ich dir die Gelegenheit geben, deine Sorgen,
                Verbesserungsvorschläge und kreativen Ideen gerne immer mit uns
                zu besprechen. Ich freue mich auf viele anregende Gespräche und
                musikalische Erlebnisse.
              </p>
              <p>
                (
                {Buffer.from(
                  "anVnZW5kbGVpdHVuZyBhdCBiZHotYmF5ZXJuLmRl",
                  "base64"
                ).toString()}
                )
              </p>
            </article>
          </li>
        </ul>
      </div>
    </LayoutBdz>
  );
};
export default bdzPeople;

export { Head } from "../../components/layout-bdz";
