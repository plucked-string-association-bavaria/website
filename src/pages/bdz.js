import React from "react";
import LayoutBdz from "../components/layout-bdz";
import bdzMainImage1 from "../images/bdz/bdz-eyecatcher.webp";

const bdzMain = () => {
  return (
    <LayoutBdz pageName="is--orchester">
      <div className="text-components">
        <header className="heading">
          <p className="prae">Bund Deutscher</p>
          <h1 className="big">
            Zupfmusiker
            <small>Landesverband Bayern e.V.</small>
          </h1>
        </header>
        <article>
          <p>
            Der{" "}
            <a href="https://zupfmusiker.de/">
              Bund Deutscher Zupfmusiker (BDZ)
            </a>{" "}
            setzt sich für die Interessen der Zupfmusik in Deutschland ein. Weil
            Kultur Ländersache ist, wird der BDZ auf Landesebene durch den
            Landesverband vertreten. Unser Verband besteht aus musizierenden
            Laien und Profis, Zupfensembles, Lehrkräften und Komponist:innen,
            sowie Instrumentenbauer:innen und Musikschulen. Wir treten für die
            Interessen der Zupfmusik und unserer Mitglieder in Politik und
            Öffentlichkeit ein.
          </p>
          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bdzMainImage1}
              alt="Symbolbild Bund Deutscher Zupfmusiker Landesverband Bayern e.V."
            />
          </figure>
        </article>
      </div>
    </LayoutBdz>
  );
};
export default bdzMain;

export { Head } from "../components/layout-bdz";
