import LayoutBljzo from "../../components/layout-bljzo";
import bljzoExcursionMelnik from "../../images/bljzo/bljzo-excursion-melnik.jpg";
import React from "react";
import { Link } from "gatsby";
import History from "../../images/global/svg/history.inline.svg";

const bljzoHistory = () => {
  return (
    <LayoutBljzo pageName="is--history">
      <div className="text-components width-content history">
        <header className="history-title">
          <figure className="historyIcon">
            <History />
          </figure>
          <h1 className="big">Geschichte</h1>
        </header>

        <article>
          <div className="timeSection">
            <div className="divider" />
            <h3>seit 2023</h3>
            <p className="timeIntro">
              Der aktuelle Leiter des BLJZO seit Beginn 2023 ist Tom Hofmann.
            </p>
          </div>

          <div className="timeSection">
            <div className="divider" />
            <h3>seit 2020</h3>
            <p className="timeIntro">
              2019 übernahm Johannes Lang die Leitung des BLJZO. Durch die allen
              bekannte Lage musste die Probenarbeit sehr sparsam ausfallen,
              dennoch kam es in der Zeit der Pandemie zu mehreren Online-Treffen
              um sich auszutauschen und auch mal den ein oder anderen Ton zu
              spielen.
            </p>
          </div>

          <div className="timeSection">
            <h3>2013 – 2019</h3>
            <p className="timeIntro">
              Ab Ende 2013 ist Julian Habryka der Dirigent des BLJZO. Zu den
              besonderen Projekten dieser Schaffensperiode zählen unter anderem
              Konzertreisen nach Frankreich und Bulgarien, die Gestaltung des
              Jubiläumskonzertes zum 90. Geburtstag von Herbert Baumann und
              mehreren Uraufführungen mit Stücken von Johannes Kern und
              Komponisten aus der Kompositionsklasse von Moritz Eggert, die
              eigens für das BLJZO geschrieben wurden. Durch das gemeinsame
              Musizieren mit anderen Orchestern, wie zum Beispiel mit dem
              Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt,
              wird der Austausch zwischen den Musiker:innen gefördert.
            </p>

            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von </small>Julian
              Habryka
            </h4>
            <ul className="timeSection_inner">
              <li>
                <strong>November 2019:</strong> Festkonzert "100 Jahre Zupfmusik
                im BDZ" beim Landesmusikfest des BDZ Bayern
              </li>
              <li>
                <strong>September 2017:</strong> Konzertreise nach Bulgarien und
                Mitwirkung beim "Fresh Music Festival" in Plovdiv. Weitere
                Konzerte in Sofia, Dospat, sowie in Belgrad (Serbien)
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img
                    src={bljzoExcursionMelnik}
                    alt="Exkursion 2017 zu den Pyramiden von Melnik, Bulgarien"
                  />
                  <figcaption>
                    Exkursion 2017 zu den Pyramiden von Melnik, Bulgarien
                  </figcaption>
                </figure>
              </li>
              <li>
                <strong>Juni 2017:</strong> Aufnahme der CD "Inferno"
              </li>
              <li>
                <strong>Mai 2016:</strong> Konzert mit vier Uraufführungen mit
                Komponisten aus der Kompositionsklasse von Moritz Eggert,
                Hochschule für Musik und Theater München in der Maria Heil der
                Kranken Kirche im Klinikum rechts der Isar, München{" "}
              </li>
              <li>
                <strong>Januar 2016:</strong> Gemeinsamer Auftritt mit dem
                Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt{" "}
              </li>
              <li>
                <strong>September 2015:</strong> Jubiläumskonzert zum 90.
                Geburtstag von Herbert Baumann im Augustinum München-Nord{" "}
              </li>
              <li>
                <strong>März 2015:</strong> Auftritt im Rahmen der "Passauer
                Saiten"
              </li>
              <li>
                <strong>September 2014:</strong> Konzertreise nach Annecy und
                Marseille mit Konzerten in Seythenex, Marseille und Annecy{" "}
              </li>
              <li>
                <strong>Juni 2014:</strong> Uraufführung der für das BLJZO
                geschriebenen Komposition "Zwergenspiel" von Johannes Kern
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>1998 – 2013</h3>
            <p className="timeIntro">
              1998 übernimmt Oliver Strömsdörfer die Leitung des BLJZO.
              Höhepunkte dieser Schaffensphase sind Rundfunkaufnahmen beim
              Bayerischen Rundfunk, die Teilnahme am Galakonzert „25 Jahre
              Landesverband Singen und Musizieren Bayern“, das Mitwirken bei der
              Landesgartenschau in Burghausen und eine gemeinsame Probenphase
              mit dem Hessischen Jugendzupforchester. Durch Eigenkompositionen
              („... du segnest mich denn!“) und Bearbeitungen mittelalterlicher
              Musik („Suite medieval“ und „Cantigas des Santa Maria“) von Silvan
              Wagner sowie Bearbeitungen von Oliver Strömsdörfer erweitert sich
              das Repertoire. Ebenso werden häufig ein bis zwei Programmpunkte
              hinzugenommen, die durch ein Gitarrenensemble gestaltet wurden.
            </p>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von </small>Oliver
              Strömsdörfer
            </h4>
            <ul className="timeSection_inner">
              <li>
                <strong>Oktober 2013:</strong> Fest- und Jubiläumskonzert "30
                Jahre BLJZO" beim Landesmusikfest des BDZ Bayern{" "}
              </li>
              <li>
                <strong>November 2011:</strong> Gemeinsame Probenphase mit dem
                Jugendzupforchester Hessen
              </li>
              <li>
                <strong>November 2006:</strong> Teilnahme am Wettbewerb für
                Auswahlorchester in Trossingen
              </li>
              <li>
                <strong>Mai 2006:</strong> Teilnahme am Eurofestival in Bamberg{" "}
              </li>
              <li>
                <strong>Juli 2004:</strong> Konzert auf der Landesgartenschau in
                Burghausen{" "}
              </li>
              <li>
                <strong>Januar 2002:</strong> Gastkonzert im Rahmen der
                „Aschaffenburger Gitarrentage“
              </li>
              <li>
                <strong>Juni 2001:</strong> 25 Jahre Landesverband Singen und
                Musizieren in Bayern, Galakonzert in Regensburg{" "}
              </li>
              <li>
                <strong>November 2000:</strong> Rundfunkaufnahmen mit dem BR{" "}
              </li>
              <li>
                <strong>Oktober 2000:</strong> Festakt zum 20jährigen Jubiläum
                der Bayerischen Musikakademie Hammelburg
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>1983 – 1998</h3>
            <p className="timeIntro">
              Elke Tober-Vogt setzt 1983 die Wiedergründung des ursprünglichen
              Jugendorchesters als „Bayerisches Landesjugendzupforchester“ um.
              Innerhalb der 15 Jahre ihres Dirigats werden zahlreiche Projekte
              verwirklicht: Konzertreisen nach Italien, Holland, Russland und
              Polen erweiteren den Horizont des Orchesters, während mit eigens
              angeregten Uraufführungen konsequent der Kontakt zur modernen
              Musik gesucht wird. Dieser Abschnitt ist auch durch
              Rundfunkaufnahmen beim Bayerischen Rundfunk und die CD “Winners”
              dokumentiert. Das Besondere an letzterer Einspielung ist, dass sie
              sich komplett aus Preisträgern des in Schweinfurt durchgeführten
              “Konrad-Wölki-Kompositionswettbewerbs” zusammensetzt.
            </p>
          </div>
          <div className="timeSection">
            <h3>1970 – 1983</h3>
            <p className="timeIntro">
              Der Vorgänger des BLJZO wird 1970 durch Gerhard Vogt unter dem
              Namen „Bayerisches Jugendzupforchester“ als erstes überregionales
              Jugendorchester aller Sparten ins Leben gerufen. 1981 wird das
              Orchester für erwachsene Mitspieler:innen geöffnet und in{" "}
              <Link to="/blzo">„Bayerisches Landeszupforchester“</Link>{" "}
              umbenannt. Unter diesem Namen existiert das Orchester bis heute.
            </p>
          </div>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoHistory;

export { Head } from "../../components/layout-bljzo";
