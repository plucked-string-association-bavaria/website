import React from "react";
import LayoutBljzo from "../../../components/layout-bljzo";
import YoutubeVideo from "../../../components/video/youtube-video";
import VideoConsentWrapper from "../../../components/video/video-consent-wrapper";

const bljzoVideos = () => {
  const getCantigaTitle = (number) => {
    return (
      "Alfonso El Sabio/Silvan Wagner: Cantiga " +
      number +
      ' (Festkonzert "30 Jahre BLJZO" Oktober 2013)'
    );
  };

  return (
    <LayoutBljzo pageName="is--media">
      <div className="text-components">
        <header className="heading">
          <h1 className="big">Videos</h1>
          <p>
            Videos werden auf unserem{" "}
            <a
              href="https://www.youtube.com/channel/UC538gHuUPa8DQ5s6e92Olkw/featured"
              target="_blank"
              rel="noreferrer"
            >
              YouTube Channel
            </a>{" "}
            als Upload oder in einer Playlist veröffentlicht.
          </p>
        </header>
        <article>
          <VideoConsentWrapper>
            <ul className="list-none width-content-wide">
              <YoutubeVideo
                slug="d9qijqfkHrM"
                title="Eduard Grieg (arr. Julian Habryka): Holberg Suite"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="9X_eGqC1qmg"
                title={getCantigaTitle(12)}
              ></YoutubeVideo>
              <YoutubeVideo
                slug="NnX2sKHv8Og"
                title={getCantigaTitle(48)}
              ></YoutubeVideo>
              <YoutubeVideo
                slug="M5LpGxqcfDg"
                title={getCantigaTitle(181)}
              ></YoutubeVideo>
              <YoutubeVideo
                slug="0ejhggKMvG0"
                title={getCantigaTitle(147)}
              ></YoutubeVideo>
              <YoutubeVideo
                slug="MOXRAs_6qMU"
                title={getCantigaTitle(18)}
              ></YoutubeVideo>
            </ul>
          </VideoConsentWrapper>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoVideos;

export { Head } from "../../../components/layout-bljzo";
