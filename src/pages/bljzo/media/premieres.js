import React from "react";
import LayoutBljzo from "../../../components/layout-bljzo";

const bljzoPremieres = () => {
  return (
    <LayoutBljzo pageName="is--media">
      <div className="text-components">
        <header className="heading">
          <h1 className="big">Uraufführungen und Widmungen</h1>
        </header>
        <article>
          <h4>
            Uraufführungen, die seit Januar 2014 durch das BLJZO angeregt
            wurden:
          </h4>
          <div className="is--repList">
            <ul className="list-none">
              <li>
                <span>
                  <strong>
                    Johannes Kern (*1992):
                    <small>Uraufführung am 10. Juni 2014 in Hammelburg</small>
                  </strong>
                  <em>
                    <strong>Zwergenspiel</strong> für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Felix Bönigk (*1993):
                    <small>Uraufführung am 25. Mai 2016 in München</small>
                  </strong>
                  <em>
                    <strong>Poéme</strong> für 2 Mandolinen, Mandola,
                    Mandoloncello, Gitarre und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Alexander Mathewson (*1992):
                    <small>Uraufführung am 25. Mai 2016 in München</small>
                  </strong>
                  <em>
                    <strong>Palmyra blutet</strong> für Bouzouki, Darbouka und
                    Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Robin Becker (*1993):
                    <small>Uraufführung am 25. Mai 2016 in München</small>
                  </strong>
                  <em>
                    <strong>Das fünfte Aquarell</strong> für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Julian Habryka (*1987):
                    <small>Uraufführung am 25. Mai 2016 in München</small>
                  </strong>
                  <em>
                    <strong>Sisyphos</strong> für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Julian Habryka (*1987):
                    <small>
                      Uraufführung am 09. September 2016 in Eichenau
                    </small>
                  </strong>
                  <em>
                    <strong>L'Inferno</strong> für Mandoline, Mandola, Gitarre
                    und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Alexander Mathewson (*1992):
                    <small>
                      Uraufführung am 07.09.2017 in Plovdiv, Bulgarien
                    </small>
                  </strong>
                  <em>
                    <strong>Thrace </strong>für Klarinette und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Svetlin Hristov:
                    <small>
                      Uraufführung am 08.09.2017 in Sofia, Bulgarien
                    </small>
                  </strong>
                  <em>
                    <strong>Dance Games </strong>für Zupforchester
                  </em>
                </span>
              </li>
            </ul>
          </div>

          <h4>
            Bearbeitungen, die seit Januar 2014 für das BLJZO angefertigt
            wurden:
          </h4>

          <div className="is--repList">
            <ul className="list-none">
              <li>
                <span>
                  <strong>
                    Henry Purcell (1659 - 1695):
                    <small>Bearbeitung: Jullian Habryka</small>
                  </strong>
                  <em>
                    <strong>King Arthur Suite</strong> für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    <small>
                      Antonio Vivaldi (1678 - 1741): Bearbeitung: Julian Habryka
                    </small>
                  </strong>
                  <em>
                    <strong>"La primavera"</strong> für Solomandoline und
                    Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Trad.:
                    <small>Bearbeitung: Julian Habryka</small>
                  </strong>
                  <em>
                    <strong>"Fröhliche Weihnacht..."</strong> für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    <small>
                      Edvard Grieg (1843 - 1907): Bearbeitung: Julian Habryka
                    </small>
                  </strong>
                  <em>
                    <strong>Suite aus Holbergs Zeit</strong>
                  </em>
                </span>
              </li>
            </ul>
          </div>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoPremieres;

export { Head } from "../../../components/layout-bljzo";
