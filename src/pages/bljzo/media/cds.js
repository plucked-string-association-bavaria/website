import React from "react";
import LayoutBljzo from "../../../components/layout-bljzo";
import { Link } from "gatsby";
import cd from "../../../images/bljzo/bljzo-CDcover.jpg";
import Quote from "../../../images/global/svg/quote.inline.svg";

const bljzoCds = () => {
  return (
    <LayoutBljzo pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">CDs</h1>
          <h4>Bestellung</h4>
          <ul>
            <li>
              <h6>
                Die CD kann zum Preis von 10€ über die{" "}
                <Link to="/bljzo/join">hier</Link> angegebene Emailadresse
                bestellt werden. Die Versandkosten trägt der Käufer.
              </h6>
            </li>
          </ul>
        </header>

        <ul className="media list-none">
          <li className="media--item">
            <figure>
              <img src={cd} alt="CD '... du segnest mich denn!'" />
            </figure>
            <article className="is--half-yRhytm">
              <h2>"... du segnest mich denn!"</h2>
              <p>
                <strong>
                  Die CD ist 2013 erschienen und enthält folgende Werke:
                </strong>
              </p>
              <ul>
                <li>
                  Olof Näslund – Concerto Nr. 3 für Sologitarre und
                  Zupforchester
                </li>
                <li>Silvan Wagner – "... du segnest mich denn!"</li>
                <li>Turlough o`Carolan – Roscommon Suite</li>
                <li>Markus Kugler – Tres Piezas de Sudamérica </li>
                <li>Otto Jezek – Heavy Metal Peppi</li>
                <li>Monty Norman – James Bond Theme</li>
              </ul>
            </article>
          </li>
        </ul>

        <article>
          <h2>Stimmen zur CD:</h2>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Oliver Strömsdörfer ist diese Mischung aus Exaktheit und
                Ausdrucksstärke im Zusammenspiel mit seinem Bayerischen
                Landesjugendzupforchester (BLJZO) beeindruckend gelungen [...].
              </p>
              <footer>
                <small>— Thomas Nytsch</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Diese Zurückhaltung stets im Dienste der Musik jenseits
                extrovertierter Selbstdarstellung ist in gewisser Weise die
                Visitenkarte des BLJZO.
              </p>
              <footer>
                <small>— Thomas Nytsch</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Aufbauend auf einem gemeinsamen rhythmischen Empfinden und
                Pulsieren gelingt es dem Dirigenten mit seinen jungen Spielern
                immer wieder Spannung zu gestalten, die wie aus einem Guss
                kommt.
              </p>
              <footer>
                <small>— Thomas Nytsch</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Was mich bei allen Stücken [der "Tres Piezas de Sudamérica"]
                absolut begeistert, ist das gewählte Tempo und das Timing des
                Orchesters. Vor allem hat mich [die] Interpretation der Milonga
                fasziniert: Genau so - und das meine ich jetzt auch so - habe
                ich mir das Stück vorgestellt.
              </p>
              <footer>
                <small>— Markus Kugler</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Ich bin ein glücklicher Urheber und [...] danke [..] dem
                Orchester von ganzem Herzen vielmals für diese perfekt gelungene
                Version meiner Komposition.
              </p>
              <footer>
                <small>— Markus Kugler</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Besonders hervorheben möchte ich die klangliche Gestaltung mit
                einer enormen dynamischen Bandbreite.
              </p>
              <footer>
                <small>— Bruno Szordikowski</small>
              </footer>
            </div>
          </blockquote>
          <blockquote>
            <span className="quote-icon">
              <Quote />
            </span>
            <div className="quote-content">
              <p>
                Ich freue mich sehr darüber, dass die "Roscommon Suite" von
                Turlough o`Carolan Eingang in Ihr Repertoire gefunden hat. Auch
                hier gefällt mir die musikalisch klangliche Einspielung sehr
                gut.
              </p>
              <footer>
                <small>— Bruno Szordikowski</small>
              </footer>
            </div>
          </blockquote>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoCds;

export { Head } from "../../../components/layout-bljzo";
