import React from "react";
import LayoutBljzo from "../../../components/layout-bljzo";

const bljzoRepertoire = () => {
  return (
    <LayoutBljzo pageName="is--media">
      <div className="text-components">
        <header className="heading">
          <h1 className="big">Repertoireliste</h1>
        </header>
        <article>
          <h4>Das aktuelle Programm des BLJZO:</h4>
          <div className="is--repList col--2md width-content-wide">
            <ul className="list-none">
              <li>
                <span>
                  <strong>Eduardo Angulo:</strong>
                  <em>Rascapango</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Antonín Dvořák:
                    <small>Arr. Michael Falter</small>
                  </strong>
                  <em>Serenade op. 22</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:
                    <small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Brandenburgisches Konzert Nr. 3 G-Dur</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Eduard Grieg:</strong>
                  <em>Drei lyrische Stücke</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Gustav Holst:</strong>
                  <em>St. Paul's Suite</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Henry Mancini:</strong>
                  <em>James Bond Theme</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Karel Svoboda:</strong>
                  <em>Drei Haselnüsse für Aschenbrödel</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    John Williams:
                    <small>Arr. Valdo Preema</small>
                  </strong>
                  <em>Star Wars Medley</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Hans Zimmer
                    <small>Arr. Valdo Preema</small>
                  </strong>
                  <em>Pirates of Zimmer</em>
                </span>
              </li>
            </ul>
          </div>

          <h4>
            Von Januar 2014 bis November 2019 unter der Leitung von Julian
            Habryka erarbeitetes Repertoire:
          </h4>
          <div className="is--repList col--2md width-content-wide">
            <ul className="list-none ">
              <li>
                <span>
                  <strong>Martin F. Ackerman:</strong>
                  <em>Toma4Tango</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Johann Sebastian Bach:</strong>
                  <em>Orchestersuite Nr. 3 D-Dur, BWV 1068</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Herbert Baumann:</strong>
                  <em>Vom Müllersburschen, dem ein Bauer das Pferd nahm</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Herbert Baumann:</strong>
                  <em> Fiamme</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Robin Becker:</strong>
                  <em>Das fünfte Aquarell</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ludwig van Beethoven:</strong>
                  <em>Romanze Nr.2 op.50</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Luiz Bonfa:</strong>
                  <em>Black Orpheus Bee</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Cédric Buissant:</strong>
                  <em>Abmur</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Francesco Civitareale:</strong>
                  <em>Danza delle Streghe</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    John Dowland:
                    <small>Consort Music: </small>
                  </strong>
                  <em>
                    Lachrimae, The King of Denmark's Galliard, Sir Henry
                    Umpton's Funeral
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Laurent Fantauzzi:</strong>
                  <em>Spleen</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>George Gershwin:</strong>
                  <em>Embraceable you</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>George Gershwin:</strong>
                  <em>I Got Rythm</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>George Gershwin:</strong>
                  <em>Variations on „I Got Rhythm“</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Edvard Grieg:
                    <small>3 lyrische Stücke: </small>
                  </strong>
                  <em>Lied des Bauern, Berceuse, Norwegischer Marsch</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Edvard Grieg:
                    <small>Suite aus Holbergs Zeit:</small>
                  </strong>
                  <em>Praeludium, Sarabande, Gavotte, Air, Rigaudon</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Julian Habryka:</strong>
                  <em>Fröhliche Weihnacht...</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Julian Habryka:</strong>
                  <em>L'Inferno</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Svetlin Hristov:</strong>
                  <em>Dance Games</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Johannes Kern:</strong>
                  <em>Zwergenspiel</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>The Song of Japanese Autumn</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Anestis Logothetis:</strong>
                  <em>Styx</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Mario Macciochi:</strong>
                  <em>Milena</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Erik Marchelie:</strong>
                  <em>Agapi Mou</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alexander Mathewson:</strong>
                  <em>Palmyra blutet</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alexander Mathewson:</strong>
                  <em>Thrace</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Wolfgang Amadeus Mozart:</strong>
                  <em> Ein musikalischer Spaß, KV 522</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Olof Näslund:</strong>
                  <em>Concerto Nr. 3 für Sologitarre und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Henry Purcell:</strong>
                  <em> King Arthur Suite</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Patrick Roux:</strong>
                  <em>Les rues mal famées</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Dmitri D. Schostakowitsch:</strong>
                  <em>Jazz-Suite Nr. 1</em>
                </span>
              </li>
              <li>
                <span>
                  <strong> Antonio Vivaldi:</strong>
                  <em>"La primavera" für Solomandoline und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Andrew York:</strong>
                  <em>Lotus Eaters</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Robert Zollitsch:</strong>
                  <em>Ayan Zamdaan</em>
                </span>
              </li>
            </ul>
          </div>
          <h4>
            Von September 1998 bis Oktober 2013 unter der Leitung von Oliver
            Strömsdörfer erarbeitetes Repertoire:
          </h4>
          <div className="is--repList col--2md width-content-wide">
            <ul className="list-none">
              <li>
                <span>
                  <strong>Zico Abreu:</strong>
                  <em>Tico Tico</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Rami Al-Regeb:</strong>
                  <em>Sug al-Safafir in Fall</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Eduardo Angulo:</strong>
                  <em>Divertimento</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Eduardo Angulo:</strong>
                  <em>Suite Mexicana</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Giuseppe Anelli:</strong>
                  <em>Fulgida Luce</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Andriano Banchieri:</strong>
                  <em>Fantasie overo canzoni alla francese</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Agustín Barrios Mangoré:</strong>
                  <em>Leyenda de España für Gitarrenquartett</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Herbert Baumann:</strong>
                  <em>
                    Concerto capriccioso für Solomandoline und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Vladimir Beljaev:</strong>
                  <em>Bagatelle für Percussion und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ignazio Bitelli:</strong>
                  <em> Corrida de Toros</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>William Byrd:</strong>
                  <em>Pavans and Galliards</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>William Byrd:</strong>
                  <em>Miserere</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alfonso Carlos Miguel:</strong>
                  <em>Back to Sirius</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Domenico Cimarosa:</strong>
                  <em>Concerto G-Dur für Solomandoline und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Francois Couperin:</strong>
                  <em>Le Tic Toc Choc</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Claude Debussy:</strong>
                  <em>Sarabande aus "Pour le piano"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Edgardo Donato:</strong>
                  <em>A media luz</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    John Dowland:
                    <small>Consort Music: </small>
                  </strong>
                  <em>
                    Lachrimae, The King of Denmark's Galliard, Sir Henry
                    Umpton's Funeral
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Dietrich Erdmann:</strong>
                  <em>Divertissement</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Manfred Flachskampf:</strong>
                  <em>Irish Folk Suite</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Enrique Granados:</strong>
                  <em>Danzas españolas 4 und 6 für Gitarrenensemble</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Edvard Grieg:
                    <small>3 lyrische Stücke: </small>
                  </strong>
                  <em>In der Heimat, An der Wiege, Es war einmal</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Edvard Grieg:
                    <small>3 lyrische Stücke: </small>
                  </strong>
                  <em>Lied des Bauern, Berceuse, Im Balladenton</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Georg Friedrich Händel:</strong>
                  <em>Suite aus der Oper "Rodrigo"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Georg Friedrich Händel:</strong>
                  <em>
                    Konzert für Solomandoline und Zupforchester (nach op. 4 Nr.
                    6)
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Johann A. Hasse:</strong>
                  <em>Konzert G-Dur für Solomandoline und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Otto Jezek:</strong>
                  <em>Der Heavy Metal Peppi</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Oliver Kälberer:</strong>
                  <em>
                    Vishnu - Times of Struggle für Percussion und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Hansjoachim Kaps:</strong>
                  <em>
                    Milonga y Samba para seis bearbeitet für Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Hansjoachim Kaps:</strong>
                  <em>Danza del Sur für Gitarrenensemble</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Jürgen Klose:</strong>
                  <em>Summertrip</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Markus Kugler:</strong>
                  <em>Tres piezas de Sudamérica</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Railroad Song</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Song of Japanese Autumn</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Mario Macciochi:</strong>
                  <em>Aux Arênes</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Mario Macciochi:</strong>
                  <em>Milena</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Claudio Mandonico:</strong>
                  <em>Preludio e Fuga</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Percy Mayfield:</strong>
                  <em>Hit the Road Jack</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alfonso Montes:</strong>
                  <em>Tepuyes für Gitarrenensemble</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Olof Näslund:</strong>
                  <em>Concerto Nr. 3 für Sologitarre und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ernesto Nazareth:</strong>
                  <em>Flora für Gitarrenensemble</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Turlough O`Carolan:</strong>
                  <em>Roscommon Suite</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Charlie Parker:</strong>
                  <em>My little Suede Shoes</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Astor Piazzolla:</strong>
                  <em>Tango apasionado</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Henry Purcell:
                    <small>Suite aus der Musik zum Sommernachtstraum: </small>
                  </strong>
                  <em>The Fairy Queen</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Michael Praetorius:</strong>
                  <em>Französische Tänze für Schlagwerk und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Maurice Ravel:</strong>
                  <em>Rigaudon aus "Le Tombeau de Couperin"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ottorino Respighi:</strong>
                  <em>Antiche Danze ed Arie</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Thomas Rose:</strong>
                  <em>Advanced Guitar Quartets</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alfonso el Sabio:</strong>
                  <em>6 Cantigas de Santa Maria</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Alfonso el Sabio:</strong>
                  <em>3 Cantigas de Santa Maria</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Giacomo Sartori:</strong>
                  <em>Fior Trentino</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Pieter van der Staak:</strong>
                  <em>Six Hits bearbeitet für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonius Streichardt:</strong>
                  <em>Fresken</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Bruno Szordikowski:</strong>
                  <em>Planxty O`Carolan</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Eythor Thorlaksson:</strong>
                  <em>Cartama für Gitarrenensemble</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Elke Tober-Vogt:</strong>
                  <em>Ein gut Dantzerey</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Trad.:</strong>
                  <em>Guantanamera</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonio Vivaldi:</strong>
                  <em>Concerto "La notte" für Flöte und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonio Vivaldi:</strong>
                  <em>
                    Concerto "La tempesta di mare" für Flöte und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonio Vivaldi:</strong>
                  <em>
                    Konzert a-moll op. 3/8 für zwei Solomandolinen und
                    Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonio Vivaldi:</strong>
                  <em>
                    Konzert G-Dur für zwei Solomandolinen und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Silvan Wagner:</strong>
                  <em> ...du segnest mich denn!</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Silvan Wagner:</strong>
                  <em>Suite medieval</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Marcel Wengler:</strong>
                  <em>Canzona und Fantasia</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Markgräfin Wilhelmine von Bayreuth:</strong>
                  <em>Concerto für zwei Sologitarren und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Christian Friedrich Witt:</strong>
                  <em>Suite G-Dur</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Konrad Wölki:</strong>
                  <em>Lieder ohne Worte</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Konrad Wölki:</strong>
                  <em>Musik für schlichte Feierstunden</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Andrew York:</strong>
                  <em>Attic bearbeitet für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>José Antonio Zambrano:</strong>
                  <em>
                    Suite Venezolana für Flöte, Percussion und Zupforchester
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>José Antonio Zambrano:</strong>
                  <em>Yurubi</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Led Zeppelin:</strong>
                  <em>Stairway to Heaven</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Robert Zollitsch:</strong>
                  <em>Ayan Zamdaan</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>2raumwohnung feat. Rhythm del Mundo:</strong>
                  <em>36 Grad</em>
                </span>
              </li>
            </ul>
          </div>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoRepertoire;

export { Head } from "../../../components/layout-bljzo";
