import React from "react";
import LayoutBljzo from "../../components/layout-bljzo";
import { Link } from "gatsby";
import { Buffer } from "buffer";
import bljzoKreuth from "../../images/bljzo/bljzo-kreuth.jpg";

const bljzoJoin = () => {
  return (
    <LayoutBljzo pageName="is--join">
      <div className="text-components">
        <header className="page-title">
          <h1 className="big">Mitmachen und Kontakt</h1>
        </header>

        <article className="participate">
          <h2>Neue Gesichter sind im Orchester herzlich willkommen.</h2>
          <p>
            Hast du Interesse, bei uns mitzuspielen? Es gibt kein besonderes
            Auswahlvorspiel, die erste Teilnahme zählt als Testprobenphase. Vor
            der Probenphase werden die Orchesternoten per Post zugeschickt, um
            sich genügend vorbereiten zu können. Da das BLJZO vom Bayerischen
            Staatsministerium für Bildung, Kultus, Wissenschaft und Kunst
            gefördert wird, ist der Teilnehmerbeitrag gering.
          </p>

          <h2>Ausschreibungen und Kontakt</h2>
          <ul>
            <li>
              Die aktuelle Ausschreibung, welche Informationen und den
              Anmeldebogen zur nächsten Probenphase enthält, wird{" "}
              <Link to="/bljzo/dates">hier</Link> veröffentlicht.
            </li>
            <li>
              Organisatorische Leitung:
              <br />
              <strong>
                Robert Kotschenreuther (
                {Buffer.from("aW5mbyBhdCBibGp6by5kZQ==", "base64").toString()})
              </strong>
            </li>
          </ul>

          <h2>Weiterführende Links</h2>
          <ul>
            <li>
              Facebook-Seite des Orchesters:
              <br />
              <a
                href="https://www.facebook.com/bljzo/"
                target="_blank"
                rel="noopener noreferrer"
              >
                https://www.facebook.com/bljzo/
              </a>
            </li>
            <li>
              Instagram-Seite des Orchesters:
              <br />
              <a
                href="https://instagram.com/bljzo?r=nametag"
                target="_blank"
                rel="noopener noreferrer"
              >
                https://instagram.com/bljzo?r=nametag
              </a>
            </li>
          </ul>
          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bljzoKreuth}
              alt="Bayerisches Landesjugendzupforchester Orchesterfoto 2018"
            />
            <figcaption>
              Bayerisches Landesjugendzupforchester 2018 in Kreuth
            </figcaption>
          </figure>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoJoin;

export { Head } from "../../components/layout-bljzo";
