import React from "react";
import LayoutBljzo from "../../components/layout-bljzo";
import Users from "../../images/global/svg/users.inline.svg";
import dirigent from "../../images/bljzo/tom.jpg";
import elias from "../../images/bljzo/elias.jpg";
import elisabeth from "../../images/bljzo/elisabeth.jpg";
import florian from "../../images/bljzo/florian.jpg";
import malte from "../../images/bljzo/malte.png";

const bljzoTeam = () => {
  return (
    <LayoutBljzo pageName="is--team">
      <div className="text-components width-page">
        <header className="page-title">
          <h1 className="big lg:text-center width-page">
            Dirigent und Dozententeam
          </h1>
        </header>

        <ul className="employees flex flex-col list-none">
          <li>
            <header>
              <h2>
                <strong>Tom Hofmann</strong>
                <span className="is--task">
                  <Users />
                  <small>Dirigent</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={dirigent} alt="Porträt Tom Hofmann" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Tom Hofmann</strong> begann seine
                musikalische Ausbildung an der Musikschule Obernburg bei Frank
                Wittstock und ist seit 2010 aktives Mitglied im BLJZO. Sowohl
                solistisch als auch im Ensemble ist Tom national und
                international erfolgreich. Nach einem Bundesfreiwilligendienst
                als Schulbegleiter an der Sokba-Schule in Schweinheim/Goldbach
                begann er 2017 das Studium Lehramt für Sonderpädagogik in
                Würzburg. Neben seinem Studium arbeitet Tom als Musiklehrer für
                integratives Instrumentalspiel an der Musikschule Obernburg e.V.
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Florian Brettschneider</strong>
                <span className="is--task">
                  <Users />
                  <small>Gitarre und Bass</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={florian} alt="Porträt Florian Brettschneider" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Florian Brettschneider</strong> ist
                als Spieler bereits langjähriges Mitglied im BLJZO. Er studierte
                Gitarre an der Hochschule für Musik und darstellende Kunst HfMdK
                Frankfurt. Er ist mehrfacher Preisträger nationaler und
                internationaler Gitarrenwettbewerbe wie dem „internationalen
                Gitarrenwettbewerb Balatonakali“.
              </p>
              <p>
                Beim hochkarätigen internationalen „Andres Segovia“
                Gitarrenwettbewerb in Velbert erspielte er sich als erster
                deutscher Gitarrist den ersten Preis in der höchsten
                Altersklasse. Als Musiker ist er auf klassischen
                Gitarrenfestivals, aber auch in Jazzbars oder PopClubs zu sehen
                und tritt mit Stars wie Jazzklarinettist Giora Feidmann auf. Als
                Komponist und Texter schrieb er unter anderem Filmsongs für die
                Kinoproduktion „Freak-City“. Als Produzent, Komponist und
                Musiker ist er für das Frankfurter Popprojekt „Das Kollektiv“,
                den Aschaffenburger Künstler Patrikk, sowie für die Popsängerin
                Dimi Rompos tätig.
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Elias Conrad</strong>
                <span className="is--task">
                  <Users />
                  <small>Gitarre und Bass</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={elias} alt="Porträt Elias Conrad" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Elias Conrad</strong> wurde 1996 in
                Weiden, Deutschland geboren und studierte Gitarre und Laute an
                der Hochschule für Musik Nürnberg, der
                Anton-Bruckner-Privatuniversität Linz und der Universität
                Mozarteum in Salzburg. Derzeit führt er sein Lautenstudium an
                der Schola Cantorum Basilensis in Basel fort. Sowohl als
                Gitarrist, als auch als Lautenist ist er mehrfacher Preisträger
                internationaler Wettbewerbe und als Solist, wie auch in
                kammermusikalischen Besetzungen und Orchestern, regelmäßig auf
                renommierten Bühnen und Festivals in Europa zu hören.
              </p>
              <p>
                <em>
                  Homepage:
                  <a href="https://www.eliasconrad.com">www.eliasconrad.com</a>
                </em>
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Elisabeth Januschko</strong>
                <span className="is--task">
                  <Users />
                  <small>Mandoline und Mandola</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={elisabeth} alt="Porträt Elisabeth Januschko" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Elisabeth Januschko</strong> erhielt
                von 2003 bis 2016 Unterricht für Gitarre und ab 2008 auch für
                Mandola/Mandoline und ist seit 2009 zuverlässige Mitspielerin im
                BLJZO. Von 2011 bis 2017 war sie zudem Mitglied im Ensemble
                Roggenstein, das seit Jahren beim Deutschen Orchesterwettbewerb
                den 1. Platz belegt. In den letzten Schuljahren und während
                ihres Freiwilligen Sozialen Jahres beim Augustinum in München,
                unterrichtete sie einzelne Gitarren- und MandolinenschülerInnen.
                Seit 2017 studiert sie Musik- und Bewegungsorientierte Soziale
                Arbeit in Regensburg.
              </p>
            </article>
          </li>

          <li>
            <header>
              <h2>
                <strong>Malte Weyland</strong>
                <span className="is--task">
                  <Users />
                  <small>Mandoline</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={malte} alt="Porträt Malte Weyland" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Malte Weyland</strong> Studierte
                Mandoline an der Hochschule für Musik und Tanz Köln und beendete
                sein Studium 2018 mit dem Master of Music. Neben dem
                künstlerischen Studium beendete er im selben Jahr auch das
                Studium der Elementaren Musikpädagogik. Während seines Studiums
                unterrichtete er an verschiedenen Musikschulen Mandoline,
                Musikalische Früherziehung und Ensembleklassen, und dirigierte
                das Mandolinen- und Gitarrenorchester Dortmund. Als Solist und
                Dozent unternimmt er Konzertreisen in Deutschland, Italien,
                England und der Schweiz und tritt in verschiedensten
                Kammermusik-Formationen auf.
              </p>
              <p>
                Seit 2018 unterrichtet Malte Weyland an der Sing- und
                Musikschule Regensburg das Instrument Mandoline, Elementares
                Musizieren und engagiert sich bei der musikalischen Grundbildung
                in Kindergärten und Kindertagesstätten.
              </p>
            </article>
          </li>
        </ul>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoTeam;

export { Head } from "../../components/layout-bljzo";
