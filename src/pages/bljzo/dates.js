import React from "react";
import LayoutBljzo from "../../components/layout-bljzo";
import BdzDatesWithFilter from "../../components/dates/bdzdates-with-filter";

const bljzoDates = () => {
  return (
    <LayoutBljzo pageName="is--dates">
      <div className="width-content">
        <header className="text-components ">
          <h1 className="page-title big">BLJZO-Termine</h1>
          <p>
            Das BLJZO trifft sich dreimal im Jahr zu mehrtägigen Probenphasen in
            den Schulferien. Dort werden unter der Leitung von Tom Hofmann
            Originalwerke und Bearbeitungen aller Epochen für Zupforchester
            einstudiert. Die erarbeiteten Programme werden am Ende der
            Probenphasen in Konzerten dargeboten. Sehen Sie hier die Aktivitäten
            des Orchesters und finden Sie alle wichtigen Informationen für das
            nächste Konzert in Ihrer Nähe.
          </p>
        </header>
        <BdzDatesWithFilter filter="BLJZO"></BdzDatesWithFilter>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoDates;

export { Head } from "../../components/layout-bljzo";
