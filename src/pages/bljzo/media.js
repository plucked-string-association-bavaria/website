import React from "react";
import LayoutBljzo from "../../components/layout-bljzo";
import { Link } from "gatsby";
import Read from "../../images/global/svg/bookopen.inline.svg";
import Disc from "../../images/global/svg/disc.inline.svg";
import Video from "../../images/global/svg/video.inline.svg";
import RepList from "../../images/global/svg/list.inline.svg";
import Premiere from "../../images/global/svg/premiere.inline.svg";

const bljzoMedia = () => {
  return (
    <LayoutBljzo pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">Repertoire und Medien</h1>
          <p>
            Erhalten Sie hier einen Einblick in die musikalische Arbeit des
            BLJZOs.
          </p>
        </header>
        <article className="is--teaser">
          <ul className="list-none width-content-wide">
            <li>
              <span>
                <Link to="/bljzo/media/cds">
                  <figure>
                    <Disc />
                  </figure>
                  <h4>CDs</h4>
                  <p>
                    Hier stellen wir unsere aktuell erhältliche CD vor. Möchtest
                    du diese Musik auch bei dir zu Hause hören?
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li>
              <span>
                <Link to="/bljzo/media/videos">
                  <figure>
                    <Video />
                  </figure>
                  <h4>Videos</h4>
                  <p>
                    Hier kannst du auf Mitschnitte von Konzerten des BLJZOs
                    zugreifen. Hör einfach mal rein, vielleicht hast du ja Lust,
                    in Zukunft mitzuspielen?
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li>
              <span>
                <Link to="/bljzo/media/repertoire">
                  <figure>
                    <RepList />
                  </figure>
                  <h4>Repertoireliste</h4>
                  <p>
                    Hier präsentieren wir alle bisher von uns als Orchester
                    erarbeiteten Werke. Gibt dir das Anregungen für neue
                    Projekte?
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li>
              <span>
                <Link to="/bljzo/media/premieres">
                  <figure>
                    <Premiere />
                  </figure>
                  <h4>Uraufführungen</h4>
                  <p>
                    Das Orchester regt regelmäßig die Komposition von neuen
                    Werken durch zeitgenössische Komponist:innen an. So haben
                    wir einen gewissen Einfluss auf das zeitgenössische
                    Musikgeschehen.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
          </ul>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoMedia;

export { Head } from "../../components/layout-bljzo";
