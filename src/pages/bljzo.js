import React from "react";
import LayoutBljzo from "../components/layout-bljzo";
import bljzoEyecatcher from "../images/bljzo/eyecatcher-bljzo.webp";
import bljzoConcertPlovdiv from "../images/bljzo/bljzo-concert-plovdiv.jpg";
import Music from "../images/global/svg/music.inline.svg";

const bljzoMain = () => {
  return (
    <LayoutBljzo pageName="is--orchester">
      <div className="text-components">
        <header className="heading">
          <p className="prae">Das Bayerische</p>
          <h1 className="big">Landesjugend&shy;zupforchester</h1>
        </header>

        <article>
          <div className="intro">
            <p>
              <span className="svg-initiale float-left mr-3 -mt-2">
                <Music />
              </span>
              Das Bayerisches Landesjugendzupforchester (BLJZO) setzt sich aus
              jungen bayerischen Musizierenden im Alter von 12 bis 24 Jahren
              zusammen. Die Besetzung des Orchesters besteht aus erster und
              zweiter Mandoline, Mandola, Gitarre und Bassgitarre/Kontrabass.
              Pro Jahr finden dreimal fünf- bis sechstägige Probenphasen in den
              Schulferien statt, die mit ein oder zwei öffentlichen Konzerten
              abgeschlossen werden. In diesen wird ein ansprechendes Programm
              einstudiert, das durch seine Mischung aus klassischer und moderner
              Zupfmusik mit Originalwerken und Bearbeitungen für Zupforchester
              aus allen Epochen besticht.
            </p>
          </div>

          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bljzoEyecatcher}
              alt="Bayerisches Landesjugendzupforchester Orchesterfoto 2021"
            />
            <figcaption>
              Bayerisches Landesjugendzupforchester 2021 in Ebermannstadt
            </figcaption>
          </figure>

          <p>
            In vielen Städten Bayerns sind Zupforchester beheimatet. Das BLJZO
            wurde 1983 von Elke Tober-Vogt gegründet, um den jungen Gitarren-
            und Mandolinenspieler:innen dieser Ensembles die Möglichkeit zu
            geben, sich über ihr Heimatorchester hinaus musikalisch betätigen zu
            können. Parallel dazu werden Jugendliche, die noch über keine
            Orchestererfahrung verfügen, über das BLJZO an das
            Orchestermusizieren herangeführt. Die Früchte dieser Arbeit mit
            engagierten Jugendlichen in einem überregionalen Orchester sind
            regelmäßige Konzerte auf hohem Niveau im In- und Ausland, bei denen
            es stets gelingt, nach nur wenigen Tagen des Probens ein
            musikalisches Zusammenwachsen hörbar zu machen. Auf diese Weise
            leistet das Orchester einen Beitrag zur Steigerung der Bekanntheit
            und Popularität der klassischen Zupfmusik.
          </p>

          <figure className="thumbnail width-content-wide no--yRythm">
            <img
              src={bljzoConcertPlovdiv}
              alt="Bayerisches Landesjugendzupforchester 2017 in Plovdiv, Bulgarien"
            />
            <figcaption>
              Bayerisches Landesjugendzupforchester 2017 in Plovdiv, Bulgarien
            </figcaption>
          </figure>

          <p>
            Die Art und Vielfalt der Probenarbeit variiert je nach
            Teilnehmerzahl einer Probenphase: Die Dozierenden bieten eine
            morgendliche Technikstunde an, bei der Grundlagen des Mandolinen-
            und Gitarrenspiels wiederholt und vertieft werden. In Stimmproben
            übt jede der Stimmen als Gruppe mit einem Dozierenden ihren
            Orchesterpart. In Quartettproben sind die Orchesterstimmen
            solistisch besetzt und das so gebildete Ensemble übt unter Anleitung
            eines Dozierenden die Orchesterstücke - oftmals spielen diese
            Ensembles am Abend den anderen Teilnehmer:innen vor. Mit
            Videoaufnahmen der abendlichen Vorspiele werden Spielbewegungen,
            Bühnenverhalten, Mimik und Körpersprache gemeinsam besprochen.
            Mitschnitte der Orchesterproben zur anschließenden gemeinsamen
            Analyse des Gehörten sind ebenfalls Teil des Unterrichtsangebots.
          </p>
          <p>
            Ziel der Probenarbeit ist, alle Spieler:innen zum selbstständigen
            Umgang mit Musik zu animieren und im Zusammenspiel das Denken in
            musikalischen Kategorien zu entwickeln. Das Dozententeam mit Tom
            Hofmann, Elisabeth Januschko, Elias Pfetscher, Florian
            Brettschneider und Malte Weyland sieht den Schwerpunkt der
            Orchesterprobenarbeit darin, Musik zu erklären, vorzuleben und jeden
            Ton mit Inhalt zu füllen. Das Verstehen und das respektvolle
            aufeinander Hören soll der Kern der Musik des BLJZO sein.
          </p>
          <p>
            Dem Dozententeam des Orchesters ist es weiterhin wichtig, den
            jugendlichen Spieler:innen Freude an der Musik zu vermitteln. Dieser
            Ansatz wird deutlich, wenn man Konzerte des Bayerischen
            Landesjugendzupforchesters hört und die Teilnehmer:innen während der
            Probenphasen (vor allem in den Pausen!) beobachtet. Hier merkt man,
            dass sie nicht nur gerne ihr Instrument spielen, sondern dass sie
            auch besonderen Spaß haben, die Zeit in konzentrierter, vor allem
            aber in entspannter und gelassener Atmosphäre miteinander zu
            verbringen.
          </p>
        </article>
      </div>
    </LayoutBljzo>
  );
};
export default bljzoMain;

export { Head } from "../components/layout-bljzo";
