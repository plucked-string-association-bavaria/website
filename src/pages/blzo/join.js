import React from "react";
import LayoutBlzo from "../../components/layout-blzo";
import { Link } from "gatsby";
import { Buffer } from "buffer";

const blzoJoin = () => {
  return (
    <LayoutBlzo pageName="is--join">
      <div className="text-components">
        <header className="page-title">
          <h1 className="big">Mitmachen und Kontakt</h1>
        </header>
        <article className="participate">
          <h2>Neue Gesichter sind im Orchester herzlich willkommen. </h2>
          <p>
            Es gibt kein besonderes Auswahlvorspiel, die ersten beiden
            Arbeitstagungen gelten als "Probezeit". Die Informationen zur
            kommenden Arbeitstagung finden sich in der Rubrik{" "}
            <Link to="/blzo/dates">Termine</Link>. Bei Interesse bitte einfach
            bei Michaela Schroll melden. Bei ihr können auch die CDs des BLZO
            bestellt werden.
          </p>
          <ul>
            <li>
              Organisatorische Leitung:
              <br />
              <strong>
                Michaela Schroll (
                {Buffer.from("aW5mbyBhdCBibHpvLmRl", "base64").toString()})
              </strong>
            </li>
            <li>
              Musikalische Leitung:
              <br />
              <strong>Oliver Kälberer</strong>
            </li>
          </ul>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoJoin;

export { Head } from "../../components/layout-blzo";
