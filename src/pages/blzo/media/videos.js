import React from "react";
import LayoutBlzo from "../../../components/layout-blzo";
import YoutubeVideo from "../../../components/video/youtube-video";
import VideoConsentWrapper from "../../../components/video/video-consent-wrapper";

const blzoVideos = () => {
  return (
    <LayoutBlzo pageName="is--media">
      <div className="text-components">
        <header className="heading">
          <h1 className="big">Videos</h1>
        </header>
        <article>
          <VideoConsentWrapper>
            <ul className="list-none width-content-wide">
              <YoutubeVideo
                slug="UzdgynDffy8"
                title="Oliver Kälberer: La Notte del Principe"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="qS3y14MPuXY"
                title="Daniel Huschert: Concerto Nr. 3, Mandolinenkonzert; Solist: Christian Laier"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="Uk_rfTnDBJI"
                title="Oliver Kälberer: Zong 1"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="ONmXEtDDBAs"
                title="Oswald von Wolkenstein/Silvan Wagner: Wolkensteiner Lieder"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="XaCNZ9HwN-k"
                title="W. A. Mozart: Sinfonie Nr. 9 C-Dur KV 73"
              ></YoutubeVideo>
              <YoutubeVideo
                slug="1Oh-sWL1YQs"
                title="J. S. Bach: Konzert e-Moll BWV 1062"
              ></YoutubeVideo>
            </ul>
          </VideoConsentWrapper>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoVideos;

export { Head } from "../../../components/layout-blzo";
