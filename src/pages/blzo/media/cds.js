import React from "react";
import LayoutBlzo from "../../../components/layout-blzo";
import cdBaumann from "../../../images/blzo/cd_baumann.jpg";
import cdMarktklaenge from "../../../images/blzo/cd_marktklaenge.jpg";
import cdVivat from "../../../images/blzo/cd_vivat.jpg";
import { Link } from "gatsby";

const blzoCds = () => {
  return (
    <LayoutBlzo pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">CDs</h1>
          <h4>
            Es gibt 3 CDs des BLZO, die per <Link to="/blzo/join">E-Mail</Link>{" "}
            bestellt werden können.
          </h4>
          <ul>
            <li>
              <h6>
                Jede CD kostet 10,– € zuzüglich der anfallenden Versandkosten.
              </h6>
            </li>
          </ul>
        </header>

        <ul className="media list-none">
          <li className="media--item">
            <figure>
              <img src={cdMarktklaenge} alt="Marktklänge" />
            </figure>
            <article className="is--half-yRhytm">
              <h2>Marktklänge</h2>
              <p>
                <strong>
                  Das Bayerische Landeszupforchester unter der Leitung von
                  Dominik Hackner spielt:
                </strong>
              </p>
              <ul>
                <li>
                  Dominik Hackner (*1968): Sketch-Book, Tanzsuite für ZO op. 57
                </li>
                <li>Rami Al-Regeb (*1978): Sug Al-Safafir in Fall</li>
                <li>Marcel Wengler (*1946): Konstellationen</li>
                <li>Valdo Preema (*1969): Epigonics 2</li>
                <li>
                  Raimo Kangro (1949 - 2001): Modus für Klavier und ZO,
                  Solistin: Sabine Spath
                </li>
                <li>
                  Eduardo Angulo (*1954): Il sogno del pesciolino, Konzert für
                  Gitarre und ZO, Solist: Michael Tröster
                </li>
                <li>Valdo Preema (*1969): Sincerely</li>
              </ul>
            </article>
          </li>
          <li className="media--item">
            <figure>
              <img src={cdVivat} alt="VIVAT – Konzertante Folklore" />
            </figure>
            <article className="is--half-yRhytm">
              <h2>VIVAT – Konzertante Folklore</h2>
              <p>
                <strong>
                  Das Bayerische Landeszupforchester unter der Leitung von
                  Gerhard Vogt spielt:
                </strong>
              </p>
              <ul className="mb-0">
                <li>Eduardo Angulo (*1954): Suite Mexicana op. 16</li>
                <li>Yasuo Kuwahara (*1946): The Song of Japanese Autumn</li>
                <li>Markus Kugler (*1971): Tres Piezas de Sudamerica</li>
                <li>Roland Leistner-Mayer (*1945): Danze di Boemia</li>
                <li>
                  Elke Tober-Vogt (*1957): Klezmer-Suite für Klarinette,
                  Akkordeon und ZO
                </li>
              </ul>
            </article>
          </li>
          <li className="media--item">
            <figure>
              <img
                src={cdBaumann}
                alt="Herbert Baumann – Vögel, Früchte und Wind"
              />
            </figure>
            <article className="is--half-yRhytm">
              <h2>Herbert Baumann – Vögel, Früchte und Wind</h2>
              <p>
                <strong>
                  Das Bayerische Landeszupforchester unter der Leitung von
                  Gerhard Vogt spielt Werke von Herbert Baumann (1925 – 2020):
                </strong>
              </p>
              <ul>
                <li>Saarländische Zupfmusik</li>
                <li>
                  Tafelmusik für Blockflöte und ZO, Solistin: Andrea Breier
                </li>
                <li>
                  Concerto Capriccioso für Solo-Mandoline und ZO, Solistin:
                  Gertrud Tröster
                </li>
                <li>
                  Es war einmal eine Müllerin – Sonatine für zwei Gitarren,
                  Solisten: Michael Tröster, Oliver Strömsdörfer
                </li>
                <li>Vögel, Früchte und Wind für Gitarre und ZO</li>
                <li>Sequenzen</li>
              </ul>
            </article>
          </li>
        </ul>
      </div>
    </LayoutBlzo>
  );
};
export default blzoCds;

export { Head } from "../../../components/layout-blzo";
