import React from "react";
import LayoutBlzo from "../../../components/layout-blzo";

const blzoRepertoire = () => {
  return (
    <LayoutBlzo pageName="is--media">
      <div className="text-components">
        <header className="heading">
          <h1 className="big">Repertoireliste</h1>
        </header>
        <article>
          <h4>
            Das Repertoire des BLZOs unter der Leitung von Oliver Kälberer (seit
            2010):
          </h4>
          <div className="is--repList col--2md width-content-wide">
            <ul className="list-none">
              <li>
                <span>
                  <strong>Eduardo Angulo:</strong>
                  <em>Il sogno del pesciolino</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:
                    <small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Präludium E-Dur BWV 870 und Fuge A-Dur BWV 886</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:
                    <small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Konzert e-moll (nach) BWV 1062</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:
                    <small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Präludium und Fuge h-moll BWV 885</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:
                    <small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Präludium 22 Des-Dur BWV 867</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Rossen Balkanski:</strong>
                  <em>Rapsodia Vissani</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Béla Bartók:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Mikrokosmos</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Ludwig van Beethoven:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Adagio WoO 43b</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ugo Bottacchiari:</strong>
                  <em>Preludio sinfonico</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johannes Brahms:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Zwei Intermezzi: Op. 119/1 h-moll und Op. 117/1 E-Dur</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johannes Brahms:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Ballade op. 10 Nr. 2</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Raffaele Calace:</strong>
                  <em>Tarantella op. 18</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Carlo Gesualdo:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Tenebrae factae sunt</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Oliver Kälberer:</strong>
                  <em>Kali &ndash; Closing the Circle</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Oliver Kälberer:</strong>
                  <em>Zong 1</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Victor Kioulaphides:</strong>
                  <em>Broadway '79</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Takashi Kubota:</strong>
                  <em>Tanzsuite Nr. 2 op. 21</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Takashi Kubota:</strong>
                  <em>Tanzsuite Nr. 3 op. 25</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Raidoh</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Wolfgang Amadeus Mozart:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Sinfonie Nr. 6 F-Dur KV 43</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Wolfgang Amadeus Mozart:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Sinfonie Nr. 9 C-Dur KV 73</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Wolfgang Amadeus Mozart:<small>Arr. Oliver Kälberer</small>
                  </strong>
                  <em>Sinfonie Nr. 23 D-Dur KV 181</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Katsumi Nagaoka:</strong>
                  <em>Yume</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Maurice Ravel:<small>Arr. Takashi Kubota</small>
                  </strong>
                  <em>Pavane pour une infnate défunte</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Salvador Ruiz de Luna:</strong>
                  <em>Zapateado del Si</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Carl Stamitz:</strong>
                  <em>Quartetto concertante G-Dur op. 14/2</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Jürgen Thiergärtner:</strong>
                  <em>Crazy Dance und Latin Dance</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Joaquin Turina:</strong>
                  <em>Orgia (aus "Danzas fantásticas" op. 22)</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Silvan Wagner:</strong>
                  <em>Wolkensteiner Mären (Lieder)</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Klaus Wüsthoff:</strong>
                  <em>Concierto de Samba</em>
                </span>
              </li>
            </ul>
          </div>
          <h4>
            Das Repertoire des BLZOs unter der Leitung von Dominik Hackner
            (2001-2010):
          </h4>
          <div className="is--repList col--2md width-content-wide">
            <ul className="list-none">
              <li>
                <span>
                  <strong>Rami Al-Regeb:</strong>
                  <em>Sug Al-Safafir in Fall - für ZO und Percussion</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Eduardo Angulo:</strong>
                  <em>Il sogno del pesciolino - Konzert für Gitarre und ZO</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Sebastian Bach:<small>Arr. Olaf v. Gonnissen</small>
                  </strong>
                  <em>
                    Konzert e-moll (BWV 1062) für 2 Mandolinen, 2 Gitarren und
                    ZO
                  </em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Ugo Bottachiari:</strong>
                  <em>Preludio sinfonico</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Johann Friedrich Fasch:<small>Arr. Detlef Tewes</small>
                  </strong>
                  <em>Konzert in D-Dur für Mandoline und ZO</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Dietrich Erdmann:</strong>
                  <em>Serenata für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Giovanni Battista Gervasio:<small>Arr. Lino Totaro</small>
                  </strong>
                  <em>Sinfonia in D-Dur</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Christoph Willibald Gluck:
                    <small>Arr. Marga Wilden-Hüsgen</small>
                  </strong>
                  <em>Ballettmusik Don Juan</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Gustav Gunsenheimer:</strong>
                  <em>Konzert Nr. 1 für Klavier und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Dominik Hackner:</strong>
                  <em>Sketch Book - Tanzsuite für Zupforchester op. 57</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Anthon Hansen:</strong>
                  <em>Concertino für Mandoline und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Oliver Kälberer:</strong>
                  <em>Vishnu - Times of Struggle</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Raimo Kangro:</strong>
                  <em>Modus für Klavier und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Raidoh - Konzert für Solomandoline und ZO</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Song of Japanese Autumn</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Yasuo Kuwahara:</strong>
                  <em>Novemberfest für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Roland Leistner-Mayer:</strong>
                  <em>Danze de Boemia</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Claudio Mandonico:</strong>
                  <em>Preludio e Fuga für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Sébastien Paci:</strong>
                  <em>Suite Campesina</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Valdo Preema:</strong>
                  <em>Sincerely</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Valdo Preema:</strong>
                  <em>Epigonics 2</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Henry Purcell:</strong>
                  <em>Symphony while the swans come foreward</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Henry Purcell:</strong>
                  <em>Songs and dances from "The Fairy Queen"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Henry Purcell:</strong>
                  <em>"Rondo" and "Musik for a while" from "King Arthur"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Valentin Roeser:</strong>
                  <em>Sonata III "à grand orchestre"</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>
                    Carl Stamitz:<small>Arr. Marga Wilden-Hüsgen</small>
                  </strong>
                  <em>Orchesterquartett F-Dur op. 4 Nr. 4</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonius Streichhardt:</strong>
                  <em>Divertimento für Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonius Streichhardt:</strong>
                  <em>Tre pezzi concertante für Mandola und ZO</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Stefano Squarzina:</strong>
                  <em>Piccolo Mondo Antico</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Elke Tober-Vogt:</strong>
                  <em>Process of change</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Elke Tober-Vogt:</strong>
                  <em>Ein gut Dantzerey</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Elke Tober-Vogt:</strong>
                  <em>Ceilidh</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Joaquín Turina:</strong>
                  <em>Orgia</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Antonio Vivaldi:</strong>
                  <em>Konzert G-Dur für zwei Mandolinen und Zupforchester</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Marcel Wengler:</strong>
                  <em>Konstellationen</em>
                </span>
              </li>
              <li>
                <span>
                  <strong>Marcel Wengler:</strong>
                  <em>
                    Zwei japanische Lieder für Gesang, Flöte, Zupf- und
                    Schlaginstrumente
                  </em>
                </span>
              </li>
            </ul>
          </div>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoRepertoire;

export { Head } from "../../../components/layout-blzo";
