import React from "react";
import LayoutBlzo from "../../components/layout-blzo";
import dirigent from "../../images/blzo/dirigent.jpg";
import mand1 from "../../images/blzo/mand1.jpg";
import mand2 from "../../images/blzo/mand2.jpg";
import malte from "../../images/bljzo/malte.png";
import guitar from "../../images/blzo/guitar.jpg";
import Users from "../../images/global/svg/users.inline.svg";

const blzoTeam = () => {
  return (
    <LayoutBlzo pageName="is--team">
      <div className="text-components width-page">
        <header className="page-title">
          <h1 className="big lg:text-center width-page">
            Dirigent und Dozententeam
          </h1>
        </header>

        <ul className="employees flex flex-col list-none">
          <li>
            <header>
              <h2>
                <strong>Oliver Kälberer</strong>
                <span className="is--task">
                  <Users />
                  <small>Dirigent</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={dirigent} alt="Porträt Oliver Kälberer" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Oliver Kälberer</strong> wurde 1964
                in Ulm geboren. Er studierte Gitarre in Würzburg bei Hans Koch
                und Komposition in München bei Prof. Dieter Acker.
              </p>
              <p>
                Ergänzende Studien bei György Kurtág, Peter Kiesewetter, György
                Ligeti und Sergiu Celibidache. Er spielt außerdem Mandoline und
                Kontrabass. Neben einer Reihe von Stücken für Schüler komponiert
                er vor allem Kammermusik und Orchesterwerke für Zupfinstrumente.
              </p>
              <p>
                Er unterrichtet Gitarre und Mandoline an der Kreismusikschule
                Fürstenfeldbruck und an der Musikschule Sauerlach. Er leitet das
                Ensemble Roggenstein sowie mehrere überregionale Zupforchester
                und ist regelmäßig Dozent für Gitarre, Mandoline, Musiktheorie,
                Dirigieren und Ensemblespiel bei Seminaren des Bundes Deutscher
                Zupfmusiker. Studienaufenthalte in Asien.
              </p>
              <p>
                <em>
                  <a href="https://www.ok-music.de">Homepage: ok-music.de</a>
                </em>
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Ariane Lorch</strong>
                <span className="is--task">
                  <Users />
                  <small>1. Mandoline</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={mand1} alt="Porträt Ariane Lorch" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Ariane Lorch</strong> studierte
                Mandoline, Gitarre, Chor- und Orchesterleitung in Kassel. Sie
                gewann zahlreiche internationale Preise als Mandolinistin und
                Dirigentin. Derzeit unterrichtet sie Mandoline, Gitarre und
                Ukulele in Nordhessen. Sie leitet das JZO Hessen und das ZO Con
                Favore.
              </p>
              <p>
                2017-2020 besuchte sie den berufsbegleitenden Masterstudiengang
                „Mit Kindern singen“ bei Prof. Thomas Holland-Moritz und Prof.
                Andreas Mohr in Osnabrück. Sie schloss den Master of Arts mit
                einer Arbeit über „Elementares Arbeiten mit Kinderorchester und
                Kinderchor“ mit Auszeichnung ab.
              </p>
              <p>
                Gemeinsam mit Ihrem Mann, dem Gitarristen Wolfgang Lorch, ist
                sie Herausgeberin einer mehrbändigen Mandolinenschule, einer
                Kindergitarrenschule und umfangreicher didaktischer
                Spielmaterialien für KinderZO und JugendZO.
              </p>
              <p>
                <em>
                  <a href="https://duo-lorch.com/">Homepage: duo-lorch.com</a>
                </em>
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Elena Kisseljow</strong>
                <span className="is--task">
                  <Users />
                  <small>2. Mandoline</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={mand2} alt="Porträt Elena Kisseljow" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Elena Kisseljow</strong> studierte
                zunächst Domra und Dirigat in Kiew, anschließend Mandoline und
                Barockmandoline bei Gertrud Weyhofen und bei Prof. Marga
                Wilden-Hüsgen an der Musikhochschule Wuppertal.
              </p>
              <p>
                Sie ist Preisträgerin renommierter internationaler Wettbewerbe.
                Zu Ihrer Konzerttätigkeit gehören CD- und Rundfunkaufnahmen,
                Auftritte mit dem Duo "Opus 1", dem Ensemble für Neue Musik
                "Klangforum Wien", mit dem Ensemble alte Musik "L ́Esprit des
                Cordes", im Quartett "Wolga-Virtuosen" mit russischen
                Musikern/Solisten und in zahlreichen Opernhäusern/Orchestern.
                Sie leitet die Mandolinenklasse an der MS Uster/Greifensee in
                der Schweiz und unterrichtet als Dozentin für Mandoline in
                Deutschland.
              </p>
              <p>
                <em>
                  <a href="https://www.mandola.eu/">Homepage: mandola.eu</a>
                </em>
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Malte Weyland</strong>
                <span className="is--task">
                  <Users />
                  <small>Mandola</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={malte} alt="Porträt Malte Weyland" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Malte Weyland</strong> Studierte
                Mandoline an der Hochschule für Musik und Tanz Köln und beendete
                sein Studium 2018 mit dem Master of Music. Neben dem
                künstlerischen Studium beendete er im selben Jahr auch das
                Studium der Elementaren Musikpädagogik. Während seines Studiums
                unterrichtete er an verschiedenen Musikschulen Mandoline,
                Musikalische Früherziehung und Ensembleklassen, und dirigierte
                das Mandolinen- und Gitarrenorchester Dortmund. Als Solist und
                Dozent unternimmt er Konzertreisen in Deutschland, Italien,
                England und der Schweiz und tritt in verschiedensten
                Kammermusik-Formationen auf.
              </p>
              <p>
                Seit 2018 unterrichtet Malte Weyland an der Sing- und
                Musikschule Regensburg das Instrument Mandoline, Elementares
                Musizieren und engagiert sich bei der musikalischen Grundbildung
                in Kindergärten und Kindertagesstätten.
              </p>
            </article>
          </li>
          <li>
            <header>
              <h2>
                <strong>Karoline Laier</strong>
                <span className="is--task">
                  <Users />
                  <small>Gitarre</small>
                </span>
              </h2>
            </header>
            <article className="is--half-yRhytm">
              <figure className="is--image">
                <span>
                  <img src={guitar} alt="Porträt Karoline Laier" />
                </span>
              </figure>
              <p>
                <strong className="text-lg">Karoline Laier</strong> Studierte
                bei Prof. Jürgen Rost an der HfM in Weimar und an der UdK Berlin
                bei Thomas Müller-Pering. 2013 schloss sie ihr Studium mit dem
                Konzertexamen ab. Sie ist Preisträgerin mehrerer nationaler und
                internationaler Wettbewerbe. 2010 erhielt sie ein
                Konzertstipendium beim Deutschen Musikwettbewerb in Bonn. Sie
                konzertiert solistisch und kammermusikalisch (v.a. im Duo
                Consensus mit Christian Laier, Mandoline oder mit Simon Etzold,
                Schlaginstrumente) und wirkte bei verschiedenen
                Rundfunkaufzeichnungen mit (NDR, MDR, Deutschlandfunk,
                Deutschland Radio Kultur). Solistische und Kammermusikalische
                Konzerte führten sie durch Europa, die USA, Japan und Brasilien.
                Karoline Laier unterrichtet an der Musikschule der Stadt Erfurt,
                sowie auf Lehrgängen und Kursen und wirkt als Herausgeberin
                einer Verlagsreihe beim Trekel-Musikverlag mit.
              </p>
              <p>
                <em>
                  <a href="https://www.duoconsensus.de">
                    Homepage: duoconsensus.de
                  </a>
                </em>
              </p>
            </article>
          </li>
        </ul>
      </div>
    </LayoutBlzo>
  );
};
export default blzoTeam;

export { Head } from "../../components/layout-blzo";
