import React from "react";
import LayoutBlzo from "../../components/layout-blzo";
import BdzDatesWithFilter from "../../components/dates/bdzdates-with-filter";

const blzoDates = () => {
  return (
    <LayoutBlzo pageName="is--dates">
      <div className="width-content">
        <header className="text-components ">
          <h1 className="page-title big">BLZO-Termine</h1>
          <p>
            Das BLZO trifft sich drei- bis viermal im Jahr zu mehrtägigen
            Probenphasen und erarbeitet unter der Leitung von Oliver Kälberer
            Originalwerke und Bearbeitungen aller Epochen für Zupforchester. Die
            erarbeiteten Programme werden in Konzerten dargeboten. Sehen Sie
            hier die Aktivitäten des Orchesters und finden Sie alle wichtigen
            Informationen für das nächste Konzert in Ihrer Nähe.
          </p>
        </header>
        <BdzDatesWithFilter filter="BLZO"></BdzDatesWithFilter>
      </div>
    </LayoutBlzo>
  );
};
export default blzoDates;

export { Head } from "../../components/layout-blzo";
