import LayoutBlzo from "../../components/layout-blzo";
import React from "react";
import History from "../../images/global/svg/history.inline.svg";
import { Link } from "gatsby";
import history2 from "../../images/blzo/blzo_schottland.jpg";
import history3 from "../../images/blzo/blzo_italien.jpg";
import Dominik from "../../images/blzo/Dominik.jpg";
import Skelettorchester from "../../images/blzo/Skelettorchester.jpg";

const blzoHistory = () => {
  const zupfbote2019lmf02 = "/assets/uploads/zupfbote_2019_lmf_02.jpg";

  return (
    <LayoutBlzo pageName="is--history">
      <div className="text-components width-content history">
        <header className="history-title">
          <figure className="historyIcon">
            <History />
          </figure>
          <h1 className="big">Geschichte</h1>
        </header>
        <article>
          <p className="historyIntro">
            Das Bayerische Landeszupforchester (BLZO) ging im Jahr 1981 aus dem
            Bayerischen Landes-Jugend-Zupf-Orchester hervor und ist eine
            Fördermaßnahme des „Bundes Deutscher Zupfmusiker, Landesverband
            Bayern e.V.“. Die Leitung des Orchesters hatte zunächst Friedrich
            Ulrich inne, darauf folgten Perioden unter der Leitung von Gerhard
            Vogt, Elke Tober-Vogt und Dominik Hackner. Aktuell steht Oliver
            Kälberer seit 2010 am Dirigentenpult.
          </p>
          <div className="divider" />

          <div className="timeSection">
            <h3>seit 2010</h3>
            <p className="timeIntro">
              Oliver Kälberer hat die Gabe, während der Proben eine Atmosphäre
              zu schaffen, in der sich Musik von selbst entfalten kann. Zuerst
              vermittelt er den Spieler:innen Einblicke in die Grundlagen und
              Zusammenhänge der Harmonik und schärft damit ihre Sinne. So
              entsteht eine solide Basis, auf der jede:r Einzelne das Wissen und
              dadurch den Mut erlangt, Zutrauen zum eigenständigen Musizieren zu
              haben. In Verbindung zu den anderen Orchestermitgliedern entsteht
              dadurch ein achtsames und vertrauensvolles Miteinander, in dem das
              Orchester gemeinsam schwingt, und mit Olivers Hilfe Wege zur
              Interpretation eines Stückes sucht und findet. Die Technik bleibt
              immer der Gestaltung der Musik untergeordnet. (
              <Link to="/blzo/media/repertoire">> zum Repertoire</Link>)
            </p>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von</small> Oliver
              Kälberer
            </h4>
            <ul>
              <li>
                <strong> Mai 2024:</strong> Auftritt beim Festival der
                Landesorchester in Wirges
              </li>
              <li>
                <strong>Februar 2024:</strong> Malte Weyland (Mandola) und
                Carlos Vivas (Gitarre) treten in das Dozententeam des BLZOs ein.
              </li>
              <li>
                <strong>Oktober 2024:</strong> Karoline Laier übernimmt die
                Stimmführung Gitarre von Carlos Vivas.
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3> Oktober 2022 – April 2023</h3>
            <p className="timeIntro">
              <p className="timeIntro">
                <Link to="https://duo-lorch.com/vita/">Ariane Lorch</Link>,
                Stimmführerin im BLZO, übernimmt den Dirigentenstab für drei
                Arbeitstagungen in Vertretung für Oliver Kälberer. Unter ihrer
                Führung werden drei Konzerte gespielt im Kurhaus Bad Abbach, im
                Kammermusiksaal der Hochschule für Musik in Würzburg und in der
                Wandelhalle im Kurpark in Bad Mergentheim. Solist an der
                Mandoline in{" "}
                <Link to="https://danielhuschert.de/de/">Huscherts</Link>{" "}
                Concerto Nr. 3:{" "}
                <Link to="https://www.duoconsensus.de/vita/">
                  Christian Laier
                </Link>
              </p>
            </p>
          </div>

          <div className="timeSection">
            <h3>2010 – 2022</h3>

            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von</small> Oliver
              Kälberer
            </h4>
            <ul>
              <li>
                <strong> November 2019:</strong> Auftritt beim Landesmusikfest
                „100 Jahre BDZ“ in der Bayerischen Musikakademie in
                Alteglofsheim{" "}
                <Link to="/blog/2020-05-11-doppelter-anlass-zum-feiern/">
                  > zum Bericht
                </Link>{" "}
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img
                    src={zupfbote2019lmf02}
                    alt="Das BLZO mit Solist Clemer Andreotti im Konzert beim Landesmusikfest 2019"
                  />
                  <figcaption>
                    Das BLZO mit Solist Clemer Andreotti im Konzert beim
                    Landesmusikfest 2019
                  </figcaption>
                </figure>
              </li>
              <li>
                <strong>September 2019:</strong> Konzert im Rahmen der 130. AT
                in der St. Matthäus Kirche in Erlangen mit Solist{" "}
                <a href="https://www.hfm-wuerzburg.de/lehre/andreotti-clemer">
                  Clemer Andreotti
                </a>{" "}
                bei Eduardo Angulo&nbsp;&ndash; „Il sogno del pesciolino“
              </li>
              <li>
                <strong>Juni 2019:</strong> Kleine Konzertreise auf Einladung
                der Konzertmeisterin Ariane Lorch nach Witzenhausen im
                nordhessischen Werra-Meißner-Kreis; Konzert mit{" "}
                <a href="http://www.mandolinenverein-wickenrode.de/Favore/Favore.html">
                  Con Favore
                </a>{" "}
                in Großalmerode, gemeinsames Stück: „Fluch der Karibik“
              </li>
              <li>
                <strong>Mai 2018:</strong> Auftritt beim Eurofestival Zupfmusik
                in Bruchsal mit{" "}
                <Link to="/blzo/media/videos">
                  „Zong 1“ &ndash; Oliver Kälberer
                </Link>{" "}
                und{" "}
                <Link to="/blzo/media/videos">
                  „Wolkensteiner Lieder“ &ndash; Silvan Wagner
                </Link>{" "}
              </li>
              <li>
                <strong>April 2018:</strong> Konzert „Saitenklänge“ in der
                Bayerischen Musikakademie Hammelburg im Rahmen der 125.
                Arbeitstagung
              </li>
              <li>
                <strong>Juni/Juli 2017: </strong>Gegenbesuch des schottischen
                Orchesters{" "}
                <a href="http://www.mandolinscotland.org/dacap.html">
                  „Da Capo Alba“
                </a>{" "}
                mit Konzert der beiden Orchestern in der Bayerischen
                Musikakademie Hammelburg; Gemeinsame Stücke: „Viva La
                Vida“&nbsp;&ndash; Greenday, „Tarantella“&nbsp;&ndash; Calace
                und „Music“&nbsp;&ndash; John Miles
              </li>
              <li>
                <strong>November 2015: </strong>Arbeitstagung in der Bayerischen
                Musikakademie Hammelburg mit Gastdirigent{" "}
                <a href="https://www.dieter-kreidler.de/">Dieter Kreidler</a>;
                Konzert zusammen mit dem{" "}
                <Link to="/bdz/seminars/uhu">uHu-Orchester</Link>{" "}
              </li>
              <li>
                <strong>Mai/Juni 2015:</strong> „
                <a href="http://www.mandolinscotland.org/dacap.html">
                  Da Capo Alba
                </a>{" "}
                and BLZO“&nbsp;&ndash; Konzertreise nach Schottland; Gemeinsame
                Stücke: „Viva La Vida“&nbsp;&ndash; Greenday,
                „Tarantella“&nbsp;&ndash; Calace und „Music“&nbsp;&ndash; John
                Miles{" "}
                <Link to="/blog/2022-01-01-konzertreise-des-blzos-nach-schottland-zu-dacapo-alba/">
                  > zum Bericht
                </Link>{" "}
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img
                    src={history2}
                    alt="Konzertreise nach Schottland: Die Kelpies bei Falkirk"
                  />
                  <figcaption>
                    Konzertreise nach Schottland: Die Kelpies bei Falkirk
                  </figcaption>
                </figure>
              </li>
              <li>
                <strong>März 2015:</strong> Konzertmeisterin{" "}
                <Link to="/blzo/team">Ariane Lorch</Link> folgt auf{" "}
                <a href="https://www.annefarahani.com/about">Anne Wolf</a>{" "}
              </li>
              <li>
                <strong>Mai/Juni 2014:</strong> Eurofestival in Bruchsal mit
                Kompositionen aus den eigenen Reihen: „Kali“&nbsp;&ndash; Oliver
                Kälberer, „Latin Dance & Crazy Dance“&nbsp;&ndash; Jürgen
                Thiergärtner
              </li>
              <li>
                <strong>Oktober 2013:</strong> Eröffnungskonzert beim
                Landesmusikfest in Alteglofsheim „30&nbsp;Jahre{" "}
                <Link to="/bljzo/">BLJZO</Link>“
              </li>
              <li>
                <strong>Juni 2012:</strong> Konzertreise nach Italien: Konzerte
                in Cervo, Savona, Gardone Valtrompia{" "}
                <Link to="/blog/2022-01-01-konzertreise-des-blzos-nach-italien/">
                  > zum Bericht
                </Link>{" "}
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img src={history3} alt="Konzertreise nach Italien" />
                  <figcaption>Konzertreise nach Italien</figcaption>
                </figure>
              </li>
              <li>
                <strong>April 2011:</strong> Konzertmeisterin Anne Wolf (bis
                Oktober 2014) löst{" "}
                <a href="http://www.gertrud-weyhofen.de/web/index.html">
                  Gertrud Weyhofen
                </a>{" "}
                ab
              </li>
              <li>
                <strong>Oktober 2010:</strong> Dirigent{" "}
                <Link to="/blzo/team">Oliver Kälberer</Link> übernimmt die
                künstlerische Leitung und löst Dominik Hackner ab
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>2001 – 2010</h3>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von</small>
              Dominik Hackner
            </h4>
            <ul>
              <li>
                <strong>Juni 2010:</strong> Auftritt beim{" "}
                <Link to="/blog/2022-05-16-die-100-arbeitstagung-das-blzo-beim-eurofestival-in-bruchsal/">
                  Eurofestival Zupfmusik
                </Link>{" "}
                in Bruchsal
              </li>
              <li>
                <strong>November 2009:</strong> Konzertreise nach{" "}
                <Link to="/blog/2022-05-16-konzertreise-des-blzos-nach-bottrop/">
                  Bottrop
                </Link>{" "}
              </li>
              <li>
                <strong>April 2009:</strong> Konzert unter Mitwirkung von{" "}
                <a href="http://www.olafvangonnissen.de">Olaf v. Gonnissen</a>{" "}
                (Schwerpunkt alte Musik)
              </li>
              <li>
                <strong>November 2006:</strong> Landesmusikfest Bayern{" "}
                <Link to="/blog/2022-05-16-13-orchester-feiern-beim-landesmusikfest-in-bayern-das-25-jährige-jubiläum-des-bayerischen-landeszupforchesters/">
                  „25 Jahre BLZO“
                </Link>{" "}
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img
                    src={Dominik}
                    alt="Das BLZO mit Dirigent Dominik Hackner beim Landesmusikfest 2006"
                  />
                  <figcaption>
                    Das BLZO mit Dirigent Dominik Hackner beim Landesmusikfest
                    2006
                  </figcaption>
                </figure>
              </li>
              <li>
                <strong>Mai 2006:</strong> Auftritt beim Eurofestival Zupfmusik
                in Bamberg
              </li>
              <li>
                <strong>Oktober 2005:</strong> Mitwirkung beim Jubiläumskonzert
                "25 Jahre Bayerische Musikakademie Hammelburg" und Aufnahme der
                CD <Link to="/blzo/media/cds">"Marktklänge"</Link>{" "}
              </li>
              <li>
                <strong>Mai 2005:</strong>{" "}
                <Link to="/blog/2023-01-02-gemeinschaftskonzert-von-blzo-und-bljzo/">
                  Gemeinschaftskonzert
                </Link>{" "}
                mit BLJZO und BLZO
              </li>
              <li>
                <strong>Juni 2004:</strong>{" "}
                <Link to="/blog/2023-01-02-konzertreise-des-blzos-nach-luxemburg-und-in-die-niederlande/">
                  Konzertreise
                </Link>{" "}
                nach Luxemburg/Niederlande
              </li>
              <li>
                <strong>September 2003:</strong> Auftritt beim{" "}
                <Link to="/blog/2023-01-02-das-blzo-beim-landesmusikfest-niedersachsen/">
                  Landesmusikfest Niedersachsen
                </Link>{" "}
              </li>
              <li>
                <strong>April 2003:</strong> Auftritt des "Skelettorchesters"
                ;-)
                <figure className="thumbnail width-content-wide no--yRythm">
                  <img src={Skelettorchester} alt="Das BLZO Skelettorchester" />
                  <figcaption>Das BLZO Skelettorchester</figcaption>
                </figure>
              </li>
              <li>
                <strong>Mai/Juni 2002:</strong> Auftritt beim{" "}
                <Link to="/blog/2023-01-02-das-blzo-beim-bundesmusikfest-eurofestival-zupfmusik-2002/">
                  Eurofestival Zupfmusik in Friedrichshafen
                </Link>{" "}
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>1998 – 2001</h3>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von </small>
              Elke Tober-Vogt
            </h4>
            <ul>
              <li>
                <strong>2000:</strong> Auftritt beim „Fest der Bayern“, einer
                offiziellen Millenniumsfeierlichkeit in Regensburg
              </li>
              <li>
                <strong>2000:</strong> Konzertreise nach England
              </li>
              <li>
                <strong>1999:</strong> Auftritt beim Landesmusikfest
              </li>
              <li>
                <strong>1998:</strong> Auftritt beim Europäischen Musikfestival
                in Friedrichshafen
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>1987 – 1997</h3>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von </small>
              Gerhard Vogt
            </h4>
            <ul>
              <li>
                <strong>1996:</strong> Konzertreise nach Japan/Thailand
              </li>
              <li>
                <strong>1996:</strong> CD-Aufnahme
              </li>
              <li>
                <strong>1995:</strong>Konzertreise nach USA/Kanada
              </li>
              <li>
                <strong> 1994:</strong> Rundfunkaufnahmen
              </li>
              <li>
                <strong> 1992:</strong> CD-Aufnahme
              </li>
              <li>
                <strong> 1991:</strong> Konzertreise in die UdSSR
              </li>
              <li>
                <strong> 1990:</strong> Konzertreise in die DDR
              </li>
              <li>
                <strong> 1990:</strong> Auftritt beim Bundesmusikfest in
                Wuppertal
              </li>
              <li>
                <strong> 1990:</strong> Rundfunkaufnahmen
              </li>
              <li>
                <strong> 1988:</strong> Rundfunkaufnahmen
              </li>
            </ul>
          </div>

          <div className="timeSection">
            <h3>1981 – 1987</h3>
            <h4 className="timeDirector">
              <small>Herausragende Projekte unter der Leitung von </small>
              Friedrich Ulrich
            </h4>
            <ul>
              <li>
                <strong>1986:</strong> Auftritt beim Bundesmusikfest in
                Schweinfurt
              </li>
              <li>
                <strong>1986:</strong> Teilnahme beim Laienorchesterwettbewerb
              </li>
              <li>
                <strong>1983:</strong> Kassettenproduktion
              </li>
              <li>
                <strong>1982:</strong> Auftritt beim Bundesmusikfest
              </li>
            </ul>
          </div>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoHistory;

export { Head } from "../../components/layout-blzo";
