import React from "react";
import LayoutBlzo from "../../components/layout-blzo";
import { Link } from "gatsby";
import Read from "../../images/global/svg/bookopen.inline.svg";
import Disc from "../../images/global/svg/disc.inline.svg";
import Video from "../../images/global/svg/video.inline.svg";
import RepList from "../../images/global/svg/list.inline.svg";

const blzoMedia = () => {
  return (
    <LayoutBlzo pageName="is--media">
      <div className="text-components width-content-wide">
        <header className="page-title">
          <h1 className="big">Repertoire und Medien</h1>
          <p>
            Erhalten Sie hier einen Einblick in die musikalische Arbeit des
            BLZOs.
          </p>
        </header>
        <article className="is--teaser">
          <ul className="list-none width-content-wide">
            <li>
              <span>
                <Link to="/blzo/media/cds">
                  <figure>
                    <Disc />
                  </figure>
                  <h4>CDs</h4>
                  <p>
                    Die musikalische Arbeit des BLZO ist in mehreren Tonträgern
                    dokumentiert.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li>
              <span>
                <Link to="/blzo/media/videos">
                  <figure>
                    <Video />
                  </figure>
                  <h4>Videos</h4>
                  <p>
                    Im Videobereich werden repräsentative Livemitschnitte
                    festgehalten.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
            <li>
              <span>
                <Link to="/blzo/media/repertoire">
                  <figure>
                    <RepList />
                  </figure>
                  <h4>Repertoireliste</h4>
                  <p>
                    Hier präsentieren wir alle bisher von uns als Orchester
                    erarbeiteten Werke.
                  </p>
                  <p className="btn">
                    <span>
                      <Read />
                    </span>
                    <strong>weiter lesen</strong>
                  </p>
                </Link>
              </span>
            </li>
          </ul>
        </article>
      </div>
    </LayoutBlzo>
  );
};
export default blzoMedia;

export { Head } from "../../components/layout-blzo";
