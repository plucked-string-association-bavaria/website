import React from "react";
import Footer from "./footer";
import webLogoSM from "../images/style/webLogoSM-bljzo.png";
import webLogoXL from "../images/style/webLogoXL-bljzo.png";
import favicon from "../images/global/bljzo_favicon.png";
import ScrollToTopButton from "./scroll-to-top-button";
import {
  MenuItemWithChildren,
  MenuItemWithGatsbyLink,
} from "./navigation/menu-items";
import DropdownItem from "./navigation/dropdown-item";
import Menu from "./navigation/menu";
import PropTypes from "prop-types";

export default function LayoutBljzo(props) {
  const contentClasses = "content " + props.pageName;
  return (
    <>
      <div id="page" className="content is--bljzo">
        <BljzoHeader />
        <section className={contentClasses}>{props.children}</section>
        <Footer currentSite="bljzo">
          <NavigationPointsBljzo isFooter={true} />
        </Footer>
      </div>
      <ScrollToTopButton backgroundColor="bg-bljzo-1st-def" />
    </>
  );
}

function BljzoHeader() {
  return (
    <header id="top" className="header">
      <Menu
        title="Bayerisches Landesjugendzupforchester"
        linkTarget="/bljzo"
        logosm={webLogoSM}
        logoxl={webLogoXL}
        borderColor="border-bljzo-border"
      >
        <NavigationPointsBljzo isFooter={false} />
      </Menu>
    </header>
  );
}

function NavigationPointsBljzo(props) {
  let focusColor = "!text-bljzo-1st-def";
  let layoutSpecificClasses = "text-bljzo-nav-def hover:text-bljzo-1st-700";
  return (
    <>
      <MenuItemWithGatsbyLink
        link="/bljzo"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Das Orchester"
      />

      <MenuItemWithGatsbyLink
        link="/bljzo/history"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Geschichte"
      />

      <MenuItemWithGatsbyLink
        link="/bljzo/dates"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Termine"
      />

      <MenuItemWithChildren
        link="/bljzo/media"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Repertoire und Medien"
      >
        <DropdownItem
          link="/bljzo/media/cds"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="CDs"
        />
        <DropdownItem
          link="/bljzo/media/videos"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Videos"
        />
        <DropdownItem
          link="/bljzo/media/repertoire"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Repertoireliste"
        />
        <DropdownItem
          link="/bljzo/media/premieres"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Uraufführungen"
        />
      </MenuItemWithChildren>

      <MenuItemWithGatsbyLink
        link="/bljzo/team"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Team"
      />

      <MenuItemWithGatsbyLink
        link="/bljzo/join"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Mitmachen und Kontakt"
      />
    </>
  );
}

LayoutBljzo.propTypes = {
  pageName: PropTypes.string.isRequired,
  children: PropTypes.node,
};

NavigationPointsBljzo.propTypes = {
  isFooter: PropTypes.bool.isRequired,
};

export function Head() {
  return (
    <>
      <title>Bayerisches Landesjugendzupforchester</title>
      <link rel="icon" type="image/png" href={favicon} />
    </>
  );
}
