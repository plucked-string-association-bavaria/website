import React from "react";
import Footer from "./footer";
import Menu from "./navigation/menu";
import webLogoSM from "../images/style/webLogoSM-blzo.png";
import webLogoXL from "../images/style/webLogoXL-blzo.png";
import favicon from "../images/global/blzo_favicon.png";
import ScrollToTopButton from "./scroll-to-top-button";
import {
  MenuItemWithChildren,
  MenuItemWithGatsbyLink,
} from "./navigation/menu-items";
import DropdownItem from "./navigation/dropdown-item";
import PropTypes from "prop-types";

export default function LayoutBlzo(props) {
  const contentClasses = "content " + props.pageName;
  return (
    <>
      <div id="page" className="is--blzo">
        <BlzoHeader />
        <section className={contentClasses}>{props.children}</section>
        <Footer currentSite="blzo">
          <NavigationPointsBlzo isFooter={true} />
        </Footer>
        <ScrollToTopButton backgroundColor="bg-blzo-1st-def" />
      </div>
    </>
  );
}

function BlzoHeader() {
  return (
    <header id="top" className="header">
      <Menu
        title="Bayerisches Landeszupforchester"
        linkTarget="/blzo"
        logosm={webLogoSM}
        logoxl={webLogoXL}
        borderColor="border-blzo-border"
      >
        <NavigationPointsBlzo isFooter={false} />
      </Menu>
    </header>
  );
}

function NavigationPointsBlzo(props) {
  let focusColor = "!text-blzo-1st-def";
  let layoutSpecificClasses = "text-blzo-nav-def hover:text-blzo-1st-700";
  return (
    <>
      <MenuItemWithGatsbyLink
        link="/blzo"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Das Orchester"
      />

      <MenuItemWithGatsbyLink
        link="/blzo/history"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Geschichte"
      />

      <MenuItemWithGatsbyLink
        link="/blzo/dates"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Termine"
      />

      <MenuItemWithChildren
        link="/blzo/media"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Repertoire und Medien"
      >
        <DropdownItem
          link="/blzo/media/cds"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="CDs"
        />
        <DropdownItem
          link="/blzo/media/videos"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Videos"
        />
        <DropdownItem
          link="/blzo/media/repertoire"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Repertoireliste"
        />
      </MenuItemWithChildren>

      <MenuItemWithGatsbyLink
        link="/blzo/team"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Team"
      />

      <MenuItemWithGatsbyLink
        link="/blzo/join"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Mitmachen und Kontakt"
      />
    </>
  );
}

LayoutBlzo.propTypes = {
  pageName: PropTypes.string.isRequired,
  children: PropTypes.node,
};

NavigationPointsBlzo.propTypes = {
  isFooter: PropTypes.bool.isRequired,
};

export function Head() {
  return (
    <>
      <title>Bayerisches Landeszupforchester</title>
      <link rel="icon" type="image/png" href={favicon} />
    </>
  );
}
