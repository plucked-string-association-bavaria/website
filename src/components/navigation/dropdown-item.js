import React from "react";
import { Link } from "gatsby";
import PropTypes from "prop-types";

export default function DropdownItem(props) {
  if (props.isFooter) {
    return (
      <li>
        <Link to={props.link}>{props.name}</Link>
      </li>
    );
  }
  return (
    <li className="w-full">
      <Link
        to={props.link}
        activeClassName={props.focusColor}
        className="block px-3 py-1
                hover:text-[var(--color-1st-700)]
                mainnav:py-2"
      >
        {props.name}
      </Link>
    </li>
  );
}

DropdownItem.propTypes = {
  link: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  focusColor: PropTypes.string.isRequired,
  isFooter: PropTypes.bool.isRequired,
};
