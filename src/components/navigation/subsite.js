import React from "react";
import { Link } from "gatsby";
import PropTypes from "prop-types";

export default function SubSiteMenu() {
  return (
    <ul className="flex justify-between px-6 mt-2 mainnav:justify-start mainnav:mt-0 ">
      <SubSite linkTarget="/bdz" highlightClass="text-1st-def" name="BDZ" />
      <SubSite
        linkTarget="/blzo"
        highlightClass="text-blzo-1st-def"
        name="Bayerisches Landes&shy;zupforchester"
      />
      <SubSite
        linkTarget="/bljzo"
        highlightClass="text-bljzo-1st-def"
        name="Bayerisches Landesjugend&shy;zupforchester"
      />
    </ul>
  );
}

function SubSite(props) {
  return (
    <Link
      to={props.linkTarget}
      activeClassName={`${props.highlightClass} font-bold`}
      partiallyActive={true}
      className="block px-3 py-2 md:py-1"
    >
      {props.name}
    </Link>
  );
}

SubSite.propTypes = {
  linkTarget: PropTypes.string.isRequired,
  highlightClass: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};
