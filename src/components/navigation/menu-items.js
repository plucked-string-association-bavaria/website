import React from "react";
import { Link } from "gatsby";
import PropTypes from "prop-types";

let topLevelListStyles =
  "w-1/2 mainnav:w-auto mb-2 mainnav:mb-0 mainnav:mr-2 mainnav:max-w-fit mainnav:min-w-0";
let topLevelLinkStyles = "relative block px-3 py-2 font-bold md:py-1";

function MenuItem(props) {
  return <li className={topLevelListStyles}>{props.children}</li>;
}

MenuItem.propTypes = {
  children: PropTypes.node,
};

export function MenuItemWithGatsbyLink(props) {
  if (props.isFooter) {
    return (
      <li>
        <Link to={props.link}>{props.name}</Link>
      </li>
    );
  }
  return (
    <MenuItem>
      <Link
        to={props.link}
        activeClassName={props.focusColor}
        className={`${topLevelLinkStyles} ${props.layoutSpecificClasses}`}
      >
        {props.name}
      </Link>
    </MenuItem>
  );
}

MenuItemWithGatsbyLink.propTypes = {
  link: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  focusColor: PropTypes.string.isRequired,
  layoutSpecificClasses: PropTypes.string.isRequired,
  isFooter: PropTypes.bool.isRequired,
};

export function MenuItemWithWebLink(props) {
  if (props.isFooter) {
    return (
      <li>
        <a href={props.link}>{props.name}</a>
      </li>
    );
  }
  return (
    <MenuItem>
      <a
        href={props.link}
        className={`${topLevelLinkStyles} ${props.layoutSpecificClasses}`}
      >
        {props.name}
      </a>
    </MenuItem>
  );
}

MenuItemWithWebLink.propTypes = {
  link: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  layoutSpecificClasses: PropTypes.string.isRequired,
  isFooter: PropTypes.bool.isRequired,
};

export function MenuItemWithChildren(props) {
  if (props.isFooter) {
    return (
      <li>
        <Link to={props.link}>{props.name}</Link>
        <ul>{props.children}</ul>
      </li>
    );
  }
  return (
    <li
      className={`
            ${topLevelListStyles}
            mainnav:relative
            group
            `}
    >
      <Link
        to={props.link}
        activeClassName={props.focusColor}
        partiallyActive={true}
        className={`
                    ${topLevelLinkStyles}
                    ${props.layoutSpecificClasses}
                    mainnav:pr-4
                    mainnav:after:absolute
                    mainnav:after:right-0
                    mainnav:after:top-[40%]
                    mainnav:after:opacity-50
                    mainnav:after:border-[5px]
                    mainnav:after:border-solid
                    mainnav:after:border-transparent
                    mainnav:after:border-t-[currentColor]
                `}
      >
        {props.name}
      </Link>

      <ul
        className="
                mainnav:absolute
                mainnav:left-1/2
                mainnav:translate-x-[-50%]
                mainnav:bg-white
                mainnav:invisible
                mainnav:z-10

                mainnav:group-hover:visible
                mainnav:group-hover:shadow-lg
                mainnav:group-hover:w-60
            "
      >
        {props.children}
      </ul>
    </li>
  );
}

MenuItemWithChildren.propTypes = {
  link: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  focusColor: PropTypes.string.isRequired,
  layoutSpecificClasses: PropTypes.string.isRequired,
  children: PropTypes.node,
  isFooter: PropTypes.bool.isRequired,
};
