import React from "react";
import { Link } from "gatsby";
import Hamburger from "hamburger-react";
import PropTypes from "prop-types";
import SubSiteMenu from "./subsite";

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.selectRef = React.createRef();
  }

  state = {
    hamburgerToggled: false,
  };

  setToggled = () => {
    this.setState({ hamburgerToggled: !this.state.hamburgerToggled });
    this.render();
  };

  render() {
    let stylingForToggledMenu = this.state.hamburgerToggled
      ? "!flex flex-col animate-fadein"
      : "";

    return (
      <nav className="flex flex-wrap px-[var(--rythm-x)] items-center mx-auto max-w-[theme('width.header')]">
        <div className="grow mainnav:grow-0 mainnav:mr-6">
          <h3 className="sr-only">
            <Link to={this.props.linkTarget}>{this.props.title}</Link>
          </h3>
          <Link
            to={this.props.linkTarget}
            className="inline-block h-14 mainnav:h-[66px]"
          >
            <img
              className="h-full mainnav:hidden"
              src={this.props.logosm}
              alt={this.props.title}
            />
            <img
              className="h-full hidden mainnav:block"
              src={this.props.logoxl}
              alt={this.props.title}
            />
          </Link>
        </div>
        <div className="mainnav:hidden">
          <Hamburger
            toggled={this.state.hamburgerToggled}
            toggle={this.setToggled}
            size={25}
          />
        </div>

        <div
          className={`hidden ${stylingForToggledMenu} grow -mx-[var(--rythm-x)] mainnav:flex mainnav:flex-col mainnav:m-0`}
        >
          <SubSiteMenu />

          <HorizontalLine borderColor={this.props.borderColor} />

          <ul className="flex flex-wrap px-6 items-start -mb-1 mainnav:mb-0">
            {this.props.children}
          </ul>

          <HorizontalLine borderColor={this.props.borderColor} />
        </div>
      </nav>
    );
  }
}

function HorizontalLine(props) {
  return (
    <div
      className={`${props.borderColor} my-2 border-b border-solid md:hidden`}
    />
  );
}

Menu.propTypes = {
  linkTarget: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
  logosm: PropTypes.string.isRequired,
  logoxl: PropTypes.string.isRequired,
};
