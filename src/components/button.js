import React from "react";
import PropTypes from "prop-types";

export default function Button(props) {
  return (
    <button
      className="px-2.5 py-1 
          border border-solid border-[var(--color-1st-def)] rounded 
          bg-[var(--color-1st-100)] hover:bg-white 
          hover:scale-105 hover:shadow-[0_0_0_3px_var(--color-1st-100)] transition-all"
      onClick={props.onClick ? props.onClick : () => void 0}
    >
      {props.children}
    </button>
  );
}

Button.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.node,
};
