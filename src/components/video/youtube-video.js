import React from "react";
import PropTypes from "prop-types";

export default function YoutubeVideo(props) {
  const src = "https://www.youtube-nocookie.com/embed/" + props.slug;

  return (
    <li className="mb-12 flex flex-col bg-black p-2 pb-4">
      <h5
        className="order-last text-center italic mt-4"
        style={{
          color: "var(--color-white)",
        }}
      >
        {props.title}
      </h5>

      {/* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625)  */}
      <div className="relative w-full pb-[56.25%]">
        <iframe
          className="absolute top-0 left-0 w-full h-full"
          src={src}
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
    </li>
  );
}

YoutubeVideo.propTypes = {
  slug: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};
