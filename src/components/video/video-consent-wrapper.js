import React, { useState } from "react";
import Button from "../button";

export default function VideoConsentWrapper(props) {
  const consentButtonName = "Cookies zulassen und Videos anzeigen";

  const [isConsentGiven, setIsConsentGiven] = useState(false);

  const getConsentWarning = () => {
    return (
      <>
        <p>
          <strong>Bitte beachten Sie:</strong>
          <br />
          Mit Klick auf den &quot;{consentButtonName}&quot;-Button erteilen Sie
          Ihre Einwilligung darin, dass Youtube auf dem von Ihnen verwendeten
          Endgerät Cookies setzt, die auch einer Analyse des Nutzungsverhaltens
          zu Marktforschungs- und Marketing-Zwecken dienen können. Näheres zur
          Cookie-Verwendung durch Youtube finden Sie in der Cookie-Policy von
          Google unter{" "}
          <a href="https://policies.google.com/technologies/types?hl=de">
            https://policies.google.com/technologies/types?hl=de
          </a>{" "}
          .
        </p>
        <p>
          <Button onClick={onGiveConsent}>{consentButtonName}</Button>
        </p>
      </>
    );
  };

  const onGiveConsent = () => {
    setIsConsentGiven(true);
  };

  return <>{isConsentGiven ? props.children : getConsentWarning()}</>;
}
