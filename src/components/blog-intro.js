import React from "react";
import { Buffer } from "buffer";

export default function BlogIntro() {
  return (
    <article>
      <p>
        Herzlich willkommen beim „Zupfboten-online“! Der BDZ Landesverband
        Bayern bietet mit dieser Website ein Portal, um musikalisch
        Interessantes aus ganz Bayern und darüber hinaus zu veröffentlichen,
        sich als Mitglied oder Musikbegeisterter über aktuelles Geschehen zu
        informieren und auch Highlights der eigenen Orchester und
        Mitspieler:innen zu präsentieren. Inhaltlich werden vor allem
        Vorankündigungen, Programme und Berichte über Veranstaltungen des
        Verbandes, sowie der Newsletter des Bayerischen Musikrates zu lesen
        sein. Aber auch die einzelnen Mitgliedsorchester können hier die
        Möglichkeit nutzen, um Konzerte, Konzertreisen, Jubiläen,
        Wettbewerbserfolge oder Ehrungen kundzutun. Damit steht der Wunsch und
        der Anspruch im Vordergrund, auch für die Öffentlichkeit ein
        interessantes Medium in Sachen Zupfmusik in Bayern zu sein. Diese Seite
        ist also ein vielseitiges Informationsmedium und lebt von jedem
        einzelnen Beitrag, der per Post oder Email (
        {Buffer.from("bmV3c0BiZHotYmF5ZXJuLmRl", "base64").toString()}) bei uns
        eingeht.
      </p>
      <p>Vielen Dank für Euer Mitgestalten und viel Spaß beim Lesen!</p>
      <p>Karin Heilgenthal</p>
    </article>
  );
}
