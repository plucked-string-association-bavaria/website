import React from "react";
import BdzDates from "./bdzdates";

export default function CourseDate(props) {
  return (
    <>
      <div className="width-content">
        <header className="text-components">
          <h2>Aktuelle Termine</h2>
        </header>
        <ul className="dates__wrapper list-none peer empty:hidden">
          <BdzDates subpageFilter={props.subpageFilter} />
        </ul>
        <div
          className="px-3 py-1.5 w-fit mx-[var(--rythm-x)] my-[var(--rythm-y)] rounded bg-white hidden peer-empty:block
        border border-solid border-[var(--color-1st-def)]"
        >
          Momentan sind keine Termine in Planung.
        </div>
      </div>
    </>
  );
}
