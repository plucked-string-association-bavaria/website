import React from "react";
import BdzDate from "./bdzdate";
import { useStaticQuery, graphql } from "gatsby";
import { isCurrentDate, hasValidTag } from "./date-filterer";

export default function BdzDates(props) {
  const data = useStaticQuery(
    graphql`
      {
        allMarkdownRemark(
          sort: { frontmatter: { beginning: ASC } }
          filter: { frontmatter: { layout: { eq: "dates" } } }
        ) {
          edges {
            node {
              frontmatter {
                title
                location
                beginning
                end
                hideTime
                showEnd
                showContinueReading
                link
                tags
              }
              excerpt
              fields {
                slug
              }
              html
            }
          }
        }
      }
    `
  );
  const edges = data.allMarkdownRemark.edges;
  const subpage = props.subpageFilter
    ? props.subpageFilter.toLowerCase().replace(" ", "-")
    : null;

  return edges
    .filter(({ node }) =>
      hasValidTag(node.frontmatter.tags, props.subpageFilter)
    )
    .filter(({ node }) =>
      hasValidTag(node.frontmatter.tags, props.currentUserFilter)
    )
    .filter(({ node }) => isCurrentDate(node.frontmatter.beginning))
    .map(({ node }) => (
      <BdzDateListElement
        key={node.fields.slug}
        node={node}
        subpage={subpage}
      />
    ));
}

function BdzDateListElement(props) {
  return (
    <li className="date-item">
      <BdzDate
        node={props.node}
        showExcerpt={true}
        subpage={props.subpage}
      ></BdzDate>
    </li>
  );
}
