function isCurrentDate(beginning) {
  const now = new Date(Date.now());
  const oneMonthAgo = new Date(now.setMonth(now.getMonth() - 1));
  if (new Date(beginning) > oneMonthAgo) {
    return true;
  } else {
    return false;
  }
}

function hasValidTag(tags, tagToMatch) {
  if (!tagToMatch) {
    return true;
  }

  return tags && tags.includes(tagToMatch);
}

module.exports = { isCurrentDate, hasValidTag };
