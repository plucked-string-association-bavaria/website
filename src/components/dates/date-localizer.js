/**
 * Takes an input date and transforms it to a localized date for Germany in a numeric format.
 *
 * Example: 2021-11-30T15:24:11.140Z becomes 30.11.21, 15:24 for hideTime = true.
 *
 * @param input a date string in ISO 8601 format
 * @param hideTime true if minutes and seconds should not be shown
 */
function localizeDateShortVersion(input, hideTime) {
  const dateConfig = {
    year: "2-digit",
    month: "2-digit",
    day: "2-digit",
  };
  if (!hideTime) {
    dateConfig.hour = "2-digit";
    dateConfig.minute = "2-digit";
  }
  return new Date(input).toLocaleDateString("de", dateConfig);
}

/**
 * Takes an input date and transforms it to a localized date for Germany in a alphanumeric format.
 *
 * Example: 2021-11-30T15:24:11.140Z becomes Dienstag, 30 November 2021
 * @param input a date string in ISO 8601 format
 */
function localizeDateLongVersion(input) {
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  return new Date(input).toLocaleDateString("de-DE", dateOptions);
}

module.exports = { localizeDateShortVersion, localizeDateLongVersion };
