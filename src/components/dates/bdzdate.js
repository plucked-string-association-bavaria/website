import React from "react";
import { Link } from "gatsby";
import { localizeDateShortVersion } from "./date-localizer";
import GoTo from "../../images/global/svg/link.inline.svg";
import Where from "../../images/global/svg/home.inline.svg";
import Read from "../../images/global/svg/bookopen.inline.svg";

export default function BdzDate(props) {
  const frontmatter = props.node.frontmatter;
  const articleClassNames =
    determineStatusClasses(frontmatter.tags) + props.showExcerpt
      ? ""
      : "is--date--details";

  const slugToDetails = props.subpage
    ? "/" + props.subpage + props.node.fields.slug
    : props.node.fields.slug;

  const tags = [];
  if (frontmatter.tags) {
    for (const tag of frontmatter.tags) {
      if (tag.toLowerCase() !== props.subpage) {
        tags.push(
          <li
            className="mb-1 px-2 rounded-sm [&:not(:last-child)]:mr-1"
            key={tag}
            style={{
              backgroundColor: "var(--color-1st-700)",
              color: "var(--color-1st-100)",
            }}
          >
            {tag}
          </li>
        );
      }
    }
  }

  return (
    <article className={articleClassNames}>
      <figure>
        <span>
          <Where />
        </span>
      </figure>
      <main>
        <div className="is--meta-wrapper">
          <dl className="is--meta is--where">
            <dt>
              <small>Wo: </small>
            </dt>
            <dd>
              <strong>{frontmatter.location}</strong>
            </dd>
          </dl>
          {renderTime(
            frontmatter.beginning,
            frontmatter.end,
            frontmatter.hideTime,
            frontmatter.showEnd
          )}
          {renderLink(frontmatter.link)}
          <ul
            className="tags mt-2 pt-3 flex items-center flex-wrap text-small empty:hidden"
            style={{
              paddingLeft: "0",
              listStyle: "none",
              borderTop: "1px dotted var(--color-1st-300)",
            }}
          >
            {tags}
          </ul>
        </div>
      </main>
      <section>
        <header>
          <h4>{frontmatter.title}</h4>
        </header>
        <footer>
          {props.showExcerpt && frontmatter.showContinueReading ? (
            <Link to={slugToDetails}>
              <span>{props.node.excerpt}</span>

              <p className="btn">
                <strong>weiter lesen</strong>
                <span>
                  <Read />
                </span>
              </p>
            </Link>
          ) : (
            <span dangerouslySetInnerHTML={{ __html: props.node.html }} />
          )}
        </footer>
      </section>
    </article>
  );
}

function determineStatusClasses(tags) {
  if (!tags) {
    return "date-item";
  }
  return tags.reduce(
    (accumulator, currentTag) =>
      accumulator + " is--status--" + currentTag.toLowerCase(),
    "date-item"
  );
}

function renderTime(beginning, end, hideTime, showEnd) {
  const renderedBeginning = localizeDateShortVersion(beginning, hideTime);
  const renderedEndTime = localizeDateShortVersion(end, hideTime);

  return (
    <>
      <dl className="is--meta is--start">
        <dt>
          <small>Beginn:</small>
        </dt>
        <dd>
          <span></span>
          <strong>{renderedBeginning}</strong>
        </dd>
      </dl>
      {renderEnd(renderedEndTime, showEnd)}
    </>
  );
}

function renderEnd(renderedEndTime, showEnd) {
  if (!showEnd) {
    return;
  }

  return (
    <dl className="is--meta is--end">
      <dt>
        <small>Ende:</small>
      </dt>
      <dd>
        <span></span>
        <strong>{renderedEndTime}</strong>
      </dd>
    </dl>
  );
}

function renderLink(link) {
  if (!link) {
    return;
  }
  return (
    <dl className="is--meta mt-4">
      <dt>
        <small>Link: </small>
      </dt>
      <dd>
        <a className="btn" href={link}>
          <span>
            <GoTo />
          </span>
          <strong>anschauen</strong>
        </a>
      </dd>
    </dl>
  );
}
