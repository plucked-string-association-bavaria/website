import React from "react";

import BdzDates from "./bdzdates";
import Filter from "../../images/global/svg/filter.inline.svg";
import Trash from "../../images/global/svg/trash.inline.svg";

export default class BdzDatesWithFilter extends React.Component {
  constructor(props) {
    super(props);
    this.selectRef = React.createRef();
  }

  state = {
    selected: "",
  };

  handleInputChange = (event) => {
    this.setState({ selected: event.target.value });
    this.render();
  };

  resetFilter = () => {
    this.setState({ selected: "" });
    this.selectRef.current.value = "";
    this.render();
  };

  render() {
    const blzoOrchestraOption =
      this.props.filter !== "BLZO" ? <option value="BLZO">BLZO</option> : "";
    const bljzoOrchestraOption =
      this.props.filter !== "BLJZO" ? <option value="BLJZO">BLJZO</option> : "";
    const bayunaOrchestraOption =
      this.props.filter !== "Bayuna" ? (
        <option value="Bayuna">Bayuna</option>
      ) : (
        ""
      );
    const KinderkursOrchestraOption =
      this.props.filter !== "Kinderkurs" ? (
        <option value="Kinderkurs">Kinderkurs</option>
      ) : (
        ""
      );

    const OsterkursOrchestraOption =
      this.props.filter !== "Osterkurs" ? (
        <option value="Osterkurs">Osterkurs</option>
      ) : (
        ""
      );
    const PfingstkursOrchestraOption =
      this.props.filter !== "Pfingstkurs" ? (
        <option value="Pfingstkurs">Pfingstkurs</option>
      ) : (
        ""
      );
    const SWSOrchestraOption =
      this.props.filter !== "Schweinfurter Seminar" ? (
        <option value="Schweinfurter Seminar">Schweinfurter Seminar</option>
      ) : (
        ""
      );
    const uHuOrchestraOption =
      this.props.filter !== "uHu" ? <option value="uHu">uHu</option> : "";

    return (
      <>
        <aside className="filter">
          <span className="filter-icon">
            <Filter />
          </span>
          <span className="select">
            <select
              className="select"
              ref={this.selectRef}
              onChange={this.handleInputChange}
              defaultValue=""
            >
              <option value="" disabled>
                Filter
              </option>
              <option value="Mandoline – Instrument des Jahres 2023">
                Mandoline – Instrument des Jahres 2023
              </option>
              <option value="Öffentlich">Öffentlich</option>
              <option value="Kurs">Kurs</option>
              <option value="Orchester">Orchester</option>
              <option value="Veranstaltungen">Veranstaltungen</option>
              <option value="Abgesagt">Abgesagt</option>
              {blzoOrchestraOption}
              {bljzoOrchestraOption}
              {bayunaOrchestraOption}
              {KinderkursOrchestraOption}
              {OsterkursOrchestraOption}
              {PfingstkursOrchestraOption}
              {SWSOrchestraOption}
              {uHuOrchestraOption}
            </select>
            <span className="focus">&nbsp;</span>
          </span>

          <button onClick={this.resetFilter}>
            <span className="trash-icon">
              <Trash />
            </span>
            <strong>zurücksetzen</strong>
          </button>
        </aside>
        <ul className="dates__wrapper list-none">
          <BdzDates
            currentUserFilter={this.state.selected}
            subpageFilter={this.props.filter}
          />
        </ul>
      </>
    );
  }
}
