import React from "react";
import { Link, useStaticQuery, graphql } from "gatsby";
import { localizeDateLongVersion } from "./dates/date-localizer";

export default function FilteredBlog(props) {
  const data = useStaticQuery(graphql`
    {
      allMarkdownRemark(
        sort: { frontmatter: { date: DESC } }
        filter: { frontmatter: { layout: { eq: "blog" } } }
      ) {
        totalCount
        edges {
          node {
            id
            frontmatter {
              title
              date(formatString: "DD MMMM, YYYY")
            }
            excerpt
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const edges = data.allMarkdownRemark.edges;
  const filteredEdges = edges.filter(
    ({ node }) =>
      props.currentUserFilter === "" ||
      new Date(node.frontmatter.date).getFullYear() === props.currentUserFilter,
  );
  return (
    <>
      <h4>
        <mark>{filteredEdges.length}</mark> Einträge
      </h4>
      {filteredEdges.map(({ node }) => (
        <>
          <article key={node.id}>
            <Link className="no-underline" to={node.fields.slug}>
              <time>{localizeDateLongVersion(node.frontmatter.date)}</time>
              <h3 className="underline">{node.frontmatter.title}</h3>
              <p>{node.excerpt}</p>
            </Link>
          </article>
          <hr />
        </>
      ))}
    </>
  );
}
