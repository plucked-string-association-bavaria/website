import React from "react";
import { Link } from "gatsby";
import webLogoBDZsm from "../images/style/webLogoSM-bdz.png";
import webLogoBDZxl from "../images/style/webLogoXL-bdz.png";
import webLogoBLZOsm from "../images/style/webLogoSM-blzo.png";
import webLogoBLZOxl from "../images/style/webLogoXL-blzo.png";
import webLogoBLJZOsm from "../images/style/webLogoSM-bljzo.png";
import webLogoBLJZOxl from "../images/style/webLogoXL-bljzo.png";
import * as footerStyles from "./footer.module.css";

const ListLink = (props) => (
  <li>
    <Link className="px-2 py-1 text-sm" to={props.to}>
      {props.children}
    </Link>
  </li>
);

export default function Layout(props) {
  return (
    <footer
      style={{
        backgroundColor: "var(--color-white)",
        boxShadow: "0 0 12px 0 rgba(3, 20, 41, 0.08)",
      }}
    >
      <div
        className={`${footerStyles.unmigrated} width-page [&_a]:opacity-80 [&_a:hover]:opacity-100`}
      >
        <div className="flex flex-col gap-4 sm:flex-row sm:flex-wrap sm:gap-6 lg:flex-nowrap lg:gap-x-8">
          <div className="flex flex-col gap-2 sm:flex-row sm:gap-4 lg:w-1/2 lg:gap-6">
            {renderOwnLogo(props.currentSite)}
            {renderDescriptionText(props.currentSite)}
          </div>

          <div className="lg:w-1/4 sm:flex-1">
            <h5>
              <strong>Information:</strong>
            </h5>
            <ul>{props.children}</ul>
          </div>

          <div className="lg:w-1/4 sm:flex-1">
            <h5>
              <strong>Links:</strong>
            </h5>
            {renderOtherLogos(props.currentSite)}
          </div>
        </div>

        <ul className="pt-8 flex flex-wrap justify-center">
          <ListLink to="/contact">Impressum</ListLink>
          <ListLink to="/data-privacy">Datenschutzerklärung</ListLink>
        </ul>
        <p className="text-center">
          <small className="text-gray-300">
            2025 &copy; by Bund Deutscher Zupfmusiker Landesverband Bayern e.V.
          </small>
        </p>
      </div>
    </footer>
  );
}

function renderDescriptionText(currentSite) {
  if (currentSite === "blzo") {
    return (
      <p className="sm:w-3/4">
        Das Bayerische Landeszupforchester (BLZO), aktuell unter der Leitung von
        Oliver Kälberer, ist ein Auswahlorchester des Bund Deutscher
        Zupfmusiker, Landesverband Bayern. Bei mehreren Konzerten im Jahr bietet
        es höchste Qualität in Zusammenspiel, Musikalität und Spielfreude.
      </p>
    );
  } else if (currentSite === "bljzo") {
    return (
      <p className="sm:w-3/4">
        Das Bayerische Landesjugendzupforchester (BLJZO), aktuell unter der
        Leitung von Tom Hofmann, ist ein Auswahlorchester des BDZ Landesverband
        Bayern für Jugendliche im Alter von 12 bis 24 Jahren. Das Orchester gibt
        Konzerte auf höchstem Niveau und ermutigt die Spieler:innen zu einem
        selbständigen Umgang mit Musik.
      </p>
    );
  } else {
    return (
      <p className="sm:w-3/4">
        Der Bund Deutscher Zupfmusiker Landesverband Bayern e.V. (BDZ LV Bayern
        e.V.) vereint Laien und professionelle Musiker:innen im Bereich der
        Zupfmusik. Er bietet Kurse für verschiedenste Zielgruppen,
        Nachwuchsförderung, Orchesterspiel in Auswahlorchestern und
        Unterstützung für Mitgliedsvereine.
      </p>
    );
  }
}

function renderOwnLogo(currentSite) {
  if (currentSite === "blzo") {
    return (
      <figure className="w-1/2 sm:w-1/4">
        <img src={webLogoBLZOsm} alt="" />
      </figure>
    );
  } else if (currentSite === "bljzo") {
    return (
      <figure className="w-1/2 sm:w-1/4">
        <img src={webLogoBLJZOsm} alt="" />
      </figure>
    );
  } else {
    return (
      <figure className="w-1/2 sm:w-1/4">
        <img src={webLogoBDZsm} alt="" />
      </figure>
    );
  }
}

/**
 * Show only the two logos which are representing the other sites.
 */
function renderOtherLogos(currentSite) {
  return (
    <div className="flex gap-4 sm:flex-col">
      {currentSite !== "bdz" ? (
        <Link to="/bdz">
          <figure>
            <img
              src={webLogoBDZxl}
              alt=""
              className="sm:w-3/4 opacity-75 saturate-50 hover:opacity-100 hover:saturate-100"
            />
          </figure>
        </Link>
      ) : null}

      {currentSite !== "blzo" ? (
        <Link to="/blzo">
          <figure>
            <img
              src={webLogoBLZOxl}
              alt=""
              className="sm:w-3/4 opacity-75 saturate-50 hover:opacity-100 hover:saturate-100"
            />
          </figure>{" "}
        </Link>
      ) : null}

      {currentSite !== "bljzo" ? (
        <Link to="/bljzo">
          <figure>
            <img
              src={webLogoBLJZOxl}
              alt=""
              className="sm:w-3/4 opacity-75 saturate-50 hover:opacity-100 hover:saturate-100"
            />
          </figure>
        </Link>
      ) : null}
    </div>
  );
}
