import React from "react";
import Footer from "./footer";
import webLogoSM from "../images/style/webLogoSM-bdz.png";
import webLogoXL from "../images/style/webLogoXL-bdz.png";
import favicon from "../images/global/bdz_favicon.png";
import ScrollToTopButton from "./scroll-to-top-button";
import DropdownItem from "./navigation/dropdown-item";
import {
  MenuItemWithChildren,
  MenuItemWithGatsbyLink,
} from "./navigation/menu-items";
import Menu from "./navigation/menu";
import PropTypes from "prop-types";

export default function LayoutBdz(props) {
  const contentClasses = "content " + props.pageName;
  return (
    <>
      <div id="page" className="is--bdz">
        <BdzHeader />
        <section className={contentClasses}>{props.children}</section>
        <Footer currentSite="bdz">
          <NavigationPointsBdz isFooter={true} />
        </Footer>
      </div>
      <ScrollToTopButton backgroundColor="bg-1st-def" />
    </>
  );
}

function BdzHeader() {
  return (
    <header id="top" className="header">
      <Menu
        title="BDZ Bayern"
        linkTarget="/bdz"
        logosm={webLogoSM}
        logoxl={webLogoXL}
        borderColor="border-border"
      >
        <NavigationPointsBdz isFooter={false} />
      </Menu>
    </header>
  );
}

function NavigationPointsBdz(props) {
  let focusColor = "!text-1st-def";
  let layoutSpecificClasses = "text-nav-def hover:text-1st-700";
  return (
    <>
      <MenuItemWithChildren
        link="/bdz/news"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Aktuelles"
      >
        <DropdownItem
          link="/bdz/news/blog"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Zupfbote"
        />
        <DropdownItem
          link="/bdz/news/dates"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Veranstaltungen"
        />
      </MenuItemWithChildren>

      <MenuItemWithGatsbyLink
        link="/bdz/advantages"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Vorteile"
      />

      <MenuItemWithChildren
        link="/bdz/seminars"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Kursangebot"
      >
        <DropdownItem
          link="/bdz/seminars/osterkurs"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Osterkurs"
        />
        <DropdownItem
          link="/bdz/seminars/pfingstkurs"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Pfingstkurs"
        />
        <DropdownItem
          link="/bdz/seminars/uhu"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="uHu-Orchester"
        />
        <DropdownItem
          link="/bdz/seminars/bayuna"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Bayuna"
        />
        <DropdownItem
          link="/bdz/seminars/schweinfurter-seminar"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Schweinfurter Seminar"
        />
        <DropdownItem
          link="/bdz/seminars/kinderkurs"
          focusColor={focusColor}
          isFooter={props.isFooter}
          name="Kinderkurse"
        />
      </MenuItemWithChildren>

      <MenuItemWithGatsbyLink
        link="/bdz/membership"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Mitgliedschaft"
      />

      <MenuItemWithGatsbyLink
        link="/bdz/people"
        focusColor={focusColor}
        layoutSpecificClasses={layoutSpecificClasses}
        isFooter={props.isFooter}
        name="Vorstand"
      />
    </>
  );
}

LayoutBdz.propTypes = {
  pageName: PropTypes.string.isRequired,
  children: PropTypes.node,
};

NavigationPointsBdz.propTypes = {
  isFooter: PropTypes.bool.isRequired,
};

export function Head() {
  return (
    <>
      <title>Bund Deutscher Zupfmusiker Landesverband Bayern</title>
      <link rel="icon" type="image/png" href={favicon} />
    </>
  );
}
