import React, { useState, useEffect } from "react";
import ArrowUp from "../images/global/svg/arrowUp.inline.svg";
import PropTypes from "prop-types";

export default function ScrollToTopButton(props) {
  const [showTopBtn, setShowTopBtn] = useState(false);

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 400) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    });
  }, []);
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };
  return (
    <>
      {showTopBtn && (
        <button
          aria-label="Scroll to top"
          className={`fixed 
            bottom-3 right-2 md:bottom-10 md:right-8
            w-12 h-12
            text-white
            ${props.backgroundColor}
            rounded-3xl
            opacity-50 hover:opacity-100
            hover:shadow-lg
            inline-flex
            justify-center
            items-center
            `}
          onClick={goToTop}
        >
          <ArrowUp />
        </button>
      )}
    </>
  );
}

ScrollToTopButton.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
};
