---
layout: blog
title: Aktuelle Informationen vom Bundesvorstand
date: 2020-10-25T19:15:35.853Z
---
#### Liebe Präsident\*innen, liebe Landesmusikleiter\*innen, liebe Ehrenmitglieder,

heute informieren wir Sie noch einmal kurz über aktuelle Themen:

* BDZ Eurofestival Zupfmusik 2023 – geplant im Mai (17.–05.) 2023 in Bruchsal. Bitte informieren Sie ihre Landes(jugend)orchesterorganisatoren.
* 2. Wettbwerb für Orchester im BDZ am 14. und 15.05.2022 (neuer Termin) in Wirges/Westerwald.
* Bitte nutzen Sie als LV die Möglichkeit der Veröffentlichungen im »Auftakt!«
* Berichten Sie uns von ihren coronabedingten Veränderungen der Landes(jugend)orchester- und Lehrgangsarbeit.
* Fachartikel sind ebenso sehr willkommen. Diese werden vor Veröffentlichung vom Bundesvorstand geprüft und gesichtet, senden Sie bitte an praesident@zupfmusiker.de oder steffen.trekel@zupfmusiker.de
* Der Projektbeirat DOW hat beschlossen, den Wettbewerb 2021 nicht in Präsenzform in Bonn durchzuführen. Die zum Wettbewerb qualifizierten Ensembles werden vom Projektbüro zu ihrer Bereitschaft der Teilnahme an einem virtuellen Wettbewerb befragt (November 2020)

Zum Abschluss möchte ich Ihnen berichten, dass es gelungen ist, die bedrohlichen Vertragsaussichten zu unserem GEMA Pauschalvertrag abzuwenden. Besonderer Dank gilt Vizepräsident Thomas Kronenberger, dem es gelungen ist, in persönlichen Verhandlungen außerordentlich gute Konditionen für unseren Verband auszuhandeln. Dies gilt vor allem vor dem Hintergrund der aktuellen coronabedingten Konzertlage unsere Mitglieder.

Mit freundlichen Grüßen\
Bund Deutscher Zupfmusiker e.V.

Dominik Hackner\
Präsident und Festivalleitung BDZ Eurofestival Zupfmusik