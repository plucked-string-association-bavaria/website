---
layout: blog
title: "Pfingstkurs in Bayern: Erfolgreiches Experimentierfeld"
date: 2023-06-24T18:30:57.089Z
---
!["Flashmop - Mandoline" Probe mit Bianca Brand ](/assets/uploads/p1390447.jpg "\"Flashmop - Mandoline\" Probe mit Bianca Brand ")

Hammelburg/Lohr. Der Bund Deutscher Zupfmusiker Landesverband Bayern veranstaltete in den Pfingstferien seinen 46. Pfingstkurs für Mandoline und Gitarre in der Bayerischen Musikakademie in Hammelburg. Bei dem einwöchigen Seminar werden standardmäßig sowohl Einzel- als auch Gruppenunterrichte angeboten: Ensemblespiel, Theorie, Liedbegleitung, Musikvideos erstellen, Singen und Tanzen, Bodypercussion und: Schnupperkurse für Kontrabass und Jazzharmonik rundeten das Programm ab.  Ein Schwerpunkt der diesjährigen Kursarbeit war ein „Flashmop“ für Mandoline: Alle ! TeilnehmerInnen versuchten sich auf dem „Instrument des Jahres“ und waren begeistert, was sie in kurzer Zeit auf der Mandoline spielen konnten – es verliebten sich dabei zwei Teilnehmer so sehr in den Klang der Mandola, dass sie sich jetzt ein Zupforchester suchen, um ihr neues Zweitinstrument praktizieren zu können! Die außerordentlich beeindruckenden Ergebnisse präsentierten die Zupfmusiker im Großen Saal der Bayerischen Musikakademie dem begeisterten Publikum. Besonders war in diesem Jahr der Beitrag des Kursorchesters, das eine junge Mandolinistin (Hannah Pfister: Konzert D-Dur von Fasch) begleitete und amerikanische Bluegrassmusik sowie einen Santana Hit zum Besten gab. Bereits zum zweiten Mal waren mutige Kontrabassschüler mit Popsongs zu hören, und wurden vom Publikum gefeiert. 
Trotz so viel Neuem im Kursangebot, nutzen zwölf Kursteilnehmer die Möglichkeit, während der Woche eine Laienmusikprüfung (D1 und D2-Kurs) abzulegen. Erfreulich war, dass die Anforderungen von den Kursteilnehmern im praktischen Teil überschritten wurden, die Prüfungen also auf hohem Niveau stattfanden.

Fünf Dozenten betreuten die 22 Zupfmusiker im Alter von 11 – 82 Jahren während der intensiven Musikwoche: Petra Breitenbach (Lohr), Bianca Brand (Forst), Oliver Dannhauser (Heroldsberg), Rosa Faerber (Würzburg) und Malte Weyland (Regensburg).

![Kursorchester mit Malte Weyland](/assets/uploads/p1390536.jpg "Kursorchester mit Malte Weyland")

!["Flashmop - Mandoline" im Konzert mit Sängerinnen aus dem Teilnehmerkreis](/assets/uploads/p1390471.jpg "\"Flashmop - Mandoline\" im Konzert mit Sängerinnen aus dem Teilnehmerkreis")

![Die beiden jüngsten Kontrabassisten des Kurses](/assets/uploads/p1390502.jpg "Die beiden jüngsten Kontrabassisten des Kurses")

![Bodypercussion mit Rosa Faerber ](/assets/uploads/p1390492.jpg "Bodypercussion mit Rosa Faerber ")