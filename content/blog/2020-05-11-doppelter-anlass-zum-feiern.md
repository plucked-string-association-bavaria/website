---
layout: blog
title: Doppelter Anlass zum Feiern
date: 2019-12-12T19:45:06.455Z
attachment: ""
---
## Landesmusikfest des BDZ LV Bayern 2019 in Alteglofsheim

\[Von Thomas Hammer, Eberhard Wilhelm & Karina Hümpfner] Vom 8. bis 10. November feierten 160 Musikerinnen und Musiker aller Altersgruppen in der Bayerischen Musikakademie Schloss Alteglofsheim: Der Landesverband Bayern des vor 100 Jahren gegründeten Bundes Deutscher Zupfmusiker (BDZ) hatte zum 10. Landesmusikfest eingeladen und fünf Vereinsorchester, drei Kammermusikensembles sowie vier überregionale Orchester waren gekommen und hinterließen bei den Besuchern durch ihre engagierten, stilsicheren Interpretationen musikalisch und spieltechnisch anspruchsvoller Werke einen hervorragenden Eindruck der Mandolinen- und Gitarrenorchesterszene. 

Beim kammermusikalischen Auftakt des Festes im Kaisersaal des Barockschlosses begeisterten das Gitarrenduo Cedric Penn & Paul Feige und das Friedberger Gitarrenquartett, beides 1. Preisträger (mit 25 Punkten) des diesjährigen Bundeswettbewerbs „Jugend Musiziert“, durch ihr gefühlvolles und temporeiches Zusammenspiel. Besonders beeindruckte die facettenreiche Interpretation der „Nebulae“ für Gitarrenensemble, mit der Olga Amelkina-Vera 2012 den Kompositionswettbewerb der Austin Classical Guitar Society gewann. Hier konnte jeder spüren, dass es sich um das Lieblingswerk des Friedberger Gitarrenquartetts handelt! Das Duo Iris Hammer, Mandoline und Heiko Holzknecht, Gitarre hatte zwei Schmankerl zu bieten: Mit dem souverän interpretierten Satz „Time’s scar“ aus Yasunori Mitsudas Soundtrack zu „Chrono Cross“ konnte das Publikum in die ebenso anspruchsvolle wie gefühlsbetonte Musik dieses Video-Spiels der Chrono-Reihe eintauchen. Beim „Tríptico Porteño“ des argentinischen Gitarristen Máximo Diego Pujol, das auf dem 19. Internationalen Gitarrenfestival Hersbruck 2018 mit Violine und Gitarre uraufgeführt worden war, überzeugte das Duo mit einer gekonnten Mischung aus Virtuosität und Musikalität. 

Das Eröffnungskonzert mit dem Bayerischen Landeszupforchester (BLZO) unter der Leitung von Oliver Kälberer beendete (offiziell) den ersten Tag des Landesmusikfestes: Zentraler Punkt des Programms war hier eine wunderbar ausgewogene Interpretation des Gitarrenkonzerts „Il sogno del pesciolino“ von Eduardo Angulo mit Clemer Andreotti als Solisten. Mit den durch Musik und Dichtung des ausgehenden Mittelalters inspirierten „Wolkensteiner Mähren“ von Silvan Wagner setzte das BLZO gemeinsam mit dem Ensemble Bayuna einen imposanten Schlusspunkt. 

Im Rahmen dieses Konzertes fand die erste aus einer Reihe von Ehrungen im Laufe der nächsten Konzerte statt. Dies aus gutem Grund, da ohne das ehrenamtliche Engagement im BDZ die musikalische Qualität der Vereinsorchester und der überregionalen Orchester nicht vorstellbar wäre. Der Präsident des BDZ LV Bayern Joachim Kaiser überreichte dem BLZO-Gitarristen Thomas Zapf für seine langjährige, anspruchsvolle Arbeit als Kassenwart die BDZ-Verdienstmedaille in Bronze. Ebenfalls mit der BDZ-Verdienstmedaille in Bronze geehrt wurde Karla Jenuwein für ihre langjährige Arbeit als Geschäftsstellenleiterin des BDZ LV Bayern und ihre 40-jährige Mitgliedschaft im Zupf-Ensemble Lohr. Dr. Thomas Goppel ließ es sich nicht nehmen, die Laudatio für die von Petra Breitenbach vorgenommene Ehrung von Joachim Kaiser mit der BDZ-Verdienstmedaille in Silber für sein erfolgreiches, 12-jähriges Wirken als Präsident des BDZ Landesverbandes Bayern zu halten.

Nach diesem wunderbaren offiziellen Ende des ersten Tages konnte man ihn noch in geselliger Runde gemeinsam ausklingen lassen. Allerdings nicht zu lange, es gibt viel Programm für den nächsten Tag. Der Samstag beginnt bereits um 9 Uhr frisch gestärkt nach dem Frühstück mit dem offiziellen Programm. Hier musste man sich zwischen dem Festivalorchester (dazu später mehr) oder dem O.T.P.Q.-Gitarrenensemble entscheiden.

In Johannes Tapperts Workshop O.T.P.Q.-Gitarrenensemble konnte man erleben, wie man durch die Besetzung des Gitarrenensembles mit Oktav-, Terz-, Prim- und Quintbassgitarren – daher der Name – gegenüber einem nur mit Primgitarren besetzten Ensemble an Farbenreichtum und Transparenz gewinnen kann.

Nicht nur Musiker, auch mehrere Mandolinen- und Gitarrenbaumeister(-innen) waren anwesend und stellten aktuelle Modelle ihrer Instrumente aus. Es bot sich die Möglichkeit, diese in aller Ruhe selbst auszuprobieren. Man konnte sich aber auch ausgewählte Modelle anhören, welche von zwei Spielern des BLJZO anhand eines immer wieder identischen Stückes vorgestellt wurden.

In drei weiteren Orchesterkonzerten blickten die Zuhörer auf die inzwischen über hundertjährige musikalische Tradition von Mandolinen- und Gitarrenorchestern zurück, gewannen anhand gekonnter Bearbeitungen eine neue Sicht auf Werke großer Meister wie Mozart oder Chopin und bekamen bei der Aufführung junger Originalkompositionen für Zupforchester einen erfrischenden Eindruck von den aktuellen musikalischen Entwicklungen.

Einen abwechslungsreichen Einblick in die musikalische Arbeit bayerischer Vereinsorchester gaben das Orchesterkonzert am Samstagnachmittag und die Abschlussmatinee am Sonntagvormittag. Bemerkenswert war hier die in Repertoire und Interpretation hörbare persönliche Note der Ensembles, sei es der traditionsbewusst auftretende Münchener Mandolinen-Zirkel, das am Jazz orientierte Gitarrenensemble „Spass by Saite“, unter anderem mit Bearbeitungen von Eberhard Wilhelm, oder der (nicht nur diesjährige) Gewinner des Bayerischen Orchesterwettbewerbs Ensemble Roggenstein mit seiner Mischung aus Meisterwerken musikalischer Weltliteratur und Kompositionen seines musikalischen Leiters Oliver Kälberer.

Den Höhepunkt des Landesmusikfestes bildete das vom Ensemble Bayuna und vom Bayerischen Landesjugendzupforchester (BLJZO) gestaltete Festkonzert am Samstagabend: Das 2016 gegründete Ensemble "Bayuna“ unter der Leitung von Silvan Wagner ist das jüngste der vier überregionalen Ensemble des BDZ Landesverbandes Bayern. Besonderheiten dieses Ensembles sind die vor allem junge Erwachsene ansprechende basisdemokratische Organisation und die konzeptionelle Beschränkung der jährlich wechselnden Programme auf ein Thema. Mit „Südamerikanischer Musik“ gab es dieses Jahr überraschende Einblicke in die engen Zusammenhänge der spätromantischen leichten Muse Europas. Silvan Wagner leitete dabei nicht nur das erfrischend und fein differenziert spielende Ensemble sondern führte mit seiner ebenso kompetenten wie unterhaltenden Moderation durch das Programm.

Nicht weniger überzeugend war die Abschiedsvorstellung von Julian Habryka als Leiter des BLJZO: mit einer eigenen, sehr gelungenen Bearbeitung von Gershwins Variations on „I Got Rhythm“ für vier Gitarren und Zupforchester gelang es dem engagierten Jugendorchester, die vielen Klangfarben und Instrumente des Sinfonieorchesters hervorragend zu imitieren. Gleiches gilt für die Orchestersuite Nr. 3 von J. S. Bach: sehr differenziert musiziert und transparent und mit Leichtigkeit erklangen die vier Sätze. Man konnte spüren und zu jeder Zeit hören, wie wichtig es den Jugendlichen war, sich von der besten Seite zu zeigen, wenn Julian zum letzten Mal dirigiert – besonders bei seinem eigenen „L’Inferno“, das den Abend beschloss.

Zu den gefürchteten Programmpunkten von Festkonzerten gehören Festreden. Dr. Thomas Goppel, Schirmherr des Landesmusikfestes und Präsident des Bayerischen Musikrates, stellte in seiner perfekt vorbereiteten, aber immer wieder spontan mit launigen Einwürfen gespickten Rede unter Beweis, dass es auch anders geht. Bemerkenswerter Weise gab es darin nicht nur einen Rückblick auf 100 Jahre BDZ sondern auch einen Blick darauf, wie angesichts des gesellschaftlichen Wandels die Zukunft der Zupfmusikszene gestaltet werden kann. Diese Frage war übrigens auch Gegenstand eines Impulsvortrages von Dr. Silvan Wagner mit anschließender (aber nicht abschließender) Diskussion am Samstagnachmittag, die erfreulich gut besucht war und offen und fair geführt wurde. Angesichts der Situation in der Zupfmusikszene (Mitgliederzahlen, Nachwuchs insbesondere im Bereich Mandoline) sollte uns dieses Thema auch in den kommenden Jahren begleiten.

Das Festkonzert war jedoch noch lange nicht das Ende des zweiten Tages. Bei der Zupferparty wurde bis lange in die Nacht zusammengesessen, sich unterhalten und bei leckerem, selbstgemachtem Essen gefeiert. Eine wunderbare Gelegenheit, sich auch orchesterübergreifend kennen zu lernen. Das BLJZO hierbei bestimmt mit einem lachenden und einem weinenden Auge. Sie mussten sich von Julian Habryka (Dirigent) und Veronika Schleicher (Dozentin) verabschieden. Ab 2020 übernimmt Johannes Lang den Dirigentenstab des BLJZO. Wir sind auf die nächsten Konzerte gespannt!

Den Abschluss des Landesmusikfestes bildete die Matinee am Sonntagvormittag.

Vor dem eigentlichen Programm gab es eine Vorführung des O.T.P.Q.-Workshops vom Vortag. Bei einem Satz aus der „Connemara-Suite“ durften sich die Zuhörer vom luftigen und durchsichtigen Klangbild dieser Besetzung selbst überzeugen. 

Dann folgten drei weitere Orchester. Das „uHu“-Zupforchester („unter Hundert“, da nicht alle Spieler über 50 Jahre alt sind), welches 2012 aus einem Wochenendlehrgang „Spaß an Zupfmusik mit 50+“ hervorgegangen und das vierte überregionale Orchester ist, sowie das Gitarrenensemble Bayreuth und das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt. Auch hier war wieder eine bunte Vielfalt an Stilen, Epochen und Interpretationen auf hohem Niveau zu hören. Man fühlte sich im Laufe des Vormittags unter anderem nach Südamerika, Spanien oder auch Wien versetzt.

Den eigentlichen Schlusspunkt setzte dann das „Festivalorchester“ unter der Leitung von Landesmusikleiterin Petra Breitenbach mit „The song of japanese autumn“ von Yasuo Kuwahara und überzeugte durch seine Klangfülle. Das Projekt wurde im Vorfeld organisiert, jeder war bei diesem Orchester willkommen. Im Endeffekt saßen dann über 80(!) Mitwirkende auf der Bühne. LV-Präsident Hajo Kaiser begrüßte die Zuhörer sogar als Minderheit, jedenfalls waren die Reihen deutlich gelichtet. Diese Riesenbesetzung, die trotz der Zufallsbesetzung und nur einer(!) Probe sehr homogen wirkte (Bravo, Petra!), und gegen die alles bisher Gehörte kammermusikalisch wirkte, ist\
natürlich das Richtige für dieses Stück.

Damit ging ein vorbildlich organisiertes Fest mit vielen tollen Beiträgen und Begegnungen zu Ende. Es war wie immer schön, dabei gewesen zu sein. Bis zum nächsten Mal (vielleicht diesmal nicht erst wieder in 6 Jahren)!

![Joachim Kaiser am Rednerpult - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_01.jpg "Joachim Kaiser am Rednerpult - Foto: Michaela Schroll")

![Clemer Andriotti mit dem BLZO - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_02.jpg "Clemer Andriotti mit dem BLZO - Foto: Michaela Schroll")

![Das Ensemble Bayuna - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_03.jpg "Das Ensemble Bayuna - Foto: Michaela Schroll")

![Das BLJZO - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_04.jpg "Das BLJZO - Foto: Michaela Schroll")

![Hermann Gräfe bei der Vorstellung seiner Gitarren - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_05.jpg "Hermann Gräfe bei der Vorstellung seiner Gitarren - Foto: Michaela Schroll")

![Das Ensemble Roggenstein - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_06.jpg "Das Ensemble Roggenstein - Foto: Michaela Schroll")

![Das Ensemble Spaß by Saite - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_07.jpg "Das Ensemble Spaß by Saite - Foto: Michaela Schroll")

![Das Ensemble uHu - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_08.jpg "Das Ensemble uHu - Foto: Michaela Schroll")

![Das Gitarrenensemble Bayreuth - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_09.jpg "Das Gitarrenensemble Bayreuth - Foto: Michaela Schroll")

![Das Mandolinen- und Gitarrenorchester der NaturFreunde Schweinfurt - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_10.jpg "Das Mandolinen- und Gitarrenorchester der NaturFreunde Schweinfurt - Foto: Michaela Schroll")

![Das Festivalorchester - Foto: Michaela Schroll](/assets/uploads/zupfbote_2019_lmf_11.jpg "Das Festivalorchester - Foto: Michaela Schroll")