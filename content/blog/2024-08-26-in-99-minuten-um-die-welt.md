---
layout: blog
title: In 99 Minuten um die Welt
date: 2024-08-26T17:57:34.498Z
---
Fast genau ein Jahr nachdem sich die Gründerin und langjährige Dirigentin des Vivaldi-Orchesters Monika Fuchs-Warmhold mit einem festlichen Konzert von ihrem Karlsfelder Publikum verabschiedet hat, präsentierte das VOK **am Samstag, 08. Juni 2024 im Bürgerhaus Karlsfeld** das erste Jahreskonzert mit dem neuen Dirigenten Heiko Holzknecht. 

![Vivaldi-Orchester Karlsfeld mit neuem Dirigenten Heiko Holzknecht](/assets/uploads/mette-photography-heiko-holzknecht-mit-dem-vivaldi-orchester-karlsfeld.jpeg "Vivaldi-Orchester Karlsfeld mit neuem Dirigenten Heiko Holzknecht")

Bereits der Konzerttitel „*In 99 Minuten um die Welt“* versprach ein musikalisch-geografisch sehr vielfältiges Programm aus allen Kontinenten, mit Kompositionen aus verschiedenen Epochen und Genres.\
Ob Barock (Antonio Vivaldi), Moderne (u.a. Daintree, Kollaps, Look into my soul, Sketch Book), Filmmusik („Pirates of Zimmer“) oder auch Rockiges wie „Nothing else matters“ von Metallica, die gesamte  Bandbreite wurde vom rund 40-köpfigen Orchester auf hohem Niveau und mit großer Spielfreude dem begeisterten Publikum präsentiert. Alle Solisten des Abends kamen aus den eigenen Orchester-Reihen. Die mehr als 400 Zuhörerinnen und Zuhörer dankten mit Beifallsstürmen. Es wurde schnell klar, dass auch unter der Leitung von Heiko Holzknecht musikalischer Anspruch und Spielfreude weiterhin an erster Stelle stehen. 



Als „Vorgruppe“ musizierten die Jüngsten, die Vivaldi „Tiger“ und „Mäuse, unter der Leitung von Nadezhda Pantina. 

![Vivaldi Tiger und Vivaldi Mäuse](/assets/uploads/mette-photography-nadzhda-pantina-und-das-vivaldi-jugendorchester.jpeg "Vivaldi Tiger und Vivaldi Mäuse")

Sie begannen die Weltreise mit der Komposition „El Paella“ von Gerhard Kloyer.

Darauf folgte ein Wiegenlied für eine Eidechse – „Lizard’s Lullaby“ von Marlo Strauß.\
Mit „Bloody Mary“ von Lady Gaga, komponiert von Stefani Germanotta, Fernando Garibay und Paul Blair verabschiedeten sich die Vivaldi-Youngsters von einem sehr gelungenen Auftritt.

![Dirigentin Nadezhda Pantina](/assets/uploads/mette-photography-vivaldi-jugendorchester.jpeg "Dirigentin Nadezhda Pantina")

Anschließend feierten die Musiker*innen des Vivaldi-Orchesters mit einem Stück des jungen Komponisten Andreas Lorson, der bereits mit 13 Jahren erste Kompositionen geschrieben hat, eine temperamentvolle **„Nacht in Buenos Aires“**. Später im Konzert folgte ein weiteres Stück **„Kollaps“** von ihm, welches mit seinen verschiedenen musikalischen Stimmungen ein Wechselbad der Gefühle auslöste



Aus **„Sketchbook“** von Dominik Hackner, einem gefragten Komponisten und Dirigenten aus dem Ahrtal, brachte das Orchester den Satz II „Funeral for Jaws“, eine Art Trauermarsch, zu der Beerdigung eines nicht ganz „Ehrenwerten“ in eher gemäßigtem Tempo, und den Satz IV, einen schnellen Gopak-Tanz, in dem die Mandola-Stimme mit längeren Solo-Parts zu glänzen wusste.



Mit dem **„Konzert für 4 Mandolinen und Zupforchester“** des Namenspatrons **Antonio Vivaldi** vollführte das Orchester eine musikalische Zeitreise in das Europa des 18. Jahrhunderts. Musikalisch und technisch brillant präsentierten das Orchester und die vier Solist*innen Brigitte Rost, Ramona Wimmer, Katrin Nozicka und Benedict Wienecke – alle aus den eigenen Orchester-Reihen - die eingängige, virtuose und schwungvolle Komposition des Barockkomponisten und Violinisten.



**„Pirates of Zimmer“,** ein aufregendes Medley von Filmmusiken des Oscar-gekrönten Komponisten Hans Zimmer, entführte das Publikum ebenfalls in verschiedene Weltregionen. Hier kam echtes Filmmusik-Feeling auf, nicht zuletzt durch den Einsatz von großem Schlagwerk, professionell eingebracht von Jürgen Schieber und seinem Schüler Linus Gottschalk an Schlagzeug und Pauken sowie von Anna Pobel am Becken.  



Nach der Pause machte das Orchester mit **„Nothing else matters“**, einem sehr nachdenklichen Stück der US-amerikanischen Heavy Metal-Band Metallica einen Ausflug in die Rockmusik. Bei diesem „Ohrwurm“, der von den Gitarre-Solisten Adam Haranghy und Andreas Froschmayer an Akustik- und E-Gitarre authentisch rockig rüberkam, und bei dem selbst die Mandolinen und Mandolen mit Akkord-Begleitung fetzig in die „Rockmusik“ einstimmten, hielt es das Publikum vor Begeisterung kaum mehr auf den Plätzen. 



Einen überraschenden Mix aus Klassik, Celtic und Bluegrass und – ja, sogar Hardcore Punk - lieferte **„Look into my Soul“**, ein „Minikonzert“ für Solo-Mandoline und Zupforchester des kalifornischen Komponisten und Gitarristen Chris Aquavella, welches – gespickt mit rhythmischen Finessen – von der Solistin Brigitte Rost wie auch vom Orchester ausdrucksstark präsentiert wurde. 



Dann ging es ganz ohne Flugzeug in den australischen Regenwald. Der australische Gitarrist und Komponist Richard Charlton hat mit seinem Stück **„Daintree“** den Regenwald und seine tierischen Bewohner lebendig werden lassen, auch hier trugen Percussion-Elemente dazu bei, sich die Töne und Geräusche im Regenwald plastisch vorzustellen. 



Als Abschluss und einer der Höhepunkte des Konzertes führte das Vivaldi-Orchester die temperamentvollen **„Tanzsuite Nr. 2“**des japanischen Komponisten Takashi Kubota auf, der mit dieser Komposition seine Begeisterung für europäische Musikstile aufzeigt, die er während seines Dirigierstudiums in Deutschland und Österreich kennenlernte. 

Hier bewies das Orchester große Ausdauer und Stehvermögen, denn es gelang am Ende eines langen Konzertabends von fast drei Stunden Dauer eine sehr konzentrierte, musikalisch und technisch überzeugende Darbietung dieser technisch und rhythmisch sehr herausfordernden Komposition mit frischem Schwung bis zum Ende zu bringen. 



Es ist der erfahrenen und überaus kompetenten Reisebegleitung, Geli Tausch und Ralf Hanrieder, zu verdanken, dass das Publikum bei dieser langen musikalischen Reise zu keinem Zeitpunkt die geographische und musikalische Orientierung verlor. Mit viel Humor wussten sie vor jeder Reiseetappe Interessantes und Skurriles zu berichten und entfachten so die Neugier der Mitreisenden immer wieder aufs Neue.  



Höchstes Lob für das Debut des neues Dirigenten Heiko Holzknecht gab es später auch vom strengen SZ-Musikkritiker Adolf Karl Gottwald. Unter der Überschrift „Der neue Kapitän hält Kurs“ heißt  es am 9. Juni:  „Für das Vivaldi Orchester Karlsfeld ist Heiko Holzknecht – das lässt sich schon nach diesem ersten Konzert sagen – ein Glücksfall.“ 

![VOK in ihrem Element](/assets/uploads/mette-photography-vivaldi-orchester-karlsfeld.jpeg "VOK in ihrem Element")

Langanhaltender Applaus und standing ovations für Orchester und Moderation waren der verdiente Lohn des Publikums, welches den Mitwirkenden noch drei Zugaben entlockte.



**Sehr erfreuliche Mitgliederentwicklung**

 

Dem Vivaldi-Orchester Karlsfeld gehören zurzeit 45 aktive Spielerinnen und Spieler an, einige Mitspieler*innen sind seit Oktober 2023 neu hinzugekommen und hatten mit dem Jahreskonzert 2024 ihren ersten Konzertauftritt im Vivaldi-Orchester. 17 Spielerinnen und Spieler musizieren derzeit im Jugendorchester.



**Weitere Auftritte in 2024**



Am Fr. 15.11.2024 gibt das Vivaldi Orchester ein Konzert in der Allerheiligenhofkirche München



Am Sa. 16.11.2024 tritt das Vivaldi Orchester auf bayerischer Ebene beim Deutschen Orchesterwettbewerb an (München).





Bericht von Margit Saar und Mechthild Merz, Mandolinenspielerinnen im Vivaldi Orchester Karlsfeld