---
layout: blog
title: Musikalisches Wochenende im Kloster
date: 2021-09-22T07:49:26.112Z
---
Das **BLZO** hatte endlich mal wieder eine Arbeitstagung! Das Wochenende im Kloster in Scheinfeld war ein *Klangbad* und durch die vielen Gespräche ein *hochprozentiger Genuss.*

Immer wieder gerne!!