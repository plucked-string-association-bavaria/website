---
layout: blog
title: Jahreskonzert »Sommerzeit« des Vivaldi Orchesters Karlsfeld
date: 2018-03-01T20:52:10.097Z
---

## Samstag, den 21. April 2018 um 19:30 Uhr im Bürgerhaus Karlsfeld

\[Von Angelika Tausch] »Sommerzeit« heißt das neue Programm, mit dem das [Vivaldi Orchester Karlsfeld](http://www.vivaldi-orchester-karlsfeld.de/) unter der Leitung von Monika Fuchs-Warmhold einen Bogen schlägt von heiterer Barockmusik bis zur Musik der Swing- und Pop-Geschichte. Festliche Musik von Henry Purcell und Antonio Vivaldi findet dabei ebenso ihren Platz wie der Jazzklassiker »I got Rythm« von George Gershwin. Eine Courante des Renaissance-Komponisten Michael Prätorius variiert im Lauf der »Variationen« von Joachim Krause zu einem zeitgenössischen Klangbild. Außerdem verwandelt sich die berühmte Air von Bach plötzlich in einen Song der 60er Jahre. Das Vibraphon ruft Erinnerungen an Lionel Hampton wach und Klezmer-Musik, Piazzollas Libertango und nicht zuletzt ein überraschendes »Sommer«-Medley stimmen Sie auf die warmen Tage ein.

Die _Vivaldi Tiger_, der talentierte Nachwuchs der Vivaldis, eröffnen in bewährter Manier das Konzert. Die Jugendlichen präsentieren das Beste aus ihrem eigenen Konzertprogramm, das sie eine Woche später am 29.4. in der Kirche St. Philipp Neri in München-Neuperlach in einem eigenen Konzert zeigen.

Karten für den 21. April gibt es ab 10. März für 15 Euro (10 Euro ermäßigt) im Reisebüro Bunk, Allacher Straße 6 und im Blätterwerk, Rathausstraße 75. Im Vorverkauf ist es möglich, sich bereits gute Plätze zu reservieren.

Wer das Konzert am Samstag verpasst, kann es am Sonntag, den 22. April 2018 um 19 Uhr in der Kirche Frieden Christi im olympischen Dorf noch einmal hören. Dort treten die Vivaldis in der Reihe »Musik im olympischen Dorf« auf.
