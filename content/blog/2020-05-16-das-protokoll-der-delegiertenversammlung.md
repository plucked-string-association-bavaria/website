---
layout: blog
title: Das Protokoll der Delegiertenversammlung
date: 2018-11-06T20:29:03.827Z
---

Das Protokoll der Delegiertenversammlung vom 21.10.2018 liegt vor. Interessierte Mitglieder können das Protokoll gerne bei der Geschäftsstelle des BDZ LV Bayern anfordern.
