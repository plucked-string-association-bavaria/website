---
layout: blog
title: Jahr der Mandoline 2023 - Aktivitäten im Landesverband Bayern
date: 2023-01-10T17:43:14.033Z
---
Zum Jahr der Mandoline 2023 hat eine Arbeitsgruppe des BDZ LV Bayern die verschiedensten Aktionen und Werbemaßnahmen zusammengestellt.

Dabei soll die Vielfalt der Musik mit Mandoline im Vordergrund stehen.

Nicht nur die beiden überregionalen Zupforchester [BLJZO](https://www.bdz-bayern.de/bljzo) und [BLZO ](https://www.bdz-bayern.de/blzo)heben besonders die Mandoline hervor, sondern auch die Heimatorchester in den verschiedenen Regionen Bayerns. Bei den beliebten [](https://www.bdz-bayern.de/bdz/seminars)Kursen und Weiterbildungen, wie den Wochenendkursen [„Musik-Spiel-Spaß für Kinder“](https://www.bdz-bayern.de/bdz/seminars/kinderkurs), den einwöchigen [Pfingst](https://www.bdz-bayern.de/bdz/seminars/pfingstkurs)- und [Osterkursen ](https://www.bdz-bayern.de/bdz/seminars/osterkurs)für Jugendliche und Erwachsene und dem Sommerseminar [„Schweinfurter Seminar“](https://www.bdz-bayern.de/bdz/seminars/schweinfurter-seminar) wird die Mandoline in Konzerten und Werbemaßnahmen der Öffentlichkeit dargestellt und Schnupperstunden auf der Mandoline angeboten. Auch die Projektorchester [„Bayuna“](https://www.bdz-bayern.de/bdz/seminars/bayuna) und das Seniorenorchester „[uHu](https://www.bdz-bayern.de/bdz/seminars/uhu)“ legen einen Schwerpunkt auf die Mandoline.

In Bayern wird an etlichen Musikschulen Mandolinenunterricht von ausgebildeten Musik- bzw. Instrumentalpädagogen*Innen angeboten; beim Wettbewerb [Jugend musiziert](https://www.jugend-musiziert.org/) ist die Mandoline in Regionalwettbewerben und dem bay. Landeswettbewerb erfolgreich vertreten.

Viele Heimatorchester haben eine lange musikalische Tradition, in den letzten Jahren feierten der Mandolinen- und Gitarrenverein Noris Süd (Leitung Petra Wiedemann) sein 100jährigen Bestehen,  der [Münchener Mandolinen-Zirkel](http://www.muenchnermandolinenzirkel.de/) (Leitung: Egbert Nöbel) sein 95-jähriges Bestehen, das [Vivaldi Orchester Karlsfeld ](https://www.vivaldi-orchester-karlsfeld.de/)(Leitung: Monika Fuchs-Warmhold) sein 50-jähriges Bestehen, das [Zupf-Ensemble Lohr ](https://www.zupf-ensemble-lohr.de/)(Leitung: Petra Breitenbach) sein 40-jähriges Bestehen und das Zupfmusikensemble [„musica a corda“ Hemhofen ](https://www.musica-a-corda.de/)(Leitung: Iris Hammer) sein 30-jähriges Bestehen.

Das [Ensemble Roggenstein ](http://www.ensembleroggenstein.de/)(Leitung: Oliver Kälberer) steht seit vielen Jahren für herausragende Leistungen. Als achtfacher Preisträger des [Deutschen Orchesterwettbewerbs](https://www.musikrat.de/dow/) (zuletzt 2021) gehört es zu den besten Gitarren- und Mandolinenensembles in Deutschland.

**Schirmherren für das Jahr der Mandoline 2023 sind die Mandolinistin Anna Torge und Prof. Jürgen Ruck (Prof. für Gitarre an der Musikhochschule Würzburg).**

Informationen zu den Veranstaltungen zum Thema "Jahr der Mandoline 2023" finden Sie in unserem [Terminkalender](https://www.bdz-bayern.de/bdz/news/dates).