---
layout: blog
title: 25 Jahre Musik, Spiel und Spaß für Kinder im Schullandheim Reichmannshausen
date: 2019-04-19T09:30:23.064Z
---

Der Bund Deutscher Zupfmusiker (BDZ) lud junge Gitarristen und Mandolinisten am 23. und 24. März 2019 zum 25. Mal zu „Musik, Spiel und Spaß – ein Wochenende für Kinder“ ein. Das diesjährige Motto lautete: „Es war einmal ... – Musik und Geschichten aus der Märchenwelt“.

Für diesen Kinderkurs reisten 20 Gitarristen und Gitarristinnen und eine Pianistin aus der
Gegend um Schweinfurt, Volkach/Münsterschwarzach, Gerolzhofen, Arnstein und Kitzingen
in das Schullandheim nach Reichmannshausen (bei Schonungen) an. Die sieben- bis
elfjährigen Musikschüler lernten zusammen mit den Dozenten Bianca Brand (Forst), Simon
Krapf (Leipzig) und Rosa Faerber (Würzburg, Kursleitung) die Melodien einiger Märchen
kennen. In drei verschiedenen Ensembles erarbeiteten sie die Lieder von den „Bremer
Stadtmusikanten“, von „Schneewittchen und die sieben Zwerge“ und „Der Hase und der
Igel“. Allen drei Liedern lagen die Melodien bekannter Kinder- und Volkslieder wie zum
Beispiel „Der Kuckuck und der Esel“ oder „Ein Vogel wollte Hochzeit machen“ zugrunde.

Doch nicht nur Ensemblespiel stand auf dem Wochenendplan: Gitarrenunterricht erhielten die
Kinder in einer Einzelstunde oder im Partnerunterricht und darüber hinaus sammelten einige
Kinder ihre ersten Erfahrungen im Orchesterspiel. Zwischen fleißigem Üben, konzentrierten
Proben und einer Rhythmuseinheit „Rhythm and Groove“, in der die jungen Musiker
Körperklänge erprobten und mit Papier musizierten, bemalten die jungen Gitarristen
zusammen mit Betreuer Jan Hembacher T-Shirts mit Märchen-Motiven, bastelten
Traumfänger, spielten Fußball und probierten gemeinsam die vom Schullandheim zur
Verfügung gestellten „Bubble Balls“ aus – ein absolutes Highlight.

Am Sonntag um 16 Uhr war es dann soweit: Die Turnhalle des Schullandheims verwandelte
sich in einen Konzertsaal und die Stühle reichten kaum für das Publikum aus: Eltern,
Großeltern, Geschwister, Freunde und andere Familienmitglieder der jungen Musiker kamen
zum Zuhören. In den Ensembles präsentierten die Kinder stolz, was sie in eineinhalb Tagen
erarbeitet hatten. Das Publikum sang den Refrain der „Bremer Stadtmusikanten“ mit und
erriet engagiert die versteckten Kinder- und Volkslieder. Auch die Dozenten und Betreuer Jan
Hembacher (Jazzgitarrist) hatten Lust, beim Abschlussvorspiel zu musizieren und so spielten
sie einen swingenden Blues als Überraschungsprogrammpunkt. Zum Schluss erklangen dann
alle 20 Gitarren plus Klavier gleichzeitig im Orchester und die Kinder sangen und spielten
„Supercalifragilisticexpialigetisch“ aus dem berühmten Mary Poppins Musical und „Das Lied
der Märchen“. Mit eifrigem Applaus ging der musikalische Kinderkurs zu Ende und leitete
die Vorfreude auf den Kurs im kommenden Jahr (14./15. März 2020) ein.

Einen herzlichen Dank an alle Eltern und Kinder, an das Schullandheim Reichmannshausen
und die Dozenten und den Betreuer. Des Weiteren einen besonderen Dank an die
Kulturstiftung des Bezirkes Unterfranken und den Bayerischen Staat, welche diesen
Kinderkurs förderten.

![Kinderkurs Reichmannshausen: die Teilnehmer mit Betreuer Jan Hembacher und den Dozenten (hinten v.l.n.r.) Simon Krapf, Bianca Brand und Rosa Faerber](/assets/uploads/zupfbote_2019_kinderkurs_01.jpg "Kinderkurs Reichmannshausen: die Teilnehmer mit Betreuer Jan Hembacher und den Dozenten (hinten v.l.n.r.) Simon Krapf, Bianca Brand und Rosa Faerber")
