---
layout: blog
title: "Vivaldi-Jugend auf Probenwochenende "
date: 2019-01-31T19:24:00.000Z
---

\[Von Angelika Tausch] _»Ach, ich wünschte, es wäre erst Freitag und wir würden gleich nochmal ein Wochenende wegfahren.«_ Der 12-jährige Sebastian, Gitarrenspieler bei den Vivaldi-Mäusen brachte es auf den Punkt. Das Probenwochenende der jüngsten Nachwuchsspieler des [Vivaldi Orchesters Karlsfeld](http://www.vivaldi-orchester-karlsfeld.de/ "Ltg.: Monika Fuchs-Warmhold") unter der Leitung von Monika Fuchs-Warmhold war rundum gelungen. Schon Ende Januar fuhren 18 Kinder zwischen 7 und 12 Jahren gemeinsam ins Jugendhaus nach Josefstal, um dort gemeinsam zu proben, zu spielen und Spaß zu haben.

Monika Fuchs-Warmhold hatte auch ein paar ganz neue Mandolinen- und Gitarren-Spieler mitgenommen, die sie entweder in einer der Karlsfelder Zupferklassen oder in ihrem Musikstudio unterrichtet. Damit die Kinder auch bei den Proben mitmachen konnten, hatte die engagierte Musikpädagogin eigens Stücke so umgeschrieben, so dass für alle eine passende Stimme dabei war. So konnten auch die jüngsten Kinder erleben, welche Freude das Musizieren in der Gruppe bereitet. Die Jugendleiterinnen Jana Burghart, Lena Huber und Melanie Weikerstorfer hatten sich ein abwechslungsreiches Programm ausgedacht, um die Pausen zwischen der Probenarbeit zu gestalten. Mit Schneeturmbauen, Geschicklichkeits-, Vertrauens- und Gemeinschaftsspielen lockerten die Kinder immer wieder ihre Finger, um danach wieder fleißig und motiviert an der Musik zu arbeiten. _»Gelungen, intensiv und richtig schön wars«_, schwärmten alle Betreuer am Ende.

Genau 2 Monate später, Ende März konnte man wieder Zupferklänge im Jugendhaus Josefstal hören. Diesmal waren 22 jugendliche Vivaldi-Tiger gekommen, um zu ratschen, zu spielen und natürlich gemeinsam Musik zu machen. Bei diesmal wunderbarem Frühlingswetter ging es in den Probenpausen viel nach draußen, um die Sonne zu genießen. Die zwei neuen Jugendleiterinnen Alina Engel und Verena Dreykorn meisterten ihren Einstand fantastisch und unterstützen Monika Fuchs-Warmhold, so dass auch dieses Probenwochenende abwechslungsreich, kreativ und produktiv war. Und für Sebastian, die Vivaldi-Maus, wurde sein Wunsch Realität. Er durfte mit einigen anderen älteren Mäusen bei den Tigern mitfahren und bei dem anspruchsvollen Programm mitproben. Am 13. April, beim großen Jahreskonzert des Vivaldi Orchesters werden die Kinder und Jugendlichen ihr Können unter Beweis gestellt haben.

![](/assets/uploads/zupfbote_2019_vivaldi.jpg)

Dankbar zeigten sich alle Verantwortlichen bei den großzügigen Fördereren des Vivaldi Orchesters, die es jedes Jahr auch bedürftigen Kindern und Jugendlichen ermöglichen, an Probenwochenenden, am Unterricht und an Aktionen des Orchesters teilzunehmen: der Sparkasse Dachau, der Bürgerstiftung Karlsfeld, dem Lions Club und dem Förderverein des Vivaldi Orchesters.
