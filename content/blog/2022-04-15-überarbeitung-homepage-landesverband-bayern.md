---
layout: blog
title: Überarbeitung Homepage Landesverband Bayern
date: 2022-04-15T15:07:06.922Z
---


Die Webseitenlandschaft des Bund Deutscher Zupfmusiker Landesverband Bayern wurde zum April 2022 großflächig erneuert.

Erstmals sind alle Seiten zentral verknüpft, d.h., dass 

* www.bljzo.de
* www.blzo.de
* www.schweinfurterseminar.de
* www.bdz-bayern.de

nicht mehr eigenständige Homepages sind, sondern in einer gemeinsam strukturierten Seite www.bdz-bayern.de zusammenlaufen. Zusätzlich wird erstmalig auch über www.bayuna.de eine Homepage des Projektorchesters Bayuna angeboten, natürlich auch in die neue Struktur eingearbeitet.

Unter https://bdz-bayern.de/bdz/news/dates werden ab jetzt zentral alle Termine des Landesverbands gebündelt. \
Der Zupfbote ist neu unter https://bdz-bayern.de/bdz/news/blog zu finden. Selbstverständlich veröffentlichen wir auch gerne die Termine und Beiträge unserer Mitgliedsorchester; die Kontaktmöglichkeit ist im Zupfboten angegeben (und hat sich auch nicht verändert).

Die gesamte Homepage befindet sich unter einer Open Source Lizenz, sodass der Quellcode unter https://gitlab.com/plucked-string-association-bavaria/website/ eingesehen werden kann.

Wir hoffen, dass Ihnen das neue Auftreten des Landesverbandes gefällt und wünschen Ihnen viel Spaß beim Erkunden der neuen Seiten.