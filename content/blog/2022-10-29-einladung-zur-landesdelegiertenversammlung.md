---
layout: blog
title: Einladung zur Landesdelegiertenversammlung
date: 2022-10-30T11:57:22.074Z
attachment: null
---
Sehr geehrte Mitglieder,

hiermit lade ich ein zur ordentlichen Landesdelegiertenversammlung am Sonntag, den **27.11.2022 von 10:30 bis 12:00 Uhr**. Aufgrund der wenig vorhersagbaren Infektionslage ist die Versammlung von vornherein als Online-Versammlung über eines der gängigen Online-Besprechungsprogramme (voraussichtlich Zoom) geplant. Der Link wird rechtzeitig, spätestens einen Tag vor der Versammlung zugeschickt. 

Folgende Tagesordnung ist vorgesehen:

1. Begrüßung und Vorstellung der Tagesordnung (vorliegende Anträge)
2. Feststellung von Anwesenheit und Stimmkraft
3. Protokollgenehmigung
4. Tätigkeitsberichte der Präsidenten
5. Bundesangelegenheiten
6. Kassenbericht des Schatzmeisters
7. Berichte aus der Geschäftsstelle
8. Bericht der Musikleiterin
9. Bericht der Jugendleiterin
10. Rechnungsprüfungsbericht
11. Entlastung des Vorstandes
12. Berichte aus den Vereinen
13. Vorstellung und Planung von Aktivitäten 2023/2024
14. Mandoline – Instrument des Jahres 2023: Vorstellung einer ersten Ideensammlung des Vorstandes, Vorschläge der Mitglieder, Absprache des weiteren Vorgehens
15. Sonstiges (allgemeine Informationen, Hinweise auf Fördermöglichkeiten, Unterstützung durch BDZ LV Bayern)

Die meisten dieser Tagesordnungspunkte sind eher kurz, so dass genug Zeit für die Punkte 12 bis 14 bleibt. Für die ausführliche Besprechung und Umsetzung von Aktivitäten zu Mandoline – Instrument des Jahres 2023 sind weitere Versammlungen vorgesehen. Anträge bitte ich mir spätestens zwei Wochen vor der Versammlung in Schriftform, bevorzugt per E-Mail, zukommen zu lassen.

Angesichts der Tatsache, dass bedingt durch das online-Format Anreisezeiten entfallen, erhoffe ich mir trotz des ungünstigen Termines eine rege Teilnahme.

Mit freundlichem Gruß\
Dr. Thomas Hammer, Präsident

![](/assets/uploads/bdz_logo_einladung.png)