---
layout: blog
title: Übergabe der Geschäftsstelle an Carolin Esgen
date: 2019-08-01T19:15:38.028Z
---

Seit dem 01.08.2019 wird die Geschäftsstelle des BDZ LV Bayern nicht mehr von Karla Jenuwein, sondern von Carolin Esgen geführt. Die neue Adresse finden Sie [hier](/contact).

Vielen Dank an Karla Jenuwein für die zuverlässige und angenehme Zusammenarbeit in den letzten 14 Jahren. Und vielen Dank an Carolin Esgen für die Bereitschaft, dieses Amt zu übernehmen!

![Carolin Esgen und Karla Jenuwein nach der Übergabe (in Begleitung von Petra Breitenbach und Joachim Kaiser)](/assets/uploads/zupfbote_2019_geschaeftsstelle.jpg "Carolin Esgen und Karla Jenuwein nach der Übergabe (in Begleitung von Petra Breitenbach und Joachim Kaiser)")
