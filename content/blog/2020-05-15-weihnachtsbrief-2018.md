---
layout: blog
title: Weihnachtsbrief 2018
date: 2018-12-25T19:32:00.000Z
---

## Liebe Zupfmusikerinnen und Zupfmusiker in Bayern,

auch zum Ende dieses Jahres möchte ich Ihnen herzlich danken für das Engagement im Rahmen der Ausübung, Unterstützung und Verbreitung der bayerischen Zupfmusik. Das verbinde ich gerne damit, über einige Ereignisse des abgelaufenen Jahres zu informieren und auf noch kommende hinzuweisen.

Gratulieren möchte ich nochmals ganz herzlich Oliver Kälberer zur Verleihung des Verdienstkreuzes der Bundesrepublik Deutschland und Petra Breitenbach zur Verleihung der Verdienstmedaille in Silber des Bundes Deutscher Zupfmusiker. Diese herausragenden Auszeichnungen sind auch ein Zeichen für die engagierte, zeitaufwendige und erfolgreiche Arbeit der Geehrten für die Zupfmusik im gesamten Bundesverband und weit darüber hinaus.

Das Eurofestival Zupfmusik hat vom 10. bis zum 13. Mai 2018 in Bruchsal als großes Fest der Zupferszene und in einer sehr positiven Atmosphäre stattgefunden. Der beeindruckende Auftritt des Bayerischen Landeszupforchesters wurde in einem voll besetzten Konzertsaal mit Standing Ovations begeistert gefeiert.

Am 21. Oktober fand in Geretsried die Landesdelegiertenversammlung statt, also das Treffen aller BDZ-Mitglieder im Landesverband Bayern. Wesentliche Inhalte waren drei Satzungsänderungen und die neue Datenschutzgrundverordnung. Die nächste Delegiertenversammlung auf Landesebene mit der Neuwahl des gesamten Präsidiums wird im Jahr 2020 stattfinden.

Im nächsten Jahr wird der BDZ, hervorgegangen aus seinen Vorgängerorganisationen DMGB (Deutscher Mandolinisten- und Gitarristenbund) und DAM (Deutscher Arbeiter-Mandolinisten-Bund), 100 Jahre. Dieses Jubiläum soll gefeiert werden. Es wird entsprechend den Vorstellungen des Bundesvorstands dazu allerdings keine zentrale Veranstaltung geben, sondern das Jubiläum soll 2019 regional in den einzelnen Landesverbänden begangen werden. Bitte senden Sie deshalb [Dominik Hackner](https://zupfmusiker.de/bdz-der-verband/der-vorstand/) aus dem Bundesverband Ideen und Termine zu, mit denen Ihr Verein/Orchester am Jubiläum mitwirken könnte.

Die sehr sehens- und vor allem hörenswerten Tage der Chor- und Orchestermusik werden im nächsten Jahr vom 29. bis 31. März in Gotha stattfinden und durch die Verleihung u.a. der »pro-musica-Plaketten« durch den Bundespräsidenten gekrönt werden.

Vom 4. bis zum 11. August 2019 findet alljährlich wieder das Sommerseminar des BDZ LV Bayern in der Bayerischen Musikakademie Hammelburg statt. Das mittlerweile 48. »Schweinfurter Seminar« erfreut sich großer Beliebtheit bei Spielerinnen und Spielern aus den Heimatvereinen der ganzen Bundesrepublik. Wer seit einigen Jahren Mandoline oder Gitarre spielt, Lust auf viel Zupfmusik in Ensembles und Orchestern hat, gerne mal über den Tellerrand schaut und sich weiterbilden möchte, ist hier genau richtig und eingeladen, sich bis spätestens Ende Mai anzumelden. Das bewährte Dozententeam mit Michael Tröster, Steffen Trekel und Bianca Brand freut sich auf Orchesterspieler, die gerne gemeinsam zwanglos musizieren. Iwan Urwalow übernimmt als Korrepetitor am Klavier gerne Begleitaufgaben und verhilft zum Zusammenspiel mit Klavier in lockerer Atmosphäre. Zusätzlich konnte für dieses Jahr André Herteux als Gitarrendozent gewonnen werden. Als Dozent für Fachdidaktik/Lehrpraxis Gitarre und »Gitarre Crossover« bringt er einen weiteren Bereich herein, der für Orchesterleiter und Zupfmusiklehrer zur Fortbildung einlädt.

Merken Sie sich bitte auch jetzt schon unser Landesmusikfest 2019 vom 8. bis 10. November vor. Die schönen Räume der Bayerischen Musikakademie im Schloss Alteglofsheim bieten einen optimalen Rahmen für diese Veranstaltung. In fünf verschiedenen Konzerten wird der Bogen von Musik des Mittelalters hin zum Jubiläumskonzert mit Musik der letzten 100 Jahre (passend zum Festjahr »100 Jahre BDZ«) hin zur Abschlussmatinee mit einer Uraufführung gespannt. Das Festival steht unter der Schirmherrschaft von Musikratspräsident Dr. Thomas Goppel.

Im Rundfunkhaus des Bayerischen Rundfunks in München wird vom 15. bis zum 17. November 2019 der [Landesentscheid](https://www.bayerischer-musikrat.de/wettbewerbe/bayerischer-orchesterwettbewerb/) als Voraussetzung für die Teilnahme am [Deutschen Orchesterwettbewerb](http://www.musikrat.de/dow/) (DOW) stattfinden. Als neue Kategorie können dieses Mal auch Jugendgitarrenensembles zusätzlich zu den etablierten Kategorien Zupforchester und Gitarrenensemble antreten. Interessierte Orchester/Ensembles fordern die Ausschreibungsunterlagen für den Landeswettbewerb beim Bayerischen Musikrat an. Der DOW 2020 findet dann vom 16. bis zum 24. Mai 2020 anlässlich des Jubiläums »250 Jahre Beethoven« in der Bundesstadt Bonn und im Rhein-Sieg-Kreis statt.

Ich wünsche Ihnen allen im Namen des gesamten Vorstands neben den Vorbereitungen für die Weihnachtszeit ein paar besinnliche Stunden im Kreise Ihrer Familien, einen hoffentlich weiterhin schönen Advent, friedliche Feiertage, einen fröhlichen Jahresbeschluss und einen unfallfreien Rutsch in ein gutes und erfolgreiches Jahr 2019.

Ihr\
Joachim Kaiser\
Präsident\
Hammelburg, im Dezember 2018
