---
layout: blog
title: Monika Fuchs-Warmhold verabschiedet sich vom Dirigentenpult -  Das
  Vivaldi-Orchester Karlsfeld e.V. sucht eine neue musikalische Leitung
date: 2023-06-14T14:18:58.164Z
attachment: ""
---
Das Vivaldi-Orchester Karlsfeld wurde 1970 von der Karlsfelder Musikpädagogin Monika Fuchs-Warmhold gegründet und erhielt unter ihrer Leitung im Laufe der Jahre zahlreiche Preise und Auszeichnungen. So wurde es weit über die Grenzen Karlsfelds und Bayerns hinaus bekannt. Konzertreisen führten das Orchester u.a. nach Spanien, Frankreich, die Niederlande, Italien, Tschechien und Russland. Es ist mittlerweile das mitgliederstärkste Laienzupforchester in Bayern. 

![Vivaldi-Orchester Karlsfeld](/assets/uploads/872cd78a-4230-4c7c-8aec-0ae31d7d023d.jpeg "Vivaldi-Orchester Karlsfeld")

Nach 53 Jahren sehr erfolgreicher Orchesterarbeit möchte Monika Fuchs-Warmhold nun den Taktstock in andere Hände übergeben. Mit einem festlichen Abschiedskonzert am **24. Juni 2023 um 19 Uhr** im Bürgerhaus in Karlsfeld verabschiedet sie sich vom Dirigentenpult und von ihrem Karlsfelder Publikum. 

![Dirigentin Monika Fuchs-Warmhold](/assets/uploads/2949fb0d-317d-4c69-b3b1-dd5936e7bebf.jpeg "Dirigentin Monika Fuchs-Warmhold")

Das Orchester sucht deshalb ab September 2023 eine engagierte musikalische Leitung mit Erfahrung in Ensembleleitung, auch im Studium befindlich, die sich für die Zupfmusik begeistert und bereit ist, das anspruchsvolle und vielfältige klassische und zeitgenössische Repertoire mit uns weiter zu entwickeln. 

Weitere Infos gibt es auf unserer Homepage:\
<https://www.vivaldi-orchester-karlsfeld.de/>

Hier sind auch Fotos und Audiodateien zu finden:\
<https://www.vivaldi-orchester-karlsfeld.de/impressionen>

Wir freuen uns sehr über Ihr/Euer Interesse und über Rückfragen gerne per E-Mail über die unter <https://www.vivaldi-orchester-karlsfeld.de/kontakt> angegebene E-Mail-Adresse des 1. Vorsitzenden.

Der Vorstand des Vivaldi-Orchesters Karlsfeld e.V.