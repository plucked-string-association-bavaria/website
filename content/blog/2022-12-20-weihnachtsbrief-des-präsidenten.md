---
layout: blog
title: Weihnachtsbrief des Präsidenten
date: 2022-12-20T07:21:10.038Z
---
Liebe Zupfmusiker*innen in und aus Bayern, 

Der Rückblick auf das hinter uns liegende Jahr erfüllt mich mit Hoffnung: Auch wenn der Anfang dieses Jahres für Amateurmusiker immer noch von starken Einschränkungen geprägt war – ich erinnere mich an Begriffe wie 15. Bayerische Infektionsschutzmaßnahmen und 2G-Plus – wurden im weiteren Verlauf wieder Präsenzveranstaltungen, Arbeitstagungen und Kurse in größerem Umfang möglich. Einladungen dazu habe ich im Rahmen meiner Möglichkeiten gerne wahrgenommen. Besonders hervorheben möchte ich hier die Jubiläen, die im Landesverband Bayern gefeiert wurden: 

Den Anfang machte das 1982 gegründete Zupfensemble Lohr unter der Leitung unserer langjährigen Landesmusikleiterin Petra Breitenbach am 21. Mai mit „Klangbildern aus 40 Jahren“. Während des Konzertes durfte ich zusammen mit Ehrenpräsident Joachim Kaiser und Vizepräsident Heiko Holzknecht ein Gründungsmitglied und drei weitere Mitspielerinnen für insgesamt 105 Jahre musikalisches Wirken im BDZ ehren. 

Am 2. Oktober feierte das Zupfmusikensemble *musica a corda* in Hemhofen sein Jubiläum mit einem Festkonzert „30+2 Jahre musica a corda“ mit Ehrung der musikalischen Leiterin Iris Hammer (Verdienstmedaille in Bronze und Ehrenbrief für 50-jähriges musikalisches Wirken im BDZ) durch Ehrenpräsidenten Joachim Kaiser. 

Das Vivaldi-Orchester Karlsfeld feierte am 22. Oktober seine Gründung durch die musikalische Leiterin Monika Fuchs-Warmhold im Jahr 1970 mit einem fulminanten Konzert „Vier Jahreszeiten – fünf Jahrzehnte“ im Bürgersaal Karlsfeld. Hier hatte ich die Ehre, ein Grußwort zu sprechen. 

Alle drei Jubiläumskonzerte hatten ein „volles Haus“, ein abwechslungsreiches Programm auf höchstem musikalischen Niveau, das das Publikum zu Begeisterungsstürmen hingerissen hat, und eine nachfolgende Feier für die Mitwirkenden und ihre Angehörigen!

 Bei den Kursen fielen der Osterkurs und die Kinderkurse zwar noch Pandemie bedingt aus, aber der Pfingstkurs an der Bayerischen Musikakademie in Hammelburg konnte erfreulicherweise wieder stattfinden. Die Teilnehmer\*innen honorierten das mit hervorragender Stimmung und Lernbereitschaft. Dass hier neben den langjährig erfahrenen Dozent\*innen Bianca Brand, Petra Breitenbach und Oliver Kälberer gleich drei neue Dozent*innen mitwirkten, erfüllt mich mit Zuversicht: Ein herzliches Willkommen für Malte Weyland, Rosa Faerber und Oliver Dannhauser! Näheres zum diesjährigen Pfingstkurs ist im Auftakt! 03/22 nachzulesen.

Das 51. Schweinfurter Seminar in der Bayerischen Musikakademie Hammelburg vom 7.-14.8.2022 hatte dieses Mal einen Schwerpunkt in alter Musik. Susanne Herre ließ interessierte Teilnehmer*innen und Gäste einer öffentlichen Abendveranstaltung an ihrem profunden Wissen über historische Instrumente (Lauten und Mandolinen), Spieltechnik, Tabulaturen, Verzierungen und Basso Continuo teilhaben. Dazu Bestand die Möglichkeit, allererste Erfahrungen im Gambenspiel zu gewinnen. Ich habe es sehr genossen, daran teilzunehmen! 

Die „uHus“ feierten mit einem Probenwochenende vom 18.-20.11.2022 ihr 10-jähriges Bestehen. Ein Wermutstropfen dabei war sicherlich, dass ihr Leiter Johannes Tappert am 27.7.2022 einem Krebsleiden erlegen war. Mit Petra Breitenbach, Michael Diedrich und Jürgen Thiergärtner, der bereits in der Vergangenheit öfter erfolgreich bei den Orchesterproben eingesprungen war, stand aber ein exzellentes Dozententeam zur Verfügung, das für Spaß am gemeinsamen Musizieren sorgt. Die „uHus“ sind übrigens kein festes Orchester und, wie der Name schon andeutet, offen für alle Altersgruppen (im Zweifelsfall sogar über 100-jährige). Wer also Spaß am gemeinsamen Musizieren sucht und dabei den Leistungsgedanken nicht in den Vordergrund stellen möchte, ist hier herzlich willkommen! 

Auch beim Bayerischen Landesjugendzupforchester hatten wir dieses Jahr einen Wechsel in der musikalischen Leitung: Johannes Lang gab bei der Arbeitstagung in Saldenburg am 9. und 10. September seine letzten beiden Konzerte mit einem bestens vorbereiteten BLJZO und gab den Taktstock an Tom Hofmann weiter, der im Januar 2023 seine erste Arbeitstagung als musikalischer Leiter des BLJZO haben wird. Ich habe es mir nicht nehmen lassen, das BLJZO bei seinem zweiten Konzert in der Schlosskapelle Ortenburg zu besuchen und beim nachfolgenden „Feier“-Abend in der Jugendherberge Saldenburg Johannes Lang für seine Arbeit mit dem BLJZO zu danken, ihm alles Gute für seine neue Stelle in Rostock zu wünschen, und Tom Hofmann als neuen musikalischen Leiter zu begrüßen. 

Das Bayerische Landeszupforchester hatte vom 14.-16. Oktober in Rohr seine erste Arbeitstagung mit Ariane Lorch als musikalischer Leiterin, die bis Mitte nächsten Jahres Oliver Kälberer vertritt und dort zusammen mit Ihrem Dozententeam die Proben an einem anspruchsvollen Konzertprogramm für das kommende Jahr begonnen hat. Willkommen heißen möchte ich auch an dieser Stelle Malte Weyland als Stimmführer der Mandoline!

Im Folgenden möchte ich einen ganz kleinen Ausblick auf das Jahr 2023 geben: 

Bald werden Sie das Kursprogramm unseres Landesverbandes erhalten. Wer schon jetzt einen Blick auf den aktuellen Planungsstand werfen möchte, kann das auf unserer neu gestalteten Internetseite tun: <https://www.bdz-bayern.de/bdz/news/dates>. Erfreulicherweise gibt es hier gegenüber den Vorjahren keine Lücken mehr, und ich bin zuversichtlich, dass endlich wieder alle Kurse und Arbeitstagungen der Auswahlorchester stattfinden werden! Nutzen Sie dieses reichhaltige Angebot – die Pandemie-Jahre haben mir sehr deutlich gezeigt, dass ohne gemeinsames Musizieren unser Leben ein ganzes Stück ärmer ist!

![Instrument des Jahres 2023](/assets/uploads/fotos-mediathek.png "Instrument des Jahres 2023")

Was das Jahr 2023 für mich ganz besonders macht, ist die 2008 von den Landesmusikräten ins Leben gerufen Initiative „Instrument des Jahres“: Ab dem 1.1.2023 wird das die Mandoline sein. Seit Monaten laufen in allen Bundesländern (und so auch bei uns) intensive Planungen für Aktionen (besondere Konzerte, Workshops, Ausstellungen, Instrumentenvorstellung an Schulen, …), mit denen wir Aufmerksamkeit auf die „Brückenbauerin“ unter den Instrumenten lenken und Neugier wecken können. Wir stehen im Kontakt zum Bayerischen Musikrat, aber auch zu den anderen Landesverbänden des BDZ. Erste Informationen sind im Internet zu finden unter <https://zupfmusiker.de/idj2023/> und <https://www.bayerischer-musikrat.de/nachrichten/2022-11-24-PM-Mandoline>. Ich bitte Sie herzlich, dazu beizutragen, dass 2023 für die Mandoline neue Impulse setzt. Gemeinsam können wir viel bewegen!

Abschließend möchte ich allen danken, die durch ihre Arbeit in unserem Landesverband ein reichhaltiges musikalisches Angebot ermöglichen, weiterhin dem Team um Peter Kroiß und Christine Heinz, das sich um die Neugestaltung der Internetseite verdient gemacht hat, Karin Heilgenthal, die dafür sorgt, dass Neuigkeiten im Zupfboten <https://www.bdz-bayern.de/bdz/news/dates> erscheinen (wenn Ihr etwas berichtenswertes habt, schickt es ihr bitte auch: Besser zweimal im Internet als keinmal), Bianca Brand, die mit ihrer effizienten Arbeit in der Geschäftsstelle für reibungslose Abläufe sorgt, und allen Kolleg*innen des Vorstandes / Landeshauptausschusses, die so unterschiedliche Aufgaben wie Datenschutz im Internet, Finanzen und Förderung, Jugendarbeit, und musikalisch fachkundige Gestaltung von Kursen und Aktivitäten unseres Verbandes ehrenamtlich bearbeitet haben. 

Ich wünsche Ihnen / Euch ein frohes Weihnachtsfest und ein glückliches Jahr 2023! 

 

Thomas Hammer