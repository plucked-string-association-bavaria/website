---
layout: blog
title: WasserWerke
date: 2019-01-31T19:30:00.000Z
---

\[Von Angelika Tausch] »WasserWerke« heißt das diesjährige Programm des [Vivaldi Orchesters Karlsfeld](http://www.vivaldi-orchester-karlsfeld.de/ "Ltg.: Monika Fuchs-Warmhold"). Am Samstag, den 13. April lädt das renommierte Zupforchester unter der Leitung von Monika Fuchs-Warmhold zum Jahreskonzert ins Bürgerhaus Karlsfeld. Entdecken Sie mit uns auf einer spritzigen Reise durch die Epochen der Musikgeschichte das Wasser in der Musik und erleben Sie, dass Wasser Musik ist.

Schippern Sie zu den Klängen von Händels Wassermusik auf der Themse, erleben sie die stürmische See in Vivaldis »Tempesta di mare«, begleiten Sie uns auf den Irrfahrten des Odysseus und werden Sie Teil des Festes wenn bei Dionysos Wasser zu Wein wird. Erleben Sie Tränen der Freude und der Traurigkeit mit Werken von John Dowland und Johann Strauss.

Die Auswahl der Stücke von Renaissance-, Barock- bis hin zu zeitgenössischer Musik zeigen die große Bandbreite des Orchesters, ebenso wie die Solisten (Mandoline, Gesang) aus eigenen Reihen. Kurzweilig endet das Konzert schließlich mit (Wasser)Werken aus Filmmusik, Rock und Pop. Und können 40 Gitarren, Mandolinen, Mandolen und Kontrabässe wirklich »Smoke on the water« spielen? Lassen Sie sich überraschen!

Eröffnet wird das Konzert vom talentierten Nachwuchs der Vivaldis. Ein paar Vivaldi Mäuse dürfen dieses Jahr erstmals mit den jugendlichen Tigern auf der großen Bühne dabei sein.

Das Konzert beginnt um 19:30 Uhr, Einlass ist um 19 Uhr. Karten gibt es ab dem 6. März für 15 Euro (10 Euro ermäßigt) im Reisebüro Bunk (Allacher Straße 6, Karlsfeld) und im Blätterwerk (Rathausstraße 75, Karlsfeld). Im Vorverkauf ist es möglich, sich bereits gute Plätze zu reservieren.

![](/assets/uploads/zupfbote_2019_vivaldi2.jpg)

Das Programm »WasserWerke« wird außerdem zu hören sein am 17. März in der Bruder-Konrad-Kirche in Gernlinden oder am 28. Juli in einer Matinée auf Schloss Amerang.
