---
layout: blog
title: Überreichung von Ehrennadeln in Gold
date: 2019-07-23T19:18:05.385Z
---

Am 19.07.2019 wurde Petra Breitenbach, Bettina Schneider und Karla Jenuwein in Lohr a. M. jeweils eine goldene Ehrennadel für ihre 40-jährige Mitgliedschaft im BDZ durch Präsidenten Joachim Kaiser übereicht. Dies würdigt ihr langjähriges Engagement für die Zupfmusik. Herzlichen Glückwunsch!

![Überreichung von Ehrennadeln in Gold.](/assets/uploads/zupfbote_2019_ehrung.jpg "Überreichung von Ehrennadeln in Gold.")
