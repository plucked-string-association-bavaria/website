---
layout: blog
title: "Interview mit Antonia Platzdasch, 21 Jahre, Mandolinistin aus Gernlinden "
date: 2022-12-30T18:04:28.596Z
---
![Antonia Platzdasch mit ihrer Mandoline 2022](/assets/uploads/antonia.jpg "Antonia Platzdasch mit ihrer Mandoline 2022")

**Hallo Antonia. Wie schön, dass wir uns heute Abend über dich und deine Liebe zur Musik und insbesondere zur Mandoline unterhalten können. Da du aktuell zum Studium in Wuppertal bist, leider nur per Video. Aber bevor wir dazu kommen, erzähle uns doch wie die Mandoline und du zusammengefunden haben?**

Antonia Platzdasch: Ich war damals vier Jahre alt und meine Schwester spielte im Zupforchester Gitarre. Da sah ich die Mandolinen und es war gewissermaßen Liebe auf den ersten Blick. Direkt im Anschluss des Konzertes durfte ich eine Mandoline ausprobieren und wollte sie spielen und lernen. So begann ich mit Unterricht an der Kreismusikschule Fürstenfeldbruck. Zuerst bei Anna Torge mittels der Suzuki-Methode, mit 8 Jahren wechselte ich zu Antje Strömsdörfer.

![Hauskonzert bei Antonias Oma 2007](/assets/uploads/antonia-2007.jpg "Hauskonzert bei Antonias Oma 2007")

**Da warst du ja wirklich noch sehr jung und hast früh dein Instrument gefunden. Hast du dann auch schon bald selbst im Orchester gespielt?**

A. P.: Anfangs musizierten wir in kleinerem Rahmen in der Gruppe und durften projektmäßig im Olchinger Kinderzupforchester mitspielen. Mit etwa 10 Jahren habe ich dann im Jugendzupforchester Eichenau begonnen und bin später zum Ensemble Roggenstein gewechselt. Und für ca. fünf Jahre habe ich mit meiner Mandoline sogar in einer JazzStreicherBigBand gespielt. Erst bei den Chilistrings, später bei den Bluestrings. Das war stilistisch mal was anderes und eine Ergänzung zur Klassik. Da bin ich durch meine beiden Brüder dazukommen, die dort Geige bzw Kontrabass gespielt haben.

![Antonia mit ihren beiden Brüdern](/assets/uploads/antonia-mit-brudern.jpg "Antonia mit ihren beiden Brüdern")

**Wow, das klingt ja nach einer richtig musikalischen Familie. Du hast aber nicht nur in regionalen Orchestern gespielt, sondern spielst auch in verschiedenen überregionalen Orchestern?**

A. P.: 2012 habe ich im BLJZO (Bayerischen Landesjugendzupforchester) angefangen und spiele dort immer noch. 2019 durfte ich das erste Mal im Bundesjugendzupforchester (BJZO) mitspielen und war seitdem jedes Jahr dabei. Da muss man sich ja immer wieder aufs Neue bewerben, um mitspielen zu dürfen. 2021 war ich Spielerin in Marseille beim EGMYO und im selben Jahr begann ich im Bayerischen Landeszupforchester (BLZO). Und dann spiele ich seit 2022 im Jugendzupforchester Baden-Württemberg und bin dort Dozentin für die 2. Mandolinen.

![Faschingsprobenphase des BLJZO 2018](/assets/uploads/antonia-2018.jpg "Faschingsprobenphase des BLJZO 2018")

**Darüber hinaus bist du doch auch noch im BDZ Bayern außerhalb der Orchester aktiv?**

A. P.: Ich bin seit 2020 in der Jugendleitung des BDZ Bayern tätig. Da ich selbst noch im BLJZO spiele bin ich mittendrin und versuche der Jugend eine Stimme zu geben. Wie kann man mehr Aufmerksamkeit erreichen? Wo werden Mittel oder Unterstützung benötigt? Wie sind die ganzen technischen Möglichkeiten wie Streaming, Social Media etc. am besten einzusetzen? Einfach die Zukunft mitzugestalten.

**Inzwischen hört man dich nicht nur als Orchesterspielerin, sondern dort auch als Solistin und bei Solokonzerten mit Symphonieorchestern. Da hast du im letzten Jahr ja auch sehr viel erlebt.**

A.P.: Das war wirklich klasse, was ich alles erleben und mitgestalten konnte. Als Solistin spielte ich 2019 das erste Mal mit dem BLJZO und vergangenes Jahr auf der Konzertreise des Jugendzupforchester Baden-Württemberg in Venedig mit dem dortigen Festivalorchester. Dann durfte ich im Herbst noch ein Solokonzert mit dem Patentorchester München spielen. Im Herkulessaal der Münchner Residenz war ich im Rahmen der Münchner Herbstakademie an der Aufführung Gustav Mahlers 7. Sinfonie beteiligt. Das ist ein wunderschöner Saal, in dem ich mir wünschen würde öfter und später auch mal Solo spielen zu können.

**Das ist so schön zu hören, mit welcher Begeisterung du von deinen Konzerten berichtest. Du hast dein Können aber auch schon früher bei verschiedenen Wettbewerben, nicht nur bei Jugend musiziert erfolgreich unter Beweis gestellt. Dabei war dir das Gewinnen aber erstmal gar nicht so wichtig, sondern die Freude am Spielen und die Herausforderung.**

A. P.: Mein erster Wettbewerb war mit 7 Jahren und dann mit 9 Jahren der Marlo-Strauß-Wettbewerb. Da habe ich jeweils gemeinsam mit einem meiner Geschwister teilgenommen. Erst später kam dann Jugend musiziert. Dort habe ich es bis zur Preisträgerin auf Bundesebene geschafft. Das war das erste Mal, dass eine gewisse Nervosität dazu kam. Es hat aber immer noch der Spaß am Musizieren überwogen und die Vorfreude auf die Erfahrungen die ich sammeln konnte und auch das wertvolle Feedback. Ich konnte zum Glück zu allen Wettbewerben ohne Leistungsdruck fahren, wurde immer gefördert. Meine bisher letzte Teilnahme war 2021 der Yasuo-Kuwahara-Wettbewerb für Mandoline solo.

![Bundeswettbewerb Jugend musiziert 2015 in Hamburg](/assets/uploads/antonia-2015jumu.jpg "Bundeswettbewerb Jugend musiziert 2015 in Hamburg")

**Dann war dir auch schon immer klar, dass du die Leidenschaft zum Beruf machen möchtest?**

A. P.: Nein, das war tatsächlich nicht der Fall. Natürlich war es immer eine Option. Daher habe ich in der 9. Klasse auch ein Praktikum an der Musikschule Fürstenfeldbruck absolviert. Sicher war ich mir aber lange nicht. Die Mandoline ist für mich eine Liebe für‘s Leben und die wollte ich nicht durch die Pflichten die ein Beruf mit sich bringt verlieren. Die Corona-Zeit hat die Aufnahmeprüfungen und den Beginn des Studiums leider nicht vereinfacht. Aber ich hatte das große Glück und konnte mir den Studienort dann aussuchen. Die Wahl ist auf Wuppertal gefallen, auch aufgrund der vielen Mitstudent:innen und der Möglichkeiten die sich dadurch ergeben.

**Zumindest viel Studierende für Mandoline, das sind ja leider generell eher wenige. Aber es klingt, als ob das Studium die richtige Entscheidung für dich war? In welchem Semester bist du denn inzwischen?**

A.P.: Oh ja, das Studium ist so eine prägende und ereignisreiche Zeit. Ich habe mit dem künstlerischen Studium „Bachelor of Music“ mit Hauptfach Mandoline an der HfMT Köln/Standort Wuppertal begonnen und bin jetzt im 5.Semester. Zwischenzeitlich habe ich parallel das Studium zur Instrumentalpädagogik begonnen bei dem ich mich im 3. Semester befinde. Der Zusammenhalt beim Studium ist toll. Statt extremer Konkurrenz überwiegt die Gemeinschaft und die gegenseitige Unterstützung. Man kann so vieles und auch sich selbst ausprobieren, Erfahrungen sammeln, Kontakte knüpfen, … Meine Bandbreite reicht aktuell von der Barockmandoline bis hin zum Jazz.

**Bleibt dir daneben überhaupt noch Zeit für Hobbys und ein Leben neben der Musik?**

A. P.: Diese Zeit muss es natürlich auch geben. Schwimmen ist für mich ein sportlicher Ausgleich neben dem Studium, üben, Konzerten etc. Das ist einfach notwendig und tut gut. Ballett habe ich schon als Kind getanzt. Das ist in den letzten Jahren aus zeitlichen Gründen leider in den Hintergrund geraten. Aber ich möchte es jetzt auch in Wuppertal wieder aufnehmen.

**Wie geht es mit deinem Studium die nächsten Jahre weiter?**

A. P.: In 1,5 Jahren möchte ich mit dem Bachelor künstlerisch abschließen und dann für ein Semester ins Ausland gehen. Da ist Japan mein Traumland. Die Mandoline ist dort verbreitet, aber doch ganz anders als bei uns. Danach werde ich noch 2 Semester bis zum pädagogischen BA benötigen. Und dann steht auch die Aufnahmeprüfung für das künstlerische Masterstudium an.

**Wir werden bestimmt noch einiges von dir hören. Was ist denn in nächster Zeit geplant?**

A. P.: Ende 2023 ist nochmal ein Solokonzert mit dem Patentorchester München vorgesehen. Im Januar, Juni und September spiele ich mit dem BLJZO das Hummel-Solokonzert. Davor darf ich am 23.03. in der Galerie Haubs in einem Förderkonzert der Münchner Konzertgesellschaft solistisch auftreten. Dann gab es im Dezember noch eine Aufzeichnung mit BR Klassik Plus zum Instrument des Jahres. Das wird am 03.03.2023 ausgestrahlt werden.

**Da steht ja wirklich einiges an. Spielst du denn lieber Solo oder im bzw. mit Orchestern?**

A. P.: Ich spiele beides sehr gerne. Es ist einerseits das große Klangerlebnis, das Verknüpfen mit anderen Instrumenten und Orchestern. Zeigen, dass eine Mandoline oder auch eine Gitarre kein Schattendasein führen müssen. Oder im Kontrast dazu, die solistische Mandoline, ihre volle Klangwelt zeigen und auch herausfordern zu können.

**Liebe Antonia, ich bedanke mich, dass du dir so viel Zeit genommen hast und mit dieser großen Begeisterung und Freude erzählt hast. Man spürt das Herz und Engagement, dass hinter der Mandoline, der Musik und dem Studium liegen. Ich wünsche dir alles Gute für das weitere Studium und hoffe noch viel von dir hören zu können.**

Das Interview führte Karina Hümpfner.