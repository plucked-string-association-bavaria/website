---
layout: blog
title: Ehrung für Petra Breitenbach
date: 2018-06-15T19:46:25.054Z
---

\[Bruchsal/Lohr a. Main] Im Rahmen des BDZ Eurofestival 2018 der Zupfmusik in Bruchsal wurde Petra Breitenbach in Würdigung Ihrer herausragenden Verdienste um die Förderung und den Einsatz für die Zupfmusik – insbesondere für Ihre mehr als 10-jährige Tätigkeit als Bayerische Musikleiterin – die Verdienstmedaille in Silber des Bund Deutscher Zupfmusiker verliehen.

![Das Bild zeigt Petra Breitenbach, Thomas Kronenberger, Präsident Bundes-BDZ, sowie Dominik Hackner.](/assets/uploads/zupfbote_2018_ehrung.jpg "Das Bild zeigt Petra Breitenbach, Thomas Kronenberger, Präsident Bundes-BDZ, sowie Dominik Hackner.")
