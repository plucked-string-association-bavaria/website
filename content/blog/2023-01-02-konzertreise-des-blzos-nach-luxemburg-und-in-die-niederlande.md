---
layout: blog
title: Konzertreise des BLZOs nach Luxemburg und in die Niederlande
date: 2004-06-13T18:13:56.096Z
---
Seine erste Auslandsreise unter Dirigent Dominik Hackner unternahm das [](http://www.blzo.de/)BLZO Anfang Juni. Ziel waren unsere westlichen Nachbarländer Luxemburg und die Niederlande. Nachdem von München bis in die Pfalz Spieler und Dirigent eingesammelt waren, fuhr der Tour-Bus zunächst nach Luxemburg-Stadt. Dort hatte man ein paar Stunden Gelegenheit, sich die Beine zu vertreten und die Stadt zu besichtigen. Besonders sehenswert war der Stadtteil Grund, der in einer tiefen Senke liegt und den man über unzählige Treppenstufen oder auch per Aufzug erreicht.

![](/assets/uploads/lux1.jpg)

Nach diesem Ausflug ging es weiter zur Unterkunft nach Larochette, wo eine mittelalterliche Doppelburg gleich alle Blicke auf sich zog. Das dortige Jugendgästehaus kann wärmstens empfohlen werden, da es ruhige und helle Zimmer bietet und man im Freien essen kann. Ein großer Spielplatz lädt auch mitreisende Kinder ein. So genoss man den Abend in entspannter Atmosphäre und bei (noch) schönem Wetter.

Am nächsten Morgen ging es aber erst einmal an die Arbeit. Das anspruchsvolle Programm mit Werken von Stamitz, Hansen, Erdmann, Gluck, Wengler, de Falla und Hackner erhielt im Kaminzimmer inmitten von Designermöbeln seinen letzten Schliff. Als man sich nach dem Mittagessen auf den Weg zum Schwimmbad von Larochette machen wollte, kam der große Wetterumschwung. Innerhalb von Minuten bezog sich der Himmel mit dunklen Wolken und es prasselten Regen und Hagel herunter. Leider begleitete diese Wetterlage das Orchester auch während der nächsten Tage.

Zunächst bleiben wir aber in Luxemburg, wo es am Abend nach Esch-sur-Alzette, eine Stadt im Südwesten des Landes, ging. Hier, fast an der Grenze zu Frankreich, war das BLZO einer Einladung von [Juan Carlos Muñoz](http://www.artemandoline.com/) und des Ensemble à Plectre Municipal d’Esch-sur-Alzette gefolgt und gab ein gelungenes Konzert in einer hellen, festlich anmutenden Kirche. Das zahlreich erschienene Publikum bedankte sich beim Orchester mit warmem, herzlichem Applaus. Ehrengast war Marcel Wengler, der sich die Interpretation seiner *Konstellationen* persönlich anhörte.

![](/assets/uploads/lux2.jpg)

Nach dem Konzert wurde das BLZO in die Probenräume des Gastgeberorchesters zu Speis und Trank mit luxemburgischen Spezialitäten eingeladen. Besonders die Pflaumen-Tarte und das Erdbeer-Tiramisù blieben lange Zeit im Geschmacksgedächtnis.

Am nächsten Tag ging es mit dem Bus durch Belgien in die Niederlande, wo man zunächst in Maastricht Station machte. Bei einer 90-minütigen Stadtführung erfuhr man Einiges zur Geschichte und zu den Sehenswürdigkeiten der Stadt. Vorherrschende Farbe – wie überall in Holland – war orange. Die beginnende Fußball-EM warf ihren farbenfrohen Schatten voraus. So war es nicht verwunderlich, dass der bayerische Bus mit dem deutschen Nummernschild immer wieder Zielscheibe verschiedener „Deutschland – Deutschland – alles ist vorbei“-Rufe in holländischem Akzent war. Abgesehen davon konnte man sich in Maastricht gut aufhalten. Hübsche Geschäfte luden zum Bummeln ein und sogar der Regen machte während der Besichtigung eine Pause. Am Abend kam das Orchester in Kerkrade an, wo es seine Unterkunft für die nächsten zwei Tage bezog. Besonders erstaunt war man über die Neue Straße bzw. Nieuwstraat, die genau auf der Grenze zu Deutschland liegt. Auf der einen Straßenseite war man in Holland, mit niederländischen Hausnummern und Autokennzeichen, direkt gegenüber auf der anderen Straßenseite in Aachen, mit den entsprechenden deutschen Schildern. Die Unterkunft in Kerkrade war so hellhörig, dass einige Spieler des BLZO eine Orchesterprobe der besonderen Art abhielten: Jeder blieb in seinem Zimmer, machte die Tür zu und spielte *Danza Espagnola* von de Falla. Und tatsächlich kam beinahe ein guter Orchesterklang dabei heraus. Dominik Hackner beschloss dann aber trotzdem, die nächste Probe im dafür vorgesehenen Gemeinschaftsraum abzuhalten.

![](/assets/uploads/lux3.jpg)

Entsprechend gut vorbereitet begab man sich zunächst nach Valkenberg, wo das holländische Gastgeberorchester um Annemie Hermans eine interessante Höhlenführung organisiert hatte. Danach fuhr man nach Stein, wo man mit dem Orchester The Strings ein gemeinsames Konzert bestritt.

![](/assets/uploads/lux4.jpg)

Da Aachen nicht weit weg war, konnte man sich auch hier über prominenten Besuch in der Person von Marga Wilden-Hüsgen freuen. Auch Verleger Theo Hüsgen fand den Weg nach Stein, um die bayerischen Zupfer zu hören. Nach dem Auftritt waren sich alle einig, dass man eine ganz besondere Leistung vollbracht hatte. Die Stimmung war hervorragend, die Akustik perfekt und das BLZO hatte das Beste aus sich heraus geholt.

Der Abend klang bei vielen, wenn auch kleinen, Gläschen Gerstensaft und netten Gesprächen mit den Gastgebern aus. Zurück in Kerkrade wurde dann nochmals kräftig mit Sekt angestoßen. Zum einen, weil Mirko Schrader seinen Ausstand als Gitarrendozent im BLZO gab und zum Anderen, weil es insgesamt eine schöne Reise war.