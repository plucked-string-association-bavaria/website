---
layout: blog
title: Gemeinschaftskonzert von BLZO und BLJZO
date: 2005-05-26T18:24:46.237Z
---
Nach zweijähriger "Vorbereitung" konnte endlich ein lang geplantes Vorhaben in die Tat umgesetzt werden. Die beiden Auswahlorchester des LV Bayern, Bayerisches LandesJugendZupfOrchester und Bayerisches Landeszupforchester hielten zusammen eine Arbeitstagung mit gemeinsamen Konzert ab. In den bayerischen Pfingstferien traf man sich in Kastell Windsor nahe Regensburg zum gemeinsamen Proben. Das BLJZO reiste bereits zwei Tage früher an und bereitete sich mit seinem künstlerischen Leiter Oliver Strömsdörfer auf das Konzert am 28. Mai vor; beide Orchester spielten jeweils 3 Werke alleine. Als dann das BLZO unter der Leitung von Dominik Hackner dazustieß, wurden vorrangig die von den beiden Dirigenten ausgewählten gemeinsamen Stücke geprobt. Dabei konnte der beim BLJZO als Dozent fungierende Mandolinist Silvan Wagner sein eigenes Werk "Suite Medieval" durch vorausgehende Erläuterungen ins richtige Licht setzen und Oliver Strömsdörfer die nun knapp 60 Musiker zu einer Einheit formen.

![](/assets/uploads/blzobljzo1.jpg)

So wurden auch im Konzert diese mittelalterlichen Melodien hervorragend vorgetragen. Dominik Hackner studierte mit den beiden Orchestern "traditionelle irakische Musik" ein: Das Werk "Sug Al-Safafir in Fall" des Komponisten Rami Al Regeb beschreibt mit westlichen Ausdrucksformen den Lärm der Kupferschmiede im Markt von Bagdad. Ergänzt mit wesentlichen Schlagwerkeinwürfen präsentierten sich die Musiker dabei spielfreudig und bestens vorbereitet. So konnte das Konzert zu einem Ohren- und Augenschmaus werden.

Es war schön zu sehen (und zu hören), dass es keine "Kleinen" und "Großen" gibt, sondern einfach Musizierende in verschiedenem Alter, die unter hervorragender Leitung der zwei Dirigenten zu einer Einheit verschmolzen. Einzig beim Fußballspielen ging dem BLZO gegen Ende "die Luft aus". Zur Pause noch 2:0 führend, mussten sich die Erwachsenen den Jugendlichen am Ende mit 2:3 geschlagen geben (fairness halber muss zugegeben werden, dass die Jugendlichen mit 14 Personen auf dem Platz standen, die Erwachsenen aber mit nur 9 Personen…). Die anderen Abenden wurden – dank des Wetters draußen im Gras sitzend – zusammen Lieder gesungen (natürlich mit Gitarrenbegleitung), oder über frühere Zeiten geplaudert; immerhin spielten viele der heute im BLZO Mitwirkenden vor einigen Jahren selbst im Jugendorchester. Eine gelungene Arbeitsphase, die Lust auf mehr macht und bestimmt eine Fortsetzung finden wird.

![](/assets/uploads/blzobljzo2.jpg)