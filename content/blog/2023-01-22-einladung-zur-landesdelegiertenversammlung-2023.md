---
layout: blog
title: Einladung zur Landesdelegiertenversammlung 2023
date: 2023-01-22T17:57:58.383Z
attachment: /assets/uploads/zupfbote_2023_satzungsaenderung.pdf
---
Sehr geehrte Mitglieder,

Hiermit lade ich ein zur ordentlichen Landesdelegiertenversammlung am **Sonntag, den 19.02.2023 von 13:30 bis 15:30 Uhr**. Da die Corona-Sonderregelungen für Mitgliederversammlungen von Vereinen zum 1.9.2022 ausgelaufen sind, wird die Landesdelegiertenversammlung gemäß § 32 BGB in Präsenz abgehalten, um auch beschlussfähig zu sein. Als Versammlungsort ist geplant das

Kath. Pfarramt St. Sebald Nürnberg-Altenfurt, Von-Soden-Straße 28, 90475 Nürnberg. Folgende Tagesordnung ist vorgesehen:

1. Begrüßung und Vorstellung der Tagesordnung (vorliegende Anträge)
2. Feststellung von Anwesenheit und Stimmkraft
3. Protokollgenehmigung
4. Berichte

   1. Tätigkeitsberichte der Präsidenten
   2. Bundesangelegenheiten
   3. Kassenbericht des Schatzmeisters
   4. Berichte aus der Geschäftsstelle
   5. Bericht der Musikleiterin
   6. Bericht der Jugendleiterin
   7. Rechnungsprüfungsbericht
5. Entlastung des Vorstandes
6. Satzungsänderung (siehe Anlage 1)
7. Berichte aus den Vereinen
8. Vorstellung und Planung von Aktivitäten 2023/2024
9. Mandoline – Instrument des Jahres 2023: Vorstellung der bisher geplanten Aktivitäten, Vorschläge der Mitglieder, Planung und Koordination weiterer Aktivitäten
10. Sonstiges (ohne Beschlüsse)

    Die Tagesordnung ist weitgehend inhaltsgleich zur virtuellen, nicht beschlussfähigen Landesdele- giertenversammlung vom 27.11.2022, so dass in vielen Fällen für die Berichte auf das Protokoll die- ser Versammlung verwiesen werden kann. Damit bleibt genug Zeit für die Punkte 6 bis 9.

    Anträge zur Tagesordnung bitte ich mir spätestens zwei Wochen vor der Versammlung in Textform, bevorzugt per E-Mail, zukommen zu lassen. Nach mehreren Jahren virtueller Versammlungen freue ich mich schon jetzt auf eine rege Teilnahme. Um mir eine gute zeitliche Planung der Sitzung zu erleichtern, bitte ich um Bestätigung der Teilnahme, Kontaktdaten siehe https://www.bdz-bayern.de/bdz/people.

    Mit freundlichem Gruß\
    Dr. Thomas Hammer, Präsident