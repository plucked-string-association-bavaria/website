---
layout: blog
title: Einladung zur kombinierten Mitgliederversammlung und Hauptausschusssitzung
date: 2018-09-15T19:38:42.776Z
attachment: /assets/uploads/zupfbote_2018_satzungsaenderung.pdf
---

Einladung zur kombinierten Mitgliederversammlung und Hauptausschusssitzung des BDZ Landesverbands Bayern e.V. am Sonntag, dem 21. Oktober 2018 um 13 Uhr im [](http://www.isarwinkel-geretsried.de)[Hotel Isarwinkel Geretsried](http://www.isarwinkel-geretsried.de).

Tagesordnung:

1. Begrüßung
2. Feststellung von Anwesenheit und Stimmkraft
3. Protokollgenehmigung
4. Tätigkeitsberichte der Präsidenten
5. Kassenbericht des Schatzmeisters
6. Bericht des GFs und aus der GSt
7. Bericht der Musikleiterin
8. Bundesangelegenheiten
9. Datenschutzgrundverordnung
10. Satzungsänderungen (siehe Anlage)
11. Landesmusikfest 2019
12. Vorliegende Anträge
13. Rechnungsprüfungsbericht
14. Entlastung des Präsidiums
15. Sonstiges

Diese Einladung zur Mitgliederversammlung entspricht § 12 unserer Satzung und ist somit rechtsgültig. Anträge zur Mitgliederversammlung sind spätestens zwei Wochen vor der Versammlung schriftlich beim Präsidenten einzureichen. Bitte informieren Sie die Geschäftsstelle per Post oder E-Mail IN JEDEM FALL darüber, ob Sie an der Versammlung teilnehmen oder nicht! Ich hoffe auf eine rege Beteiligung und wünsche schon jetzt eine gute Anreise.

Mit freundlichem Gruß\
Joachim Kaiser, Landesverbandspräsident
