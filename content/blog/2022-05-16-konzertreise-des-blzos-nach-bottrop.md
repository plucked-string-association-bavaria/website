---
layout: blog
title: Konzertreise des BLZOs nach Bottrop
date: 2009-11-03T20:29:04.050Z
---
von Thomas Hammer

Mitspieler des BLZO und Begleitpersonen hatten am Allerheiligen-Wochenende Gelegenheit, die angenehmen Seiten des Ruhrgebiets auf einer Konzertreise nach Bottrop kennen zu lernen. Die Initialzündung dafür erfolgte wohl in einem Gespräch zwischen der Musikleiterin des BDZ Landesverband Bayern e.V. Petra Breitenbach, dem Dirigent des BLZO Dominik Hackner und dem Präsidenten des BDZ-NRW e.V. Ingo Brzoska im zeitigen Frühjahr 2009.

 Für die Vorbereitung des musikalischen Teils der Reise – ein Matinee-Konzert des Zupforchesters Kirchellen unter Leitung von Ingo Brzoska und des Bayerischen Landeszupforchesters unter Leitung von Dominik Hackner – wurde eine Arbeitstagung in der Bayerischen Musikakademie Alteglofsheim angesetzt und durchgeführt. Nach anfangs verhaltener Resonanz lief die Organisation durch Daniel Ambarjan zu Hochtouren auf. Ein Bus wurde gechartert, die Anreise der aus verschiedenen Teilen Bayerns kommenden „Busfahrer“ geplant, Quartier im von außen futuristisch anmutenden Ruhrstadt Hostel & Hotel gebucht, und es wurde ein touristisches Rahmenprogramm auf die Beine gestellt. Das gros der Teilnehmer traf sich auf dem Parkplatz Talavera in Würzburg, wo sich dank Daniels telefonischer Nachfrage dann auch der gut ausgestattete Reisebus einfand. Weitere Teilnehmer der Fahrt wie der Präsident des BDZ LV Bayern e.V. Joachim Kaiser stiegen bei Hammelburg zu. Mit zwei weiteren „Boxenstops“ brachte uns der routinierte Fahrer („Ich heiße Norbert, man kann mit mir reden“) sicher und zügig an unser Ziel, das Ruhrstadt Hostel.

![Das Ruhrstadt Hostel](/assets/uploads/1.jpg "Das Ruhrstadt Hostel")

Motiviert durch die frühe Ankunft regte eine der dienstältesten Mitspielerinnen des BLZO (Elke B. aus L.) an, eine erste Probe durchzuführen, die auch unter kollektiver Leitung startete.\
Nachdem sich die anwesenden Teile des Orchesters – die erste Mandoline war stark unterbesetzt – tapfer und dank offener Ohren mit achtbarem Erfolg durch Teile des Konzertprogramms gearbeitet hatten, erschien unsere Konzertmeisterin Gertrud Weyhofen und brachte mit der Übernahme der Leitung einen anderen Wind in die Probe.

Ingo Brzoska setzte dem um 19 Uhr mit der Einladung zu einem gemütlichen Abendessen im Hotelrestaurant Charisma ein Ende: Dort war für uns bereits eingedeckt, und eine Reihe von Mitspielern des Zupforchesters Kirchhellen erwartete uns, inzwischen verstärkt durch weitere Mitspieler und unseren Dirigenten. Gestärkt durch ein Tischbuffet mit griechischen Speisen durften wir dann in einem Quiz über das Ruhrgebiet im Allgemeinen und Bottrop im Besonderen unser (Nicht-)wissen unter Beweis stellen. Unser Quizmaster Ingo hatte sich akribisch vorbereitet und konnte mit einer ganzen Reihe für uns unerwarteter und beeindruckender Fakten aufwarten, z.B. dass das Ruhrgebiet aufgrund des Bergbaus stellenweise um rund 20 m abgesackt ist.

Am Sonntagvormittag probten wir im Konzertsaal des August Everding Kulturzentrums unsere Beiträge zum Matinee Konzert, und die Kirchhellener hatten bei einer Gesamtprobe des „Song of Japanese Autumn“ von Yasuo Kuwahara Gelegenheit, sich auf Dominiks Interpretation dieses gemeinsam unter seiner Leitung aufgeführten Werkes einzustellen.

Ingo Brzoska setzte dem um 19 Uhr mit der Einladung zu einem gemütlichen Abendessen im Hotelrestaurant Charisma ein Ende: Dort war für uns bereits eingedeckt, und eine Reihe von Mitspielern des Zupforchesters Kirchhellen erwartete uns, inzwischen verstärkt durch weitere Mitspieler und unseren Dirigenten. Gestärkt durch ein Tischbuffet mit griechischen Speisen durften wir dann in einem Quiz über das Ruhrgebiet im Allgemeinen und Bottrop im Besonderen unser (Nicht-)Wissen unter Beweis stellen. Unser Quizmaster Ingo hatte sich akribisch vorbereitet und konnte mit einer ganzen Reihe für uns unerwarteter und beeindruckender Fakten aufwarten, z.B. dass das Ruhrgebiet aufgrund des Bergbaus stellenweise um rund 20 m abgesackt ist.

Am Sonntagvormittag probten wir im Konzertsaal des August Everding Kulturzentrums unsere Beiträge zum Matinee Konzert, und die Kirchhellener hatten bei einer Gesamtprobe des „Song of Japanese Autumn“ von Yasuo Kuwahara Gelegenheit, sich auf Dominiks Interpretation dieses gemeinsam unter seiner Leitung aufgeführten Werkes einzustellen.

Es folgte der sportlich-touristische Teil der Reise: 12 Uhr bis 12:30 Uhr Essen fassen in Eigenregie, Fahrt zum Hotel, Instrumente verstauen und einmal kurz durchschnaufen, Anfahrt zur Halde Beckstraße, Aufstieg auf die rund 80 m hohe Halde (über 350 Stufen waren zu bewältigen), für Schwindelfreie weiterer Aufstieg auf die noch einmal 42 m darüber gelegene Aussichtsplattform des Tetraeders (*das* Wahrzeichen von Bottrop).

![Das BLZO am Tetraeder in Bottrop](/assets/uploads/7.jpg "Das BLZO am Tetraeder in Bottrop")

Und dann ... erst einmal einen fantastischen Rundblick genießen – vom Alpincenter Bottrop mit der längsten Indoor-Skipiste der Welt im Vordergrund bis weit über die Schalke-Arena hinaus.

Mit der Besichtigung der Kohlenwäsche der Zeche Zollverein im Norden Essens stand um 15 Uhr bereits der nächste Punkt auf unserem Besichtigungsprogramm.

![Besichtigung der Zeche Zollverein in Essen](/assets/uploads/8.jpg "Besichtigung der Zeche Zollverein in Essen")

Um einem Erschlaffen der Wadenmuskulatur vorzubeugen, gab es auch hier Treppen zu steigen. Die hervorragende, gut zweistündige Führung vermittelte uns dabei ein sehr eindrückliches Bild der Arbeitsbedingungen* in dieser 1932 eröffneten Anlage mit dem vier Schächte ersetzenden Schacht 12, die einem damals völlig neuen, architektonischen Konzept folgend mit einer Förderleistung von bis zu 12.000 Tonnen täglich Maßstäbe für die nächsten Jahrzehnte setzte und seit der Stilllegung 1986 anschauliches Zeichen für den Strukturwandel im Ruhrgebiet, UNESCO Weltkulturerbe sowie Veranstaltungsort für Konzerte und Ausstellungen wurde – und natürlich der Ort, an dem Franken und Bayern eine für sie bis dahin weitgehend fremde Welt kennen lernen konnten.

Wer danach noch Energie übrig hatte, begab sich mit dem Bus zum CentrO in Oberhausen, Europas größtem Einkaufs- und Freizeitzentrum. Ausgezehrt von diesem Marathon fielen wir dann ins „Exil“, einem Türkischen Restaurant in Bottrop, ein, wo wir uns bei reichlich, vor allem aber sehr leckerem Essen von einem Buffet stärken, entspannen und nett unterhalten konnten. Auch hier sorgten die Kirchhellener persönlich dafür, dass wir uns wie zu Hause fühlten. Wenig zu beneiden war an diesem Abend unser Busfahrer, der auf der Fahrt zum „Exil“ nicht nur mit niedrigen Brücken sondern auch mit den mit einem eher rustikal anmutenden Halloween-Brauch – Eier werfenden Kindern – zu kämpfen hatte. Er hat’s mit Fassung ertragen! 

Am Sonntagmorgen schließlich kamen wir nach einer kurzen Anspielprobe und Ablaufplanung zum Höhepunkt und eigentlichen Zweck unserer Reise – dem Matinee-Konzert im August Everding Saal, das mit einem Publikum von rund 200 Personen erfreulich gut besucht war. Der erste Teil des abwechslungsreichen Konzerts wurde vom Zupforchester Kirchhellen unter Leitung von Ingo Brzoska gestaltet mit „Al Andalus“ für vier Gitarren und Zupforchester von Peter Brekau, „Veevalvaja (Aquarius)“ für zwei Gitarren und Zupforchester von dem estnischen Komponisten und Astronomieliebhaber Urmas Sisask, und dem 1. Satz aus Fried Walters „Ein kleines Violinkonzert“ mit Begleitung von Zupforchester und Blockflötengruppe. Die jugendlichen Solisten standen hier klar im Vordergrund und wurden für ihr musikalisches Spiel mit viel Applaus belohnt.

![Auftritt des Zupforchesters Kirchhellen](/assets/uploads/14.jpg "Auftritt des Zupforchesters Kirchhellen")

An Ansprachen durfte es bei diesem Anlass natürlich auch nicht fehlen: Zu Wort kamen unter anderem der Kulturdezernent der Stadt Bottrop und die Präsidenten der BDZ Landesverbände Nordrheinwestfalen und Bayern (auf die Wiedergabe der kurzweiligen Reden verzichte ich hier aus Platzgründen).

Der zweite Teil des Konzertes wurde vom BLZO mit Johann Sebastian Bachs „Concerto e-moll“ (nach BWV 1062) für 2 Mandolinen (Gertrud Weyhofen, Sabine Spath), 2 Gitarren (Jürgen Thiergärtner, Daniel Ambarjan) und Zupforchester in der Bearbeitung von Olaf Van Gonnissen barock eröffnet (Dominik Hackner hier als Continuo-Spieler am Cembalo).

![Auftritt des BLZOs](/assets/uploads/15.jpg "Auftritt des BLZOs")

Es ging unter der temperamentvollen Leitung von Dominik Hackner über die Romantik (Ugo Bottachiaris „Preludio Sinfonico“) zu zeitgenössischen Kompositionen des estnischen Komponisten Valdo Prema über („Sincerely“ und „Epigonics 2“). Den stimmungsvollen Abschluss des mit viel Applaus bedachten Konzertes bildete dann Yasuo Kuwaharas inzwischen zu den Zupfmusikklassikern zählende Komposition „The Song of Japanese Autumn“ in der gemeinsamen, von Dominik Hackner geleiteten Aufführung des Zupforchesters Kirchhellen und des BLZO.

Von den Kirchheller Spielern bewirtet mit Pizza „satt“ fuhren wir zurück nach Würzburg – Ankunft fast auf die Minute genau um 20 Uhr. Ein herzlicher Dank sei an dieser Stelle noch einmal ausgesprochen unserem Organisator Daniel für die tolle Durchführung der Konzertreise und die launigen Ansagen, der musikalischen Leitung, den Stimmführern und Solisten für professionellen musikalischen Beistand, unserem Präsidenten Joachim Kaiser für die vielfältige Unterstützung mit Wort und Digitalkamera, und ganz besonders den Kirchhellern mit ihrem musikalischen Leiter und Präsidenten Ingo Brzoska für die tolle Vorbereitung, den herzlichen Empfang, und vieles andres mehr.

\*Lärmbelastung 120 dB, Staubbelastung riesig, Lebenserwartung unter Tage 50 plus X; das Bild des letzten Grubenpferdes Mücke und seines Führers, dessen Alter wir durch die Bank 20 Jahre zu hoch schätzten, wird sicherlich vielen in Erinnerung bleiben.