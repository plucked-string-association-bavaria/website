---
layout: blog
title: "„Im Urwald da ist schwer was los, da klingt und tönt es ganz famos…“ "
date: 2024-03-13T07:18:26.816Z
attachment: null
---
Unter diesem Motto lud der Bund deutscher Zupfmusiker (BDZ) junge GitarristInnen am 02. und 03. März 2024 zum „Musik, Spiel und Spaß – ein Wochenende für Kinder“ ein. 

Für diesen Kinderkurs reisten 22 junge Gitarristen und Gitarristinnen aus den Gegenden um Schweinfurt, Volkach/Münsterschwarzach, Iphofen und Kitzingen in das Schullandheim nach Reichmannshausen an. Die sieben- bis zwölfjährigen MusikschülerInnen lernten zusammen mit den Dozentinnen Bianca Brand (Forst), Lukas Becker und Markus Joppich (beide Würzburg) verschiedene Melodien rund um das Thema Urwald kennen. Vor allem der Ohrwurm „Probier‘s mal mit Gemütlichkeit“ wurde von den Kindern gerne geübt und gespielt. Sie erhielten nicht nur Einzelunterricht auf ihrem Instrument, sondern sammelten auch Erfahrungen in kleinen Ensembles und beim Orchesterspiel.

![Unterricht mit Lukas Becker](/assets/uploads/bdz_kinderkurs_unterricht1.jpeg "Unterricht mit Lukas Becker")

![Unterricht mit Bianca Brand](/assets/uploads/bdz_kinderkurs_unterricht2-1.jpeg "Unterricht mit Bianca Brand")

![Unterricht mit Markus Joppich](/assets/uploads/bdz_kinderkurs_unterricht3.jpeg "UNterricht mit Markus Joppich")

Am Sonntag verwandelte sich die Turnhalle des Schullandheimes in einen Vorspielsaal. Dafür bastelten die Kinder während des Wochenendes mit Betreuerin Fiona Brand Urwald-Dekorationen als Bühnenbild und probten fleißig für ihren Auftritt. Eltern, Geschwister und Verwandte konnten beim Abschlussvorspiel am Sonntag um 15 Uhr bei „Tanz des Tigers“, „Affenrock“, „Ruhe im Urwald“ und „Tanz in der Coco-Disco-Bar“ über die musikalischen Darbietungen ihrer Kinder staunen und den Gitarrenklängen lauschen. Zu „Dance Monkey“ gab es einen kleinen Tanz, ehe die Zuhörer selbst bei „The lion sleeps tonight“ im Kanon mitsingen durften.

![Kinderkurs Reichmannshausen März 24](/assets/uploads/presse_bild_bdz_kinderkurs.jpg "Kinderkurs Reichmannshausen März 24")

Das Foto zeigt die teilnehmenden Kinder mit Kursleitung Bianca Brand (hinten 1. von links), Dozent Markus Joppich (hinten, 2.v.l.), Lukas Becker (hinten rechts außen) und Betreuerin Fiona Brand (hinten, 3. v. l.).

Man darf gespannt sein, welches Motto im nächsten Jahr auf dem Programm steht, wenn im März 2025 wieder zum musikalischen Wochenende für Kinder eingeladen wird.