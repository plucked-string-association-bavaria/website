---
layout: blog
title: Wochenendlehrgang „Spaß an Zupfmusik mit uHu“ des BDZ LV Bayern in der
  Bayerischen Musikakademie Hammelburg
date: 2021-11-30T15:24:11.140Z
attachment: ""
---
Seit 2012 wird in Bayern ein Lehrgang speziell für die Altersgruppe um 50+ angeboten: frei von Leistungsdruck und anderen Zwängen steht der Spaß an Zupfmusik im Vordergrund. In Form von Ensembles, Liedbegleitungs- und anderen Kleingruppen können sich die Teilnehmer ihre Lieblings-Betätigungsfelder mit ihrem Instrument auswählen und praktizieren.

Alle Beteiligte (Teilnehmer und die Dozenten Michael Diedrich und Johannes Tappert) kamen nach der Corona Zwangspause in 2020 wieder mit großer Freude angereist, wussten sie doch bereits, **was** sie erwarten würde – und **wen** sie wiedersehen konnten. Der „Versuch eines überregionalen Zupforchesters“ in dieser Altersgruppe ist zu einer festen Einrichtung mit besonderen Freundschaften geworden. Damit sich die „Mitvierziger“ vom Kurstitel und dem ZO nicht ausgegrenzt fühlen, bestand die Runde von Beginn an auf diesem Namen: „Spaß an Zupfmusik mit uHus“ (unter Hundert…)

In kleinen Ensembles wurden neue Stücke ausprobiert, im uHu-Zupforchester die 2019 begonnenen und dann mangels Probemöglichkeit ausgesetzten Werke wieder aufgefrischt und in der Liedbegleitung gab es Hintergrundinfos zu „Killing me softly“ von Roberta Flack bis Fugees.

Mit warming ups und technischen Tipps startete man gut in den Tag und genoss die Möglichkeit des gemeinsamen Musizierens und den Austausch nach der langen Pause. Da nahezu alle geimpft/geboostert sind, gibt es auch wieder Pläne für zwei Samstagsproben in 2022, bevor es von 18.-20. November zum 10-jährigen Jubiläum wieder in die Musikakademie geht.

Die uHus nehmen gerne neue Spieler auf - Interessierte können sich bei [Petra Breitenbach](https://newsite.bdz-bayern.de/bdz/people) melden.

Kosten ab 15 Teilnehmer: 18 € je Probe (Bei 10 – 15 TN 22 €/Probe).

![Ensembleprobe bei den uHus](/assets/uploads/ensemble-1a.jpg "Ensembleprobe bei den uHus")

![Ensembleprobe bei den uHus](/assets/uploads/ensemble-2a.jpg "Ensembleprobe bei den uHus")

![Zufriedene Dozenten der uHus](/assets/uploads/zufriedene-dozenten-a.jpg "Zufriedene Dozenten der uHus")

![Zufriedene Teilnehmer bei den uHus](/assets/uploads/zufriedene-teilnehmer-a.jpg "Zufriedene Teilnehmer bei den uHus")