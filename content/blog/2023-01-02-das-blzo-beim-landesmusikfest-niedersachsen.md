---
layout: blog
title: Das BLZO beim Landesmusikfest Niedersachsen
date: 2003-09-27T18:28:36.549Z
---
Als im Mai 2003 eine Anfrage aus Niedersachsen kam, ob das Bayerische Landeszupforchester beim dortigen Landesmusikfest im September spielen würde, konnte innerhalb von nur wenigen Tagen geklärt werden, daß sich nicht nur die Leitung des Orchesters (allen voran Dominik Hackner) sehr gerne dazu bereit erklärt, sondern auch sehr viele Spieler bereit waren, dort mitzuwirken. Also wurde ein Konzertprogramm ausgewählt, alle Teilnehmer angeschrieben, eine Kostenkalkulation aufgemacht, ein Hotel gesucht, Essen bestellt, ein Probenraum reserviert, telefoniert, gefaxt, gemailt... und dann war klar:
Wir fahren nach Hildesheim zum Landesmusikfest Niedersachsens, das am 27. und 28. September 2003 stattfinden sollte.

Sehr gut geplant und organisiert vom dortigen Vorstand wurden wir im Hildesheimer Kreishaus, im dem auch die Konzerte stattfinden sollten, aufs Freundlichste aufgenommen und begrüßt; hervorragend untergebracht waren wir im "Gollart's Hotel Deutsches Haus".

Der Festakt sah an den zwei Tagen drei Orchesterkonzerte vor, bei denen neben örtlichen Musikvereinen (wie der Mandolinen- und Lautenvereinigung Hildesheim e. V., die hiermit ihr 90-jähriges Bestehen feierten) das Zupforchester Niedersachsens (Traditionsorchester des Landesverbandes Niedersachsen im BDZ) und das Niedersächsische Landeszupforchester unter der Leitung des Landesvorsitzenden Ulrich Beck mitwirkten.

![](/assets/uploads/lmf2003-1.jpg)

Eine große Erwartungshaltung wurde uns gegenüber zu Tage gelegt:\
Wie im Vorwort der Festschrift zu lesen war, "wird der Auftritt des renommierten Bayerischen Landeszupforchesters mit international bekannten Solisten ein besonderes Bonbon sein" (Dr. U. Kumme, Oberbügermeister und Dr. K. Deufel, Oberstadtdiektor Hildesheim) und "das BLZO unser Landesmusikfest ganz erheblich bereichern und einen besonderen künstlerischen Höhepunkt setzen" (Vorstand des Landesverbandes Niedersachsen im BDZ).

Im Rahmen des 3. Orchesterkonzertes spielte das BLZO unter der Leitung vom Dominik Hackner "Preludio e Fuga" von Claudio Mandonico. Ein kurzweiliges Stück, das den Einstieg in das eigentliche Hauptwerk liefern sollte: Das Konzert für Gitarre und Zupforchester "Il sogno del pesciolino" von Eduardo Angulo. Das mittlerweile zum "Lieblingsstück" des Orchesters aufgestiegene Werk liefert einen Höhepunkt nach dem anderen. Zusammen mit dem international hervorragend angesehenen Solisten Michael Tröster konnte das Orchester in allen Belangen voll überzeugen und zeigte in den drei Sätzen "Allegro assai", "Andante" und "Allegro vivace" Zupfmusik vom Feinsten. Dominik Hackner hatte die Spieler "voll im Griff" und konnte gerade im letzten Satz einen Sturm entfachen, der beim Orchester und dem bestens aufgelegten Solisten, unterstützt von Bastian Brand am Schlagwerk, in einem Feuerwerk der Zupfmusik gipfelte.\
Begeisterte Zuhörer applaudierten minutenlang. Standing ovations war der Lohn für einen kräftezerrendes, aber herrliches Konzerterlebnis nicht nur für die Zuhörer, sondern auch für die Spieler und Spielerinnen.

![](/assets/uploads/lmf2003-2.jpg)