---
layout: blog
title: "Die 100. Arbeitstagung: Das BLZO beim Eurofestival in Bruchsal"
date: 2010-06-07T19:25:13.902Z
---
von Svenja Schlieckau, Michaela Schroll und Karina Hümpfner

So lange haben wir darauf  hingearbeitet, nun ist es soweit: Bei Dominiks letztem Konzert mit uns sollte nun „Raidoh“ von Yasuo Kuwahara im Rahmen des Eurofestivals in Bruchsal aufgeführt werden.\
Die meisten Orchestermitglieder sind schon am Mittwoch Abend angereist, da am Donnerstag in der Früh um 9 Uhr bereits die Probe in unserem Quartier stattfand. Erst am Donnerstag anzureisen erschien vielen zu knapp, denn bei Unpünktlichkeit wurde mit gnadenlosem Entzug der Essensmarken gedroht (es konnte ja noch keiner ahnen, dass der Verlust gar nicht so groß sein würde) ...\
Unser Chef-Organisator Daniel hat sich am Abend für uns stundenlang an der Rezeption aufgeopfert, um mit der Rezeptionistin („dem Mädle von drüben“) die Zimmer  zu verteilen. Trotz riesigem Loch im Magen, das Daniel ignorierte, wurde die Zimmerbelegung diskutiert, auf dem Plan bunt angestrichen, durchgestrichen, verworfen und dann wieder dazugeschrieben. Erst dann stand die endgültige Zimmeraufteilung fest.

In der Probe am Donnerstag wurden noch mal alle Ungereimtheiten des Stückes aus dem Weg geräumt, um für das Konzert am Abend gerüstet zu sein. Für den Dirigenten schien das doch mit sehr viel Anstrengung verbunden, sodass bereits um 10 Uhr der erste T-Shirt-Wechsel erfolgte...open end...\
Bis zum leichtverdaulichen Abendessen im Catering-Zelt um 16:45 Uhr (!) durften wir uns auf dem Festivalgelände vergnügen. Um 17:30 Uhr hatten wir dann unsere Anspielprobe im Rechtenbergsaal. Um etwa 20:50 Uhr wurden wir von der Garderobe hinter die Bühne gebracht, um dort auf unseren Auftritt zu warten. Vielen (vor allem den Vegetariern) knurrte nach dem sehr frühen und nicht sehr üppigem Abendessen der Magen, was noch durch die herumstehenden Brezen verstärkt wurde... Könnte man mit den Augen essen, der Karton wäre im Nu vertilgt gewesen...

Unser Konzert war dann ein voller Erfolg. Das Publikum tobte, vor allem wegen Gertruds hervorragender solistischer Leistung. Mit Sicherheit auch deswegen, weil Dominik uns nicht nur in musikalischer Hinsicht aus der Patsche helfen kann, sondern auch immer ein Hustenbonbon für seine halb erstickenden Spieler in seiner Tasche bereit hält...

Nach dem Konzert verbrachten wir noch einen sehr feucht-fröhlichen Abend in unserem Hotel. An dieser Stelle vielen Dank an Sascha, unserem Barkeeper, der uns drei Abende lang anstandslos ertragen hat.

Nach einer kurzen Nacht startete der Freitag für uns BLZO'ler mit einem gemütlichen Frühstück. Anschließend fuhren die meisten Orchestermitglieder nach Bruchsal, um sich die Oper „Blond, entführt-gerettet!?“ anzuschauen. Die Geschichte der Oper ist an „Die Entführung aus dem Serail“ angelehnt. Wer das nicht gesehen hat, hat wirklich was verpasst! Der restliche Tag war wieder gefüllt mit Konzerten, und auch unseren „Türsteherdiensten“ in tollen gelben T-Shirts. Am Abend fand die „Zupfer-Night“ im Schloss statt. Leider gab es einige Saal- und Programmänderungen, sodass man nur durch Zufall in den richtigen Konzerten landete...\
Auch dieser Abend klang wieder feucht-fröhlich bei Sascha in der Bar aus. 

Am Samstag besuchten wir wieder fleißig Konzerte. Nachmittags spielte das Niedersächsische LZO mit Dominik am Kontrabass, was sich viele von uns natürlich nicht entgehen ließen...\
Als abendliches Abschlusskonzert wurden nochmal richtige „Schmankerl“ geboten. Das Mülheimer ZO spielte mit Mirko Schrader als Solist „Vögel, Früchte und Wind“ von Herbert Baumann. Es dauerte einige Zeit, bis sich Mirko auf der Bühne eingerichtet hatte: Die Hosenbeine und Hemdsärmel wurden gerichtet, der Fußschemel passte nicht gleich, und zu guter Letzt mussten noch die Fingernägel mit Stirnschweiß benetzt werden, um den besten Ton erzeugen zu können. Damit hatte Mirko schon einige Lacher auf seiner Seite. Nichtsdestotrotz war es eine super solistische Leistung von Mirko mit dem Mülheimer ZO.

Bei der „C.P.O. Rhapsodie“ von Stefano Squarzina schlich sich (fast unbemerkt im knallig-gelben Türsteher-T-Shirt) Mirko wieder auf die Bühne, um keck mit den shaking-eggs das Stück percussionistisch zu untermalen.\
Als einer der Höhepunkte des Festivals aus unserer Sicht spielte noch das Hessische ZO vereinigt mit dem Kubota Philomandolinen Orchester. Der Klang dieser vielen Musiker war gigantisch!

Nach dem Konzert fand die Zupfer-Party in der Innenstadt statt. Zapfi holte eine Runde Bier aus dem Getümmel. Danach trafen wir uns wieder im Hotel und tranken in netter Runde den einen oder anderen Sekt, den wir vorher in einer weiblichen Sektdurst-Kauflust erworben hatten. Aber praktischerweise hatten wir gleich einen Sekt da, um auf Iris’ Geburtstag anzustoßen! Der wurde auch noch mit einem Ständchen besungen.\
Und schon war es wieder Sonntag, das Eurofestival ging  mit einem Matinee- Konzert zu Ende.

An dieser Stelle bedanken wir uns noch mal ganz herzlich bei Dominik für die tolle Zeit! Das Wochenende in Bruchsal war ein super-schöner Abschluss! 

DANKE – für Deine Geduld bei den Proben\
DANKE – für Dein langjähriges Engagement\
DANKE – für die lustigen Abende\
DANKE – für die schönen Konzerte\
DANKE – für die schönen Reisen\
DANKE - FÜR ALLES, DOMINIK!!!