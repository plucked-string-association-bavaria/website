---
layout: blog
title: Das BLZO beim Bundesmusikfest - "eurofestival zupfmusik 2002"
date: 2002-05-31T18:32:24.004Z
---
Von Donnerstag, den 30. Mai, bis Sonntag, den 2. Juni 2002 fand in Friedrichshafen am Bodensee das "Eurofestival Zupfmusik 2002" statt.

Wunderbar gelegen und bestens organisiert konnte bei herrlichem Wetter Konzerten gelauscht werden, Vorträge und Ausstellungen besucht werden. Das Graf-Zeppelin-Haus bot dabei ein erstklassiges Ambiente. Ca. 1000 aktive Teilnehmer, Spitzenensembles und Solisten aus Deutschland, Frankreich, Italien, Luxemburg, Österreich, Russland, Kroatien, den USA und Japan (und vielen mehr) überzeugten in den insgesamt 14 Konzerten mit barocken und klassischen Werken, romantisch-virtuoser Musik und vor allem zeitgenössischer Musik, aber auch Folklore und Blues bis hin zur Tangomusik.

![](/assets/uploads/eurofestival-2002-1.jpg)

Die dabei am Abend in lockerer Atmosphäre durchgeführten "Nachtcafés" gaben dem Festival eine runden Tagesausklang.

Auch das Bayerische Landeszupforchester wirkte dabei in einem der Konzerte mit. Zusammen mit drei weiteren Orchestern wurde das Konzert am Donnerstag abend um 20:00 Uhr gestaltet.

![](/assets/uploads/eurofestival-2002-2.gif)

Nachdem die Spieler/Innen schon am Mittwoch abend nach Friedrichshafen angereist waren und am nächsten Tag gestärkt durch ein hervorragendes Frühstücksbuffet im Hotel eine kurze (General-)Probe absolvierten, waren sie bestens fürs Konzert gerüstet. Ein 20-minütiges Einspielen auf der Bühne musste am Nachmittag genügen, um Spannung aufzubauen und Konzertatmosphäre zu schnuppern.\
Beim Konzert legten die Spieler/Innen aber dann richtig los: Unter dem Dirigat von Dominik Hackner überzeugte sowohl das Orchester als auch der Solist in vollen Zügen. Mirko Schrader an der Sologitarre stellte in Eduardo Angulos "Il sogno del pesciolino" seine ganze Virtuosität unter Beweis. Mit warmer Tongebung und excellenter Musikalität kombiniert mit hervorragender Technik ließ er seine Gitarre von dem Fisch "Alevin"erzählen, einem Meeresbewohner der Küsten Mexikos. Ob atmosphärisch die Klangwelt des tiefen Ozeans darstellend oder den Ausflug des Fischleins in turbulente Gewässer, das Orchester unterstützte des Solisten vortrefflich und erntete dafür langanhaltenden Applaus. Standing ovations und ein nicht endendes Kommen und Gehen des Dirigenten und des Solisten (Dominik und Mirko kamen fünf mal zum Verbeugen hinter der Bühne hervor) zeigten den wahrhaft gelungenen Auftritt des Bayerischen Auswahlorchesters.

![](/assets/uploads/eurofestival-2002-3.jpg)

An den folgenden drei Tagen des Festivals konnten die Spieler/Innen des BLZO die Konzerte, das Wetter und die Umgebung geniessen und verbrachten so schöne Tage in Friedrichshafen.

Eine Bootsfahrt auf dem Bodensee, viele Besuche von Eisdielen oder Cafes, der Besuch des Zeppelin-Museums und des Affenbergs Salem oder einfach nur gemütliches Beisammensitzen auf den Grünanlagen des Graf-Zeppelin-Hauses waren nur einige "Freizeitbeschäftigungen", die sich die Spieler/Innen zwischen den Konzertbesuchen einfallen ließen.

Rundum ein schönes Festival mit einem bestens gelungenen Konzert und bester Stimmung läßt dieses Event zu einem unvergesslichen Aufenthalt am Bodensee werden.