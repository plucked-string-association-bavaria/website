---
layout: blog
title: Dirigentenwechsel und Konzertdebut im Vivaldi-Orchester Karlsfeld
date: 2024-04-24T08:56:28.942Z
---
***In 99 Minuten um die Welt***



**Jahreskonzert des Vivaldi-Orchesters Karlsfeld am 08. Juni 2024,19:30**

 

Fast genau ein Jahr nachdem sich die Gründerin und langjährige Dirigentin des Vivaldi-Orchesters, Monika Fuchs Warmhold, mit einem festlichen Konzert von ihrem Karlsfelder Publikum verabschiedet hat, freuen wir uns auf  die Premiere mit unserem neuen Dirigenten Heiko Holzknecht. 

 

![Dirigent Heiko Holzknecht](/assets/uploads/f89ab635-cfd4-4fa4-8c48-da98dd15aaff.jpeg "Dirigent Heiko Holzknecht")

Unsere Weltreise beginnt mit einer temperamentvollen „Nacht in Buenos Aires“ von Andreas Lorson. Mit „Daintree“ von Richard Charlton lassen wir den Regenwald und seine tierischen Bewohner lebendig werden und Kollaps, ein weiteres Stück von Andreas Lorson, wird ein Wechselbad der Gefühle auslösen.  

 

Das  „Konzert für 4 Mandolinen und Zupforchester“ unseres Namenspatrons Antonio Vivaldi führt uns zurück ins Europa des 18. Jahrhunderts und mit „Look into my Soul“ spielen wir ein ausdrucksstarkes „Minikonzert“ für Mandoline und Zupforchester des kalifornischen Komponisten und Gitarristen Chris Aquavella. Beide Stücke bestreiten SolistInnen aus den eigenen Reihen.

 

Auch „Pirates“, ein aufregendes Medley von Filmmusiken des Oscar - gekrönten Komponisten Hans Zimmer, entführt das Publikum in verschiedene Weltregionen.

 

„Sketchbook“ von Dominik Hackner birgt skurrile Eindrücke von der Beerdigung eines nicht ganz „Ehrenwerten“ und ein Wiegenlied für eine blinde Katze. Der  japanische Komponist Takashi Kubota zeigt mit der temperamentvollen „Tanzsuite Nr. 2“ seine Begeisterung für europäische Musikstile.

![Vivaldi-Orchester Karlsfeld](/assets/uploads/a5678d4b-a142-4ad7-981c-c1b904b96c4d.jpeg "Vivaldi-Orchester Karlsfeld")

 

Und last but not least machen wir mit „Nothing else matters“ der US-amerikanischen Heavy Metal-Band Metallica, wie gewohnt auch einen Ausflug in die Rockmusik.

 

Unsere Jüngsten besuchen unter der Leitung von Nadezhda Pantina die „Höhle des Bergkönigs“ (Edward Grieg) und spielen ein Wiegenlied für eine Eidechse – „Lizard’s Lullaby“ von Marlo Strauß.

 

**Das Konzert findet statt im Bürgerhaus Karlsfeld, Allacherstraße 1, 85757 Karlsfeld, Einlass ist ab 19:00 Uhr. Tickets gibt es im Vorverkauf ab 29.04.2024 bei München Ticket**