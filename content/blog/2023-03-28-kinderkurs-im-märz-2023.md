---
layout: blog
title: Kinderkurs im März 2023
date: 2023-03-28T19:05:02.340Z
---
 **„Lieder und Songs zum Feiern…“** 



Unter diesem Motto lud der Bund deutscher Zupfmusiker (BDZ) junge GitarristInnen und MandolinistInnen am 10. und 11. März 2023 zum „Musik, Spiel und Spaß – ein Wochenende für Kinder“ ein. 

Für diesen Kinderkurs reisten 12 junge Gitarristen und Gitarristinnen aus den Gegenden um Schweinfurt, Volkach/Münsterschwarzach, Würzburg und Kitzingen in das Schullandheim nach Schaippach (Gemünden) an. 

Die acht- bis zwölfjährigen MusikschülerInnen lernten zusammen mit den Dozentinnen Bianca Brand (Forst) und Rosa Faerber (Würzburg, Kursleitung) verschiedene Melodien rum um das Thema kennen. Die jungen Gitarristinnen und Gitarristen spielten in kleinen Ensembleformationen. Doch nicht nur Ensemblespiel stand auf dem Wochenendplan: Gitarrenunterricht erhielten die Kinder in einer Einzelstunde, sie sammelten ihre ersten Erfahrungen im Orchesterspiel und lernten ein neues Instrument kennen: Die Mandoline. Das Instrument des Jahres 2023 spielten alle Kinder unter Anleitung von Bianca Brand, sodass beim Abschlussvorspiel alle Kinder auch ihre neu erlernten Fähigkeiten auf der Mandoline präsentieren konnten. Betreuerin Fiona Brand bastelte mit den Kindern während ihrer Pausen Armbänder, malte und spielte mit ihnen. Ein bunter Abend mit Spielen ließ den ersten Tag des Kurses ausklingen. 

Am Sonntag um 16 Uhr war es dann so weit: Der Saal des Schullandheims verwandelte sich in einen Konzertsaal und die Stühle reichten kaum für das Publikum aus: Eltern, Großeltern, Geschwister, Freunde und andere Familienmitglieder der jungen MusikerInnen kamen zum Zuhören. In den Ensembles präsentierten die Kinder stolz, was sie in eineinhalb Tagen erarbeitet hatten. Die Mandolinen begleiteten Lieder zum Mitsingen, sodass das Publikum engagiert mitsang und bei den Liedern „If you’re happy“ und beim „Körperteilblues“ freudig mitbewegten. Auch die Dozentinnen und Betreuerin Fiona Brand hatten Lust, beim Abschlussvorspiel zu musizieren und spielten „Maniac“ aus dem Film Flashdance in der Besetzung Kontrabass, Gitarre und Mandoline als Überraschungsprogrammpunkt. Zum Schluss erklangen dann alle 13 Gitarren plus Kontrabass gleichzeitig im Orchester mit der bekannten Melodie von Vivaldis Frühling aus den Vier Jahreszeiten. Als zweites Orchesterstück erklang „Die Hände zum Himmel“ und sowohl Kinder als auch Eltern sangen gemeinsam zum Abschluss. Mit eifrigem Applaus ging der musikalische Kinderkurs zu Ende und leitete die Vorfreude auf den Kurs im kommenden Jahr (am 02./03. März 2024 in Reichmannshausen) ein. 

Einen herzlichen Dank an alle Eltern und Kinder, an das Schullandheim Schaippach und die Dozentinnen und die Betreuerin. Des Weiteren ein besonderer Dank an die Kulturstiftung des Bezirks Unterfranken und den Bayerischen Staat, welche diesen Kinderkurs fördern. 

*Rosa Färber*

![Das Foto zeigt die Teilnehmenden mit Betreuerin Fiona Brand und den Dozentinnen Rosa Faerber und Bianca Brand (hintere Reihe v.l.n.r.).](/assets/uploads/kiku2023-gruppe_reichmhs.jpeg "Das Foto zeigt die Teilnehmenden mit Betreuerin Fiona Brand und den Dozentinnen Rosa Faerber und Bianca Brand (hintere Reihe v.l.n.r.).")