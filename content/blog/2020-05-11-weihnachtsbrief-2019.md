---
layout: blog
title: Weihnachtsbrief 2019
date: 2019-12-24T20:14:32.411Z
---

#### Liebe Zupfmusikerinnen und Zupfmusiker in Bayern,

zum Jahresabschluss möchte ich Sie wie auch bereits in den Vorjahren über einige Ereignisse des abgelaufenen Jahres informieren und auf noch Kommendes hinweisen.

Vor allem aber danke ich Ihnen zuvor ganz herzlich für Ihr Engagement innerhalb der Zupfmusik in Bayern. Mit Ihrem Willen zur Weiterbildung an Ihrem Instrument und in der Theorie sowie Ihrem Einsatz in der Vorbereitung und Durchführung musikalischer Darbietungen gelingt die positive Weiterentwicklung der Zupfmusik in Bayern und darüber hinaus.

Bei der Bundeshauptausschusssitzung am 9. März 2019 in Ottweiler/Saar wurde Frederic Lederle als neuer Schatzmeister gefunden und auch per Wahl bestätigt. Thema war dort auch das Euro-Festival, das sich weiterhin bewährt hat. Wunschgemäß wird es beim nächsten Mal einige Änderungen geben. So wird die Organisation der Teilnehmerbeiträge ohne eine Erhöhung einhergehen, damit das Problem, dass viele sehr spät planen, entschärft wird. Außerdem soll der Preis der Einzelfotos vermindert und Videos sollen beim nächsten Festival von allen Ensembles gemacht werden.

Eine sehr eindrucksvolle und sehr erfolgreiche Veranstaltung war das 10. Landesmusikfest unseres Landesverbandes. Sie fand vom 8. bis zum 10. November 2019 in der Bayerischen Musikakademie Schloss Alteglofsheim statt. Die Schirmherrschaft hatte dankenswerterweise der Präsident des Bayerischen Musikrates, Herr Dr. Thomas Goppel, übernommen, der auch die Festrede hielt. Das mit Instrumentenausstellungen, Workshops und vielen weiteren interessanten Programmpunkten angefüllte Wochenende hatte auch zum Ziel, die Leistungsfähigkeit der bayerischen Gitarristen und Mandolinisten deutlich zu machen. Und 13 Zupfmusikensembles mit weit über 100 Laienmusikern aus allen Teilen Bayerns überzeugten in einer Vielzahl von Konzerten. So wurde das Landesmusikfest zu einem vollen Erfolg. Besten Dank sage ich nochmals den Organisatoren Bianca Brand, Christine Heinz, Peter Kroiß und Petra Breitenbach.

Beim 10. Bayerischen Orchesterwettbewerb am 15. und 16. November 2019, erfreulicherweise wieder in den Studios des Bayerischen Rundfunks in München, hat das Ensemble Roggenstein unter der Leitung von Oliver Kälberer mit hervorragenden 23,6 Punkten die Weiterleitung zum Deutschen Bundeswettbewerb im Mai 2020 in Bonn erreicht. Außerdem erhielten diese Weiterleitung zum Deutschen Orchesterwettbewerb, neben sieben weiteren Ensembles, El Polifemo unter der Leitung von Johannes Stickroth mit 22 Punkten und das Friedberger Jugendgitarrenorchester unter der Leitung von Stefan Schmidt mit 23,3 Punkten. Allen drei Zupf-Ensembles gratuliere ich ganz, ganz herzlich zu ihren Erfolgen und wünsche ihnen in Bonn ähnlich herausragende Ergebnisse!

Im Jahr 2019 gab es mehrere Ehrungen zu feiern. So erhielten am 19. Juli im Rahmen eines Symphoniekonzertes in der Stadthalle Lohr Bettina Schneider, Karla Jenuwein und Petra Breitenbach die Goldene Ehrennadel für ihr 40-jähriges engagiertes Wirken im BDZ. Im Rahmen unseres Landesmusikfestes erhielten Thomas Zapf für seine erfolgreiche Tätigkeit als Landesschatzmeister und Karla Jenuwein für ihre langjährige Leitung der Geschäftsstelle und des Notenarchivs die Ehrenmedaille des BDZ in Bronze. Und ich durfte mich an gleicher Stelle sehr über den überraschenden Erhalt der Verdienstmedaille in Silber freuen.

Am 7. März nächsten Jahres findet um 14 Uhr in Ottweiler/Saar die BDZ-Mitglieder-/Delegiertenversammlung auf Bundesebene statt. Jedes Mitglied ist eingeladen, daran teilzunehmen, um die Geschicke des BDZ mit zu lenken.

Vom 20. bis zum 22. März 2020 finden in Dessau-Roßlau (Sachsen-Anhalt) die Tage der Chor- und Orchestermusik statt. Höhepunkt ist dabei die bundesweite Verleihung der vom Bundespräsidenten gestifteten Zelter- und Pro-Musica-Plaketten für Chor- und Orchestervereinigungen, die eine mindestens 100-jährige Tradition nachweisen können. Es handelt sich dabei um die höchste für Laienensembles ausgesprochene Auszeichnung. Ausgerichtet werden diese drei Tage von der Bundesvereinigung Deutscher Orchesterverbände (BDO), deren Mitglied auch der BDZ ist.

Vielleicht sehen wir uns ja auch beim Deutschen Musiktreffen 60plus vom 18. bis zum 20. September 2020 in Bad Kissingen. Daran können Ensembles, Musikvereine, Teilgruppen oder auch Einzelpersonen kostenlos teilnehmen. Die Anmeldung beim Bundesmusikverband Chor & Orchester ist noch bis zum 31. März 2020 möglich.

Sehr herzlich und auch ein wenig eindringlich lade ich alle BDZ-Mitglieder in Bayern ein, die nächste Mitgliederversammlung auf Landesebene am 15. November 2020 in Alteglofsheim zu besuchen. Ein wesentlicher Tagesordnungspunkt ist dabei die Neuwahl des gesamten Präsidiums.

Weitere interessante Veranstaltungen im kommenden Jahr entnehmen Sie bitte unserer Homepage. Nun noch eine Bitte: Wenn die Mitgliedsorchester spätestens bis Ende Januar die Mitgliedermeldung an die Bundesgeschäftsstelle senden, schicken Sie doch bitte gleich auch eine Kopie an unsere Geschäftsstelle in Lohr/Main.

Wie bereits in den Vorjahren bitte ich außerdem darum, Beiträge und Berichte über Konzerte, Probenphasen, Kurse, Orchesterreisen o. ä. direkt über die Homepage an die Redaktion des Zupfboten zu senden. Nur so ist ein reger Austausch und Kontakt untereinander möglich.

Für den Dezember und den Jahreswechsel wünsche ich Ihnen allen im Namen des gesamten Vorstands zwischen der alljährlichen Adventshektik auch einige besinnliche Stunden, ein schönes Weihnachtsfest, möglichst im Kreise der Familie, und einen sicheren »Rutsch« in ein gutes, erfolgreiches und hoffentlich friedliches neues Jahr 2020.

Ihr\
Joachim Kaiser\
Präsident\
Hammelburg, im Dezember 2019
