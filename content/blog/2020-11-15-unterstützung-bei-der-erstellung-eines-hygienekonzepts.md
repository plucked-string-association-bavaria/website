---
layout: blog
title: Unterstützung bei der Erstellung eines Hygienekonzepts
date: 2020-11-15T19:18:04.536Z
---

Der Vorstand des Bund Deutscher Zupfmusiker LV Bayern bietet seinen Mitgliedern Unterstützung bei der Erstellung eines Hygienekonzepts (z.B. für die Proben eines Vereinsorchesters) an. Bitte [kontaktieren](/bdz/people) Sie hierfür den Präsidenten Thomas Hammer.
