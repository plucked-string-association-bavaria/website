---
layout: blog
title: 25 Jahre Musik, Spiel und Spaß für Kinder im Schullandheim Schaippach
date: 2019-04-19T18:11:00.000Z
attachment: ""
---

„Es war einmal...“ – Geschichten und Musik aus der Märchenwelt. Unter diesem Motto lud der Bund Deutscher Zupfmusiker Landesverband Bayern (BDZ) im März über vierzig
Gitarren-, Mandolinen-, Violin-, und Violoncelloschüler aus Lohr, Karlstadt, Veitshöchheim
und Würzburg ins Schullandheim nach Schaippach bei Gemünden (Unterfranken) ein.
In diesem Jahr war es das 25. Mal und ein silbernes „Dienstjubiläum“ für Petra Fröhlen,
Würzburg und Petra Breitenbach, Lohr. Die beiden Kolleginnen suchten vor 26 Jahren nach
einer Möglichkeit, junge Schüler schon für die Zupfmusik zu begeistern, die noch zu jung für
die traditionellen Oster- und Pfingstkurse waren. Das Konzept eines Wochenendkurses in
einem wohnortnahen Schullandheim begeisterte von Anfang an die jungen Instrumentalisten:
Einzelunterricht, den viele sonst nicht gewohnt sind, Technikeinheiten, spielerische Theorie,
Rhythmusblocks, Basteln, bunter Abend, Ensembles und Orchesterprobe. All diese Einheiten
wurden und werden ständig überprüft und den Möglichkeiten und Anforderungen stets
angepasst.

So gibt es auch seit sechs Jahren die Kombination mit Streichinstrumenten. Sie war wieder
eine tolle Erfahrung für beide Instrumentengruppen – vor allem das große Orchester mit 13
Streichern und 29 Zupfern bot ein einmaliges und rauschendes Klangerlebnis!
Die sieben bis elfjährigen Musikschüler erhielten Einzelunterricht auf ihrem Instrument,
machten aber auch die ersten Erfahrungen im Orchesterspiel und in einem kleineren
Ensemble. Viel Spaß hatten sie auch am Liedbegleiten und themenbezogenen Rhythmicals.

Zwischendurch wurde gespielt, die gute Versorgung im Schullandheim beim Tischdienst
verstärkt und mit Unterstützung der Betreuerinnen Sophie Hausdörfer, Ann-Kathrin
Engelhaupt und Paula Völker Traumfänger gebastelt, die beim Abschlusskonzert für
besonderen Bühnenschmuck sorgten.

Am Sonntag im knapp einstündigen Abschlussvorspiel waren die Eltern ganz begeistert, was
ihre Sprösslinge gemeinsam mit den Dozenten Petra Breitenbach, Andreas Franzky, Petra
Fröhlen, Rainer Nürnberger, John Walkowiak und Oliver Thediek in eineinhalb Tagen
erarbeitet hatten. Der Verbindung von John Walkowiak (Sing- und Musikschule Lohr) zur
Dorfgemeinschaft Hohenroth ist es zu verdanken, dass das interne Abschlussvorspiel wieder
im Festsaal der Dorfgemeinschaft stattfinden konnte. Alle Teilnehmer waren begeistert von
der großen Bühne und der Atmosphäre des Saales und bemühten sich besonders um eine gute
Vorstellung: das zahlreiche Publikum (die Stühle reichten kaum) nahm es begeistert auf und
applaudiert eifrig!

Die Kinder sind schon neugierig auf das Motto im nächsten Jahr, wenn sie wieder am
14./15.03.2020 zu „Musik, Spiel und Spaß für Kinder“ nach Schaippach kommen werden.
Gefördert wird der Wochenendkurs aus Mitteln der Kulturstiftung des Bezirkes Unterfranken
und des Bayerischen Staates.

![Kinderkurs Schaippach: Probe des Kursorchesters mit Oliver Thediek als Dirigent im Saal des Schullandheimes](/assets/uploads/zupfbote_2019_kinderkurs_02.jpg "Kinderkurs Schaippach: Probe des Kursorchesters mit Oliver Thediek als Dirigent im Saal des Schullandheimes")

![Kinderkurs Schaippach: das Kursorchester auf der Bühne](/assets/uploads/zupfbote_2019_kinderkurs_03.jpg "Kinderkurs Schaippach: das Kursorchester auf der Bühne")

![Kinderkurs Schaippach: die Dozenten Rainer Nürnberger, Petra Fröhlen, John Walkowiak, Petra Breitenbach, Oliver Thediek und Andreas Franzky bei ihrem Beitrag im Abschlussvorspiel](/assets/uploads/zupfbote_2019_kinderkurs_04.jpg "Kinderkurs Schaippach: die Dozenten Rainer Nürnberger, Petra Fröhlen, John Walkowiak, Petra Breitenbach, Oliver Thediek und Andreas Franzky bei ihrem Beitrag im Abschlussvorspiel")

![Kinderkurs Schaippach: ein kleines Präsent zum 25jährigen Kursjubiläum für Petra Fröhlen](/assets/uploads/zupfbote_2019_kinderkurs_05.jpg "Kinderkurs Schaippach: ein kleines Präsent zum 25jährigen Kursjubiläum für Petra Fröhlen")
