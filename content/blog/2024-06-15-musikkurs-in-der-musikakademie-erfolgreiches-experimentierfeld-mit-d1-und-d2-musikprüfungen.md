---
layout: blog
title: "Musikkurs in der Musikakademie:  Erfolgreiches Experimentierfeld mit D1
  und D2 Musikprüfungen"
date: 2024-06-15T09:17:34.688Z
---
![Kursorchester unter der Leitung von Oliver Kälberer](/assets/uploads/p1400667.jpg "Kursorchester unter der Leitung von Oliver Kälberer")

Hammelburg. Der Bund Deutscher Zupfmusiker Landesverband Bayern veranstaltete seinen 47. Musikkurs für Mandoline und Gitarre in der Bayerischen Musikakademie in Hammelburg. Bei dem einwöchigen Seminar werden standardmäßig sowohl Einzel- als auch Gruppenunterrichte angeboten: Ensemblespiel, Theorie, Liedbegleitung, Musiktheorie „de luxe“, Singen und Tanzen, bodypercussion (besonders große Gruppe) und Schnupperkurse für Kontrabass und Improvisation rundeten das Programm ab.  Die außerordentlich beeindruckenden Ergebnisse präsentierten die Zupfmusiker im Kammermusiksaal der Bayerischen Musikakademie dem begeisterten Publikum. Besonders war in diesem Jahr der Beitrag des Kursorchesters, das bei „Hit the Road Jack“ ein Gesangstrio (Franziska Rößner, Hannah Pfister, Fiona Brand) begleitete und frühklassische Musik von Leopold Mozart zum Besten gab. Bereits zum dritten Mal waren mutige Kontrabassschüler mit von der Partie und wurden vom Publikum gefeiert.

Trotz so viel Neuem im Kursangebot, nutzen 16 Kursteilnehmer die Möglichkeit, während der Woche eine Laienmusikprüfung (D1 und D2-Kurs) abzulegen.

Erfreulich war, dass die Anforderungen von den Kursteilnehmern im praktischen Teil überschritten wurden, die Prüfungen also auf hohem Niveau stattfanden.

Sechs Dozenten betreuten die 36 Zupfmusiker im Alter von 12 – 78 Jahren während der intensiven Musikwoche: Petra Breitenbach (Lohr), Bianca Brand (Forst), Oliver Dannhauser (Heroldsberg), Oliver Kälberer (Wessobrunn), Oliver Thedieck (Güntersleben) und Malte Weyland (Regensburg).

![Verleihung der Urkunden für bestandene D1- und D2-Prüfungen während des Pfingstkurses 2024](/assets/uploads/p1400551.jpg "Verleihung der Urkunden für bestandene D1- und D2-Prüfungen während des Pfingstkurses 2024")

![Body Percussion Gruppe im Konzert, Ltg. Bianca Brand](/assets/uploads/p1400591.jpg "Body Percussion Gruppe im Konzert, Ltg. Bianca Brand")

![Liedbegleitung (Einstudierung Oliver Thedieck) mit Kontrabass-Verstärkung (Ltg. Oliver Dannhauser)](/assets/uploads/p1400599.jpg "Liedbegleitung (Einstudierung Oliver Thedieck) mit Kontrabass-Verstärkung (Ltg. Oliver Dannhauser)")

![Mandolinen Praxisgruppe Ltg. Malte Weyland](/assets/uploads/p1400611.jpg "Mandolinen Praxisgruppe Ltg. Malte Weyland")

![Dozentenquintett ](/assets/uploads/p1400641.jpg "Dozentenquintett ")

![„Hit the Road Jack“ im Gesangstrio](/assets/uploads/p1400690-2.jpg "„Hit the Road Jack“ im Gesangstrio")