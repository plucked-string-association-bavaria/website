---
layout: blog
title: "Pfingstkurs in Bayern: Erfolgreiches Experimentierfeld"
date: 2019-06-30T19:20:03.987Z
---

Der BDZ Landesverband Bayern veranstaltete in den Pfingstferien seinen 44. Pfingstkurs für Mandoline und Gitarre in der Bayerischen Musikakademie in Hammelburg. Bei dem einwöchigen Seminar werden standardmäßig sowohl Einzel- als auch Gruppenunterrichte angeboten: Ensemblespiel, Theorie, Liedbegleitung, Dirigieren, bodypercussion und neu: Schnupperkurse für Kontrabass und Jazzharmonik rundeten das Programm ab. Ein deutlicher und erkennbarer Schwerpunkt der diesjährigen Kursarbeit waren alle möglichen Formen der Liedbegleitung auf verschiedenen Instrumenten (20 von 33 Teilnehmern!), für ein Ausprobieren am Kontrabass entschieden sich 10 Teilnehmer und für Dirigieren sieben. Die außerordentlich beeindruckenden Ergebnisse präsentierten die Zupfmusiker im voll besetzten Großen Saal der Bayerischen Musikakademie dem begeisterten Publikum. Besonders war in diesem Jahr der Beitrag von zwei Teilnehmern, die im Dirigierkurs die Grundschläge und Einsätze geben lernten, und wie sie ihre musikalische Vorstellung in Bewegungen übertragen können. Erstmals waren fünf mutige Kontrabassschüler mit einem Motiv aus Mahlers 1. Symphonie (nach nur je zwei Unterrichtseinheiten) zu hören, und wurden vom begeisterten Publikum zu einer Zugabe ermuntert. Auch die neun Teilnehmer des Jazzharmonik-Kurses trauten sich mit Improvisationen auf die Bühne, was sie zu Kursbeginn nicht für möglich gehalten hatten.

Bei so viel Neuem im Angebot, nutzen nur zwei Kursteilnehmer die Möglichkeit, während der Woche eine Laienmusikprüfung (D-Kurs) abzulegen.\
Erfreulich war, dass die Anforderungen von den beiden Kursteilnehmern im praktischen Teil überschritten wurden, die Prüfungen also auf hohem Niveau stattfanden.

Fünf Dozenten betreuten die 33 Zupfmusiker im Alter von 10 bis 78 Jahren während der intensiven Musikwoche: Petra Breitenbach (Lohr), Michael Diedrich (Mosbach), Oliver Kälberer (Wessobrunn), Oliver Dannhauser (Würzburg) und Xenia Trendel (Lohr).

Gleich im Anschluss an den Pfingstkurs feierte die Bayerische Musikakademie Hammelburg die Einweihung des sanierten Klostertraktes. Nach zweijähriger Bauzeit und über 13 Millionen Euro Baukosten steht nun das komplette Klosterareal bildungshungrigen Musikerinnen und Musikern zur Verfügung.

Die erste Landesmusikakademie Deutschlands verfügt in ihrem 39. Jahr über komfortable Einzel- und Doppelzimmer für 140 Personen, zahlreiche Übungsräume (die vier neuen Säle sind mit dem hauseigenen Tonstudio verbunden), einen Serenadenhof und einen neuen Speisesaal, bei dem der ehemalige Innenhof und Kreuzgang überdacht wurde. Zur Feier des architektonischen Wurfes gehörte auch ein großes Instrument: Prof. Jörg Wachsmuth reiste mit der größten Tuba der Welt an, um gemeinsam mit dem Symphonischen Blasorchester Volkach aufzuspielen.

![Mandolinentechnikgruppe mit Kontrabassbegleitung](/assets/uploads/zupfbote_2019_pfingstkurs_01.jpg "Mandolinentechnikgruppe mit Kontrabassbegleitung")

![Die "Aufzug People" singen selbstgedichtete Lieder von den Erlebnissen auf dem Kurs](/assets/uploads/zupfbote_2019_pfingstkurs_02.jpg 'Die "Aufzug People" singen selbstgedichtete Lieder von den Erlebnissen auf dem Kurs')

![Liedbegleitung stand hoch im Kurs (Xenia Trendel)](/assets/uploads/zupfbote_2019_pfingstkurs_03.jpg "Liedbegleitung stand hoch im Kurs (Xenia Trendel)")

![Jazzharmonik und Improvisation auf Gitarren, Mandolinen, Mandola und Kontrabass mit Oliver](/assets/uploads/zupfbote_2019_pfingstkurs_04.jpg "Jazzharmonik und Improvisation auf Gitarren, Mandolinen, Mandola und Kontrabass mit Oliver")

![Teilnehmer des Schnupperkurses Kontrabass präsentieren sich mit Mahlers 1. Sinfonie](/assets/uploads/zupfbote_2019_pfingstkurs_05.jpg "Teilnehmer des Schnupperkurses Kontrabass präsentieren sich mit Mahlers 1. Sinfonie")

![Alle Kursteilnehmer](/assets/uploads/zupfbote_2019_pfingstkurs_06.jpg "Alle Kursteilnehmer")

![Prof. Jörg Wachsmuth mit der größten Tuba der Welt](/assets/uploads/zupfbote_2019_pfingstkurs_07.jpg "Prof. Jörg Wachsmuth mit der größten Tuba der Welt")

Bilder: Petra Breitenbach, Michael Wischlitzki und Erdinc Yurdacul
