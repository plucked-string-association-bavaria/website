---
layout: blog
title: "Einladung zur Landeshauptausschuss-Sitzung und
  Landesdelegierten-Versammlung:  24.11.2024 von 10:00 bis 12:00 Uhr, virtuell"
date: 2024-10-15T05:32:14.979Z
---
Sehr geehrte Mitglieder, 

Hiermit lade ich ein zur ordentlichen Landesdelegiertenversammlung am Sonntag, den 24.11.2024 von 10:00 bis 12:00 Uhr. In den vergangenen Jahren ist die Beteiligung bei Versammlungen mit persönlicher Anwesenheit nur wenig über den erweiterten Vorstand unseres Landesverbandes hinausgegangen. Die im Februar 2023 verabschiedete und genehmigte Satzungsänderung erlaubt virtuelle Versammlungen. Deshalb wird die Landesdelegiertenversammlung virtuell stattfinden. 

Es gibt eine ganze Reihe von Plattformen für virtuelle Versammlungen, die mit vorher zu installierender dezidierter Software oder über Web Browser wie Google Chrome oder Microsoft Edge genutzt werden können. Für diese virtuelle Versammlung wird Microsoft Teams genutzt werden. Ich bitte dringend darum, sich vorher damit vertraut zu machen und ggf. technische Probleme zu lösen. 

Abstimmungen und Wahlen werden rechtssicher virtuell mittels votesUP! durchgeführt. Eine rechtzeitige Anmeldung der Teilnahme an der Versammlung ist deshalb erforderlich, damit die Abstimmenden vorher mit ihrer Stimmkraft in das System eingepflegt werden können. 

Folgende Tagesordnung ist vorgesehen: 

1. Begrüßung und Vorstellung der Tagesordnung (vorliegende Anträge)
2. Feststellung von Anwesenheit und Stimmkraft
3. Protokollgenehmigung
4. Berichte
5. a) Tätigkeitsberichte der Präsidenten\
   b) Bundesangelegenheiten\
   c) Kassenbericht des Schatzmeisters\
   d) Berichte aus der Geschäftsstelle\
   e) Bericht der Musikleiterin\
   f)  Bericht der Jugendleiterin\
   g) Rechnungsprüfungsbericht
6. Entlastung des Vorstandes
7. Berichte aus den Vereinen
8. Vorstellung und Planung von Aktivitäten 2024/2025
9. Neuwahlen des Vorstandes (Präsident, Vizepräsident, Schatzmeister, Musikleiter, Jugendleiter) und der Rechnungsprüfer durch die Mitgliederversammlung
10. Neuwahlen der Beisitzer durch den Landeshauptausschuss
11. Sonstiges (ohne Beschlüsse)



Anträge zur Landesdelegierten-Versammlung bitte ich in Textform (z.B. per E-Mail) spätestens bis zum 9. November 2024 bei mir einzureichen. Weiterhin bitte ich um Anmeldung per E-Mail (mit der dann auch für die Abstimmung verwendeten Adresse) bis zum 9. November 2024. Bitte beachten Sie, dass bei Vertretung eines Vereins durch ein Mitglied, das kein vertretungsberechtigtes Vorstandsmitglied ist, eine beigefügte Vertretungsvollmacht notwendig ist. 

Mit freundlichen Grüßen, 

Dr. Thomas Hammer, Präsident