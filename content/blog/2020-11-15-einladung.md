---
layout: blog
title: Einladung
date: 2020-11-01T19:16:43.062Z
---

#### an alle Mitglieder des Landesverbands Bayern im Bund Deutscher Zupfmusiker e.V. zur kombinierten Mitgliederversammlung und Hauptausschusssitzung des BDZ Landesverbands Bayern e.V. am Sonntag, dem 15. November 2020 um 13 Uhr virtuell

Tagesordnung

1. Begrüßung
2. Feststellung von Anwesenheit und Stimmkraft
3. Protokollgenehmigung
4. Tätigkeitsberichte der Präsidenten
5. Kassenbericht des Schatzmeisters
6. Berichte des Geschäftsführers und aus der Geschäftsstelle
7. Bericht der Musikleiterin
8. Vorliegende Anträge
9. Corona-Ereignisse
10. Möglichkeiten der Unterstützung für die Vereine durch den Vorstand
11. Satzungsänderung zur Besetzung des Hauptausschusses
12. Rechnungsprüfungsbericht
13. Entlastung des Präsidiums
14. **Neuwahlen des Präsidiums, der Kassenprüfer und der Beisitzer**
15. Verabschiedungen
16. Sonstiges

Für die kombinierte Mitgliederversammlung und Hauptausschusssitzung gibt es in Zeiten der Corona-Pandemie leider einige Änderungen zu den bisherigen Zusammenkünften, bei deren Berücksichtigung und Umsetzung ich um Ihr Verständnis und Ihre Unterstützung bitte. Um auf kurzfristige Änderungen direkt hinweisen zu können und auch aus rechtlichen Gründen ist es für die Anmeldung unabdingbar, dass Sie Ihre Telefonnummer und Ihre E-Mail-Adresse angeben. Anmeldungen ohne diese Daten können nicht berücksichtigt werden.

Diese Einladung zur Mitgliederversammlung entspricht § 12 unserer Satzung und ist somit rechtsgültig. Anträge zur Mitgliederversammlung sind spätestens eine Woche vor der Versammlung schriftlich beim Präsidenten einzureichen. Bitte informieren Sie die Geschäftsstelle zeitnah per E-Mail in jedem Fall, ob und wie Sie an der Versammlung teilnehmen! **Anmeldeschluss ist der 11. November 2020!** Details zur Anmeldung finden Sie unten.

Die Zugangsinformationen für die virtuelle Teilnahme werden Sie so rechtzeitig vor der Sitzung erhalten, dass Sie sich vorher damit vertraut machen können. Ich hoffe auf eine rege Beteiligung und wünsche schon jetzt eine gute Internet-Verbindung.

Mit freundlichem Gruß\
Joachim Kaiser, Landesverbandspräsident

Für die Rückmeldung bis spätestens 11.11.20 an die Geschäftsstelle per Post (an: Zur Kehlspitze 24, 97816 Lohr am Main) oder per E-Mail sind die folgenden Daten notwendig:

- Ihr Name und Vorname
- Ihre Telefonnummer und Ihre E-Mail-Adresse
- die Auswahl aus den zwei Möglichkeiten\
  – Ich werde am 15.11.20 ab 13.00 Uhr an der Landesdelegiertenversammlung virtuell teilnehmen und bitte um die Zugangsdaten\
  – Ich werde am 15.11.20 nicht teilnehmen
- und Ihre Unterschrift
