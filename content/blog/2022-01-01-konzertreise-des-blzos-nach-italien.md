---
layout: blog
title: Konzertreise des BLZOs nach Italien
date: 2012-06-21T13:56:37.996Z
attachment: /assets/uploads/italienreise-blzo-juni-2012.pdf
---
"Genießt mit offenen Augen die italienische Landschaft mit deren Farben und Blumendüften und lasst dieses Gefühl "sentimento" in unsere Musikgestaltung einfließen." So die Empfehlung des Dirigenten auf der Konzertreise nach Italien (San Bartolomeo al mare (Ligurien), Savona und Desenzano del Garda).

von Dorothee v. Heydebrand

**Sonntag**

Nach enormer Vorarbeit von Sabine und Daniel, dem Austauschen zahlreicher Mails und als endlich alle Autobahnen, Rasthöfe usw. mehr oder weniger klar waren, konnte die Reise beginnen. Bei strömendem Regen stiegen wir, von den in Würzburg Zugestiegenen herzlich begrüßt, am Parkplatz Berkheim in den bereits wartenden Bus. Das Wetter konnte nur besser werden! Was wir gleich zu Anfang lernten: „Auf Einzelschicksale kann keine Rücksicht genommen werden, es sei denn es wäre der Dirigent, ein Solist oder eine 1. Mandoline.“

Nach einer abwechslungsreichen Fahrt, diversen Ansagen von Daniel, großer Hitze und teurem Klo auf dem Schweizer Parkplatz, dickstem Nebel und starkem Regen in den Bergen, versorgt mit Würstchen und Unmengen von selbstgebackenen Kuchen (Danke den Bäckern!), kamen wir wie geplant gegen 20.00 Uhr in San Bartolomeo al Mare (Ligurien) an. Auf dem Busparkplatz begrüßte uns unsere Quartiermeisterin\
Sabine, die mit der Wahl des entzückenden kleinen Hotels direkt ins Schwarze getroffen hatte. Die ruhige Lage inmitten von Aprikosenhainen, ein kleiner Garten mit Swimmingpool und ein wunderschöner Blick aus den Zimmern auf das gegenüberliegende Bergdörfchen Cervo machte dem Namen „Bellavista“ alle Ehre. Dies galt aber leider nicht für diejenigen, die zu viert in der „Kellerabstellkammer“ des Hotels untergebracht waren und deshalb verständlicherweise mit dem Schicksal haderten. Auch die Einzelzimmer lagen zur anderen Seite und eine Baustelle machte deren Bewohnern, zu denen auch Oliver gehörte, das Leben bzw. das Ausschlafen schwer.

![Unterkunft in Cervo](/assets/uploads/italien1.jpg "Die Unterkunft in Cervo")

**Montag**

Es erwartete uns ein entspannter Vormittag, mit Bummeln auf dem Wochenmarkt, Kauf von Olivenöl, Baden im Meer und nach einem wundervollen Mittagessen am Nachmittag ein ebenso entspannter lockerer Oliver, der uns bei der kurzen „Schnupperprobe“ empfahl, mit offenen Augen die italienische Landschaft mit deren Farben und Blumendüfte zu genießen, und dieses Gefühl „sentimento“ in unsere Musikgestaltung einfließen zu lassen. Am Abend war ein Ausflug nach Imperia mit gemeinsamem Pizzaessen geplant. Der Bus fuhr uns auf kurviger Straße entlang der Küste, immer mit Blick auf das blaue Meer und elegante italienische Villen in gepflegten\
blumenreichen Gärten bis nach Imperia. Dort genossen wir das kostenlose Fahrstuhlfahren bis in die Altstadt (der Dom war leider schon zu), den herrlichen Blick und anschließend ein opulentes Abendessen: Spaghetti mit Meeresfrüchten, sechs verschiedene Sorten Pizza, Dessert nach Wahl und Rotwein ohne Ende.Der Service war ein logistisches Meisterwerk und gipfelte im Geschenk (?) des Wirtes: einer riesigen Flasche Limoncello, die Heiko mit der verbliebenen Restmenge geistesgegenwärtig konfiszierte und später nur sehr ungern an Elke zur Aufbewahrung und späteren allgemeinen Verwendung übergab.

![Pizzaessen in Imperia](/assets/uploads/italien2.jpg "Pizzaessen in Imperia")

**Dienstag**

Nach einem reichen Frühstück wurde der kleine Speisesaal des Hotels zum Probenraum umfunktioniert. In drangvoller Enge, aber mit vorzüglicher Laune und viel Schwung gaben wir unseren Stücken den letzten Schliff. Die Schlagzeuger wurden in den Flur verbannt und konnten Olivers Dirigat durch eine milchige Glasscheibe nur bedingt folgen, schlugen (!) sich aber tapfer und besonders die große Trommel ließ das Hotel in seinen Grundfesten erzittern. Der Hotelkoch ließ sich Gott sei Dank davon nicht einschüchtern und so konnten wir im Anschluss an die Probe mit Genuss ein 4-gängiges feines italienisches Menü genießen. Bis der Bus uns zu unserem Konzertort in Cervo abholte, hatten wir noch 3 Stunden Zeit, die jeder auf seine Weise nutzte. 

Die Busfahrt nach Cervo gestaltete sich reichlich kurz, denn der Bus konnte die kleinen Sträßchen des Ortes nicht befahren und entließ uns am Fuße einer langen Treppe. In Konzertkleidung, mit Instrument und Notenständer ging es hinauf zum „Oratorio“ unserem Konzertort. Eine etwas schweißtreibende Angelegenheit, die nur die ganz Durchtrainierten ohne Schnaufen bewältigten. Die Ansicht der romantischen Gässchen, hin und wieder ein Blick aufs blaue Meer weit unter uns, das ganze Flair eines alten italienischen Bergstädtchen bezauberte uns dennoch, und als sich das „Oratorio“ als eine romanische Kirche mit günstiger Akustik entpuppte, freuten sich alle sehr auf das Konzert.

Überall im Ort konnten wir die Plakate entdecken auf denen wir angekündigt waren und nach der Anspielprobe genossen wir den so liebevoll vorbereiteten Empfang. Am Fuße der Barockkirche San Giovanni Battista war für uns ein Buffet mit italienischen Leckereien und Prosecco auf dem romantischen Kirchplatz aufgebaut. Die Abendsonne tauchte die Fassade der Kirche und die der umliegenden Häuser in wunderbare weiche Farben. Schwalbenschwärme durchzogen zwitschernd den blauen Himmel. Eine unglaubliche Atmosphäre!

So beflügelt wurde das Konzert zu einem großen Erfolg! Jeder Spieler wuchs über sich hinaus, unser Dirigent strahlte und die große Spielfreude teilte sich auch dem Publikum mit, das in Scharen gekommen war. Bravorufe erschallten, zufällige Hörer aus München waren begeistert und vom Kulturverein Cervo gab es Dankesreden, verbunden mit der Überreichung einer wunderbaren eingravierten Tafel und eines Bildbandes über die Geschichte des Ortes.

Unser Präsident Hans-Joachim Kaiser revanchierte sich mit einer grandiosen Rede, und die tapfere Sabine fasste diese und die Bewandtnis unseres Gastgeschenkes auf Italienisch zusammen, wofür ihr unsere Hochachtung und der Applaus des Publikums gewiss waren. Aber damit nicht genug!

Wir waren nach dem Konzert auch noch zu einem mehrgängigen Abendessen eingeladen! Den Abstieg zu\
unserem Bus schafften einige wegen der zahlreichen Stufen und des Weines gegen 1.00 Uhr morgens nur mit weichen Knien, aber ganz Hartgesottene ließen es sich nicht nehmen, den ganzen Weg durch die laue italienische Vollmondnacht bis zum Hotel zurück zu laufen.

![Unser Konzertort - Oratorio in Cervo](/assets/uploads/italien3.jpg "Unser Konzertort - Oratorio in Cervo")

**Mittwoch**

Neuer Tag, neues Glück! Es war geplant auf dem Weg zum nächsten Übernachtungs- und Konzertort Savona bis nach Finale Ligure zu fahren und dort für längere Zeit das Meer zu genießen. Doch die Angst um die Instrumente im heißen Kofferraum des Busses überwog, und so fuhren wir (leider) weiter bis Savona.

Wie in Cervo sind auch die Sträßchen von Savona zum Befahren mit einem großen Reisebus nur bedingt geeignet! Wir verursachten sozusagen eine totale Verkehrsblockade und unser Bus konnte nur abbiegen, weil eine zufällig vorbeikommende Polizeistreife diese Straße für andere Verkehrsteilnehmer sperrte. Doch damit nicht genug! Zu unserem Übernachtungsort – dem Seminario Vescovo – einer Ausbildungsstätte für angehende Theologen, führte ein geschwungener schmaler Sandweg gesäumt von Nadelbäumen. Hier leistete unser Busfahrer Präzisionsarbeit! In Zeitlupe und auf den Millimeter genau gelang es ihm, den Bus durch die Allee direkt vor das Gebäude zu lotsen.

In Anbetracht der schon vergangenen Zeit und des Rückweges verzichteten wir dann aber auf die angebotene Möglichkeit wieder zurück ans Meer zu fahren. Wir beschlossen nach dem Beziehen der Zimmer in kleinen Grüppchen Savona zu erkunden und unterschiedlichen Neigungen (shoppen, essen, Kultur) nachzugehen.

Dank einer zuvorkommenden Lehrerin die wir nach dem Weg fragten, und eines vor geschichtlicher Begeisterung geradezu überschäumendem Dombediensteten gewannen Barbara, Christl, Mechthild, Moni und ich tiefe Einblicke historischer (Dom, Sixtinische Kapelle) und kulinarischer (Eisdiele) Art und auch noch zwei Zuhörer für unser Konzert am folgenden Tag, denn nirgendwo sahen wir Plakate. Auch im Oratorio – unserem Konzertort – fanden wir nichts Schriftliches und die dort anwesenden Angestellten wussten von nichts.

**Donnerstag**

Bei der Konzertprobe am nächsten Morgen klärte sich alles. Der begnadete Mandolinist Carlo Aonzo, der bei zwei Stücken unser Solist sein sollte, erklärte uns, er sei erst am Vormittag von einer Konzertreise aus den USA zurückgekehrt und habe voller Entsetzen bemerkt, dass seine Anweisungen die Presse zu benachrichtigen und Reklame zu machen nicht befolgt wurden, aber er würde seine Freunde und Bekannten mobilisieren.

Unsere Unterkunft in Savona war ein ehemaliges Kloster mit entsprechender meditativer Atmosphäre. Ein wunderschöner Innenhof mit umschließendem Kreuzgang und ein karger Speisesaal. Das Essen (und besonders der intensive Rotwein!) war aber gut, wenn man von dem mit viel Plastikgeschirr und gruseligem Maschinenkaffee (in Italien!) versehenem Frühstück absah.

Nach dem Mittagessen wurde die Möglichkeit angeboten, den ausgefallenen Strandbesuch vom Vortag nachzuholen, was mit Freude aufgenommen wurde. Allerdings war der Aufenthalt dort durch die zeitraubende Hin- und Rückfahrt etwas kurz geraten, mussten wir doch pünktlich um 19.00 Uhr zur Probe im Oratorio del Christo Resorto sein. Unser Busfahrer Dominik hatte auch so seinen Ehrgeiz und ließ es sich nicht nehmen, uns freundlicherweise durch die kleinen Sträßchen Savonas direkt vor die „Haustür“ zu fahren. So ersparte er uns den Fußmarsch in Konzertkleidung und viele Treppen (diesmal abwärts) und war zu unserer Freude auch als Zuhörer bei dem Konzert dabei.

Das Oratorio, ein Kirchensaal aus dem 17. Jahrhundert, ausgeschmückt mit vielen Skulpturen, Stuck an den Wänden und geschnitzten Altaraufsätzen, die bei der berühmten Karfreitagsprozession durch die Straßen getragen werden, hat eine ganz eigentümliche berührende Atmosphäre. Diese wirkte sich dann auch auf unsere Konzert aus, zu dem doch noch Publikum gekommen war und auch währendem zahlreiche Spontanbesucher angelockt durch die Klänge bei offener Tür den Saal betraten.

Unser Konzertprogramm:\
Zu Beginn das Präludium E-Dur (BWV 870) und die Fuge A-Dur (BWV 886) von J.S. Bach\
Danach folgte der 1. Satz des Raidoh für Mandoline und Zupforchester von Yasuo Kuwahara. Anne Wolf spielte mit virtuoser Intensität und großer Gestaltungskraft, die das Orchester mitriss und das lange Werk wie aus einem Guss erscheinen ließ. Anschließend das Preludio Sinfonico von Ugo Bottacchiari. Wir folgten geschmeidig dem ausdrucksstarken Dirigat von Oliver und es gelang uns eine solche Dichte und musikalische Ausstrahlung, dass wir in den Augen von manchem Zuhörer Tränen glitzern sahen und wir selbst auch ganz berührt waren.

Nach der Pause dann das Konzert e-moll (nach BWV 1062) mit den Solisten Anne Wolf, Sabine Spath (Mandolinen) und Jürgen Thiergärtner und Daniel Ambarjan (Gitarren). Auch hier wurde der zweite langsame Satz inmitten der virtuosen anderen Sätze zu einem musikalischen Erlebnis mit Gänsehautfeeling.

Danach die Tanzsuite von Kubota! Wir fürchteten schon um den Stuck und die Barockengel, aber alles blieb heil obwohl die Schlagzeuger (Alexander Lenk und Jonas Miederer) ihr Bestes gaben. Das Publikum war aus dem Häuschen!

Danach eine komplette Zugabenserie: unser Gast an der Mandoline Carlo Aonzo spielte im Stehen, bestechend virtuos und mit viel Agogik Da un Balcone ungherese von Roberto Bruzzone und testete unsere Reaktionsfähigkeit als begleitendes Orchester. Danach Broadway ` 79 von Victor Kioulaphides im kongenialen Duo mit Anne (ebenfalls stehend!). Zu guter Letzt noch Orgia von Joachim Turina, wo wir uns nochmal so richtig austoben konnten! Obwohl das Publikum immer noch nicht genug hatte, packten wir unsere Sachen, bestiegen den Bus, der die Straßen der Altstadt blockiert hatte und ließen uns in eine Pizzeria bringen um den Abend gebührend ausklingen zu lassen. Oliver fand zwei Wort zu unserem Spiel: „Einfach Grandios! “, was uns sehr freute!

![Das Refektorium in Savona](/assets/uploads/italien4.jpg "Das Refektorium in Savona")

**Freitag**

Der nächste Tag bescherte uns eine Busfahrt nach Desenzano del Garda, wo wir wiederum in einem hübschen Hotel namens Bertha untergebracht waren. Nach dem Essen entschieden sich die meisten von uns zu einem Ausflug ins trubelige Sirmione. Im Bus brachte uns Christl die römische Geschichte des Ortes und das bewegte Leben des röm. Dichters Catull näher. Danach konnte jeder nach eigenem Gusto seinen Interessen nachgehen. - Lustigerweise trafen wir uns dann fast vollzählig in der Villa des Catull wieder. So war Christls Vortrag auf fruchtbaren Boden gefallen! Nach dem Besuch des informativen Museums genossen wir die Atmosphäre des großen angeschlossenen Gartens, der Blick über den Gardasee mit blühenden Oleanderbüschen und die Ruinen inmitten des bezaubernden Olivenhaines. Fotomotive ohne Ende!

![Am Gardasee](/assets/uploads/italien5.jpg "Am Gardasee")

**Samstag**

Obwohl Gewitter und Regen angesagt waren, konnten wir am Morgen bei herrlichstem Wetter noch durch Desenzano schlendern, die Burg erforschen und die letzten Mitbringsel für die Daheimgebliebenen erstehen. Nach dem Essen kam es endlich zum langersehnten Tretbootfahren, dem heimlichen Höhepunkt der Reise! Sofort danach Abfahrt zum letzten Konzert in das Bergstädtchen Inzino, wohin uns das dort ansässige Orchester „Il Plettro“ eingeladen hatte. Der Konzertort war diesmal ein aufgelassenes, ehemaliges, plüschiges Kino aus den 60ern auf dessen Bühne im ersten Teil des Konzertes das einheimische Orchester feinsinnig Filmmelodien mit Gesang präsentierte und anschließend die rhythmisch verzwickte Rhapsodia von Squarzina zu Gehör brachte.

Den zweiten Teil gestalteten wir, wobei ein trommelnder Gewitterschauer auf das wohl etwas undichte Dach und einige Tropfen auf Christls Noten während des Konzertes eher mehr Schwüle alsAbkühlung brachten. Der zu Anfang gähnend leere Zuschauerraum füllte sich bis zum letzten Stück komplett. Eine etwas langwierig geratende Begrüßungsrede mit fast kompletter Inhaltsangabe eines unserem Präsidenten überreichten historischen Bildbandes brachte Oliver in der Hitze der gleißenden Lampen und auch uns Spieler fast ans Ende unseres körperlichen Vermögens. So waren wir nur noch fähig mit „Il Plettro“ gemeinsam in drangvoller Enge auf der Bühne wie geplant Calaces Tarantella in rasendem Tempo zweimal hintereinander durchzujagen und verzichteten auf die geplante Orgia, was einigen von uns, trotz der Hitze, Worte des Bedauerns entlockte.

Anschließend wurden wir vom Orchester „Il Plettro“ mit wunderbaren kulinarischen Häppchen verwöhnt und bedauerten sehr, dass wegen der Kürze der Zeit (unser Busfahrer war wegen der geplant frühen Abfahrt am nächsten Morgen zu einer neunstündigen Nachtpause gezwungen) kein näherer Kontakt zu den Spielern des anderen Orchesters möglich war. 

Auf der Rückfahrt im Bus gab es noch zahlreiche Dankesreden. Insbesondere an die Quartiermacherin Sabine, an den Organisator Daniel, an den Dirigenten Oliver, das mitfahrende Orchester, an das fluktuierende Ansageteam bestehend aus Bianca, Elke und Sabine, an die CD Verkäuferinnen Ulrike und Angela, und nicht zuletzt an unseren unerschütterlichen Busfahrer Dominik.

Der nächste Tag brachte die Heimreise und die Hoffnung auf weitere BLZO Unternehmen....\
Frankreich, China, Indien ??????

Fazit: Es war wunderschön!

![Die logistische Meisterleistung unseres Busfahrers](/assets/uploads/italien6.jpg "Die logistische Meisterleistung unseres Busfahrers")

Den Bericht mit Fotos finden Sie im Anhang.