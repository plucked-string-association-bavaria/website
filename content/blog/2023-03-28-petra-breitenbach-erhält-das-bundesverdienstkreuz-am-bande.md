---
layout: blog
title: Petra Breitenbach erhält das Bundesverdienstkreuz am Bande
date: 2023-03-28T19:27:30.336Z
---
**„Wenn andere sich zurücklehnen, schwingt sie den Taktstock“**

In festlichem Rahmen erhielt Petra Breitenbach, Musikleiterin des BDZ LV Bayern, am 16.02.2023 in der Würzburger Residenz den „Verdienstorden am Bande“ der Bundesrepublik Deutschland. Überreicht wurde ihr das Zeichen höchster Würdigung als „öffentliche Anerkennung für ihr verdienstvolles Wirken“ vom Regierungspräsident Unterfrankes Dr. Eugen Ehmann im Rahmen einer festlichen Veranstaltung. Petra Breitenbach aus Lohr ist seit vielen Jahrzehnten haupt- und ehrenamtlich im Bereich der Musikpädagogik sowie der musikalischen Förderung tätig. Ihr besonderer Einsatz gilt dabei der Zupfmusik.

![Das Zupf-Ensemble Lohr unter der Leitung von Petra Breitenbach in Aktion während des Festaktes (Foto: Johannes Hardenacke, Presse Regierung von Unterfranken)](/assets/uploads/1_pb_ze-lohr-spielt.jpeg "Das Zupf-Ensemble Lohr unter der Leitung von Petra Breitenbach in Aktion während des Festaktes (Foto: Johannes Hardenacke, Presse Regierung von Unterfranken)")

Musikalisch umrahmt wurde der Vormittag durch das Zupf-Ensemble Lohr, das von Petra Breitenbach seit der Gründung 1982 ehrenamtlich geleitet wird. Neben den Proben organisiert sie auch dessen öffentliche Auftritte. In Zupfmusikerkreisen genießt das Ensemble seit Jahrzehnten einen hervorragenden Ruf. Viele dieser SpielerInnen waren SchülerInnen der diplomierten Musik- und Instrumentalpädagogin und spiel(t)en im Bayerischen Landesjugend- und Landeszupforchester. Auch Petra Breitenbach ist im überregionalen BLZO seit der Gründung 1981 eine wertvolle Stütze in der Mandolinenstimme, gerne übernimmt sie auch das Spiel auf dem Mandoloncello und zeigt ihre große Bandbreite im Orchesterspiel.

 Die Musikerin hat sich seit ihrer Kindheit mit Leidenschaft der Musik und vor allem den Zupfinstrumenten gewidmet. Sie selbst erzielte 1980 beim Wettbewerb „Jugend musiziert“ einen 2. Preis auf Bundesebene und hilft seitdem zahlreichen jungen MusikerInnen ebenfalls erfolgreich teilzunehmen. 

Als Mitglied der Jury beim Regionalwettbewerb „Jugend musiziert“ in Schweinfurt übernimmt sie seit vielen Jahren bereitwillig den Juryvorsitz in der Kategorie Zupfinstrumente. Sie sorgt mit ihrer langjährigen pädagogischen Erfahrung sowie ihrer verbindlichen und sympathischen Art für eine faire Bewertung der TeilnehmerInnen aller Altersgruppen. Dabei hat die Motivation aller Beteiligten für sie oberste Priorität.

 Im BDZ LV Bayern ist Petra Breitenbach, die hauptberuflich die Sing- und Musikschule Lohr leitet und dort Gitarre und Mandoline unterrichtet, seit ihres Musikstudiums aktiv. Bereits seit 1980 unterrichtet sie als Dozentin beim alljährlich in der Bay. Musikakademie Hammelburg stattfindenen „Pfingstkurs“ des BDZ, den sie seit über 30 Jahren auch leitet. Seit 1999 ist sie Mitglied des Kuratoriums der Bay. Musikakademie Hammelburg, 2004 wurde sie hier zur stellv. Vorsitzenden gewählt. Als Prüfungsvorsitzende übernimmt sie jährlich die Abnahme vieler D-Musikprüfungen bei Jugendlichen und Erwachsenen.

 

Darüber hinaus unterrichtet sie bei dem 1994 von ihr mit ins Leben gerufenen Kinderkurs „Musik, Spiel und Spaß“ im Schullandheim Schaippach jährlich rund 30 Kinder auf Mandoline und Gitarre und fördert mit viel Engagement und Motivation das gemeinsame Musizieren.

Seit 1998 wirkt sie ehrenamtlich im Musikbeirat des LV Bayern mit, seit 2004 ist sie als Landesmusikleiterin Vorstandsmitglied und sorgt mir ihrer Leidenschaft bayern- und bundesweit für eine Weiterentwicklung der Zupfmusik. Hier ist ihr die Arbeit im Team ein besonderes Anliegen. Die Landesmusikfeste des LV Bayern (2006 in der Bay. Musikakademie Hammelburg, 2013 und 2019 in der Bay. Musikakademie Schloss Alteglofsheim) wurden unter ihrer organisatorischen und künstlerischen Leitung ein voller Erfolg, da es immer gelungen ist, viele verschiedene Talente zu bündeln. 

![Verleihung des Verdienstordens:  v.l.n.r: Joachim Kaiser (Ehrenpräsident des BDZ LV Bayern), Dr. Mario Paul (Erster Bürgermeister der Stadt Lohr a. M.), Petra Breitenbach, Sabine Sitter (Landrätin MSP), Dr. Eugen Ehmann (Regierungspräsident). (Foto: Bernhard Schneider)](/assets/uploads/2_pb-und-gratulanten.jpeg "Verleihung des Verdienstordens:  v.l.n.r: Joachim Kaiser (Ehrenpräsident des BDZ LV Bayern), Dr. Mario Paul (Erster Bürgermeister der Stadt Lohr a. M.), Petra Breitenbach, Sabine Sitter (Landrätin MSP), Dr. Eugen Ehmann (Regierungspräsident). (Foto: Bernhard Schneider)")

Vor allem im Jahr 2023, in dem die Mandoline als „Instrument des Jahres“ geehrt wird, ist es ein Anliegen für Petra Breitenbach, ein Netzwerk zwischen Musikschule, Verband und Musikhochschule zu flechten. Dieses soll künftige Lehrkräfte besser auf die Arbeit in der Praxis und bei den Kursen vorbereiten.

„Ich möchte weiterhin schöne Musik machen und Menschen aller Altersgruppen dazu bringen, selbst zu musizieren“, erklärt Petra Breitenbach. Das Musizieren sei für sie immer stärkend und sinnstiftend für ein erfülltes Leben. 

![Das Zupf-Ensemble Lohr mit  hintere Reihe v.l.n.r: Joachim Kaiser (Ehrenpräsident des BDZ LV Bayern), Sabine Sitter (Landrätin MSP), Dr. Eugen Ehmann (Regierungspräsident), Petra Breitenbach, Dr. Mario Paul (Erster Bürgermeister der Stadt Lohr a.Main). (Foto: Bernhard Schneider)](/assets/uploads/3_pb-mit-ze-lohr-und-gratulanten.jpeg "Das Zupf-Ensemble Lohr mit  hintere Reihe v.l.n.r: Joachim Kaiser (Ehrenpräsident des BDZ LV Bayern), Sabine Sitter (Landrätin MSP), Dr. Eugen Ehmann (Regierungspräsident), Petra Breitenbach, Dr. Mario Paul (Erster Bürgermeister der Stadt Lohr a.Main). (Foto: Bernhard Schneider)")

Als Gratulanten reihten sich neben der Vorstandschaft des BDZ LV Bayern (u.a. der Ehrenvorsitzende Joachim Kaiser) und der Landrätin für den Landkreis Main Spessart, Sabine Sitter auch der Erste Bürgermeister der Stadt Lohr Herr Dr. Mario Paul ein, der twitterte: „So ist sie und so kennen wir sie, unsere Petra Breitenbach. Auch am Tag der Verleihung des Bundesverdienstordens ist ihr Engagement ungebrochen. Wenn andere sich zurücklehnen, schwingt sie den Taktstock. Herzlichen Glückwunsch und voll verdient!“