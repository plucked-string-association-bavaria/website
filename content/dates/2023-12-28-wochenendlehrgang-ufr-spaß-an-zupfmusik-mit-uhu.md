---
layout: dates
title: 'Wochenendlehrgang Ufr: Spaß an Zupfmusik mit "uHu"'
location: Bay. Musikakademie Hammelburg
beginning: 2024-11-29T17:00:05.402Z
end: 2024-12-01T13:00:05.414Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - uHu
---
Ein Wochenende mit viel Zupfmusik, Freude und guter Laune:

Frei von Leistungsdruck und anderen Zwängen (aber nicht ohne Ehrgeiz) steht der Spaß an Zupfmusik im Vordergrund. In Form von Ensembles, Liedbegleitungs- und anderen Kleingruppen können sich die Teilnehmer:innen ihre Lieblings-Betätigungsfelder mit ihrem Instrument auswählen und praktizieren. Zum Ausklang der täglichen Arbeit trifft man sich auf ein Glas Wein und zum Plaudern und Lachen im Felsenkeller.

Zielgruppe: Hobbymusiker:innen, die nach längerer Pause wieder angefangen haben, zu musizieren, bzw. erwachsene ZO-Spieler:innen, die gerne mal nur mit gleichaltrigen oder älteren zusammen musizieren wollen (ohne untere/obere Altersgrenze!).

Leitung: Petra Breitenbach\
Dozenten: Jürgen Thiergärtner, Michael Diedrich

Kosten (für Unterricht, Unterkunft im DZ und Vollverpflegung in der Bay. Musikakademie Hammelburg):\
BDZ-Mitglieder aus Bayern: 120 Euro\
Nichtmitglieder: 155 Euro

Anmeldung ab Mitte des Jahres 2024 möglich.