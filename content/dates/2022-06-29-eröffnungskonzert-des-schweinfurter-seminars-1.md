---
layout: dates
title: Eröffnungskonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2022-08-07T18:00:58.551Z
end: ""
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Kurs
  - Veranstaltung
---
*Es ergeht herzliche Einladung zum Eröffnungskonzert des 51. Schweinfurter Seminars im großen Saal der Musikakademie Hammelburg.*

Es musizieren drei Dozenten des Seminars:

**Steffen Trekel (Mandoline)** und **Michael Tröster (Gitarre)** zählen zu den großen Spitzenduos dieser kammermusikalischen Besetzung. Pure Freude am musikalischen Ausdruck garantieren ein intensives Musikerlebnis. Zu hören sind Werke von W. A. Mozart, R. Calace und M. D. Pujol, mit denen die beiden Musiker eine große Bandbreite der Zupfmusik zeigen und ein unterhaltsames und lebendiges Klangerlebnis bieten.

Der Pianist **Iwan Urwalow** leitete viele Jahre eine gefragte Klavierklasse an der Musikakademie Kassel. Er übernimmt beim Seminar die Rolle des Korrepetitors und begleitet die Zupfmusiker bei ihren Werken für Gitarre und Klavier bzw. Mandoline und Klavier. Beim Konzert zeigt er sein solistisches Können und brilliert mit Werken von I. Albeniz und F. Chopin. So schafft Urwalow eine gelungene Mischung aus Euphorie und Melancholie.

Der Eintritt ist frei!

<!--EndFragment-->