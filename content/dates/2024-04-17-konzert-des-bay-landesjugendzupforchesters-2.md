---
layout: dates
title: "ABGESAGT: Konzert des Bay. Landesjugendzupforchesters"
location: Veranstaltungsforum, Fürstenfeldbruck
beginning: 2024-09-06T17:00:25.393Z
end: 2024-09-06T18:00:25.408Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - BLJZO
  - Abgesagt
---
Das BLJZO gibt ein Konzert unter Leitung von Tom Hofmann.

Zu hören sind Werke von Johann Nepomuk Hummel (Mandolinenkonzert G-Dur, Solistin: Antonia Platzdasch), Alexander Mathewson (Bulgarian Suite), Miwa Naito (Okinawa-Suite), Simon Wolz (Klangspiele) und Hermeto Pascoal (Bearb. T. Hofmann) (Serenata).