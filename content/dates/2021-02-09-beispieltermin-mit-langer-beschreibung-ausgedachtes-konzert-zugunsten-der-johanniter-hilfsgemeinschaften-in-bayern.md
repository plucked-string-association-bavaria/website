---
layout: dates
title: "Beispieltermin mit langer Beschreibung: Ausgedachtes Konzert zugunsten
  der Johanniter Hilfsgemeinschaften in Bayern"
location: St. Markus Kirche, Gabelsbergerstraße 6, 80333 München
beginning: 2021-09-22T19:00:35.100Z
end: 2021-09-31T17:36:35.232Z
showEnd: false
hideTime: false
showContinueReading: true
link: https://www.markuskirche-muenchen.de/
tags:
  - BLJZO
---

Bayerisches Landeszupforchester mit Werken von

- Rossen Balkanski
- Eduardo Angulo
- Oliver Kälberer

1. Johann Sebastian Bach
2. Maurice Ravel
3. Takashi Kubota

Eintritt frei.

> Unter dem Motto „Musik aus aller Welt“ lädt das Zupforchester Essingen am Samstag, den 3. November um 19 Uhr zu seinem Herbstkonzert in die Prot. Kirche in Essingen ein. Als musikalische Rarität genießt das Orchester überregionales Ansehen und gilt nicht nur in Liebhaberkreisen als Geheimtipp für diesen speziellen Orchestersound. Musiziert wird in der Besetzung mit Mandolinen, Mandolen, Gitarren und einem Kontrabass unter professioneller Leitung der Konzertmandolinistin Denise Wambsganß.\
> Die Spieler/innen aller Altersgruppen kommen aus der näheren und weiteren Umgebung von Essingen und entstammen größtenteils der intensiven vereinsinternen Instrumentalausbildung für Mandoline und Gitarre, aus der in den letzten Jahren regelmäßig Preisträger des Wettbewerbs "Jugend musiziert" bis hin auf Bundesebene hervorgegangen sind.

Mitgestaltet wird dieses Konzert vom Zupforchester Niederwürzbach, das mit zu den besten Mandolinenorchestern im Saarland zählt. Seit Jahresbeginn führt Christine Eckstein-Puhl als Dirigentin das Orchester.\
Die beiden Ensembles werden sich jeweils mit einem eigenen Programmblock präsentieren und sich im letzten Teil des Konzerts zu einem **großen Gemeinschaftsorchester** vereinigen.

Auf dem Programm steht eine attraktive und anspruchsvolle Mischung überwiegend zeitgenössischer Zupforchesterliteratur und zeigt damit ein weites Spektrum _moderner Zupfmusik_ aus aller Welt. Es handelt sich dabei fast ausschließlich um Originalwerke aus Deutschland, Frankreich, England, Norwegen, Estland, Australien und Japan.

Der Eintritt ist frei - Spenden werden herzlich erbeten.
