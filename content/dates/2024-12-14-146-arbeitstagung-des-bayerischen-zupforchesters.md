---
layout: dates
title: 146. Arbeitstagung des Bayerischen Zupforchesters
location: NN
beginning: 2026-01-16T16:00:59.331Z
end: 2026-01-18T12:00:59.370Z
showEnd: true
hideTime: true
showContinueReading: false
tags:
  - BLZO
  - Orchester
---
