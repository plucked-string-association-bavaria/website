---
layout: dates
title: '"Synagogenkonzert" des Zupf-Ensemble Lohr'
location: Alte Synagoge Urspringen
beginning: 2023-10-27T17:30:51.412Z
end: 2023-10-27T19:15:51.428Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupf-Ensemble Lohr lädt ein zum "Synagogenkonzert". Zu hören sind u.a. die "Sonata VI" von V. Roeser, "Intermezzo Capriccioso" und "Gavotte Serenade" von A. Amadei und die "Klezmersuite" von E. Tober-Vogt.

Im "Concertino Siciliano" von R. Paulsen-Bahnsen spielt Janin Schweizer die Soloflöte.

Höhepunkt ist die Aufführung der "Arie L'amour" und der "Fantasie" über Themen aus "Carmen" von G. Bizet mit den Sängerinnen Isabell Lang, Daniela Heidenfelder und Simone Sommer.

Isabell Lang singt auch die Lieder "Summertime" und "I got Rhythm" von G. Gershwin, begleitet vom Zupf-Ensemble.

Die Leitung hat Petra Breitenbach.

Der Eintritt kostet 10 Euro.