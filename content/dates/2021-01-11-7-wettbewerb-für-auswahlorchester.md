---
layout: dates
title: 7. Wettbewerb für Auswahlorchester
location: Trossingen
beginning: 2022-11-12T18:41:03.266Z
end: ""
hideTime: true
showEnd: false
showContinueReading: true
---

[Hier](https://zupfmusiker.de/2020/09/22/7-wettbewerb-fuer-auswahlorchester/) finden sie mehr Informationen und die digitale Ausschreibung des Wettbewerbs.
