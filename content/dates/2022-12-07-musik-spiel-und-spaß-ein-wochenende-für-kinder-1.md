---
layout: dates
title: Musik, Spiel und Spaß 2023 - Ein Wochenende für Kinder in Schaippach
location: Schullandheim Schaippach
beginning: 2023-03-11T08:00:49.103Z
end: 2023-03-12T16:00:49.124Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder von 6 bis 12 Jahren, die seit mindestens 1 Jahr Mandoline, Violine, Violoncello oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, gemeinsames Basteln, Fußball spielen, rhythmische Spiele, Wanderungen und ganz viel Spaß\
Abschlusskonzert am Sonntag, 16 Uhr

Leitung: Petra Breitenbach\
Dozenten: Petra Fröhlen, Tobias Zerlang-Roesch, Andreas Franzky, Rainer Nürnberger/John Walkowiak

Kursgebühren:\
Jugendliche, BDZ-Mitglieder: 108 €\
Jugendliche, Nichtmitglieder: 138 €

Anmeldung bis 14.02.2023

Anmeldung online möglich:\
https://form.jotformeu.com/heikoholzknecht/Kinderkurs_2023_Schaippach