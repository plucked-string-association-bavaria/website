---
layout: dates
title: 143. Arbeitstagung des Bayerischen Landeszupforchesters
location: BegegnungsCentrum Eine Welt, Neuendettelsau
beginning: 2025-02-14T16:00:15.057Z
end: 2025-02-16T14:22:15.090Z
showEnd: true
hideTime: true
showContinueReading: true
tags:
  - BLZO
  - Orchester
---
in Planung, mit Konzert