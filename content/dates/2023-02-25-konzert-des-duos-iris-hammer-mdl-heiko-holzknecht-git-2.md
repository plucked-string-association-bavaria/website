---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: Stadtbibliothek, Marktplatz 1, Erlangen
beginning: 2023-08-17T17:00:15.079Z
end: 2023-08-17T18:00:15.102Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) gestaltet das Eröffnungskonzert zur Ausstellung "Mandoline - Instrument des Jahres"

https://www.duohammerholzknecht.de/