---
layout: dates
title: 134. Arbeitstagung des Bayerischen Landeszupforchesters
location: Tagungshaus Reimlingen
beginning: 2022-07-01T06:41:49.310Z
end: 2022-07-03T06:41:49.334Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.tagungshaus-reimlingen.de/
tags:
  - BLZO
  - Orchester
---
**Programm:**

W. A. Mozart: Sinfonie Nr. 23

Béla Bartók: Mikrokosmos

Maurice Ravel: Pavane pour une infante défunte

Ludwig van Beethoven: Adagio

Carlo Gesualdo: Tenebrae factae sunt