---
layout: dates
title: Konzert der Mandolinenfreunde Heimbuchenthal "Um die Welt in Hemschedahl"
location: Kirche St. Martin, Hauptstr. 27, 63872 Heimbuchenthal
beginning: 2024-04-27T17:00:49.080Z
end: 2024-04-27T19:00:49.090Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Die Mandolinenfreunde Heimbuchenthal (Leitung: Sabine Geis) gestalten zusammen mit dem Gesangverein Edelweiss und den Elsavataler Musikanten das Konzert "Um die Welt in Hemschedahl".