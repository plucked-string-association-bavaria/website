---
layout: dates
title: Konzert des Münchener Mandolinenzirkels
location: Ammersee, 82211 Herrsching
beginning: 2024-08-25T12:00:21.512Z
end: 2024-08-25T16:00:21.525Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Konzert beim Kapellentag am Ammersee!

An allen 4 Uferseiten bietet der Kapellentag im Rahmen des Musikfestes **AMMERSEErenade** Kultur\
und trifft auf bayerische Tradition, eingebettet in ein traumhaftes Stück Natur.\
Eine ideale Symbiose von Kultur, Natur, Kirche und Bewegung für Jung und Alt.\
Der Konzertbesuch des Münchener Mandolinenzirkels (Leitung: Eulee Nam) lässt sich wunderbar mit einer Radtour - von Kapelle zu Kapelle - rund um den Ammersee kombinieren.\
Weitere Infos und Programm auf [www.ammerseerenade.de](https://www.ammerseerenade.de)