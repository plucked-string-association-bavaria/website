---
layout: dates
title: 9. Arbeitstagung Bayuna
location: Jugendbildungsstätte Burg Hoheneck, 91472 Ipsheim
beginning: 2022-03-11T14:23:18.838Z
end: 2022-03-13T14:23:18.877Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.burg-hoheneck.de/
tags:
  - Bayuna
  - Kurs
---
**Was passiert?** Auffrischen des Repertoires.    

**Programm:** Beethoven, Kagel