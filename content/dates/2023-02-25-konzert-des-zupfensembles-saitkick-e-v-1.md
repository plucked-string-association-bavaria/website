---
layout: dates
title: Konzert des Zupfensembles SaitKick e.V. "Lovely Memories"
location: Pfarrer-Hersam-Haus, Salzstraße 13, 97447 Gerolzhofen
beginning: 2024-04-20T17:00:12.858Z
end: 2024-04-20T19:00:12.882Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Für alle Musikliebhaber hält das Zupfensemble SaitKick e.V. ein besonderes Ereignis bereit – "Lovely Memories", ein bezauberndes Konzert, das im Pfarrer-Hersam-Haus in Gerolzhofen stattfindet.

Das Zupfensemble SaitKick verspricht unter der Leitung von Maximilian Zink einen Abend voller fesselnder Melodien und unvergesslicher Momente. Das Programm reicht von Billie Eilishs und Khalids "Lovely", das durch zarte Melodien und zeitgenössischen Pop glänzt. Das Ensemble zeigt seine klassische Seite mit der "Sonata in g-Moll op. 2 Nr. 6" von Tomaso Albinoni, während Andreas Lorsons zeitgenössische Komposition "Kollaps" eine gelungene Brücke zwischen den Epochen schlägt.

Der Eintritt ist frei, jedoch sind Spenden herzlich willkommen.