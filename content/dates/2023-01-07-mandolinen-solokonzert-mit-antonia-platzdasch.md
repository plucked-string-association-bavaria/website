---
layout: dates
title: Mandolinen-Solokonzert mit Antonia Platzdasch
location: München
beginning: 2023-03-23T18:00:00.000Z
end: 2023-03-23T08:00:00.000Z
showEnd: false
hideTime: true
showContinueReading: false
tags:
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Solokonzert mit Antonia Platzdasch, ausgerichtet von der Münchner Konzertgesellschaft.

Zu hören ist Literatur aus jeder Epoche, auch mit der Barockmandoline

**Konzert nicht öffentlich! Nur für geschlossene Gesellschaft**