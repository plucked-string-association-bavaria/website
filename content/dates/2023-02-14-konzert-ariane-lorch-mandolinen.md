---
layout: dates
title: Konzert Ariane Lorch, Mandolinen
location: Schloss Homburg/Main, 97855 Triefenstein
beginning: 2023-06-11T15:00:32.113Z
end: 2023-06-11T16:30:32.142Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Die Mandolinistin Ariane Lorch spielt ein Geschichtskonzert durch die Epochen auf 4 verschiedenen Mandolinen. Es erklingen Werke von G. Ph. Telemann, P. Denis, R. Calace, D. Huschert und G. Yoshida. Die Sonatine c-moll von L. v. Beethoven erklingt im Duo mit Michael Günther (Hammerklavier)