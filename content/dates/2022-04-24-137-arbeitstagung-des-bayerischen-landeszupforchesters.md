---
layout: dates
title: 137. Arbeitstagung des Bayerischen Landeszupforchesters
location: Musikakademie Schloß Weikersheim
beginning: 2023-04-28T15:00:13.439Z
end: 2023-05-01T11:00:13.515Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.jmd.info/musikakademie-schloss-weikersheim
tags:
  - BLZO
  - Orchester
  - Mandoline – Instrument des Jahres 2023
---
in Planung, mit Konzert