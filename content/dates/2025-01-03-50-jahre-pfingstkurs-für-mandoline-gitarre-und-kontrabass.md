---
layout: dates
title: 50 Jahre Pfingstkurs für Mandoline, Gitarre und Kontrabass
location: Bayerische Musikakademie Hammelburg
beginning: 2025-06-09T11:00:33.197Z
end: 2025-06-13T22:00:33.206Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Pfingstkurs
---
Einwöchiger Lehrgang für Mandoline, Gitarre und Kontrabass.

Beginn: Montag, 09.06.25, Anreise bis 13 Uhr\
Ende: Samstag, 14.06.25, nach dem Abschlusskonzert gegen 12 Uhr

Ein fachliches Angebot von Einzelunterricht am Zupfinstrument, Techniktraining, Theorieunterricht und Ensemblespiel wird ergänzt durch Zusatzkurse wie Liedbegleitung, Bodypercussion und Schnupperkurs Kontrabass. Es können die Laienmusikprüfungen D1, D2 und D3 abgelegt werden. 

Eröffnungskonzert am Montag, 09.06.2025 um 19 Uhr mit Malte Vief, Gitarre Solo.\
Abschlusskonzert der TeilnehmerInnen und des Kursorchesters am 14.06.25, 11 Uhr.

Leitung: Petra Breitenbach\
Dozenten: Bianca Brand, Oliver Dannhauser, Oliver Kälberer, Oliver Thedieck, Malte Weyland

Kursgebühr (incl. Unterricht, Unterkunft im DZ und Vollverpflegung):

Jugendliche Mitglieder des BDZ LV Bayern: 250 €\
Erwachsene Mitglieder des BDZ LV Bayern: 350 €\
Jugendliche Nichtmitglieder: 350 €\
Erwachsene Nichtmitglieder: 470 €

Anmeldung möglich bis 31.03.2025 unter:

**https://form.jotform.com/250025114278347**

<!--EndFragment-->