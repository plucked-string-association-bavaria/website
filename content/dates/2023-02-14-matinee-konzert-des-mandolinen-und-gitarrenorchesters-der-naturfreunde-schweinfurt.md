---
layout: dates
title: Jahreskonzert des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Naturfreundehaus Schweinfurt, Friedrich-Ebert-Str. 1
beginning: 2023-11-11T18:30:53.863Z
end: 2023-11-11T20:15:53.888Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupforchester spielt sein Jahreskonzert unter der Leitung von Bianca Brand.

 Zur Aufführung gelangen das Konzert D-Dur von J.F. Fasch für Solomandoline und Zupforchester (Solistin: Hannah Pfister, Mandoline) und das Concertino für Soloinstrument und Zupforchester von G. Gunsenheimer (Solistin: Sabine Stawitzki, Fagott). 

Außerdem werden Werke von M. Tröster, A. Amadei, S. Wagner, Coldplay und den Comedian Harmonists zu Gehör kommen. Das Orchester wird verstärkt durch Percussion.

Die Spielerinnen und Spieler des Orchester freuen sich, Sie in Schweinfurt begrüßen zu dürfen.\
Eintritt: 12 Euro, 8 Euro erm. (SchülerInnen/Studierende)