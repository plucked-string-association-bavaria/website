---
layout: dates
title: Konzert des Zupfensembles SaitKick e.V.
location: Pfarrer-Hersam-Haus, Salzstr. 13, Gerolzhofen
beginning: 2023-05-20T17:00:23.142Z
end: 2023-05-20T18:00:23.164Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble SaitKick spielt unter der Leitung von Maximilian Zink sein vielseitiges Konzertprogramm mit Werken von Rossini bis Coldplay. Zum ersten Mal wird der einzigartige Klang von Gitarre, Mandola und Mandoline von Percussion unterstützt. Auch die E-Gitarre ist mit dabei.

Einlass: 18:30 Uhr