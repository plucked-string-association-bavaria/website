---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: ev.luth. Kirche, Von-Seckendorff-Str. 1, 91074 Herzogenaurach
beginning: 2023-10-22T15:00:26.943Z
end: 2023-10-22T16:30:26.959Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) musiziert zusammen mit dem Kammerorchester "Concertino Ducale".

https://www.duohammerholzknecht.de/