---
layout: dates
title: 47. Pfingstkurs für Mandoline, Gitarre und Kontrabass
location: Bayerische Musikakademie Hammelburg
beginning: 2024-05-20T10:00:09.128Z
end: 2024-05-25T09:00:09.145Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Pfingstkurs
---
Einwöchiger Lehrgang für Mandoline, Gitarre und Kontrabass.

Beginn: Montag, 20.05.24, Anreise bis 13 Uhr\
Ende: Samstag, 25.05.24, nach dem Abschlusskonzert gegen 12 Uhr

Ein fachliches Angebot von Einzelunterricht am Zupfinstrument, Techniktraining, Theorieunterricht und Ensemblespiel wird ergänzt durch Zusatzkurse wie Liedbegleitung, Bodypercussion, Schnupperkurs Kontrabass, Dirigieren und Improvisation. Es können die Laienmusikprüfungen D1, D2 und D3 abgelegt werden. Abschlusskonzert der TeilnehmerInnen und des Kursorchesters am 25.05.24 in der Musikakademie.

Leitung: Petra Breitenbach\
Dozenten: Bianca Brand, Oliver Dannhauser, Oliver Kälberer, Malte Weyland

Kursgebühr (incl. Unterricht, Unterkunft im DZ und Vollverpflegung):

Jugendliche Mitglieder des BDZ LV Bayern: 250 €\
Erwachsene Mitglieder des BDZ LV Bayern: 350 €\
Jugendliche Nichtmitglieder: 350 €\
Erwachsene Nichtmitglieder: 470 €

Anmeldung möglich bis 16.03.2024 unter:

https://form.jotform.com/220503730794352