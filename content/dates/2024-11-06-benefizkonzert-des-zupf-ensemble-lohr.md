---
layout: dates
title: Benefizkonzert des Zupf-Ensemble Lohr
location: Ulmer Haus, 97816 Lohr a. M.
beginning: 2024-11-15T18:30:22.224Z
end: 2024-11-15T19:30:22.236Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Zupf-Ensemble Lohr spielt ein Benefizkonzert zugusten des neuen
Glockenstuhl der evang.-luth. Auferstehungskirche Lohr.

Solistin: Inken Hochapfel, Blockflöte. Die Leitung hat Petra Breitenbach.