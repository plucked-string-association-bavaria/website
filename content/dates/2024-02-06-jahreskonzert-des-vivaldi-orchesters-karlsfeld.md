---
layout: dates
title: Jahreskonzert des Vivaldi Orchesters Karlsfeld
location: Bürgerhaus, Allacher Str. 1, 85757 Karlsfeld
beginning: 2024-06-08T17:30:44.434Z
end: 2024-06-08T19:30:44.447Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
In 99 Minuten um die Welt

Musik aus allen Kontinenten - lauschen Sie den Klängen des Vivaldi Orchesters Karlsfeld rund um den ganzen Globus. Nicht nur im Programm befinden sich die Vivaldis auf neuen Wegen: Das von Monika Fuchs-Warmhold gegründete Ensemble aus rund 40 Musikerinnen und Musikern konzertiert zum ersten Mal unter der Leitung seines neuen Dirigenten Heiko Holzknecht.