---
layout: dates
title: Jahreskonzert des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Naturfreundehaus Schweinfurt, Friedrich-Ebert-Str. 1
beginning: 2024-10-19T17:30:55.552Z
end: 2024-10-19T19:30:55.569Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Unter dem Titel "Schweinfurter Komponisten" spielt das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt sein Jahreskonzert (Leitung: Bianca Brand).

Zu hören sind Werke von R. Vollmann, M. Tröster, E. Tober-Vogt, L. Schmidt, G. Gunsenheimer, J. Klose und J. Thiergärtner. Die Solistinnen Christiane Baur (Querflöte), Sabine Stawitzki (Fagott) und Bastian Brand (Percussion) bereichern das Orchester.

Die Spielerinnen und Spieler des Orchester freuen sich, Sie in Schweinfurt begrüßen zu dürfen.\
Eintritt: 12 Euro, 8 Euro erm. (SchülerInnen/Studierende)