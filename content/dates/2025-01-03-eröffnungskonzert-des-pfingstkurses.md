---
layout: dates
title: Eröffnungskonzert des Pfingstkurses
location: Hammelburg, Bay. Musikakademie
beginning: 2025-06-09T17:00:32.351Z
end: 2025-06-09T18:15:32.363Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Pfingstkurs
---
**Wir feiern 50 Jahre Pfingstkurs!**

Das Eröffnungskonzert gestaltet Malte Vief (Gitarre Solo) am Pfingstmontag um 19 Uhr im Saal der Musikakademie Hammelburg. 

Der Musiker, Komponist und Gitarrist schöpft aus den großen Möglichkeiten des Saiteninstrumentes Gitarre, er lotet diese aus, erneuert, erweitert und erschafft ein neues Klangerlebnis. Er musiziert mal ruhig reminiszierend, mal wild verarbeitend, mal klassisch-konzertant, mal folkig oder neuartig virtuos. Spannende Interpretationen, kompositorischer Witz und eine hohe Virtuosität machen dieses Konzert sicherlich zu einem ganz besonderen Erlebnis.

<!--EndFragment-->