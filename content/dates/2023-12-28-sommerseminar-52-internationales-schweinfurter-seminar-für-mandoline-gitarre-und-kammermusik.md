---
layout: dates
title: "Sommerseminar: 53. Internationales Schweinfurter Seminar für Mandoline,
  Gitarre und Kammermusik"
location: Hammelburg
beginning: 2024-08-04T14:00:36.701Z
end: 2024-08-11T08:00:36.713Z
showEnd: true
hideTime: true
showContinueReading: true
link: https://www.bmhab.de/
tags:
  - Öffentlich
  - Kurs
  - Schweinfurter Seminar
  - Veranstaltung
---
Einwöchiger Kurs für Zupfinstrumentenspieler:innen ab 18 Jahren aller Leistungsstufen.  Sowohl Spieler:innen aus dem Laienbereich und den verschiedenen Heimatorchestern, als auch deren Leiter:innen, Ausbilder:innen und Lehrer:innen werden durch die namhaften Dozent:innen gefördert und unterrichtet. Jede:r Teilnehmer:in erhält dabei täglich 30 Minuten Einzelunterricht und kann sich zudem aus einem reichhaltigen Unterrichtsangebot (Technikstunden, Dirigieren, didaktische Vorträge, Forumsunterricht, Workshops) einen eigenen Stundenplan zusammenstellen. Viele Ensemble-Stunden und Konzerte runden das Seminar ab und geben Anregungen für das eigene Musizieren.

Leitung: Bianca Brand (Mandoline, Gitarre)\
Dozententeam: Michael Tröster (Gitarre), Stefan Schmidt (Gitarre), Malte Vief (Gitarre), Steffen Trekel (Mandoline), Laura Engelmann (Mandoline, Gitarre), Iwan Urwalow (Klavierkorrepetition)

Kosten für Unterricht:\
für BDZ-Mitglieder aus Bayern: 190 Euro\
für Studenten/Schüler, BDZ-Mitglieder aus Bayern: 160 Euro\
für alle anderen: 280 Euro

Kosten für Unterkunft/Vollverpflegung in der Bay. Musikakademie Hammelburg:\
für DZ pro Person: 290 Euro\
für EZ pro Person: 430 Euro

Anmeldung ab sofort möglich unter: 

https://form.jotform.com/200324600553340

Anmeldeschluss: 18. Mai 2024