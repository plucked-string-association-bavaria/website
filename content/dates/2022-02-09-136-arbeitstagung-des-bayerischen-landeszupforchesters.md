---
layout: dates
title: 136. Arbeitstagung des Bayerischen Landeszupforchesters
location: Gästehaus St. Georg Kloster Weltenburg, Asamstr. 32, 93309 Kehlheim
beginning: 2023-02-10T16:00:23.987Z
end: 2023-02-12T12:00:24.015Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.gaestehaus.kloster-weltenburg.de
tags:
  - BLZO
  - Orchester
  - Mandoline – Instrument des Jahres 2023
---
Mit Matinee-Konzert am Sonntag in Bad Abbach