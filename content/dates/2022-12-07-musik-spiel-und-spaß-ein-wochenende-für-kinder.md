---
layout: dates
title: Musik, Spiel und Spaß 2023 - Ein Wochenende für Kinder in Reichmannshausen
location: Schullandheim Reichmannshausen
beginning: 2023-03-11T08:00:51.874Z
end: 2023-03-12T16:00:51.896Z
showEnd: true
hideTime: false
showContinueReading: true
link: ""
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder, die seit mindestens einem Jahr Mandoline oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, rhythmische Spiele, gemeinsames Basteln, Fußball spielen, Wanderungen und viel Spaß\
Abschlusskonzert am Sonntag, 16 Uhr

Leitung: Rosa Faerber\
Dozenten: Bianca Brand, Alexander Stöhr

Kursgebühren (incl. Unterricht, Unterkunft und Vollverpflegung):\
Jugendliche BDZ-Mitglieder: 108 €\
Jugendliche Nichtmitglieder: 138 €

Anmeldung bitte bis 14.02.2023

Anmeldung auch online möglich:

**https://form.jotform.com/230111172071336**