---
layout: dates
title: Konzert des Münchener Mandolinen-Zirkels
location: Forum 2, Nadistr. 3, 80809 München
beginning: 2023-11-05T18:30:40.102Z
end: 2023-11-05T20:00:40.117Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
In diesem Konzert werden die international unterschiedlichen Stile und Facetten der Mandoline zum Klingengebracht: Zu hören sind Werke aus Italien, Deutschland, Spanien, Japan und Amerika. Ein Highlight werden zwei Solokonzerte sein: das Konzert in C-Dur für Solo-Mandoline und Zupforchester von Antonio Vivaldi in einer Bearbeitung von Siegfried Behrend und das Solokonzert Grave und Fandango von Luigi Boccherini für Solo-Gitarre und Zupforchester (Original: Gitarre + Streichquintett). Leitung: Eulee Nam

<!--EndFragment-->