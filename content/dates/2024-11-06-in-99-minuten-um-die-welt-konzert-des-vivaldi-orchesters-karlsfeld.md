---
layout: dates
title: '"In 99 Minuten um die Welt" - Konzert des Vivaldi Orchesters, Karlsfeld'
location: Allerheiligen Hofkirche München, Residenzstr. 1
beginning: 2024-11-15T19:00:02.914Z
end: 2024-11-15T21:00:02.928Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Vivaldi-Orchester Karlsfeld geht erneut auf Reisen und wiederholt damit weitgehend das Jahreskonzert vom 8. Juni in der besonderen Atmosphäre der Allerheiligen Hofkirche in München. Zur Aufführung kommen u.a. Werke von Antonio Vivaldi, Andreas Lorson, Richard Charlton, Chris Aquavella und Takashi Kubota. 

Leitung: Heiko Holzknecht