---
layout: dates
title: "Sommerseminar: 51. Internationales Schweinfurter Seminar für Mandoline,
  Gitarre und Kammermusik"
location: Hammelburg
beginning: 2022-08-07T13:00:50.671Z
end: 2022-08-14T08:00:50.825Z
showEnd: true
hideTime: true
showContinueReading: true
link: https://www.bmhab.de/
tags:
  - Öffentlich
  - Kurs
---
Ein Kurs für Gitarristen/-innen und Mandolinisten/-innen aller Alters- und Leistungsstufen. Instrumentalisten, die Spaß und Freude an der Zupfmusik und dem gemeinsamen Musizieren haben. 

[Mehr Informationen](https://www.bdz-bayern.de/bdz/seminars/schweinfurter-seminar) und [Flyer](https://bdz-bayern.de/assets/uploads/flyer-sw-seminar-2022-web.pdf)

[Anmeldung](https://form.jotform.com/heikoholzknecht/SWSeminar2022)