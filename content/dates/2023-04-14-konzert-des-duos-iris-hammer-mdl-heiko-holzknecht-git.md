---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: St. Gereon, Nürnberger Str. 1, 91301 Forchheim
beginning: 2023-11-10T18:30:11.176Z
end: 2023-11-10T20:30:11.212Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) musiziert zum Thema "Sonne, Regen, Wolken und Wind - Atmosphärisches aus allen Kontinenten"

https://www.duohammerholzknecht.de/