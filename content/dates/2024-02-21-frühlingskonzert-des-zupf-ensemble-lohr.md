---
layout: dates
title: Frühlingskonzert des Zupf-Ensemble Lohr
location: Alte Turnhalle, 97816 Lohr a. M.
beginning: 2024-04-20T17:30:07.820Z
end: 2024-04-20T19:30:07.833Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupf-Ensemble Lohr lädt ein zum "Frühlingskonzert". 

Zu hören sind u.a. das "Concertino für Altblockflöte und  Orchester" von G. Gunsenheimer, das "Konzert in D-Dur" von G. P. Telemann, "Sug Al-Safafir in Fall" von Rami Al-Regeb, "Arie L'amour" und "Fantasie" über Themen aus "Carmen" von G. Bizet, u.a.

Die Leitung hat Petra Breitenbach.