---
layout: dates
title: Konzert des Zupfensemble Lohr "Il Mandolino"
location: Alte Turnhalle, Gärtnerstr. 2, Lohr a. Main
beginning: 2023-05-13T17:30:43.441Z
end: 2023-05-13T19:30:43.464Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble Lohr spielt ein Konzert unter der Leitung von Petra Breitenbach. Neben Werken von D. Kreidler, A. Streichardt, M. Kugler und C. Stamitz ist das "Concerto in col maggiore" für 2 Mandolinen und Zupforchester von A. Vivaldi zu hören. Solistinnen sind Christine Heinz und Mareike Burba, die außerdem einige Duowerke zu Gehör bringen.