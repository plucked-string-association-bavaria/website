---
layout: dates
title: Abschlusskonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2023-08-12T17:30:05.311Z
end: 2023-08-12T19:00:05.339Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Öffentlich
---
Das Abschlusskonzert des Schweinfurter Seminars gestalten die Teilnehmerinnen und Teilnehmer des Kurses. 

Gesamtleitung: Bianca Brand

Einstudierung durch das Dozententeam:\
Steffen Trekel (Mandoline), Denise Wambsganß (Mandoline), Michael Tröster (Gitarre), Boris Tesic (Gitarre), Karoline Laier (Gitarre), Iwan Urwalow (Klavierkorrepetition)