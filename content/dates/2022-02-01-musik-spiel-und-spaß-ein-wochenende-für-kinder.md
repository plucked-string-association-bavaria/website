---
layout: dates
title: Musik, Spiel und Spaß 2022 - Ein Wochenende für Kinder in Schaippach
location: Schaippach
beginning: 2022-10-22T06:47:29.956Z
end: 2022-10-23T06:47:30.000Z
showEnd: true
hideTime: true
showContinueReading: false
link: ""
tags:
  - Kurs
  - Öffentlich
---
<https://form.jotformeu.com/heikoholzknecht/Kinderkurs_2023_Schaippach>