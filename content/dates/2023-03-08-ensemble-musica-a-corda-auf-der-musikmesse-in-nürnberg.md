---
layout: dates
title: Ensemble musica a corda auf der Musikmesse in Nürnberg
location: Nürnberg (Center Stage Saal München 1)
beginning: 2023-03-25T09:00:19.719Z
end: 2023-03-25T10:00:19.734Z
showEnd: true
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
---
Am Samstag, 25. März 2023 von 10:00-11:00 Uhr 

![](/assets/uploads/fotos-mediathek.png)

\
wird das Ensemble musica a corda als Botschafter des Bubenreuther Museumsvereins Bubenreutheum e.V. auf der Musikmesse (Teil der Messe Freizeit, Touristik und Garten)\
in Nürnberg zu hören sein. Wir haben die Ehre, anlässlich der Wahl der Mandoline zum Instrument des Jahres 2023 unsere Instrumente und unsere Musik einer sehr breiten Öffentlichkeit vorzustellen und würden uns sehr freuen, wenn auch Sie vorbeischauen würden.