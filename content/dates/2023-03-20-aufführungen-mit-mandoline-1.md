---
layout: dates
title: Opernaufführungen mit Mandoline
location: Bay. Staatsoper, München
beginning: 2023-06-27T18:00:41.344Z
end: 2023-11-11T19:00:41.366Z
showEnd: false
hideTime: true
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Die Oper "Otello" von Giuseppe Verdi wird in der Bayerischen Staatsoper München mit **Antje und Oliver Strömsdörfer** an den **Mandolinen** aufgeführt.

Aufführungstermine:

27., 30.06.23

29.10.23

03., 08. und 11.11.23