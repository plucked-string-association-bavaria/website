---
layout: dates
title: Musikalische Abendstunde mit dem Zupfmusikensemble musica a corda
location: Kath. Pfarrkirche Maria Königin, Fritz-Friedrich-Str. 9, 91334 Hemhofen
beginning: 2022-07-20T17:30:00.000Z
end: 2022-07-20T19:30:00.000Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
---
![]()