---
layout: dates
title: 46. Pfingstkurs für Mandoline, Gitarre und Kontrabass
location: Bayerische Musikakademie Hammelburg
beginning: 2023-05-29T11:00:35.191Z
end: 2023-06-03T10:00:35.210Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Pfingstkurs
---
Der einwöchige Lehrgang für Mandoline, Gitarre und Kontrabass legt einen besonderen Schwerpunkt in diesem Jahr auf die Mandoline.

Ein fachliches Angebot von Einzelunterricht am Zupfinstrument, Techniktraining, Theorieunterricht und Ensemblespiel wird ergänzt durch Zusatzkurse wie Schnupperstunden und Liedbegleitung auf der Mandoline, sowie Vorträge über die Vielsaitigkeit des Instruments. Im Kursorchester werden Werke mit Solo-Mandoline einstudiert und beim Abschlusskonzert am 03.06.23 in der Musikakademie aufgeführt.

Leitung: Petra Breitenbach

Dozenten: Bianca Brand, Oliver Dannhauser, Rosa Faerber, Malte Weyland

Kursgebühr (incl. Unterricht, Unterkunft im DZ und Vollverpflegung):

Jugendliche Mitglieder des BDZ LV Bayern: 250 €\
Erwachsene Mitglieder des BDZ LV Bayern: 350 €\
Jugendliche Nichtmitglieder: 350 €\
Erwachsene Nichtmitglieder: 470 €

(Anmeldung nicht mehr möglich)