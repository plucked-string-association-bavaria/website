---
layout: dates
title: Bezirkskonzert der mittelfränkischen Zupforchester
location: Kirche Heilige Familie, Erlangen-Tennenlohe
beginning: 2023-10-29T15:30:25.220Z
end: 2023-10-29T17:30:25.296Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Der Nürnberger Mandolinen- und Gitarrenverein "Noris Süd" (Leitung: Birgit Schiller), das Mandolinenorchester Girgner (Leitung: Siegfried Girgner) , das Gitarrenensemble "Spaß by Saite" (Leitung: Elli Wilhelm), der Tennenloher Mandolinen- und Gitarrenspielkreis (Leitung: Winfried Hübner), und dasZupfmusikensemble „musica a corda“ (Leitung: Iris Hammer) bieten ein abwechslungsreiches Programm mit Musik unterschiedlichster Epochen und Stilrichtungen.

<!--EndFragment-->