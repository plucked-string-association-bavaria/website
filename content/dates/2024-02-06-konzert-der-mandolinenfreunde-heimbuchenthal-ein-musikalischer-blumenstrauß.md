---
layout: dates
title: Konzert der Mandolinenfreunde Heimbuchenthal "Ein musikalischer Blumenstrauß"
location: "Kirche St. Rochus, Rochus-Str. 1, 63849 Volkersbrunn "
beginning: 2024-05-05T15:00:46.872Z
end: 2024-05-05T17:00:46.880Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Die Mandolinenfreunde Heimbuchenthal (Leitung: Sabine Geis) gestalten zusammen mit dem Rochus-Chor Volkersbrunn das Konzert "Ein musikalischer Blumenstrauß".