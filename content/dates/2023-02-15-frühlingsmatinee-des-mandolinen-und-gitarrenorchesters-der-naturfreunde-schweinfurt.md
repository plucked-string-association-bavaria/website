---
layout: dates
title: Frühlingsmatinee des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Naturfreundehaus Schweinfurt, Friedrich-Ebert-Str. 1
beginning: 2023-04-23T09:00:44.341Z
end: 2023-04-22T22:00:44.368Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupforchester spielt eine Frühlingsmatinee unter der Leitung von Bianca Brand.  Zur Aufführung gelangt u. a. das Konzert D-Dur von J.F. Fasch für Solomandoline und Zupforchester. Solistin ist Hannah Pfister, Mandoline. Außerdem werden Werke von M. Maciocchi, S. Joplin, A. Amadei, S. Wagner und K. Schindler zu Gehör kommen. Das Orchester wird verstärkt durch Querflöte und Percussion.

Die Spielerinnen und Spieler des Orchester freuen sich, Sie in Schweinfurt begrüßen zu dürfen.\
Der Eintritt ist frei, um Spenden wird gebeten.