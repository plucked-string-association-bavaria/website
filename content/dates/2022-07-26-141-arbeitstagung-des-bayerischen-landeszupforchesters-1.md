---
layout: dates
title: 142. Arbeitstagung des Bayerischen Landeszupforchesters
location: Kloster Rohr, 93352 Rohr
beginning: 2024-10-11T15:00:09.551Z
end: 2024-10-13T11:37:09.566Z
showEnd: true
hideTime: true
showContinueReading: true
tags:
  - BLZO
  - Orchester
---
in Planung, mit Konzert in Bad Abbach - 11 Uhr - Kurhaus