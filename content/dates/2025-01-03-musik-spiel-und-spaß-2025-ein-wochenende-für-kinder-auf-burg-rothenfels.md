---
layout: dates
title: Musik, Spiel und Spaß 2025 - Ein Wochenende für Kinder auf Burg Rothenfels
location: Burg Rothenfels
beginning: 2025-03-22T08:00:18.235Z
end: 2025-03-23T15:30:18.250Z
showEnd: true
hideTime: false
showContinueReading: true
link: ""
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder, die seit mindestens einem Jahr Mandoline, Violine, Violoncello oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, rhythmische Spiele, gemeinsames Basteln, Fußball spielen, Wanderungen und viel Spaß\
Abschlusskonzert am Sonntag, 16 Uhr

Leitung: Petra Breitenbach\
Dozenten: Petra Fröhlen, Andreas Franzky, Rainer Nürnberger, Katrin Prütting, Tobias Zerlang-Roesch 

Kursgebühren (incl. Unterricht, Unterkunft und Vollverpflegung):\
Jugendliche BDZ-Mitglieder: 108 €\
Jugendliche Nichtmitglieder: 138 €

Anmeldung bis 28.02.2025 möglich:

**https://form.jotform.com/250025798036357**

<!--EndFragment-->