---
layout: dates
title: Promenaden-Konzert des Bayerischen Landeszupforchesters
location: Wandelhalle Kurhaus Bad Mergentheim
beginning: 2023-05-01T08:30:35.497Z
end: 2023-05-01T09:30:35.564Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Lassen Sie sich mit dem Instrument des Jahres 2023 -  der Mandoline - mit tollen Klängen verführen. Als Höhepunkt hören Sie das Concerto Nr. 3 für Solomandoline und Schlagzeug von Daniel Huschert.  Als Solist spielt für Sie an der Mandoline Christian Laier, Percussion Fabio Schroll. \
Außerdem erwarten Sie noch interessante Werke von R. Balkanski, L. van Beethoven, W. A. Mozart und M. Ravel.

Wir freuen uns, Sie in Bad Mergentheim begrüßen zu dürfen.

![](/assets/uploads/plakat-blzo-bad-mergentheim-1.jpg)