---
layout: dates
title: Konzert des Münchener Mandolinen-Zirkels
location: A.-F.-Bildungszentrum, Schlierseestr. 47, München-Giesing
beginning: 2023-11-12T14:30:45.319Z
end: 2023-11-12T15:30:45.347Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
In diesem Konzert werden die international unterschiedlichen Stile und Facetten der Mandoline zum Klingengebracht: Zu hören sind Werke aus Italien, Deutschland, Spanien, Japan und Amerika. Ein Highlight werden zwei Solokonzerte sein: das Konzert in C-Dur für Solo-Mandoline und Zupforchester von Antonio Vivaldi in einer Bearbeitung von Siegfried Behrend und das Solokonzert Grave und Fandango von Luigi Boccherini für Solo-Gitarre und Zupforchester (Original: Gitarre + Streichquintett). Leitung: Eulee Nam

<!--EndFragment-->