---
layout: dates
title: Gemeinschaftskonzert des Mandolinen- und Gitarrenorchesters der
  Naturfreunde Schweinfurt, des Zupfensemble "Spätlese" Hessen und des
  Zupforchesters Schweinfurt
location: Kirche der Bay. Musikakademie Hammelburg
beginning: 2024-10-05T17:00:00.602Z
end: 2024-10-05T19:00:00.618Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt (Leitung: Bianca Brand), das Zupfensemble "Spätlese" Hessen (Leitung: Carmen und Jürgen Thiergärtner) und das Zupforchester Schweinfurt (Leitung: Carmen Thiergärtner) gestalten zusammen ein Konzert in der Kirche neben der Bay. Musikakademie Hammelburg.

Höhepunkt eines abwechslungsreichen Konzertes wird die gemeinsame Aufführung der Traumbilder von R. Vollmann.

Der Eintritt ist frei, um Spenden wird gebeten.