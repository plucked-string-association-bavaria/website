---
layout: dates
title: Konzert mit Streicher
location: '"Alte Turnhalle", 97816 Lohr a. Main'
beginning: 2023-11-12T16:00:00.168Z
end: 2023-11-12T17:00:00.186Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Streicher-Serenade zur Finnissage der Ausstellung von SpessArt: Es erklingt das Concerto G-Dur von J. N. Hummel mit der Solistin Antonia Platzdasch (Mandoline)