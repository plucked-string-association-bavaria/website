---
layout: dates
title: Jahreskonzert des Münchener Mandolinenzirkels
location: Anton-Fingerle-Bildungszentrum, Schlierseestr. 47, 81539 München-Giesing
beginning: 2024-11-10T14:30:33.618Z
end: 2024-11-10T16:30:33.628Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Der Müchener Mandolinenzirkel spielt sein Jahreskonzert unter der Leitung von Eulee Nam.