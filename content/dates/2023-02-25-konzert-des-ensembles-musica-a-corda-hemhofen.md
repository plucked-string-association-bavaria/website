---
layout: dates
title: Konzert des Ensembles musica a corda, Hemhofen
location: Senioren Centrum St. Anna, Am Brauhaus 1, Höchstadt a.d. Aisch
beginning: 2023-03-19T15:15:42.352Z
end: 2023-03-19T16:15:42.377Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble musica a corda musiziert unter der Leitung von Iris Hammer.