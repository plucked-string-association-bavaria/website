---
layout: dates
title: Dozentenkonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2023-08-08T18:00:51.394Z
end: 2023-08-08T19:50:51.416Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
---
Das Dozentenkonzert des Schweinfurter Seminar gestaltet der mehrfach preisgekrönte klassische Gitarrist und Dozent Boris Tesic.

Seine eigene musikalische Reise begann in der Klasse von Predrag Stankovic, einem Lehrer, der neue Maßstäbe für die klassische Gitarre gesetzt hat und unter dessen Anleitung sich viele bekannte Gitarristen unserer Zeit entwickelt haben. Danach war Boris ein Stipendiat am renommierten Royal College of Music in London und hat seine Fähigkeiten am Conservatorium Maastricht bei Carlo Marchione und in Kassel bei Michael Tröster verfeinert.

Seit 2013 unterrichtet er an der Musikakademie in Kassel, wo er es genießt, sein umfangreiches Wissen mit Studenten aus vielen Teilen der Welt zu teilen. Darüber hinaus schreibt Boris regelmäßig für das Magazin "Akustik Gitarre" und leitet das lokale Amateur-Zupfensemble "Herkules".