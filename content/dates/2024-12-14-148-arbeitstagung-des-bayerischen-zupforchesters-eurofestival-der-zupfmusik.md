---
layout: dates
title: 148. Arbeitstagung des Bayerischen Zupforchesters - Eurofestival der Zupfmusik
location: Bruchsal
beginning: 2026-05-14T15:00:03.096Z
end: 2026-05-17T11:00:03.109Z
showEnd: true
hideTime: true
showContinueReading: true
link: https://www.zupfmusiker.de/eurofestival/
tags:
  - BLZO
  - Öffentlich
  - Orchester
  - Veranstaltung
---
