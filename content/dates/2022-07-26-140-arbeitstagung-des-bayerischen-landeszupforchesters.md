---
layout: dates
title: 140. Arbeitstagung des Bayerischen Landeszupforchesters
location: Bayerische Musikakademie Hammelburg
beginning: 2024-04-26T15:00:00.485Z
end: 2024-04-28T13:30:00.000Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Orchester
---
in Planung, mit Konzert - Uhrzeit noch in Klärung