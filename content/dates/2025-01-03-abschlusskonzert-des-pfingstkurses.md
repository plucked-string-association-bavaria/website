---
layout: dates
title: Abschlusskonzert des Pfingstkurses
location: Hammelburg, Bay. Musikakademie
beginning: 2025-06-14T09:00:35.062Z
end: 2025-06-13T22:00:35.073Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Pfingstkurs
---
**Wir feiern 50 Jahre Pfingstkurs!**

Das Abschlusskonzert des Pfingstkurses gestalten die KursteilnehmerInnen unter der Gesamtleitung von Petra Breitenbach.

Zu hören sind Solowerke für Mandoline und Gitarre und kammermusikalisches, sei es als Duo, Trio oder in kleinen Ensembles. Die Bodypercussion-Gruppe wird ein rhythmisches Werk mit Körperinstrumenten und Boomwhakers aufführen, die beiden Liedbegleitungsgruppen werden bekannte Charts mit den Zupfinstrumenten begleiten und dazu singen.\
Höhepunkt ist sicherlich das in der Woche einstudierte Werk des Kursorchesters unter Leitung von Oliver Kälberer.

Der Eintritt ist frei.

<!--EndFragment-->