---
layout: dates
title: Matineekonzert des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Naturfreundehaus Schweinfurt, Friedrich-Ebert-Str. 1
beginning: 2024-04-14T09:00:27.917Z
end: 2024-04-13T22:00:27.931Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt (Leitung: Bianca Brand) spielt ein Matineekonzert mit Werken von R. Vollmann, M. Tröster, E. Tober-Vogt, J. Klose und G. Gunsenheimer.

Der Eintritt ist frei, um Spenden wird gebeten.