---
layout: dates
title: Auftritte des Fränkischen Zupforchesters
location: Bay. Musikakademie Hammelburg, Serenadenhof
beginning: 2024-04-21T13:30:13.184Z
end: 2024-04-21T16:30:13.193Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Die Bay. Musikakademie Hammelburg wird 44 Jahre alt und feiert das „Festival der musikalischen Vielfalt“.

Hierzu präsentieren sich die verschiedensten Instrumente und Gruppierungen.

Der BDZ LV Bayern spielt mit einem "Fränkischen Zupforchester" unter der Leitung von Landesmusikleiterin Petra Breitenbach : \
V. Roeser (bearb. E. Tober-Vogt): Sonata VI – a grand orchestre\
G. Gunsenheimer: Sonate Nr. 1 für Altblockflöte und Orchester \
Coldplay (bearb. K. Schindler): Viva la vida\
R. Charles: Hit the road Jack 

Auftrittszeiten sind: 14.30 - 15.00 Uhr, 15.30 - 16.00 Uhr, 16.30 - 17.00 Uhr