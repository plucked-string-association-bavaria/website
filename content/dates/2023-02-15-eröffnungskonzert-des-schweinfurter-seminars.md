---
layout: dates
title: Eröffnungskonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2023-08-06T18:00:46.072Z
end: 2023-08-06T19:50:46.093Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Öffentlich
---
Die Dozenten des Schweinfurter Seminar gestalten das Eröffnungskonzert solistisch und kammermusikalisch:

1. Konzerthälfte:\
Karoline Laier, Gitarre\
Duo Consensus: Christian Laier, Mandoline & Karoline Laier, Gitarre\
\
2. Konzerthälfte:\
Michael Tröster, Gitarre\
Steffen Trekel, Mandoline\
Denise Wambsganß, Mandoline\
Bianca Brand, Mandoline/Gitarre\
Iwan Urwalow, Klavier