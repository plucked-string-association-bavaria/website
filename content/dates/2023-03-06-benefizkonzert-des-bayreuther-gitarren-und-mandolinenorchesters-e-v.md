---
layout: dates
title: Benefizkonzert des Bayreuther Gitarren- und Mandolinenorchesters e.V.
location: Lutherkirche, Bayreuth
beginning: 2023-11-19T16:00:31.267Z
end: 2023-11-19T17:00:31.295Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Bayreuther Gitarren- und Mandolinenorchester e.V. unter Leitung von Daniel Ambarjan musiziert in den Besetzungen Gitarrenensemble und Zupforchester zu Gunsten der Lutherkirche Bayreuth.