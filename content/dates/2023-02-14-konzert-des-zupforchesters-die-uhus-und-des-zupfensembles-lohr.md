---
layout: dates
title: Konzert des Zupforchesters die "uHus" und des Zupfensembles Lohr
location: Bay. Musikakademie Hammelburg
beginning: 2023-11-26T13:00:05.989Z
end: 2023-11-26T14:30:06.013Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - uHu
---

Das Zupforchester die "uHus" und das Zupfensemble Lohr spielen ein Gemeinschaftskonzert unter der Leitung von Petra Breitenbach und Jürgen Thiergärtner. Es erklingen Werke von J. A. Hasse, D. Kreidler, M. Kugler, F. Nagura und A. Starck. Als Solistinnen sind bei den Konzerten für Solomandoline und Zupforchester Karin Heilgenthal und Antje Richter-Merk zu hören.
