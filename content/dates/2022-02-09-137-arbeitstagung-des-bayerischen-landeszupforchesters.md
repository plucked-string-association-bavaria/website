---
layout: dates
title: 138. Arbeitstagung des Bayerischen Landeszupforchesters
location: Musikakademie Hammelburg
beginning: 2023-10-13T20:20:27.345Z
end: 2023-10-15T20:20:27.363Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.bmhab.de/
tags:
  - BLZO
  - Orchester
  - Mandoline – Instrument des Jahres 2023
---
In Planung, mit Konzert am Sonntag um 14:30 Uhr