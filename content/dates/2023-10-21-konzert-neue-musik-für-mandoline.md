---
layout: dates
title: Konzert "Neue Musik für Mandoline"
location: Kammermusiksaal der Hochschule für Musik, Würzburg
beginning: 2023-11-27T18:30:22.637Z
end: 2023-11-27T19:30:22.651Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Werke von G. Petrassi, H. W. Henze, E. Krenek, u.a.

Detlef Tewes, Mandoline - Jürgen Ruck, Gitarre - Andreas Mildner, Harfe