---
layout: dates
title: Konzert des Ensembles musica a corda, Hemhofen
location: Maria Königin, Kaulberg 18, Hemhofen
beginning: 2023-03-20T18:30:22.650Z
end: 2023-03-20T19:30:22.671Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble musica a corda lädt unter der Leitung von Iris Hammer zu einer musikalischen Abendmusik ein.