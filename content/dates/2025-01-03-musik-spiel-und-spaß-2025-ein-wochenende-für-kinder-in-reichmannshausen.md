---
layout: dates
title: Musik, Spiel und Spaß 2025 - Ein Wochenende für Kinder in Reichmannshausen
location: Schullandheim Reichmannshausen
beginning: 2025-03-29T08:00:44.782Z
end: 2025-03-30T14:00:44.803Z
showEnd: true
hideTime: false
showContinueReading: true
link: ""
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder, die seit mindestens einem Jahr Mandoline oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, rhythmische Spiele, gemeinsames Basteln, Fußball spielen, Wanderungen und viel Spaß\
Abschlusskonzert am Sonntag, 15 Uhr

Leitung: Bianca Brand\
Dozenten: Lukas Becker, Markus Joppich

Kursgebühren (incl. Unterricht, Unterkunft und Vollverpflegung):\
Jugendliche BDZ-Mitglieder: 108 €\
Jugendliche Nichtmitglieder: 138 €

Anmeldung bis 28.02.25 möglich:

**https://form.jotform.com/250025668812355**

<!--EndFragment-->