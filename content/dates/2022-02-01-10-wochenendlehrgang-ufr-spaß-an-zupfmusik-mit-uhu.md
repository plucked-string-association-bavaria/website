---
layout: dates
title: 10. Wochenendlehrgang Ufr - Spaß an Zupfmusik mit "uHu"
location: Bayerische Musikakademie Hammelburg
beginning: 2022-11-18T07:49:01.098Z
end: 2022-11-20T07:49:01.124Z
showEnd: true
hideTime: true
showContinueReading: false
tags:
  - Kurs
  - Orchester
---

[Die uHus](/seminars)
