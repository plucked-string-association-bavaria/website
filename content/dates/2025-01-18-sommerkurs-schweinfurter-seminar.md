---
layout: dates
title: Sommerkurs - Schweinfurter Seminar
location: Hammelburg, Bay. Musikakademie
beginning: 2025-08-10T14:00:54.993Z
end: 2025-08-17T08:00:55.007Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Schweinfurter Seminar
---
Int. Schweinfurter Seminars für Mandoline, Gitarre und Kammermusik findet statt von Sonntag, 10.08.2025, 16 Uhr bis Sonntag, 17.08.25, 10 Uhr.

Dozententeam:\
Bianca Brand (Leitung, Mandoline, Gitarre)\
Laura Engelmann (Mandoline)\
Karin Scholz (Gitarre)\
Boris Tesic (Gitarre)\
Steffen Trekel (Mandoline)\
Michael Tröster (Gitarre)\
Iwan Urwalow (Klavierkorrepetition)

Kursinhalte:\
täglich Einzelunterricht (25 min)\
Instrumentaltechnik, Kammermusik, Ensemble, Zupforchester\
Workshops u.a.:\
Die Rolle der Gitarre in Oper und Symphonischer Musik\
Klassenunterricht Zupferklasse - Möglichkeiten zur Nachwuchsgewinnung im Vereinsorchester\
„Energieflüsse nutzen“\
Atem & Ausdruck: Mit der Kraft des Atems zur musikalischen Tiefe\
Die Kunst der Melodieführung: Praxis und Grenzen auf der Gitarre\
Bodypercussion\
Dirigieren\
Literaturkunde\
Yoga für MusikerInnen\
Tai-Chi Chuan & Qigong

Kurs-Kosten:\
€ 190,- für BDZ-Mitglieder aus Bayern
€ 160,- erm. für BDZ-Mitglieder aus Bayern (Studenten, Schüler)
€ 280,- für alle anderen

Unterkunft (Doppelzimmer) mit Vollpension (vier Mahlzeiten)
in der Bay. Musikakademie Hammelburg
€ 290.- für alle

Anmeldeschluss: 23. Mai 2025

Anmeldung direkt hier möglich:\
https://form.jotform.com/250066466466361