---
layout: dates
title: Konzert des Vivaldi Orchesters, Karlsfeld
location: Bürgerhaus, Allacher Str. 1, Karlsfeld
beginning: 2023-06-24T17:00:51.796Z
end: 2023-06-24T19:00:51.825Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupforchester spielt sein Jahreskonzert unter der Leitung von Monika Fuchs-Warmhold, die sich nach 53 (!) Jahren damit vom Dirigentenpult verabschiedet.