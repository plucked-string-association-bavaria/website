---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: Hoftheater, Plinganserstr. 6, München
beginning: 2023-05-18T17:30:51.069Z
end: 2023-05-18T18:30:51.095Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) musiziert zum Thema "Sonne, Regen, Wolken und Wind - Atmosphärisches aus allen Kontinenten.

https://www.duohammerholzknecht.de/