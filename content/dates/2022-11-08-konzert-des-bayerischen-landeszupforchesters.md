---
layout: dates
title: Matinee-Konzert des Bayerischen Landeszupforchesters
location: Kurhaus Bad Abbach, Kaiser-Karl-V.-Allee 5, 93077 Bad Abbach
beginning: 2023-02-12T10:00:31.035Z
end: 2023-02-11T23:34:31.108Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
![](/assets/uploads/blzo_flyer_badabbach_r-1.jpg)

Das Bayerische Landeszupforchester lädt ein zum Matinee-Konzert im Kurhaus Bad Abbach.

Lassen Sie sich mit dem Instrument des Jahres 2023 -  der Mandoline - mit tollen Klängen verführen. Als Höhepunkt hören Sie das Concerto Nr. 3 für Solomandoline und Schlagzeug von Daniel Huschert.  Als Solist spielt für Sie an der Mandoline Christian Laier, Percussion Fabio Schroll. Außerdem erwarten Sie noch interessante Werke von R. Balkanski, L. van Beethoven, W. A. Mozart und M. Ravel.

Wir freuen uns, Sie in Bad Abbach begrüßen zu dürfen.