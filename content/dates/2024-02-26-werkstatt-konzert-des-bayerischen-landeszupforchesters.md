---
layout: dates
title: Werkstatt-Konzert des Bayerischen Landeszupforchesters
location: Kammermusiksaal der Bayerischen Musikakademie Hammelburg
beginning: 2024-04-28T12:30:20.400Z
end: 2024-04-28T13:30:20.464Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Öffentlich
  - Veranstaltung
  - Orchester
---
Erstmals im Programm des BLZO erklingt eine der großen Hamburger Sinfonien (Wq 183/1) von Carl Philipp Emanuel Bach in einer Bearbeitung für ZO von Oliver Kälberer. Mit diesem Werk öffnet Bach den musiksprachlichen Horizont des ausklingenden Barock. Im liberalen Hamburg angekommen und nach dem Verlassen des Hofs von Friedrich dem Großen in Potsdam geistig erfrischt, findet Bach in dieser Sinfonie eine völlig neue, sehr virtuose Tonsprache mit unerhörten Rhythmen und abrupten Wechseln in der Harmonik, die das Publikum staunen lassen.

In eine völlig andere musikalische Welt fühlt sich das Orchester mit der zweiten Neueinstudierung im Programm 2024 ein. Das Werk Russian Rag des amerikanischen Komponisten George L. Cobb ist ein Ragtime, der auf den drei Eröffnungsakkorden von Rachmaninoffs Prelude in cis-Moll basiert.

Das BLZO bereitet sich in dieser Arbeitstagung auf die Teilnahme am Landesorchestertreffen zu Himmelfahrt in Montabaur vor. Das Programm für dieses nationale Festivalevent erhält in Hammelburg seinen Feinschliff. So wird der 1. Satz aus dem Konzert für Solomandoline, ZO und Schlagzeug des Berliner Komponisten Daniel Huschert mit dem Erfurter Mandolinisten Christian Laier an der Mandoline und „La Notte del Principe“ aus der Feder des Dirigenten aufgeführt. Der 2. Satz „Tenebrae factae sunt“ verarbeitet eines der Responsorien des Fürsten von Venosa Don Carlo Gesualdo (1560-1613) und ist Anlass für eine Reflexion über Glück und Verderben, über Licht und Dunkelheit („I. Eclipsis“).

Abgerundet wird das Programm durch zwei weitere Bearbeitungen des ständigen künstlerischen Leiters Oliver Kälberer: 3 Klavierstücke aus dem Mikrokosmos von Béla Bartok und Maurice Ravels berühmte „Pavane pour une infante défunte“. Unterstützt wird der Dirigent von einem vierköpfigen Dozententeam mit Ariane Lorch, Elena Kisseljow, Malte Weyland und Carlos Vivas.