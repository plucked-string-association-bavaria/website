---
layout: dates
title: Landesdelegiertenversammlung 2023
location: Nürnberg-Altenfurt, Von-Soden-Straße 28, 90475 Nürnberg
beginning: 2023-02-19T12:30:00.000Z
end: 2023-02-19T14:30:00.000Z
showEnd: true
hideTime: false
showContinueReading: false
tags:
  - Veranstaltung
---
