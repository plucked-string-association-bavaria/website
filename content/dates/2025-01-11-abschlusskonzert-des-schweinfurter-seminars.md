---
layout: dates
title: Abschlusskonzert des Schweinfurter Seminars
location: Hammelburg, Bay. Musikakademie
beginning: 2025-08-16T17:30:17.964Z
end: 2025-08-16T19:30:17.979Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Schweinfurter Seminar
---
Das Abschlusskonzert des Schweinfurter Seminars spielen die KursteilnehmerInnen in verschiedenen Besetzungen. Neben Soloauftritten mit Mandoline oder Gitarre, sind kammermusikalische Werke mit Klavier zu hören und das Kursorchester mit run 40 Mitwirkenden.

Die Gesamtleitung hat Bianca Brand.

Der Eintritt ist frei.