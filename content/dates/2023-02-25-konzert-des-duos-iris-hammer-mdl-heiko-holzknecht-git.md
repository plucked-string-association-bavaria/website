---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: Klosterkirche Münchaurach, Mühlberg 11, Aurachtal
beginning: 2023-04-23T15:00:37.953Z
end: 2023-04-23T16:00:37.983Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) musiziert zum "Münchauracher Klosterfrühling".

https://www.duohammerholzknecht.de/