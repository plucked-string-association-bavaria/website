---
layout: dates
title: 13. Arbeitstagung Bayuna
location: Jugendherberge Bamberg
beginning: 2024-03-15T13:18:40.790Z
end: 2024-03-17T13:18:40.812Z
showEnd: true
hideTime: true
showContinueReading: true
tags:
  - Bayuna
  - Orchester
  - Kurs
---
Bei der 13. Arbeitstagung von Bayuna wird an einer Gegenüberstellung von Renaissance-Musik von Ravenscroft und zeitgenössiger Musik von Svetlin Hristov und Alexander Mathewson gearbeitet. Die Probenphase wird mit einem Konzert abgeschlossen.