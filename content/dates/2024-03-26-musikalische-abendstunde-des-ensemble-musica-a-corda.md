---
layout: dates
title: Musikalische Abendstunde des Ensemble musica a corda
location: ev.luth. Franziskushaus Röttenbach, Wallweg 11, 97341 Röttenbach
beginning: 2024-04-14T17:00:41.289Z
end: 2024-04-14T18:00:41.299Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Ensemble musica a corda unter der musikalischen Leitung von Iris Hammer lädt zu einer musikalischen Abendstunde ein. Es erwartet Sie ein kurzweiliges Programm mit erstklassigen Solisten: Heiko Holzknecht, Gitarre, und Andreas Kinne, Oboe, werden den Klang des Mandolinen- und Gitarrenorchesters bereichern. 

Wir freuen uns, Sie zu unserem rund einstündigen Konzert (ohne Pause) begrüßen zu dürfen.