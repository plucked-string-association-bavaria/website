---
layout: dates
title: 144. Arbeitstagung des Bayerischen Landeszupforchesters
location: Bildungshaus St. Ursula in Erfurt
beginning: 2025-04-30T15:00:37.926Z
end: 2025-05-04T20:52:37.954Z
showEnd: true
hideTime: true
showContinueReading: false
tags:
  - BLZO
  - Veranstaltung
---
Konzertreise - das BLZO besucht das Landeszupforchester Thüringen in Erfurt.

Wir planen ein tolles gemeinsames Konzert - lassen Sie sich überraschen.

Außerdem erkunden wir naürlich die Landeshauptstadt Erfurt - wir sind gespannt.