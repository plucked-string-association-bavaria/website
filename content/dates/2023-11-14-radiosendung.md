---
layout: dates
title: Radiosendung
location: Radio Bayern 2
beginning: 2023-12-17T18:30:57.330Z
end: 2023-12-17T19:00:57.350Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Mandoline – Instrument des Jahres 2023
---
**Musik für Bayern:**\
**Klassik, Pop und Jazz - selbstgemacht**

Musik für Bayern - Musik aus Franken: In den Sendungen von BR Franken Musik werden insbesondere die vielen regionalen Laienchöre mit ihren hochengagierten Chorleiterinnen und Chorleitern vorgestellt. Von Gospels und Popsongarrangements bis hin zum großen Oratorium - für jeden ist etwas dabei. Dabei gilt es, die Musik der Region in ihrer ganzen Bandbreite zu entdecken.

Im Mittelpunkt der Sendung stehen diesmal die Musikerin und Bundesverdienstkreuz-Trägerin Petra Breitenbach, der Mandolinist Malte Weyland und das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt.