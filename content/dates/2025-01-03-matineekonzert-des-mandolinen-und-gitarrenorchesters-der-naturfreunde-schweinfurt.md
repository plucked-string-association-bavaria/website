---
layout: dates
title: Matineekonzert des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Naturfreundehaus, 97421 Schweinfurt
beginning: 2025-04-06T09:00:26.689Z
end: 2025-04-06T10:30:26.703Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt (Leitung: Bianca Brand) spielt ein Matineekonzert mit Werken von R. Vollmann, J. Klose, R. Charlton, L. Schmidt, F. Caroso, J. Thiergärtner.

Dabei wird das Orchester unterstützt durch Querflöte (Christiane Baur) und Percussion (Tabea Brand).

Der Eintritt ist frei.