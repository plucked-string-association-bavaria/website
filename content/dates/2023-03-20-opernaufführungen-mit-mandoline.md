---
layout: dates
title: Opernaufführungen mit Mandoline
location: Gärtnerplatztheater München
beginning: 2023-04-04T18:00:00.000Z
end: 2023-04-16T18:00:50.606Z
showEnd: false
hideTime: true
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Die Oper "Don Giovanni" von W. A. Mozart wird mit **Antje Strömsdörfer** an der **Mandoline** aufgeführt.

Aufführungstermine:\
04., 14. und 16. April 2023