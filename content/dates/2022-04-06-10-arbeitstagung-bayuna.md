---
layout: dates
title: 10. Arbeitstagung Bayuna
location: wdl Kinderburg Starnberg
beginning: 2022-10-21T06:35:31.272Z
end: 2022-10-23T06:35:31.303Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.wdl.de/kinderburg/
tags:
  - Bayuna
  - Kurs
---
Details werden später bekannt gegeben.