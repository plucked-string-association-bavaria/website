---
layout: dates
title: 2. Wettbewerb für Orchester im BDZ
location: Bürgerhaus Wirges, Montchaninplatz 1, 56422 Wirges
beginning: 2022-05-14T17:38:19.482Z
end: ""
hideTime: true
showEnd: false
showContinueReading: true
---

[Hier](https://zupfmusiker.de/events/2-wettbewerb-fuer-orchester-im-bdz/) finden Sie die Ausschreibung und den Flyer zum Wettbewerb.
