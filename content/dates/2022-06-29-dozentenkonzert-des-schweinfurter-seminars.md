---
layout: dates
title: Dozentenkonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2022-08-09T18:00:32.231Z
end: ""
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Kurs
  - Veranstaltung
---

![](/assets/uploads/bmh-konzert-august22-herre_susanne-.jpg)

_Es ergeht herzliche Einladung zum Dozentenkonzert des 51. Schweinfurter Seminars im Kammermusiksaal der Musikakademie Hammelburg._

Am **Dienstag, 09. August 2022** zeigt die Musikerin **Susanne Herre** um **20:00 Uhr** ihre Vielsa(e)itigkeit auf Saiteninstrumenten. Die studierte Mandolinistin, Lautenistin und Gambistin erklärt in einem Gesprächskonzert das Spiel auf Gambe, Lauten und historischen Mandolinen. Dabei sind u.a. Werke von Jean de Sainte-Colombe, Giovanni Girolamo Kapsberger und Filippo Sauli zu hören. Ein spannender musikalischer Abend mit Erläuterungen zur historischen Aufführungspraxis auf Zupf- und Streichinstrumenten ist garantiert.

Der Eintritt ist frei.

<!--EndFragment-->
