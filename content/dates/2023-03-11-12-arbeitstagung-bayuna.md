---
layout: dates
title: 12. Arbeitstagung Bayuna
location: Jugendherberge Oberammergau
beginning: 2023-10-13T09:28:24.451Z
end: 2023-10-15T09:28:24.470Z
showEnd: true
hideTime: true
showContinueReading: true
tags:
  - Bayuna
  - Orchester
  - Kurs
---
Bei der 12. Arbeitstagung von Bayuna steht zeitgenössische Musik im Vordergrund.

Es werden Werke von Svetlin Hristov, Alexander Mathewson und Silvan Wagner für Zupforchester und Gitarrenensemble einstudiert.