---
layout: dates
title: 135. Arbeitstagung des Bayerischen Landeszupforchesters
location: "Tagungshaus im Kloster Rohr, Abt-Dominik-Prokop-Platz 1, 93352 Rohr "
beginning: 2022-10-14T06:46:21.515Z
end: 2022-10-16T06:46:21.553Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.kloster-rohr.de/tagungshaus/kontakt.html
tags:
  - BLZO
  - Orchester
---
**Programm**:

W. A. Mozart: Sinfonie Nr. 23\
Béla Bartók: Mikrokosmos\
Maurice Ravel: Pavane pour une infante défunte\
Ludwig van Beethoven: Adagio\
Carlo Gesualdo: Tenebrae factae sunt\
Rossen Balkanski: Rapsodia Vissani\
Daniel Huschert: Concerto Nr. 3 \
Brahms: Intermezzo Opus 117 Nr. 1

**Anmeldeschluss:** 05.09.2022

kein Konzert geplant