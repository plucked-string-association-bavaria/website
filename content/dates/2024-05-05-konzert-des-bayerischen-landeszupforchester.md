---
layout: dates
title: "Konzert des Bayerischen Landeszupforchester "
location: Bürgerhaus Wirges
beginning: 2024-05-11T17:30:47.518Z
end: 2024-05-11T19:30:47.556Z
showEnd: true
hideTime: true
showContinueReading: true
tags:
  - BLZO
  - Öffentlich
  - Veranstaltung
---
Beim Festival der Landesorchester spielt das Bayerische Landeszupforchester zur Eröffnung des Abendkonzertes:

* [Bayerisches Landeszupforchester](https://zupfmusiker.de/festival-der-landesorchester/bayerisches-landeszupforchester/ "Bayerisches Landeszupforchester")
* [Landeszupforchester Thüringen](https://zupfmusiker.de/festival-der-landesorchester/landeszupforchester-thueringen/ "Landeszupforchester Thüringen")
* [Württembergisches Zupforchester](https://zupfmusiker.de/festival-der-landesorchester/wuerttembergisches-zupforchester/ "Württembergisches Zupforchester")
* [Jugendzupforchester Nordrhein-Westfalen](https://zupfmusiker.de/festival-der-landesorchester/jugendzupforchester-nordrhein-westfalen/ "Jugendzupforchester Nordrhein-Westfalen")

**Unser Programm:**

**La Notte del Principe**

* Eclipsis
* Tenebrae factae sunt (nach Gesualdo)

**Concerto Nr. 3**\
*dem Berliner Zupforchester gewidmet*

* 1. Satz: Allegro