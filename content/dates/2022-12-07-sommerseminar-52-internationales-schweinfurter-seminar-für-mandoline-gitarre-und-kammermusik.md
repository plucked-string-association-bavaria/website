---
layout: dates
title: "Sommerseminar: 52. Internationales Schweinfurter Seminar für Mandoline,
  Gitarre und Kammermusik"
location: Hammelburg
beginning: 2023-08-06T14:00:46.019Z
end: 2023-08-13T08:00:46.044Z
showEnd: true
hideTime: false
showContinueReading: true
link: https://www.bmhab.de/
tags:
  - Öffentlich
  - Kurs
  - Mandoline – Instrument des Jahres 2023
  - Schweinfurter Seminar
---
Wir begrüßen bei unserem einwöchigen Kurs Zupfinstrumentenspieler:innen ab 18 Jahren aller Leistungsstufen. \
Mandolinen-, Mandola- und Gitarrenspieler:innen, die Lust am gemeinsamen Musizieren und Freude an der Zupfmusik haben, sind bei uns herzlich willkommen. \
Sowohl Spieler:innen aus dem Laienbereich und den verschiedenen Heimatorchestern, als auch deren Leiter:innen, Ausbilder:innen und Lehrer:innen werden durch die namhaften Dozent:innen gefördert und unterrichtet. 

Leitung: Bianca Brand\
Dozententeam für Gitarre: Michael Tröster, Boris Tesic, Karoline Laier\
Dozententeam für Mandoline: Steffen Trekel, Denise Wambsganß \
Klavierkorrepetition: Iwan Urwalow

**Kursinhalte:**\
Jede:r Teilnehmer:in erhält täglich 25 Minuten Einzelunterricht und kann sich zudem aus diesem reichhaltigen Unterrichtsangebot einen eigenen Stundenplan zusammenstellen:\
*o*  individuelle Technikklassen für Mandoline und Gitarre\
*o*  Elementare Improvisation (mit und ohne Instrument)\
*o*  Literaturkunde (u.a. Zupforchesterliteratur aus Japan)\
*o*  Workshops "der passende Fingersatz", "wie gehe ich mit einem neuen Stück um?"\
*o*  Ensemble- und Kammermusikstunden\
*o*  großes Kurs-Zupforchester

Die Mandolinenstudentin Antonia Platzdasch bietet weiterhin diese Workshops an:\
*o*  Erlernen von komplizierter Rhythmik\
*o*  Stimmführung im Orchester\
*o*  gesundes Musizieren

**Konzerte:**\
Eröffnungkonzert: Sonntag, 06.08.2023, 20 Uhr\
Duo Consensus: Christian Laier (Mandoline) und Karoline Laier (Gitarre);\
die Dozenten spielen in kammermusikalischen Besetzungen

Dozentenkonzert: Dienstag, 08.08.2023, 20 Uhr\
Boris Tesic, Gitarre Solo

Abschlusskonzert: Samstag, 12.08.2023, 19.30 Uhr\
Die Kurs-Teilnehmer*Innen spielen in verschiedenen Besetzungen

**Kursgebühr:**\
Mitglieder des BDZ LV Bayern ermäßigt (Schüler/Studenten): € 160,00\
Mitglieder des BDZ LV Bayern Erwachsene: € 190,00\
Nichtmitglieder oder Mitglieder anderer Bundesländer: € 280,00

**Unterkunft/Vollverpflegung** in der Bay. Musikakademie Hammelburg:\
im Doppelzimmer: € 290,00\
Einzelzimmer (begrenzt verfügbar): Zuschlag pro Person pro Nacht: € 20,00

**Anmeldung nicht mehr möglich, Kurs ist bereits voll belegt.**