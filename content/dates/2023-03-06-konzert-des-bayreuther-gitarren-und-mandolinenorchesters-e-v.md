---
layout: dates
title: Konzert des Bayreuther Gitarren- und Mandolinenorchesters e.V.
location: Serenade am Victoria Becken, Ökologisch-Botanischer Garten Bayreuth
beginning: 2023-06-23T16:00:14.239Z
end: 2023-06-23T17:00:14.258Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Bayreuther Gitarren- und Mandolinenorchester e.V. unter Leitung von Daniel Ambarjan musiziert zum 18. Mal unter freiem Himmel im Ökologisch-Botanischen Garten Bayreuth im Wechsel mit unterhaltsamen Gedichten vorgetragen von Sabine Heucke-Gareis.