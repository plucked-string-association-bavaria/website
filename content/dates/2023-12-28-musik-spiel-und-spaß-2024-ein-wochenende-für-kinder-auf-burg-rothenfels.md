---
layout: dates
title: Musik, Spiel und Spaß 2024 - Ein Wochenende für Kinder auf Burg Rothenfels
location: Burg Rothenfels
beginning: 2024-03-09T08:00:06.267Z
end: 2024-03-10T16:00:06.283Z
showEnd: true
hideTime: false
showContinueReading: true
link: ""
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder, die seit mindestens einem Jahr Mandoline, Violine, Violoncello oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, rhythmische Spiele, gemeinsames Basteln, Fußball spielen, Wanderungen und viel Spaß\
Abschlusskonzert am Sonntag, 16 Uhr

Leitung: Petra Breitenbach\
Dozenten: Petra Fröhlen, Tobias Zerlang-Rösch,  Andreas Franzky, Rainer Nürnberger

Kursgebühren (incl. Unterricht, Unterkunft und Vollverpflegung):\
Jugendliche BDZ-Mitglieder: 108 €\
Jugendliche Nichtmitglieder: 138 €

Anmeldung bis 12.02.2024 möglich:

<https://form.jotform.com/heikoholzknecht/2024Rothenfels>