---
layout: dates
title: Konzert Ensemble Bayuna
location: Johanniskapelle Bamberg
beginning: 2024-03-17T10:00:59.936Z
end: 2024-02-25T17:03:59.969Z
showEnd: false
hideTime: false
showContinueReading: false
link: https://www.bayuna.de
tags:
  - Bayuna
  - Orchester
  - Veranstaltung
  - Öffentlich
---
Das Zupforchester Bayuna unter der Leitung von Silvan Wagner präsentiert in seinem aktuellen Programm bisher unaufgeführte Bearbeitungen wundervoller Renaissance-Miniaturen von Thomas Ravenscoft. Mit „Alone at the Wedding“ von Alexander Mathewson und „Dance games“ von Svetlin Hristov wird das Programm durch zwei sehr rhythmisch-tänzerische Stücke junger zeitgenössischer osteuropäischer Komponisten ergänzt. „Alone at the Wedding“ wurde extra für das Orchester mit einer Solovioline (Patricia Hibler) neu gesetzt. Freuen Sie sich auf ein spannendes Konzert mit selten gespielten Werken.