---
layout: dates
title: Konzert des Ensembles musica a corda, Hemhofen
location: St. Bonifaz, Leopoldstr. 36, Nürnberg
beginning: 2023-10-08T14:30:00.408Z
end: 2023-10-08T15:30:00.435Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble musica a corda musiziert unter der Leitung von Iris Hammer.