---
layout: dates
title: Konzert des Bayerischen Landeszupforchesters
location: Kammermusiksaal der Musikhochschule Würzburg
beginning: 2023-04-30T17:30:25.343Z
end: 2023-04-30T19:33:25.398Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
  - Öffentlich
---
Lassen Sie sich von dem Instrument des Jahres 2023 - der Mandoline - mit tollen Klängen verführen, im Zusammenspiel mit Gitarren, Mandolen und Kontrabass. 
Zum Höhepunkt des Konzerts werden Sie das Concerto Nr. 3 für Solomandoline und Schlagzeug von Daniel Huschert hören. Als Solist spielt für Sie an der Mandoline Christian Laier, Percussion Fabio Schroll.
Außerdem erwarten Sie noch interessante Werke von R. Balkanski, L. van Beethoven, W. A. Mozart und M. Ravel.

![](/assets/uploads/blzo-plakat-würzburg-1.jpg)