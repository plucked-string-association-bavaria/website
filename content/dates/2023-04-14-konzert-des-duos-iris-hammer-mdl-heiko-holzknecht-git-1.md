---
layout: dates
title: Konzert des Duos Iris Hammer (Mdl) & Heiko Holzknecht (Git)
location: Pfarrkirche St. Martin, Hauptstr. 47, 91330 Eggolsheim
beginning: 2023-05-21T17:00:16.679Z
end: 2023-05-21T18:30:16.694Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Duo Iris Hammer (Mandoline) und Heiko Holzknecht (Gitarre) musiziert zum Thema "Sonne, Regen, Wolken und Wind - Atmosphärisches aus allen Kontinenten.

https://www.duohammerholzknecht.de/