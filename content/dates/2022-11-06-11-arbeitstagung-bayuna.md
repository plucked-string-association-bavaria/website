---
layout: dates
title: 11. Arbeitstagung Bayuna
location: Bruder-Klaus-Heim, Violau
beginning: 2023-03-03T17:01:11.502Z
end: 2023-03-05T17:01:11.532Z
showEnd: true
hideTime: true
showContinueReading: false
link: https://www.jotform.com/223083685035355
tags:
  - Bayuna
---
Frühjahrsprobenphase mit Erarbeitung eines neuen Programms.