---
layout: dates
title: Konzert des Ensembles musica a corda, Hemhofen
location: Ratssaal des Kulturvereins Möhrendorf e.V., Hauptstr. 16, Möhrendorf
beginning: 2023-10-27T17:00:14.476Z
end: 2023-10-27T18:00:14.500Z
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Zupfensemble musica a corda musiziert unter der Leitung von Iris Hammer.