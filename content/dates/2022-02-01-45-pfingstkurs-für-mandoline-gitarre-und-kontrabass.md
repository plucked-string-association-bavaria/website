---
layout: dates
title: 45. Pfingstkurs für Mandoline, Gitarre und Kontrabass
location: Bayerische Musikakademie Hammelburg
beginning: 2022-06-06T06:39:14.189Z
end: 2022-06-11T06:39:14.235Z
showEnd: true
hideTime: true
showContinueReading: false
tags:
  - Kurs
  - Öffentlich
---
