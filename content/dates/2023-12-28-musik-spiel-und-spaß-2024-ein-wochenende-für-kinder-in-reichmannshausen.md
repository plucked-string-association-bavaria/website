---
layout: dates
title: Musik, Spiel und Spaß 2024 - Ein Wochenende für Kinder in Reichmannshausen
location: Schullandheim Reichmannshausen
beginning: 2024-03-02T08:00:46.557Z
end: 2024-03-03T15:30:46.572Z
showEnd: true
hideTime: false
showContinueReading: true
link: ""
tags:
  - Kurs
  - Öffentlich
  - Veranstaltung
  - Kinderkurs
---
Ein musikalisches Wochenende für Kinder, die seit mindestens einem Jahr Mandoline oder Gitarre spielen.

Kursinhalte: Einzelunterricht, Gruppenunterricht, Kursorchester, rhythmische Spiele, gemeinsames Basteln, Fußball spielen, Wanderungen und viel Spaß\
Abschlusskonzert am Sonntag, 16 Uhr

Leitung: Bianca Brand\
Dozenten: Lukas Becker, Markus Joppich

Kursgebühren (incl. Unterricht, Unterkunft und Vollverpflegung):\
Jugendliche BDZ-Mitglieder: 108 €\
Jugendliche Nichtmitglieder: 138 €

Anmeldung bis 10.02.24 möglich:

<https://form.jotform.com/230111172071336>