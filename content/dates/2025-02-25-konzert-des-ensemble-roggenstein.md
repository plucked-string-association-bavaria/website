---
layout: dates
title: Konzert des Ensemble Roggenstein
location: RAUMdurchKUNST, Hauptstr. 28, 82404 Sindelsdorf
beginning: 2025-03-14T18:00:34.970Z
end: 2025-03-14T20:00:34.984Z
showEnd: false
hideTime: false
showContinueReading: true
link: https://www.ensembleroggenstein.de
tags:
  - Öffentlich
  - Veranstaltung
  - Orchester
---
Das Ensemble Roggenstein unter der Leitung von Oliver Kälberer spielt Werke von Giovanni Gabrieli, Johann Sebastian Bach, Ludwig van Beethoven, Johannes Brahms, Béla Bartók und Oliver Kälberer sowie Lautenlieder von John Dowland mit Yvonne Fontane, Mezzosopran.

<http://www.ensembleroggenstein.de>\
<https://www.raumdurchkunst.de>

![](/assets/uploads/foto-roggenstein.jpg)

<!--EndFragment-->