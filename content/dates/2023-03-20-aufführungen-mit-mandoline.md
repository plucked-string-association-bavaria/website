---
layout: dates
title: Aufführungen mit Mandoline
location: München, Leipzig, Prag, Amsterdam, Lathi
beginning: 2023-05-18T18:00:44.030Z
end: 2023-05-30T18:00:44.052Z
showEnd: false
hideTime: true
showContinueReading: false
tags:
  - Öffentlich
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Die 7. Symphonie von Gustav Mahler wird mit dem Symphonieorchester des Bayerischen Rundfunks und **Antje Strömsdörfer** an der **Mandoline** aufgeführt.

18., 19. und 20.05.23: Isarphilharmonie München\
22.05.23: Gewandhaus Leipzig\
24.05.23: Smetana Saal Prag\
26.05.23: Concertgebouw Amsterdam\
30.05.23: Sibelius Hall Lahti (Finnland)