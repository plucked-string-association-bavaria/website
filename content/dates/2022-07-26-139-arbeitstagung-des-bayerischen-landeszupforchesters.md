---
layout: dates
title: 139. Arbeitstagung des Bayerischen Landeszupforchesters
location: Bildungshaus Kloster Schwarzenberg, 91443 Scheinfeld
beginning: 2024-02-02T16:00:00.000Z
end: 2024-02-04T12:29:01.984Z
showEnd: true
hideTime: true
showContinueReading: true
link: https://www.kloster-schwarzenberg.de/zu-gast-im-kloster/
tags:
  - BLZO
  - Orchester
---
in Planung, diesmal ohne Konzert