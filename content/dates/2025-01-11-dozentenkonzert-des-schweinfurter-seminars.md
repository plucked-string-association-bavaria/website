---
layout: dates
title: Dozentenkonzert des Schweinfurter Seminars
location: Hammelburg, Bay. Musikakademie
beginning: 2025-08-12T18:00:51.348Z
end: 2025-08-12T19:30:51.358Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Schweinfurter Seminar
---
Das Dozentenkonzert des Schweinfurter Seminars gestalten Laura Engelmann, Mandoline und Boris Tesic, Gitarre.

Der Eintritt ist frei.