---
layout: dates
title: 141. Arbeitstagung des Bayerischen Landeszupforchesters - Festival der
  Landesorchester
location: Bürgerhaus Wirges, Montchaninplatz 1, 56422 Wirges
beginning: 2024-05-10T15:00:44.277Z
end: 2024-05-12T12:34:44.302Z
showEnd: true
hideTime: true
showContinueReading: true
link: https://www.hotel-eisbach.de
tags:
  - BLZO
  - Orchester
  - Veranstaltung
---
**Festival der Landesorchester in Wirges**

Darauf können wir uns freuen:

* Konzertauftritte von 
* Landesjugendzupforchester
* Landesjugendgitarrenorchester
* Landeszupforchester
* Landesseniorenorchester
* Bezirksorchester

\- Uraufführungen von Auftragskompositionen, die ursprünglich für das Eurofestival 2022 geplant waren

* Workshops
* Ausstellungen
* Festival-Orchester

**Wir freuen uns auf ein fröhliches Wiedersehen mit viel guter Musik und angeregtem Austausch.**