---
layout: dates
title: Konzert des Bayreuther Gitarren- und Mandolinenorchesters e.V.
location: Stiftskirche Maria Immaculata, Kloster Speinshart
beginning: 2023-05-14T15:00:38.025Z
end: 2023-05-14T16:00:38.049Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
  - Mandoline – Instrument des Jahres 2023
---
Das Bayreuther Gitarren- und Mandolinenorchester e.V. unter Leitung von Daniel Ambarjan musiziert in großartiger Atmosphäre der Stiftskirche Werke von Angulo, Telemann, Mozart, Brahms, Goodwyn, Wüller, uvm.