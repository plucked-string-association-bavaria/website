---
layout: dates
title: Abschlusskonzert des Schweinfurter Seminars
location: Bay. Musikakademie Hammelburg
beginning: 2022-08-13T17:30:39.106Z
end: ""
showEnd: false
hideTime: false
showContinueReading: false
tags:
  - Öffentlich
  - Kurs
  - Veranstaltung
---
*Es ergeht herzliche Einladung zum Abschlusskonzert des 51. Schweinfurter Seminars im Kammermusiksaal der Musikakademie Hammelburg:*

Die Kursteilnehmer*Innen musizieren kammermusikalische Werke für Zupfinstrumente, Werke für Gitarre und Mandoline mit Klavier und die in der Woche einstudierten Stücke für Ensembles und Zupforchester. Es erwartet Sie ein abwechslungsreicher Gehörgenuss.

Der Eintritt ist frei!

<!--EndFragment-->