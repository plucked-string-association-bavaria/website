---
layout: dates
title: Eröffnungskonzert des Schweinfurter Seminars
location: Hammelburg, Bay. Musikakademie
beginning: 2025-08-10T18:00:55.685Z
end: 2025-08-10T19:30:55.700Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Veranstaltung
  - Schweinfurter Seminar
---
Das Eröffnungskonzert des Schweinfurter Seminars spielen das Duo Bergerac mit Karin Scholz und Peter Ernst an den Gitarren.

Der Eintritt ist frei.