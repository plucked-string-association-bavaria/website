---
layout: dates
title: Matineekonzert des Mandolinen- und Gitarrenorchesters der Naturfreunde
  Schweinfurt
location: Rudolf-Winkler-Haus, Schulring 3, 97475 Zeil a. M.
beginning: 2024-07-07T09:00:11.284Z
end: 2024-07-07T10:30:11.300Z
showEnd: false
hideTime: false
showContinueReading: true
tags:
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Das Mandolinen- und Gitarrenorchester der Naturfreunde Schweinfurt (Leitung: Bianca Brand) spielt ein Matineekonzert mit dem Titel "Viva la vida - viva la musica" mit Werken von R. Vollmann, M. Tröster, E. Tober-Vogt, Coldplay und den Comedian Harmonists.

Eintritt: 12 Euro (Abendkasse), 10 Euro (Vorverkauf)