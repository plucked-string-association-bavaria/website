---
layout: dates
title: Konzert des Bayerischen Landeszupforchesters
location: Kurhaus Bad Abbach
beginning: 2024-10-13T09:00:28.071Z
end: 2024-10-12T22:00:28.115Z
showEnd: true
hideTime: false
showContinueReading: true
tags:
  - BLZO
  - Öffentlich
  - Orchester
  - Veranstaltung
---
Mit Werken von Carl Philipp Emanuel Bach, Johann Sebastian Bach, Maurice Ravel, Oliver Kälberer werden wir Ihnen einen besonderen Hörgenuss präsentieren.